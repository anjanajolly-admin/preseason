import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_cubit.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_state.dart';
import 'package:pre_seasoned/screens/signup_screen/signup_screen.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);
  static const String routeName = "/ChangePasswordScreenScreen";

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  bool isLoader = false;

  @override
  void initState() {
    super.initState();
  }

  final TextEditingController _emailId = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          title: Text("Forgot Password"),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        body: BlocListener<BaseCubit, BaseState>(
          bloc: BlocProvider.of<BaseCubit>(context),
          listener: (context, BaseState state) {
            setState(() {
              if (state is PasswordChangeConfirmState) {
                isLoader = false;
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AppWidgets.getAlertDialog(
                      title: "Alert",
                      content: "Please check your email to change the password",
                      confirmButton: "Ok",
                      cancelButton: "Cancel",
                      onCancelTap: () {
                        Navigator.of(context).pop();
                      },
                      onConfirmTap: () {
                        Navigator.of(context).pop();
                      },
                    );
                  },
                );
              } else if (state is LoadingState) {
                setState(() {
                  isLoader = true;
                });
                return;
              } else if (state is PasswordChangeFailureState) {
                isLoader = false;
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AppWidgets.getAlertDialog(
                      title: "Alert",
                      content: state.failureMsg,
                      confirmButton: "Ok",
                      cancelButton: "Cancel",
                      onCancelTap: () {
                        Navigator.of(context).pop();
                      },
                      onConfirmTap: () {
                        Navigator.of(context).pop();
                      },
                    );
                  },
                );
              } else {
                isLoader = false;
                BotToast.showText(text: "Something went wrong.Try again later");
              }
            });
          },
          child: InkWell(
            focusColor: Colors.transparent,
            highlightColor: Colors.transparent,
            hoverColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              AppConstants.hideKeyboard(context);
            },
            child: Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 16),
                  Container(
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "E-mail",
                          style: TextStyle(
                            color: Color(0xfff2f2f7),
                            fontSize: 16,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(height: 4),
                        AppWidgets.getEditText(
                          isObscureText: false,
                          controller: _emailId,
                          onChanged: (value) {
                            // _emailId.text = value;
                          },
                          keyboardType: TextInputType.emailAddress,
                          maxLength: 100,
                          hint: " Enter your mail id ",
                          inputFormatter: [
                            FilteringTextInputFormatter.deny(RegExp(" ")),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 80),
                  AppWidgets.getWtaButton(
                      isLoader: isLoader,
                      buttonName: "Verify Email",
                      fontSize: 16,
                      width: double.infinity,
                      height: 48,
                      onPressed: () async {
                        setState(() {
                          {
                            if (isValidEmail(_emailId)) {
                              isLoader = true;
                              setState(() {
                                context
                                    .read<BaseCubit>()
                                    .forgotPassword(_emailId.text);
                              });
                            } else {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AppWidgets.getAlertDialog(
                                    title: "Alert",
                                    content: "Please enter valid Email Id",
                                    confirmButton: "Ok",
                                    cancelButton: "Cancel",
                                    onCancelTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    onConfirmTap: () {
                                      Navigator.of(context).pop();
                                    },
                                  );
                                },
                              );
                            }
                          }
                        });
                      }),
                  SizedBox(height: 40),
                ],
              ),
            ),
          ),
        ));
  }
}
