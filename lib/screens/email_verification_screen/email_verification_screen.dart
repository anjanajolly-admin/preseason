import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/screens/login_screen/login_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class EmailVerificationScreen extends StatefulWidget {
  const EmailVerificationScreen({Key? key}) : super(key: key);
  static const String routeName = "/EmailVerificationScreen";

  @override
  _EmailVerificationScreenState createState() =>
      _EmailVerificationScreenState();
}

class _EmailVerificationScreenState extends State<EmailVerificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          toolbarHeight: 0,
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        body: _getBody());
  }

  Widget _getBody() {
    return SafeArea(
      child: Center(
        child: Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Thank you for Signing up.\nAs part of the verification process, we have sent an email to your email id:\n${AppInjector.resolve<AppPreferences>().getEmail()}.\n\nKindly open your email and click on the verification link. After verifying , you can come back and login into the app",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: "Proxima Nova",
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                height: 24,
              ),
              AppWidgets.getWtaButton(
                isLoader: false,
                buttonName: "Re-Login",
                fontSize: 16,
                width: double.infinity,
                height: 48,
                onPressed: () {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pushNamedAndRemoveUntil(
                          LoginScreen.routeName, (route) => false);
                  print("SignUp");
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
