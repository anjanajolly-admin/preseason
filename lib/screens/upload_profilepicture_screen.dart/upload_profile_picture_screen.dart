import 'dart:io';
import 'dart:ui';

import 'package:bot_toast/bot_toast.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pre_seasoned/screens/username_screen/username_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class UploadProfilePictureScreen extends StatefulWidget {
  final String emailId;
  final String password;
  final String firstName;
  final String lastName;
  const UploadProfilePictureScreen(
      {Key? key,
        required this.emailId,
        required this.password,
        required this.firstName,
        required this.lastName})
      : super(key: key);
  static const routeName = '/UploadProfilePictureScreen';
  @override
  _UploadProfilePictureScreenState createState() =>
      _UploadProfilePictureScreenState();
}

class _UploadProfilePictureScreenState
    extends State<UploadProfilePictureScreen> {
  File? _profileImageFile;
  bool isImageLoader = false;

  _getProfileFromGallery() async {
    var pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 640,
      maxHeight: 480,
    );
    if (pickedFile != null) {
      setState(() {
        _profileImageFile = File(pickedFile.path);
        isImageLoader = false;
      });
    }
  }

  /// Get from Camera
  _getProfileFromCamera() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 640,
      maxHeight: 480,
    );
    if (pickedFile != null) {
      setState(() {
        _profileImageFile = File(pickedFile.path);
        isImageLoader = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          brightness: Brightness.dark,
          backgroundColor: AppWidgets.backgroundColor,
          elevation: 0,
          actions: [],
        ),
        body: Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Set your Profile Picture',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 28),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 24),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      InkWell(
                          child: (_profileImageFile == null)
                              ? DottedBorder(
                            child: Icon(
                              Icons.add_outlined,
                              size: 148,
                            ),
                            borderType: BorderType.Circle,
                            dashPattern: [10, 5, 10, 5, 10, 5],
                          )
                              : Column(
                            children: [
                              CircleAvatar(
                                  radius: 80.0,
                                  backgroundImage:
                                  FileImage(_profileImageFile!)),
                            ],
                          ),
                          onTap: () {
                            showModalBottomSheet(
                                isDismissible: false,
                                enableDrag: false,
                                context: context,
                                builder: ((builder) {
                                  return Container(
                                    color: AppWidgets.backgroundColor,
                                    width: MediaQuery.of(context).size.width,
                                    height:
                                    MediaQuery.of(context).size.width * .3,
                                    child: Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                      children: [
                                        Text(
                                          'Choose Profile photo',
                                          style: TextStyle(
                                              color: AppWidgets.buttonTextColor,
                                              fontSize: 18),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Column(
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: [
                                                IconButton(
                                                    onPressed: () {
                                                      isImageLoader = true;
                                                      Navigator.pop(context);
                                                      _getProfileFromCamera()();
                                                    },
                                                    icon: Icon(
                                                      Icons.camera,
                                                      size: 36,
                                                      color: AppWidgets
                                                          .buttonTextColor,
                                                    )),
                                                SizedBox(
                                                  height: 8,
                                                ),
                                                Text(
                                                  'Camera',
                                                  style: TextStyle(
                                                      color: AppWidgets
                                                          .buttonTextColor,
                                                      fontSize: 16),
                                                )
                                              ],
                                            ),
                                            Column(
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: [
                                                IconButton(
                                                    onPressed: () {
                                                      isImageLoader = true;
                                                      Navigator.pop(context);
                                                      _getProfileFromGallery();
                                                    },
                                                    icon: Icon(
                                                      Icons.image,
                                                      size: 36,
                                                      color: AppWidgets
                                                          .buttonTextColor,
                                                    )),
                                                SizedBox(
                                                  height: 8,
                                                ),
                                                Text(
                                                  'Gallery',
                                                  style: TextStyle(
                                                      color: AppWidgets
                                                          .buttonTextColor,
                                                      fontSize: 16),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  );
                                }));
                          }),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        'Upload Photo',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      AppWidgets.getTextButton(
                          "Skip", AppWidgets.buttonTextColor, 16, () {
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pushNamed(UsernameScreen.routeName, arguments: [
                          widget.emailId,
                          widget.password,
                          widget.firstName,
                          widget.lastName,
                          null
                        ]);
                      }),
                    ],
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  AppWidgets.getWtaButton(
                    buttonName: "Continue",
                    fontSize: 16,
                    width: double.infinity,
                    height: 54,
                    isLoader: false,
                    onPressed: () {
                      (_profileImageFile == null)
                          ? BotToast.showText(
                          text:
                          "Please Upload your profile picture or click skip")
                          : AppInjector.resolve<AppRoutes>()
                          .navigatorKey
                          .currentState!
                          .pushNamed(UsernameScreen.routeName, arguments: [
                        widget.emailId,
                        widget.password,
                        widget.firstName,
                        widget.lastName,
                        _profileImageFile
                      ]);
                    },
                  ),
                  SizedBox(
                    height: 8,
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
