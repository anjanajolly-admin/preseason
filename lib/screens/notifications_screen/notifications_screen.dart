import 'dart:convert';
import 'dart:convert' as convert;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/models/qb_event.dart';
import 'package:quickblox_sdk/models/qb_subscription.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);
  static const String routeName = "/NotificationScreen";

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  List? notificationsList;
  bool notificationScreenLoader = false;

  @override
  void initState() {
    super.initState();

    notificationScreenLoader = true;

    getPushNotifications();
    getPushSubscriptions();
    // getByIdNotification();
  }

  Future<void> getPushNotifications() async {
    await AppInjector.resolve<QuickbloxApiService>()
        .getNotifications()
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        if ((value.data as List).length == 0) {
          notificationsList = [];
        } else {
          notificationsList = [];
          for (var index = 0; index < (value.data as List).length; index++) {
            var payload = (utf8.decode(base64.decode(
                ((value.data[index] as QBEvent).payload)
                    .toString()
                    .substring(13))));
            notificationsList!.add(convert.jsonDecode(payload));
          }
        }
      } else {
        notificationsList = [];
      }
      setState(() {
        notificationScreenLoader = false;
      });
    });
  }

  Future<QuickbloxApiResponse<String>> getByIdNotification() async {
    try {
      QBEvent? qbEvent = await QB.events.getById(37438602);
      int? notificationId = qbEvent!.id;
      var payload = (utf8.decode(base64.decode(((qbEvent.payload!)))));
      print('threadpayload $payload, ${convert.jsonDecode(payload)}');
      return QuickbloxApiResponse.completed(qbEvent);
    } on PlatformException catch (e) {
      return QuickbloxApiResponse.error("error retrieving $e");
    }
  }

  Future<void> getPushSubscriptions() async {
    await AppInjector.resolve<QuickbloxApiService>()
        .getPushSubscriptions()
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(
            'pushSubscriptions , ${(value.data[0] as QBSubscription).deviceToken}');
      } else {
        setState(() {
          notificationScreenLoader = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
          title: Text(
            'Notifications',
            style: TextStyle(
              fontFamily: 'Proxima nova',
            ),
          ),
        ),
        body: Stack(
          children: [
            notificationsList == null
                ? Container()
                : notificationsList!.isEmpty
                ? Center(
                child: Container(
                  child: Text(
                    "No Notifications yet",
                    style: TextStyle(
                        fontFamily: 'Proxima nova',
                        fontSize: 20,
                        color: Colors.white),
                  ),
                ))
                : Container(
                child: ListView.builder(
                    itemCount: notificationsList!.length,
                    physics: AlwaysScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index1) {
                      return Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        child: notificationTile(
                            notificationsList![index1]![
                            'channelProfilePhoto'],
                            notificationsList![index1]!['channelName'],
                            notificationsList![index1]!['threadName'],
                            notificationsList![index1]!['message']),
                      );
                    })),
            notificationScreenLoader
                ? Center(
                child: Container(
                  alignment: Alignment.center,
                  child: SizedBox(
                      height: 50,
                      width: 50,
                      child: CircularProgressIndicator.adaptive(
                          strokeWidth: 2,
                          backgroundColor: AppWidgets.buttonTextColor,
                          valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.white))),
                ))
                : Container()
          ],
        ));
  }

  Widget notificationTile(
      image, String title, String subtitle, String content) {
    return Column(
      children: [
        SizedBox(
          height: 16,
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CachedNetworkImage(
                width: 48,
                height: 48,
                imageUrl: image,
                fit: BoxFit.fill,
                placeholder: (context, url) => Image.asset(
                  "assets/images/channel_cover.png",
                  fit: BoxFit.fill,
                ),
                errorWidget: (context, url, error) {
                  print('errorWidget ${error.toString()} \n$url');
                  return Image.asset(
                    "assets/images/channel_cover.png",
                    fit: BoxFit.fill,
                  );
                },
              ),
              SizedBox(width: 8),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    subtitle,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    content,
                    style: TextStyle(
                      color: Color(0xffa4a4ab),
                      fontSize: 12,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: 16,
        ),
        Container(
          height: 1,
          width: double.infinity,
          color: AppWidgets.titleColor3,
        ),
        SizedBox(
          height: 8,
        ),
      ],
    );
  }
}
