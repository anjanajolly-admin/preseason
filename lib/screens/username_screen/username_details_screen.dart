import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pre_seasoned/screens/upload_profilepicture_screen.dart/upload_profile_picture_screen.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class UsernameDetailsScreen extends StatefulWidget {
  final String emailId;
  final String password;
  const UsernameDetailsScreen(
      {Key? key, required this.emailId, required this.password})
      : super(key: key);
  static const String routeName = "/UsernameDetailsScreen";

  @override
  _UsernameDetailsScreenState createState() => _UsernameDetailsScreenState();
}

class _UsernameDetailsScreenState extends State<UsernameDetailsScreen> {
  final TextEditingController _firstName = TextEditingController();
  final TextEditingController _lastName = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          brightness: Brightness.dark,
          backgroundColor: AppWidgets.backgroundColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => AppInjector.resolve<AppRoutes>()
                .navigatorKey
                .currentState!
                .pop(),
          ),
        ),
        backgroundColor: Color(0xff2c2c2e),
        body: InkWell(
          focusColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () {
            AppConstants.hideKeyboard(context);
          },
          child: _getBody(),
        ));
  }

  Widget _getBody() {
    return SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 16),
                        Text(
                          "Tell us more about you",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 28,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(height: 24),
                        Column(
                          children: [
                            Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "First name",
                                    style: TextStyle(
                                      color: Color(0xfff2f2f7),
                                      fontSize: 16,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(height: 4),
                                  AppWidgets.getEditText(
                                    isObscureText: false,
                                    controller: _firstName,
                                    onChanged: (value) {
                                      // _firstName.text = value;
                                    },
                                    keyboardType: TextInputType.name,
                                    maxLength: 100,
                                    hint: "",
                                    inputFormatter: [
                                      LengthLimitingTextInputFormatter(20),
                                      FilteringTextInputFormatter.allow(
                                          RegExp("[a-zA-Z]")),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            SizedBox(height: 16),
                            Container(
                              height: 74,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Last name",
                                    style: TextStyle(
                                      color: Color(0xfff2f2f7),
                                      fontSize: 16,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(height: 4),
                                  AppWidgets.getEditText(
                                    isObscureText: false,
                                    controller: _lastName,
                                    onChanged: (value) {
                                      // _lastName.text = value;
                                    },
                                    keyboardType: TextInputType.name,
                                    maxLength: 20,
                                    hint: "",
                                    inputFormatter: [
                                      LengthLimitingTextInputFormatter(20),
                                      FilteringTextInputFormatter.allow(
                                          RegExp("[a-zA-Z]")),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    )),
                SizedBox(height: 20),
                AppWidgets.getWtaButton(
                    isLoader: false,
                    buttonName: "Continue",
                    fontSize: 16,
                    width: double.infinity,
                    height: 48,
                    onPressed: () {
                      _firstName.text.isEmpty || _lastName.text.isEmpty
                          ? showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AppWidgets.getAlertDialog(
                            title: "Alert",
                            content:
                            "First Name and Last Name are mandatory.",
                            confirmButton: "Ok",
                            cancelButton: "Cancel",
                            onCancelTap: () {
                              Navigator.of(context).pop();
                            },
                            onConfirmTap: () {
                              Navigator.of(context).pop();
                            },
                          );
                        },
                      )
                          : AppInjector.resolve<AppRoutes>()
                          .navigatorKey
                          .currentState!
                          .pushNamed(UploadProfilePictureScreen.routeName,
                          arguments: [
                            widget.emailId,
                            widget.password,
                            _firstName.text,
                            _lastName.text
                          ]);
                      print("");
                    }),
                SizedBox(height: 20),
              ],
            )));
  }
}
