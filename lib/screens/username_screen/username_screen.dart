import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mixpanel_flutter/mixpanel_flutter.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_cubit.dart';
import 'package:pre_seasoned/screens/email_verification_screen/email_verification_screen.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class UsernameScreen extends StatefulWidget {
  final String emailId;
  final String password;
  final String firstName;
  final String lastName;
  final dynamic profilePhoto;

  const UsernameScreen({
    Key? key,
    required this.emailId,
    required this.password,
    required this.firstName,
    required this.lastName,
    required this.profilePhoto,
  }) : super(key: key);
  static const String routeName = "/UsernameScreen";

  @override
  _UsernameScreenState createState() => _UsernameScreenState();
}

class _UsernameScreenState extends State<UsernameScreen> {
  final TextEditingController _userName = TextEditingController();
  var isLoader = false;
  @override
  void initState() {
    super.initState();
    // initMixpanel();
  }

  initMixpanel() async {
    mixpanel = await Mixpanel.init("c4b6cba1cc4506afee6ae57cacc328cf",
        optOutTrackingDefault: false);
    var distinctId = await mixpanel.getDistinctId();
    print(distinctId);
  }

  late Mixpanel mixpanel;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          brightness: Brightness.dark,
          backgroundColor: AppWidgets.backgroundColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => AppInjector.resolve<AppRoutes>()
                .navigatorKey
                .currentState!
                .pop(),
          ),
        ),
        backgroundColor: Color(0xff2c2c2e),
        body: InkWell(
          focusColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () {
            AppConstants.hideKeyboard(context);
          },
          child: _getBody(),
        ));
  }

  Widget _getBody() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
              child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 16),
              SizedBox(
                child: Text(
                  "Set your username",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 28,
                    fontFamily: "Proxima Nova",
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(height: 24),
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Username",
                          style: TextStyle(
                            color: Color(0xfff2f2f7),
                            fontSize: 16,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(height: 4),
                        AppWidgets.getEditText(
                          isObscureText: false,
                          controller: _userName,
                          onChanged: (value) {},
                          keyboardType: TextInputType.name,
                          maxLength: 100,
                          hint: "",
                          inputFormatter: [
                            LengthLimitingTextInputFormatter(20),
                            FilteringTextInputFormatter.allow(
                                RegExp("[a-zA-Z _]")),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ],
          )),
          AppWidgets.getWtaButton(
              isLoader: isLoader,
              buttonName: "Continue",
              fontSize: 16,
              width: double.infinity,
              height: 48,
              onPressed: () async {
                isLoader = true;
                setState(() {
                  context
                      .read<BaseCubit>()
                      .emailSignIn(
                          widget.emailId,
                          widget.password,
                          widget.firstName,
                          widget.lastName,
                          _userName.text,
                          widget.profilePhoto)
                      .then((list) {
                    if (list[0]) {
                      setState(() {
                        isLoader = false;
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pushReplacementNamed(
                                EmailVerificationScreen.routeName);
                      });
                    } else {
                      setState(() {
                        isLoader = false;
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AppWidgets.getAlertDialog(
                              title: "Alert",
                              content: "${list[1]}",
                              confirmButton: "Ok",
                              cancelButton: "Cancel",
                              onCancelTap: () {
                                Navigator.of(context).pop();
                              },
                              onConfirmTap: () {
                                Navigator.of(context).pop();
                              },
                            );
                          },
                        );
                      });
                    }
                  });
                });
              }),
          SizedBox(height: 24),
        ],
      ),
    );
  }
}
