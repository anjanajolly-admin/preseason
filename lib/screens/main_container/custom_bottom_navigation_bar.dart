import 'package:flutter/material.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  final ValueChanged<int> onTap;
  final int index;

  const CustomBottomNavigationBar(
      {Key? key, required this.onTap, required this.index})
      : super(key: key);

  @override
  _CustomBottomNavigationBarState createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  late int _currentTabIndex;

  @override
  void initState() {
    if (widget.index != null) {
      _currentTabIndex = widget.index;
    } else {
      _currentTabIndex = MainScreenBottomTab.Home;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 49,
      color: Colors.black,
      child: Row(
        children: [
          ///
          _singleButton(
              icons: _getActiveIcon(MainScreenBottomTab.Home)
                  ? 'assets/images/home_active_icon.png'
                  : 'assets/images/home_inactive_icon.png',
              index: MainScreenBottomTab.Home,
              label: "Home"),

          ///
          _singleButton(
              icons: _getActiveIcon(MainScreenBottomTab.Explore)
                  ? 'assets/images/explore_icon_active.png'
                  : 'assets/images/explore_icon_inactive.png',
              index: MainScreenBottomTab.Explore,
              label: "Explore"),

          ///
          _singleButton(
              icons: _getActiveIcon(MainScreenBottomTab.Messages)
                  ? 'assets/images/message_home_activate_icon.png'
                  : 'assets/images/message_home_deactivate_icon.png',
              index: MainScreenBottomTab.Messages,
              label: "Messages"),

          ///
          _singleButton(
              icons: _getActiveIcon(MainScreenBottomTab.Profile)
                  ? 'assets/images/profile_active.png'
                  : 'assets/images/profile_inactive.png',
              index: MainScreenBottomTab.Profile,
              label: "Profile"),
        ],
      ),
    );
  }

  _singleButton(
      {required String icons,
      required String label,
      required int index,
      double iconSize = 24.0}) {
    return Expanded(
      child: FlatButton(
        highlightColor: Color(0xFF4BB4F5).withOpacity(0.2),
        splashColor: Color(0xFF4BB4F5).withOpacity(0.1),
        padding: EdgeInsets.all(0),
        onPressed: () {
          widget.onTap(index);

          if (_currentTabIndex != index) {
            setState(() {
              _currentTabIndex = index;
            });
            setState(() {});
          }
        },
        child: Container(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 2.5,
                width: 44,
                color: (index == _currentTabIndex)
                    ? AppWidgets.buttonTextColor
                    : Colors.transparent,
              ),
              Expanded(child: Container()),
              Image.asset(icons),
              // AppIcon.svgIcon(icons, iconSize: iconSize),
              SizedBox(height: 5),
              Text(label,
                  style: TextStyle(
                    color: (index == _currentTabIndex)
                        ? AppWidgets.buttonTextColor
                        : AppWidgets.titleColor3,
                  )),
              Expanded(child: Container()),
            ],
          ),
        ),
      ),
    );
  }

  _getActiveIcon(int index) {
    if (_currentTabIndex == index) return true;
    return false;
  }
}

class MainScreenBottomTab {
  static int Home = 0;
  static int Explore = 1;
  static int Messages = 2;
  static int Profile = 3;
}
