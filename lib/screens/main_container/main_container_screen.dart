import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_cubit.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_state.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/screens/login_screen/login_screen.dart';
import 'package:pre_seasoned/screens/tab_explore_screen/explore_screen.dart';
import 'package:pre_seasoned/screens/tab_home_screen/home_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/messages_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/profile_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';

import 'custom_bottom_navigation_bar.dart';

class MainContainerArg {
  final int tap;

  var mapCoordinates;
  MainContainerArg(this.tap, {this.mapCoordinates});
}

class MainContainerScreen extends StatefulWidget {
  static const routeName = "/MainContainer";

  final MainContainerArg? arg;

  MainContainerScreen({Key? key, this.arg}) : super(key: key);

  @override
  _MainContainerScreenState createState() => _MainContainerScreenState();
}

class _MainContainerScreenState extends State<MainContainerScreen> {
  late int _currentTabIndex;
  List<Widget> _allPages = [
    HomeScreen(
        key: GlobalKey(),
        isFromBottom: false,
        managedChannels: null,
        followedChannels: null,
        unreadMessageCount: {}),
    ExploreChannels(
        key: GlobalKey(),
        isFromBottom: false,
        exploreChannelsList: [],
        joinExploreIndex: []),
    MessagesScreen(),
    ProfileScreen(),
  ];

  @override
  void initState() {
    initializePage();

    if (widget.arg != null && widget.arg!.tap != null) {
      _currentTabIndex = (widget.arg!.tap);
    } else {
      _currentTabIndex = MainScreenBottomTab.Home;
    }
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  initializePage() async {
    print("initializePage in");
    var exploreChannelsPage = await _getExplorePage(false);
    var homeScreenPage = await _getHomePage(false);
    setState(() {
      _allPages.removeAt(0);
      _allPages.insert(0, homeScreenPage);
      _allPages.removeAt(1);
      _allPages.insert(1, exploreChannelsPage);
    });
  }

  Future<Widget> _getHomePage(isFromBottom) async {
    Widget homeScreenWidget = HomeScreen(
        key: GlobalKey(),
        isFromBottom: isFromBottom,
        managedChannels: [],
        followedChannels: [],
        unreadMessageCount: {});

    var managedChannels = [];
    var followedChannels = [];
    Map<String, int?> unreadMessageCount = {};

    var managedChannelResponse =
    await AppInjector.resolve<ApiService>().channelsUserManges();

    if (managedChannelResponse.status == ApiResponseStatus.completed) {
      managedChannels = convert.jsonDecode(managedChannelResponse.data);
    } else if (managedChannelResponse.status == ApiResponseStatus.error) {
      managedChannels = [];
    } else {
      managedChannels = [];
    }

    var followedChannelResponse =
    await AppInjector.resolve<ApiService>().channelsUserFollows();

    if (followedChannelResponse.status == ApiResponseStatus.completed) {
      followedChannels = convert.jsonDecode(followedChannelResponse.data);

      for (var channelIndex = 0;
      channelIndex < followedChannels.length;
      channelIndex++) {
        int? unreadCount = await getUnreadMessagesCount(
            followedChannels[channelIndex]['generalThread']['feed_id']);
        unreadMessageCount[followedChannels[channelIndex]['generalThread']
        ['feed_id']] = unreadCount;
        List threadsList = followedChannels[channelIndex]['threads'];
        for (var threadIndex = 0;
        threadIndex < threadsList.length;
        threadIndex++) {
          int? unreadCount =
          await getUnreadMessagesCount(threadsList[threadIndex]['feed_id']);
          unreadMessageCount[threadsList[threadIndex]['feed_id']] = unreadCount;
        }
      }
    } else if (followedChannelResponse.status == ApiResponseStatus.error) {
      followedChannels = [];
    } else {
      followedChannels = [];
    }

    homeScreenWidget = HomeScreen(
        key: GlobalKey(),
        isFromBottom: isFromBottom,
        managedChannels: managedChannels,
        followedChannels: followedChannels,
        unreadMessageCount: unreadMessageCount);

    return homeScreenWidget;
  }

  Future<int?> getUnreadMessagesCount(feedId) async {
    QBFilter filter = QBFilter();
    filter.field = "_id";
    filter.operator = QBChatDialogFilterOperators.ALL;
    filter.value = feedId;

    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    var getDialogDetails = await AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit);
    if (getDialogDetails.status == QuickbloxApiResponseStatus.completed) {
      return ((getDialogDetails.data[0] as QBDialog).unreadMessagesCount);
    } else {
      return -1;
    }
  }

  Future<Widget> _getExplorePage(isFromBottom) async {
    Widget exploreScreenWidget = ExploreChannels(
        key: GlobalKey(),
        isFromBottom: isFromBottom,
        exploreChannelsList: [],
        joinExploreIndex: []);
    var value =
    await AppInjector.resolve<ApiService>().suggestedChannelsForUser();
    if (value.status == ApiResponseStatus.completed) {
      var exploreChannelsList = convert.jsonDecode(value.data);
      var joinExploreIndex =
      List.generate(exploreChannelsList!.length, (index) => false);
      exploreScreenWidget = ExploreChannels(
          key: GlobalKey(),
          isFromBottom: isFromBottom,
          exploreChannelsList: exploreChannelsList,
          joinExploreIndex: joinExploreIndex);
    } else if (value.status == ApiResponseStatus.error) {
      exploreScreenWidget = ExploreChannels(
          key: GlobalKey(),
          isFromBottom: isFromBottom,
          exploreChannelsList: [],
          joinExploreIndex: []);
    } else {
      exploreScreenWidget = ExploreChannels(
          key: GlobalKey(),
          isFromBottom: isFromBottom,
          exploreChannelsList: [],
          joinExploreIndex: []);
    }

    return exploreScreenWidget;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocListener<BaseCubit, BaseState>(
            bloc: BlocProvider.of<BaseCubit>(context),
            listener: (context, state) async {
              print("BlocListener Home Screen=>$state");
              if (state is LogOutSuccessState) {
                AppInjector.resolve<AppPreferences>().setUserId(-1);
                AppInjector.resolve<AppPreferences>()
                    .isLogged("LogOutSuccessState");
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamedAndRemoveUntil(
                    LoginScreen.routeName, (route) => false);
              } else if (state is LoadingState) {
                return;
              } else if (state is ShowMessageState) {
                BotToast.showText(
                  text: state.errorMsg,
                  duration: Duration(seconds: 2),
                );
              }
            },
            child: IndexedStack(children: _allPages, index: _currentTabIndex)),
        bottomNavigationBar: CustomBottomNavigationBar(
          onTap: (int index) async {
            if (index != _currentTabIndex) {
              setState(() {
                _currentTabIndex = index;
              });
              if (index == 0) {
                print("Check count 1 ");
                var homePage = await _getHomePage(true);
                var explorePage = await _getExplorePage(true);
                setState(() {
                  print("Check1 ");
                  _allPages.removeAt(0);
                  _allPages.insert(0, homePage);
                  _allPages.removeAt(1);
                  _allPages.insert(1, explorePage);
                });
              } else if (index == 1) {
                print("Check count 1 ");
                var homePage = await _getHomePage(true);
                var explorePage = await _getExplorePage(true);
                setState(() {
                  print("Check1 ");
                  _allPages.removeAt(0);
                  _allPages.insert(0, homePage);
                  _allPages.removeAt(1);
                  _allPages.insert(1, explorePage);
                });
              }
            }
          },
          index: _currentTabIndex,
        ),
      ),
    );
  }
}
