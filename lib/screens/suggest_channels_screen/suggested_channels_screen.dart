import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/screens/create_channel_screens/create_channel_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/friends_screen/find_friends_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class SuggestedChannelsScreen extends StatefulWidget {
  const SuggestedChannelsScreen({Key? key}) : super(key: key);
  static const String routeName = "/SuggestedChannelsScreen";

  @override
  _SuggestedChannelsScreenState createState() =>
      _SuggestedChannelsScreenState();
}

class _SuggestedChannelsScreenState extends State<SuggestedChannelsScreen> {
  var suggestedChannels = [];
  bool isSuggestedChannelsLoader = false;
  int joinLoaderIndex = -1;
  List<bool> joinChannelIndex = [];
  bool isLoader = false;
  bool isFollowedOrManaged = false;
  @override
  void initState() {
    super.initState();
    setState(() {
      isSuggestedChannelsLoader = true;
    });
    _getSuggestedApiCall();
  }

  _getSuggestedApiCall() {
    AppInjector.resolve<ApiService>().suggestedChannelsForUser().then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          isSuggestedChannelsLoader = false;
          suggestedChannels = convert.jsonDecode(value.data);
          joinChannelIndex =
              List.generate(suggestedChannels.length, (index) => false);
          print("Suggested Channel $joinChannelIndex");
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          isSuggestedChannelsLoader = false;

          BotToast.showText(text: value.message[1]);
        });
      } else {
        setState(() {
          isSuggestedChannelsLoader = false;
          BotToast.showText(text: "Something went wrong.Please try later");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => AppInjector.resolve<AppRoutes>()
                .navigatorKey
                .currentState!
                .pop()),
        actions: [
          AppWidgets.getTextButton(
              "Create Channel", AppWidgets.buttonTextColor, 16, () {
            AppInjector.resolve<AppRoutes>()
                .navigatorKey
                .currentState!
                .pushNamed(CreateChannelScreen.routeName, arguments: [
              null,
            ]);
          })
        ],
        elevation: 0,
        brightness: Brightness.dark,
        backgroundColor: AppWidgets.backgroundColor,
      ),
      body: _getBody(),
    );
  }

  Widget _getBody() {
    return Stack(
      children: [
        SingleChildScrollView(
          child: Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                children: [
                  SizedBox(
                    height: 30,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      "App will be more fun if you join a channel",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 28,
                        fontFamily: "Proxima Nova",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Suggested Channels",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  ListView.builder(
                      itemCount: suggestedChannels.length,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, channelIndex) {
                        return Container(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                channelIndex % 2 == 0
                                    ? Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    _suggestedChannelViews(
                                        suggestedChannels, channelIndex),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    channelIndex + 1 <
                                        suggestedChannels.length
                                        ? _suggestedChannelViews(
                                        suggestedChannels,
                                        channelIndex + 1)
                                        : Container(
                                      width: (MediaQuery.of(context)
                                          .size
                                          .width /
                                          2) -
                                          25,
                                    )
                                  ],
                                )
                                    : SizedBox(
                                  height: 15,
                                )
                              ]),
                        );
                      }),
                ],
              )),
        ),
        isSuggestedChannelsLoader
            ? Center(
          child: Container(
            child: SizedBox(
                height: 50,
                width: 50,
                child: CircularProgressIndicator.adaptive(
                    strokeWidth: 2,
                    backgroundColor: AppWidgets.buttonTextColor,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Colors.white)
                )),
          ),
        )
            : Container(),
        Positioned(
            child: Padding(
              padding: EdgeInsets.only(bottom: 24.0),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: InkWell(
                  onTap: () {
                    isLoader = true;
                    isFollowedOrManaged
                        ? BotToast.showText(
                        text:
                        "You have to create a channel or must follow one channel")
                    // : AppInjector.resolve<AppRoutes>()
                    //     .navigatorKey
                    //     .currentState!
                    //     .pushNamedAndRemoveUntil(
                    //         MainContainerScreen.routeName, (route) => false);
                        : AppInjector.resolve<AppRoutes>()
                        .navigatorKey
                        .currentState!
                        .pushNamedAndRemoveUntil(
                        FindFriendsScreen.routeName, (route) => false);
                  },
                  child: Container(
                    width: 140,
                    height: 48,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x1e000000),
                          blurRadius: 30,
                          offset: Offset(0, 10),
                        ),
                      ],
                      color: Color(0xff0080ff),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Continue",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Icon(
                          Icons.arrow_right_alt,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ))
      ],
    );
  }

  Widget _suggestedChannelViews(suggestedChannel, channelIndex) {
    return Container(
      width: (MediaQuery.of(context).size.width / 2) - 25,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Color(0xff1c1c1e),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: (MediaQuery.of(context).size.width / 2) - 25,
                  height: 160,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                      bottomLeft: Radius.circular(0),
                      bottomRight: Radius.circular(0),
                    ),
                  ),
                  child: Stack(
                    children: [
                      Positioned.fill(
                        child: Container(
                          width: (MediaQuery.of(context).size.width / 2) - 25,
                          height: 160,
                          child: suggestedChannels[channelIndex]
                          ['profilePhoto'] ==
                              null
                              ? Image.asset(
                            "assets/images/suggested_channel_cover.png",
                            fit: BoxFit.fill,
                          )
                              : CachedNetworkImage(
                            imageUrl: suggestedChannels[channelIndex]
                            ['profilePhoto']['url'],
                            fit: BoxFit.fill,
                            placeholder: (context, url) => Image.asset(
                              "assets/images/suggested_channel_cover.png",
                              fit: BoxFit.fill,
                            ),

                            // placeholder: (context, url) =>
                            //     CircularProgressIndicator(),
                            errorWidget: (context, url, error) {
                              print(
                                  'errorWidget ${error.toString()} \n$url');
                              return Image.asset(
                                "assets/images/suggested_channel_cover.png",
                                fit: BoxFit.fill,
                              );
                            },
                          ),
                        ),
                      ),
                      Positioned.fill(
                        child: Container(
                          padding: EdgeInsets.only(right: 8, bottom: 8),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  color: Color(0x7f0c162d),
                                ),
                                padding: const EdgeInsets.all(4),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${suggestedChannels[channelIndex]['membersCount'] == null ? "0" : suggestedChannels[channelIndex]['membersCount']} MEMBERS",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 10,
                                        fontFamily: "Proxima Nova",
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        suggestedChannels[channelIndex]['name'],
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 12),
                InkWell(
                  focusColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  onTap: () {
                    setState(() {
                      joinLoaderIndex = channelIndex;
                    });
                    if (joinChannelIndex[channelIndex]) {
                      joinChannelApi(
                          AppInjector.resolve<AppPreferences>().getUserId(),
                          suggestedChannels[channelIndex]['id'],
                          "unfollow")
                          .then((value) {
                        if (value[0]) {
                          setState(() {
                            joinChannelIndex[channelIndex] = false;
                            joinLoaderIndex = -1;
                          });
                        } else {
                          setState(() {
                            joinLoaderIndex = -1;
                            BotToast.showText(text: value[1]);
                          });
                        }
                      });
                    } else {
                      joinChannelApi(
                          AppInjector.resolve<AppPreferences>().getUserId(),
                          suggestedChannels[channelIndex]['id'],
                          "follow")
                          .then((value) {
                        if (value[0]) {
                          setState(() {
                            joinChannelIndex[channelIndex] = true;
                            joinLoaderIndex = -1;
                          });
                        } else {
                          setState(() {
                            joinLoaderIndex = -1;
                            BotToast.showText(text: value[1]);
                          });
                        }
                      });
                    }
                  },
                  child: joinChannelIndex[channelIndex]
                      ? Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Color(0xff07cf87),
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 8,
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Text(
                            "Joined",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        Container(
                          width: 14,
                          height: 14,
                          child: Image.asset(
                            "assets/images/suggested_channel_tick.png",
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(width: 4),
                        joinLoaderIndex == channelIndex
                            ? SizedBox(
                            height: 10,
                            width: 10,
                            child: CircularProgressIndicator.adaptive(
                                strokeWidth: 1,
                                backgroundColor: AppWidgets.buttonTextColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.white)
                            ))
                            : Container()
                      ],
                    ),
                  )
                      : Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Color(0xff3a3a3c),
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 8,
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Join",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(width: 4),
                        joinLoaderIndex == channelIndex
                            ? SizedBox(
                            height: 10,
                            width: 10,
                            child: CircularProgressIndicator.adaptive(
                                strokeWidth: 2,
                                backgroundColor: AppWidgets.buttonTextColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.white)
                            ))
                            : Container()
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<List> joinChannelApi(userId, channelId, type) async {
    var response = await AppInjector.resolve<ApiService>().joinChannel({
      "channel_id": channelId,
      "user_id": userId,
    }, type);

    if (response.status == ApiResponseStatus.completed) {
      return [true, ""];
    } else if (response.status == ApiResponseStatus.error) {
      print("response data init ${response.message[1]}");
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }
}
