import 'dart:convert' as convert;
import 'dart:ui';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/screens/create_channel_screens/channel_details_screen.dart';
import 'package:pre_seasoned/screens/create_channel_screens/create_channel_screen.dart';
import 'package:pre_seasoned/screens/notifications_screen/notifications_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';

class HomeScreen extends StatefulWidget {
  final bool isFromBottom;
  final List? managedChannels;
  final List? followedChannels;
  final Map<String, int?> unreadMessageCount;
  const HomeScreen(
      {Key? key,
        required this.isFromBottom,
        required this.managedChannels,
        required this.followedChannels,
        required this.unreadMessageCount})
      : super(key: key);
  static const String routeName = "/HomeScreen";

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List? managedChannels;
  List? followedChannels;
  Map<String, int?>? unreadMessageCount;
  // bool isLoader = false;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  bool isFromBottom = false;

  @override
  void initState() {
    super.initState();
    // isLoader = true;
    managedChannels = widget.managedChannels;
    followedChannels = widget.followedChannels;
    isFromBottom = widget.isFromBottom;
    // _getAllApiCalls();
    unreadMessageCount = widget.unreadMessageCount;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  Future<int?> getUnreadMessagesCount(feedId) async {
    QBFilter filter = QBFilter();
    filter.field = "_id";
    filter.operator = QBChatDialogFilterOperators.ALL;
    filter.value = feedId;

    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    var getDialogDetails = await AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit);
    if (getDialogDetails.status == QuickbloxApiResponseStatus.completed) {
      return ((getDialogDetails.data[0] as QBDialog).unreadMessagesCount);
    } else {
      return -1;
    }
  }

  Future<void> _getAllApiCalls() async {
    AppInjector.resolve<ApiService>().channelsUserManges().then((value) async {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          managedChannels = convert.jsonDecode(value.data);
        });

        print("Manage Channel $managedChannels");
        // for (var channelIndex = 0;
        //     channelIndex < managedChannels!.length;
        //     channelIndex++) {
        //   List threadsList = managedChannels![channelIndex]['threads'];
        //   for (var threadIndex = 0;
        //       threadIndex < threadsList.length;
        //       threadIndex++) {
        //     int? unreadCount = await getUnreadMessagesCount(
        //         threadsList[threadIndex]['feed_id']);
        //     unreadMessageCount![threadsList[threadIndex]['feed_id']] =
        //         unreadCount;
        //
        //     setState(() {});
        //   }
        // }
      } else if (value.status == ApiResponseStatus.error) {
        managedChannels = [];
        BotToast.showText(text: value.message[1]);
      } else {
        managedChannels = [];
        BotToast.showText(text: "Something went wrong.Please try later");
      }
    });

    AppInjector.resolve<ApiService>().channelsUserFollows().then((value) async {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          followedChannels = convert.jsonDecode(value.data);
        });
        for (var channelIndex = 0;
        channelIndex < followedChannels!.length;
        channelIndex++) {
          int? unreadCount = await getUnreadMessagesCount(
              followedChannels![channelIndex]['generalThread']['feed_id']);
          unreadMessageCount![followedChannels![channelIndex]['generalThread']
          ['feed_id']] = unreadCount;
          List threadsList = followedChannels![channelIndex]['threads'];
          for (var threadIndex = 0;
          threadIndex < threadsList.length;
          threadIndex++) {
            int? unreadCount = await getUnreadMessagesCount(
                threadsList[threadIndex]['feed_id']);
            unreadMessageCount![threadsList[threadIndex]['feed_id']] =
                unreadCount;
          }
        }
      } else if (value.status == ApiResponseStatus.error) {
        followedChannels = [];
        BotToast.showText(text: value.message[1]);
      } else {
        followedChannels = [];
        BotToast.showText(text: "Something went wrong.Please try later");
      }
    });
    setState(() {
      // isLoader = false;
      // isFromBottom = false;
    });
  }

  Future<Null> refresh() async {
    await Future.delayed(Duration(seconds: 2));
    refreshKey.currentState?.show();
    setState(() {
      // isLoader = true;
      _getAllApiCalls();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xff2c2c2e),
        appBar: AppBar(
            backgroundColor: AppWidgets.backgroundColor,
            brightness: Brightness.dark,
            elevation: 0,
            actions: [
              IconButton(
                onPressed: () {
                  setState(() {
                    AppInjector.resolve<AppRoutes>()
                        .navigatorKey
                        .currentState!
                        .pushNamed(CreateChannelScreen.routeName, arguments: [
                      null,
                    ]);
                  });
                },
                icon: Image.asset(
                  "assets/images/add_notification_icon.png",
                  fit: BoxFit.fill,
                ),
              ),
              IconButton(
                onPressed: () {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pushNamed(NotificationScreen.routeName);
                },
                icon: Image.asset(
                  "assets/images/notification_icon.png",
                  fit: BoxFit.fill,
                ),
              ),
            ],
            leadingWidth: 100,
            leading: Center(
              child: Text(
                "Home",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontFamily: "Proxima Nova",
                  fontWeight: FontWeight.w600,
                ),
              ),
            )),
        body: _getBody());
  }

  Widget _getBody() {
    return Stack(children: [
      (managedChannels == null || followedChannels == null) && !isFromBottom
          ? Positioned.fill(
        child: Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: SizedBox(
                      height: 30,
                      width: 30,
                      child: CircularProgressIndicator.adaptive(
                          strokeWidth: 2,
                          backgroundColor: AppWidgets.buttonTextColor,
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Colors.white))),
                )
              ],
            )),
      )
          : RefreshIndicator(
        displacement: 50,
        strokeWidth: 2,
        backgroundColor: Colors.white,
        color: AppWidgets.buttonTextColor,
        onRefresh: refresh,
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 24),
                  managedChannels == null
                      ? Container()
                      : managedChannels!.isEmpty
                      ? Container()
                      : Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment:
                    CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        child: Text(
                          "Channels you manage",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 28,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      // managedChannels == null
                      //     ? Container()
                      //     : managedChannels!.isEmpty
                      //         ? Container(
                      //             decoration: BoxDecoration(
                      //               borderRadius:
                      //                   BorderRadius.circular(20),
                      //               border: Border.all(
                      //                 color: Color(0xff464649),
                      //                 width: 1,
                      //               ),
                      //               color: Color(0xff1c1c1e),
                      //             ),
                      //             padding: EdgeInsets.all(12),
                      //             child: SizedBox(
                      //               child: Text(
                      //                 "You are not managing any channels.It will be more fun if you Create a channel from add button",
                      //                 textAlign: TextAlign.center,
                      //                 style: TextStyle(
                      //                   color: Colors.white,
                      //                   fontSize: 16,
                      //                   fontFamily: "Proxima Nova",
                      //                   fontWeight: FontWeight.w600,
                      //                 ),
                      //               ),
                      //             ),
                      //           )
                      //         :
                      ListView.builder(
                        itemCount: managedChannels!.length,
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder:
                            (BuildContext context, index) {
                          return Column(
                            children: [
                              AppWidgets
                                  .getHomeScreenChannelCard(
                                index: index,
                                channelsList: managedChannels!,
                                channelsUnreadMessageCount:
                                null,
                                onTap: () {
                                  AppInjector.resolve<
                                      AppRoutes>()
                                      .navigatorKey
                                      .currentState!
                                      .pushNamed(
                                      ChannelDetailsScreen
                                          .routeName,
                                      arguments: [
                                        managedChannels![index]
                                        ['id'],
                                        false,
                                        managedChannels![index][
                                        'profilePhoto'] ==
                                            null
                                            ? null
                                            : managedChannels![
                                        index][
                                        'profilePhoto']
                                        ['url'],
                                        managedChannels![index][
                                        'coverPhoto'] ==
                                            null
                                            ? null
                                            : managedChannels![
                                        index][
                                        'coverPhoto']
                                        ['url'],
                                        true,
                                        false
                                      ]).then((_) {
                                    setState(() {
                                      _getAllApiCalls();
                                    });
                                  });
                                },
                              ),
                              SizedBox(height: 16)
                            ],
                          );
                        },
                      ),
                    ],
                  ),
                  managedChannels == null
                      ? Container()
                      : managedChannels!.isEmpty
                      ? Container()
                      : SizedBox(height: 20),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        child: Text(
                          "Channels you follow",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 28,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      followedChannels == null
                          ? Container()
                          : followedChannels!.isEmpty
                          ? Container(
                        decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.circular(20),
                          border: Border.all(
                            color: Color(0xff464649),
                            width: 1,
                          ),
                          color: Color(0xff1c1c1e),
                        ),
                        padding: EdgeInsets.all(12),
                        child: SizedBox(
                          child: Text(
                            "You have not followed any channels.It will be more fun if you Join a channel from explore tab",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      )
                          : ListView.builder(
                        itemCount: followedChannels!.length,
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder:
                            (BuildContext context, index) {
                          return Column(
                            children: [
                              AppWidgets
                                  .getHomeScreenChannelCard(
                                index: index,
                                channelsList: followedChannels!,
                                channelsUnreadMessageCount:
                                unreadMessageCount!,
                                onTap: () {
                                  print(
                                      "check ${followedChannels![index]['id']}");

                                  AppInjector.resolve<
                                      AppRoutes>()
                                      .navigatorKey
                                      .currentState!
                                      .pushNamed(
                                      ChannelDetailsScreen
                                          .routeName,
                                      arguments: [
                                        followedChannels![index]
                                        ['id'],
                                        false,
                                        followedChannels![index]
                                        [
                                        'profilePhoto'] ==
                                            null
                                            ? null
                                            : followedChannels![
                                        index][
                                        'profilePhoto']
                                        ['url'],
                                        followedChannels![index]
                                        [
                                        'coverPhoto'] ==
                                            null
                                            ? null
                                            : followedChannels![
                                        index][
                                        'coverPhoto']
                                        ['url'],
                                        false,
                                        false
                                      ]).then((value) {
                                    setState(() {
                                      _getAllApiCalls();
                                    });
                                  });
                                },
                              ),
                              SizedBox(height: 16)
                            ],
                          );
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    ]);
  }
}
