import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class InviteMemberScreen extends StatefulWidget {
  final inviteCode;
  const InviteMemberScreen({Key? key, this.inviteCode}) : super(key: key);
  static const String routeName = "/InviteMemberScreen";

  @override
  _InviteMemberScreenState createState() => _InviteMemberScreenState();
}

class _InviteMemberScreenState extends State<InviteMemberScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Invite Members"),
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
      ),
      body: Center(child: _getBody()),
      backgroundColor: AppWidgets.backgroundColor,
    );
  }

  Widget _getBody() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 150,
            height: 150,
            child: Image.asset(
              "assets/images/invite_member.png",
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            height: 32,
          ),
          SizedBox(
            child: Text(
              "Invite a Member to your\nChannel!",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 28,
                fontFamily: "Proxima Nova",
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          SizedBox(
            height: 32,
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Share the code to invite members",
                    style: TextStyle(
                      color: Color(0xfff2f2f7),
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 8),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(
                        color: Colors.white,
                        width: 1,
                      ),
                    ),
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // width: 303,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                child: SizedBox(
                                  child: Text(
                                    widget.inviteCode,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                              InkWell(
                                focusColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                splashColor: Colors.transparent,
                                onTap: () {
                                  Clipboard.setData(
                                      ClipboardData(text: widget.inviteCode));
                                  BotToast.showText(text: "Invite Code copied");
                                },
                                child: Container(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                    "assets/images/invite_member_copy.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16),
              AppWidgets.getWtaButton(
                  buttonName: "Invite Member",
                  isLoader: false,
                  fontSize: 16,
                  width: double.infinity,
                  height: 48,
                  onPressed: () {})
            ],
          ),
          SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }
}
