import 'dart:async';
import 'dart:convert' as convert;
import 'dart:io';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/screens/channel_settings_screens/channel_info_screens/channel_info_screen.dart';
import 'package:pre_seasoned/screens/create_channel_screens/channel_details_screen.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class CreateChannelScreen extends StatefulWidget {
  final editChannelResponse;
  const CreateChannelScreen({Key? key, required this.editChannelResponse})
      : super(key: key);
  static const String routeName = "/CreateChannelScreen";
  @override
  _CreateChannelScreenState createState() => _CreateChannelScreenState();
}

class _CreateChannelScreenState extends State<CreateChannelScreen> {
  final TextEditingController _channelName = TextEditingController();
  final TextEditingController _channelBio = TextEditingController();
  bool isButtonLoader = false;
  bool isImageLoader = false;
  bool isCategoryLoader = false;
  File? _profileImageFile;
  File? _coverImageFile;
  List<String> interestTypesSelectedName = [];
  List<int> interestTypesSelectedId = [];

  @override
  void initState() {
    super.initState();
    if (widget.editChannelResponse != null) {
      editScreenInitialize();
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {});
  }

  Future<void> editScreenInitialize() async {
    _channelName.text = widget.editChannelResponse['name'];
    _channelBio.text = widget.editChannelResponse['bio'];
    var categoryList = widget.editChannelResponse['categories'] as List;
    for (var index = 0; index < categoryList.length; index++) {
      interestTypesSelectedName.add(categoryList[index]["name"]);
      interestTypesSelectedId.add(categoryList[index]["id"]);
    }
    _coverImageFile = widget.editChannelResponse!["coverPhoto"] == null
        ? null
        : await urlToFile(widget.editChannelResponse!["coverPhoto"]["url"]);

    _profileImageFile = widget.editChannelResponse!["profilePhoto"] == null
        ? null
        : await urlToFile(widget.editChannelResponse!["profilePhoto"]["url"]);
  }

  Future<File> urlToFile(String imageUrl) async {
    var rng = new Random();
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File file = new File('$tempPath' + (rng.nextInt(100)).toString() + '.png');
    http.Response response = await http.get(Uri.parse(imageUrl));
    await file.writeAsBytes(response.bodyBytes);
    return file;
  }

  Future<File> getImageFileFromAssets(String path) async {
    final byteData = await rootBundle.load('assets/$path');

    final file = File('${(await getTemporaryDirectory()).path}/$path');
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
    return file;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => AppInjector.resolve<AppRoutes>()
                .navigatorKey
                .currentState!
                .pop(context)),
        title: Text(widget.editChannelResponse == null
            ? "Create Channel"
            : "Edit Channel"),
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
      ),
      backgroundColor: AppWidgets.backgroundColor,
      body: InkWell(
        focusColor: Colors.transparent,
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: () {
          AppConstants.hideKeyboard(context);
        },
        child: SingleChildScrollView(child: _getBody()),
      ),
    );
  }

  Widget _getBody() {
    return Stack(children: [
      Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                  focusColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  onTap: () {
                    showModalBottomSheet(
                        isDismissible: false,
                        enableDrag: false,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        backgroundColor: AppWidgets.backgroundColor,
                        context: context,
                        builder: (context) {
                          return StatefulBuilder(builder: (BuildContext context,
                              StateSetter setState /*You can rename this!*/) {
                            return Container(
                              height: 110,
                              margin: EdgeInsets.all(10),
                              child: Column(
                                children: [
                                  AppWidgets.getWtaButton(
                                      buttonName: "Gallery",
                                      fontSize: 14,
                                      width: double.infinity,
                                      height: 48,
                                      isLoader: false,
                                      onPressed: () {
                                        isImageLoader = true;
                                        Navigator.pop(context);
                                        _getCoverFromGallery(
                                            width: (MediaQuery.of(context)
                                                .size
                                                .width -
                                                40),
                                            height: 136);
                                      }),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  AppWidgets.getWtaButton(
                                      buttonName: "Camera",
                                      fontSize: 14,
                                      width: double.infinity,
                                      height: 48,
                                      isLoader: false,
                                      onPressed: () {
                                        isImageLoader = true;
                                        Navigator.pop(context);
                                        _getCoverFromCamera(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width -
                                                40,
                                            height: 136);
                                      }),
                                ],
                              ),
                            );
                          });
                        });
                  },
                  child: widget.editChannelResponse == null ||
                      widget.editChannelResponse!["coverPhoto"] == null
                      ? _coverImageFile == null
                      ? Container(
                      width: MediaQuery.of(context).size.width - 40,
                      height: 136,
                      child: Stack(
                        children: [
                          Positioned.fill(
                            child: Image.asset(
                              "assets/images/channel_cover.png",
                              fit: BoxFit.fill,
                            ),
                          ),
                          Positioned.fill(
                            bottom: -16,
                            right: -16,
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.circular(100),
                                  color: Color(0x9918191f),
                                ),
                                padding: const EdgeInsets.only(
                                    left: 8,
                                    right: 20,
                                    top: 8,
                                    bottom: 20),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment:
                                  MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 20,
                                      height: 20,
                                      child: isImageLoader
                                          ? Container(
                                          child:
                                          CircularProgressIndicator())
                                          : Image.asset(
                                        "assets/images/camera_icon.png",
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ))
                      : Container(
                    width: MediaQuery.of(context).size.width - 40,
                    height: 136,
                    child: Stack(
                      children: [
                        Positioned.fill(
                          child: Image.file(
                            _coverImageFile!,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Positioned.fill(
                          bottom: -16,
                          right: -16,
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(100),
                                color: Color(0x9918191f),
                              ),
                              padding: const EdgeInsets.only(
                                  left: 8,
                                  right: 20,
                                  top: 8,
                                  bottom: 20),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 20,
                                    height: 20,
                                    child: isImageLoader
                                        ? Container(
                                        child:
                                        CircularProgressIndicator())
                                        : Image.asset(
                                      "assets/images/camera_icon.png",
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                      : Container(
                      width: MediaQuery.of(context).size.width - 40,
                      height: 136,
                      child: Stack(
                        children: [
                          _coverImageFile != null
                              ? Positioned.fill(
                            child: Image.file(
                              _coverImageFile!,
                              fit: BoxFit.fill,
                            ),
                          )
                              : Positioned.fill(
                              child: CachedNetworkImage(
                                imageUrl: widget.editChannelResponse![
                                "coverPhoto"]["url"],
                                fit: BoxFit.fill,
                                placeholder: (context, url) =>
                                    Image.asset(
                                      "assets/images/channel_cover.png",
                                      fit: BoxFit.fill,
                                    ),

                                // placeholder: (context, url) => CircularProgressIndicator(),
                                errorWidget: (context, url, error) {
                                  print(
                                      'errorWidget ${error.toString()} \n$url');
                                  return Image.asset(
                                    "assets/images/channel_cover.png",
                                    fit: BoxFit.fill,
                                  );
                                },
                              )),
                          Positioned.fill(
                            bottom: -16,
                            right: -16,
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: Color(0x9918191f),
                                ),
                                padding: const EdgeInsets.only(
                                    left: 8, right: 20, top: 8, bottom: 20),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment:
                                  MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 20,
                                      height: 20,
                                      child: isImageLoader
                                          ? Container(
                                          child:
                                          CircularProgressIndicator())
                                          : Image.asset(
                                        "assets/images/camera_icon.png",
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ))),
              SizedBox(height: 56),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Channel Name",
                      style: TextStyle(
                        color: Color(0xfff2f2f7),
                        fontSize: 16,
                        fontFamily: "Proxima Nova",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(height: 4),
                    AppWidgets.getEditText(
                        controller: _channelName,
                        onChanged: (channelName) {
                          // _channelName.text = channelName;
                        },
                        keyboardType: TextInputType.text,
                        maxLength: 100,
                        hint: "",
                        inputFormatter: [],
                        isObscureText: false),
                  ],
                ),
              ),
              SizedBox(height: 16),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Expanded(
                          child: Text(
                            "Channel Category",
                            style: TextStyle(
                              color: Color(0xfff2f2f7),
                              fontSize: 16,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        isCategoryLoader
                            ? SizedBox(
                            height: 20,
                            width: 20,
                            child: CircularProgressIndicator.adaptive(
                              strokeWidth: 2,
                              backgroundColor: Colors.white,
                            ))
                            : InkWell(
                          focusColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          hoverColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onTap: () {
                            setState(() {
                              isCategoryLoader = true;
                            });

                            AppInjector.resolve<ApiService>()
                                .getAllInterest()
                                .then((response) {
                              if (response.status ==
                                  ApiResponseStatus.completed) {
                                setState(() {
                                  isCategoryLoader = false;
                                  showModalBottomSheet(
                                      isDismissible: false,
                                      enableDrag: false,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(10.0),
                                      ),
                                      backgroundColor:
                                      AppWidgets.backgroundColor,
                                      context: context,
                                      builder: (context) {
                                        return StatefulBuilder(builder:
                                            (BuildContext context,
                                            StateSetter
                                            setModalState /*You can rename this!*/) {
                                          return _getCategoryWidget(
                                              convert
                                                  .jsonDecode(
                                                  response.data)
                                                  .length,
                                              convert.jsonDecode(
                                                  response.data),
                                              setModalState);
                                        });
                                      });
                                });
                              } else if (response.status ==
                                  ApiResponseStatus.error) {
                                setState(() {
                                  isCategoryLoader = false;
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AppWidgets.getAlertDialog(
                                        title: "Alert",
                                        content: response.message[1],
                                        confirmButton: "Ok",
                                        cancelButton: "Cancel",
                                        onCancelTap: () {
                                          Navigator.of(context).pop();
                                        },
                                        onConfirmTap: () {
                                          Navigator.of(context).pop();
                                        },
                                      );
                                    },
                                  );
                                });
                              } else {
                                return [
                                  false,
                                  "Something went wrong.Please try later"
                                ];
                              }
                            });
                          },
                          child: Icon(
                            Icons.edit,
                            color: Colors.white,
                            size: 20.0,
                            semanticLabel:
                            'Text to announce in accessibility modes',
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 4),
                    InkWell(
                      focusColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          isCategoryLoader = true;
                        });

                        AppInjector.resolve<ApiService>()
                            .getAllInterest()
                            .then((response) {
                          if (response.status == ApiResponseStatus.completed) {
                            setState(() {
                              isCategoryLoader = false;
                              showModalBottomSheet(
                                  isDismissible: false,
                                  enableDrag: false,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  backgroundColor: AppWidgets.backgroundColor,
                                  context: context,
                                  builder: (context) {
                                    return StatefulBuilder(builder: (BuildContext
                                    context,
                                        StateSetter
                                        setModalState /*You can rename this!*/) {
                                      return _getCategoryWidget(
                                          convert
                                              .jsonDecode(response.data)
                                              .length,
                                          convert.jsonDecode(response.data),
                                          setModalState);
                                    });
                                  });
                            });
                          } else if (response.status ==
                              ApiResponseStatus.error) {
                            setState(() {
                              isCategoryLoader = false;
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AppWidgets.getAlertDialog(
                                    title: "Alert",
                                    content: response.message[1],
                                    confirmButton: "Ok",
                                    cancelButton: "Cancel",
                                    onCancelTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    onConfirmTap: () {
                                      Navigator.of(context).pop();
                                    },
                                  );
                                },
                              );
                            });
                          } else {
                            return [
                              false,
                              "Something went wrong.Please try later"
                            ];
                          }
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.only(left: 10),
                        width: double.infinity,
                        height: 48,
                        decoration: BoxDecoration(
                          color: AppWidgets.backgroundColor,
                          border: Border.all(
                            color: AppWidgets.titleColor4,
                            width: 1,
                          ), // set border width
                          borderRadius: BorderRadius.all(Radius.circular(
                              8.0)), // set rounded corner radius
                        ),
                        child: Container(
                            height: 100.0,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                children: getInterestTypes.toList(),
                              ),
                            )),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 16),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Channel Bio",
                      style: TextStyle(
                        color: Color(0xfff2f2f7),
                        fontSize: 16,
                        fontFamily: "Proxima Nova",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(height: 4),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Container(
                              height: 120,
                              padding: EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                color: AppWidgets.backgroundColor,
                                border: Border.all(
                                  color: AppWidgets.titleColor4,
                                  width: 1,
                                ), // set border width
                                borderRadius: BorderRadius.all(Radius.circular(
                                    8.0)), // set rounded corner radius
                              ),
                              child: TextField(
                                controller: _channelBio,
                                inputFormatters: [],
                                obscureText: false,
                                onChanged: (channelBio) {},
                                maxLines: null,
                                style: TextStyle(
                                  color: AppWidgets.titleColor3,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: "Proxima Nova",
                                ),
                                decoration: InputDecoration(
                                  hintText: "",
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                    color: AppWidgets.titleColor3,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "Proxima Nova",
                                  ),
                                ),
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 16),
              AppWidgets.getWtaButton(
                  buttonName:
                  widget.editChannelResponse == null ? "Create" : "Done",
                  isLoader: isButtonLoader,
                  fontSize: 16,
                  width: double.infinity,
                  height: 48,
                  onPressed: () {
                    _channelName.text.trim() == ""
                        ? showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AppWidgets.getAlertDialog(
                          title: "Alert",
                          content: "Channel Name should not be empty",
                          confirmButton: "Ok",
                          cancelButton: "Cancel",
                          onCancelTap: () {
                            Navigator.of(context).pop();
                          },
                          onConfirmTap: () {
                            Navigator.of(context).pop();
                          },
                        );
                      },
                    )
                        : _channelBio.text.trim() == ""
                        ? showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AppWidgets.getAlertDialog(
                          title: "Alert",
                          content: "Channel Bio should not be empty",
                          confirmButton: "Ok",
                          cancelButton: "Cancel",
                          onCancelTap: () {
                            Navigator.of(context).pop();
                          },
                          onConfirmTap: () {
                            Navigator.of(context).pop();
                          },
                        );
                      },
                    )
                        : interestTypesSelectedName.isEmpty
                        ? showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AppWidgets.getAlertDialog(
                          title: "Alert",
                          content:
                          "Channel Category types should not be empty",
                          confirmButton: "Ok",
                          cancelButton: "Cancel",
                          onCancelTap: () {
                            Navigator.of(context).pop();
                          },
                          onConfirmTap: () {
                            Navigator.of(context).pop();
                          },
                        );
                      },
                    )
                    /*  : _coverImageFile == null ||
                                        _profileImageFile == null
                                    ? showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AppWidgets.getAlertDialog(
                                            title: "Alert",
                                            content:
                                                "Profile photo or cover photo is not uploaded",
                                            confirmButton: "Ok",
                                            cancelButton: "Cancel",
                                            onCancelTap: () {
                                              Navigator.of(context).pop();
                                            },
                                            onConfirmTap: () {
                                              Navigator.of(context).pop();
                                            },
                                          );
                                        },
                                      )*/
                        : widget.editChannelResponse == null
                        ? createChannelApi(
                        _channelName.text,
                        _channelBio.text,
                        _profileImageFile,
                        _coverImageFile,
                        interestTypesSelectedId)
                        .then((value) {
                      if (value[0]) {
                        setState(() {
                          isButtonLoader = false;
                        });
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .popAndPushNamed(
                            ChannelDetailsScreen
                                .routeName,
                            arguments: [
                              value[1]['id'],
                              true,
                              value[1]['profilePhoto'] == null
                                  ? null
                                  : value[1]['profilePhoto']
                              ['url'],
                              value[1]['coverPhoto'] == null
                                  ? null
                                  : value[1]['coverPhoto']
                              ['url'],
                              true,
                              false
                            ]);
                      } else {
                        setState(() {
                          isButtonLoader = false;
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AppWidgets
                                  .getAlertDialog(
                                title: "Alert",
                                content: value[1],
                                confirmButton: "Ok",
                                cancelButton: "Cancel",
                                onCancelTap: () {
                                  Navigator.of(context).pop();
                                },
                                onConfirmTap: () {
                                  Navigator.of(context).pop();
                                },
                              );
                            },
                          );
                        });
                      }
                    })
                        : updateChannelApi(
                        _channelName.text,
                        _channelBio.text,
                        _profileImageFile,
                        _coverImageFile,
                        interestTypesSelectedId)
                        .then((value) {
                      if (value[0]) {
                        setState(() {
                          isButtonLoader = false;
                          AppInjector.resolve<AppRoutes>()
                              .navigatorKey
                              .currentState!
                              .pop(ChannelInfoScreen
                              .routeName);
                        });
                      } else {
                        setState(() {
                          isButtonLoader = false;
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AppWidgets
                                  .getAlertDialog(
                                title: "Alert",
                                content: value[1],
                                confirmButton: "Ok",
                                cancelButton: "Cancel",
                                onCancelTap: () {
                                  Navigator.of(context).pop();
                                },
                                onConfirmTap: () {
                                  Navigator.of(context).pop();
                                },
                              );
                            },
                          );
                        });
                      }
                    });
                  }),
              SizedBox(
                height: 20,
              )
            ],
          )),
      Positioned(
        left: 40,
        top: 90,
        child: Container(
          width: 80,
          height: 80,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                focusColor: Colors.transparent,
                highlightColor: Colors.transparent,
                hoverColor: Colors.transparent,
                splashColor: Colors.transparent,
                onTap: () {
                  showModalBottomSheet(
                      isDismissible: false,
                      enableDrag: false,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      backgroundColor: AppWidgets.backgroundColor,
                      context: context,
                      builder: (context) {
                        return StatefulBuilder(builder: (BuildContext context,
                            StateSetter setState /*You can rename this!*/) {
                          return Container(
                            height: 110,
                            margin: EdgeInsets.all(10),
                            child: Column(
                              children: [
                                AppWidgets.getWtaButton(
                                    buttonName: "Gallery",
                                    fontSize: 14,
                                    width: double.infinity,
                                    height: 48,
                                    isLoader: false,
                                    onPressed: () {
                                      isImageLoader = true;
                                      Navigator.pop(context);
                                      _getProfileFromGallery(
                                          height: 80, width: 80);
                                    }),
                                SizedBox(
                                  height: 10,
                                ),
                                AppWidgets.getWtaButton(
                                    buttonName: "Camera",
                                    fontSize: 14,
                                    width: double.infinity,
                                    height: 48,
                                    isLoader: false,
                                    onPressed: () {
                                      isImageLoader = true;
                                      Navigator.pop(context);
                                      _getProfileFromCamera(
                                          width: 80, height: 80);
                                    }),
                              ],
                            ),
                          );
                        });
                      });
                },
                child: Container(
                    width: 80,
                    height: 80,
                    child: widget.editChannelResponse == null ||
                        widget.editChannelResponse!["profilePhoto"] == null
                        ? _profileImageFile == null
                        ? DottedBorder(
                      child: Center(
                        child: Icon(
                          Icons.add_outlined,
                          color: Colors.white,
                        ),
                      ),
                      borderType: BorderType.RRect,
                      radius: Radius.circular(12),
                      color: Colors.white,
                      dashPattern: [10, 5, 10, 5, 10, 5],
                    )
                        : Stack(children: [
                      Positioned.fill(
                          child: Container(
                            child: Image.file(
                              _profileImageFile!,
                              fit: BoxFit.fill,
                            ),
                          )),
                      Positioned.fill(
                        bottom: -16,
                        right: -16,
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.circular(100),
                              color: Color(0x9918191f),
                            ),
                            padding: const EdgeInsets.only(
                                left: 8,
                                right: 20,
                                top: 8,
                                bottom: 20),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              children: [
                                Container(
                                  width: 20,
                                  height: 20,
                                  child: isImageLoader
                                      ? Container(
                                      child:
                                      CircularProgressIndicator())
                                      : Image.asset(
                                    "assets/images/camera_icon.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ])
                        : Stack(children: [
                      _profileImageFile != null
                          ? Positioned.fill(
                          child: Container(
                            child: Image.file(
                              _profileImageFile!,
                              fit: BoxFit.fill,
                            ),
                          ))
                          : Positioned.fill(
                          child: Container(
                              width: 80,
                              height: 80,
                              child: CachedNetworkImage(
                                imageUrl: widget.editChannelResponse![
                                "profilePhoto"]["url"],
                                fit: BoxFit.fill,
                                placeholder: (context, url) =>
                                    Image.asset(
                                      "assets/images/channel_cricbuzz_icon.png",
                                      fit: BoxFit.fill,
                                    ),

                                // placeholder: (context, url) => CircularProgressIndicator(),
                                errorWidget: (context, url, error) {
                                  print(
                                      'errorWidget ${error.toString()} \n$url');
                                  return Image.asset(
                                    "assets/images/channel_cricbuzz_icon.png",
                                    fit: BoxFit.fill,
                                  );
                                },
                              ))),
                      Positioned.fill(
                        bottom: -16,
                        right: -16,
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: Color(0x9918191f),
                            ),
                            padding: const EdgeInsets.only(
                                left: 8, right: 20, top: 8, bottom: 20),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              children: [
                                Container(
                                  width: 20,
                                  height: 20,
                                  child: isImageLoader
                                      ? Container(
                                      child:
                                      CircularProgressIndicator())
                                      : Image.asset(
                                    "assets/images/camera_icon.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ])),
              )
            ],
          ),
        ),
      )
    ]);
  }

  Widget _getCategoryWidget(length, value, setModalState) {
    return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        "Select Category",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    InkWell(
                      focusColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        interestTypesSelectedName = [];
                        interestTypesSelectedId = [];
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pop();
                      },
                      child: Container(
                        width: 24,
                        height: 24,
                        child: Image.asset(
                          "assets/images/cancel_icon.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 16),
              ListView.builder(
                itemCount: length,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index1) {
                  return Container(
                      child: Column(
                        children: [
                          InkWell(
                            focusColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            onTap: () {
                              if (interestTypesSelectedId
                                  .contains(value[index1]["id"])) {
                                setModalState(() {
                                  interestTypesSelectedName
                                      .remove(value[index1]["name"]);
                                  interestTypesSelectedId.remove(value[index1]["id"]);
                                });
                              } else {
                                setModalState(() {
                                  interestTypesSelectedName.add(value[index1]["name"]);
                                  interestTypesSelectedId.add(value[index1]["id"]);
                                });
                              }
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                interestTypesSelectedName
                                    .contains(value[index1]['name'])
                                    ? Container(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                    "assets/images/single_square_select.png",
                                    fit: BoxFit.fill,
                                  ),
                                )
                                    : Container(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                    "assets/images/single_square_deselect.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  child: Text(
                                    value[index1]["name"],
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 12)
                        ],
                      ));
                },
              ),
              SizedBox(height: 16),
              AppWidgets.getWtaButton(
                  buttonName: "Done",
                  isLoader: false,
                  fontSize: 16,
                  width: double.infinity,
                  height: 48,
                  onPressed: () {
                    setState(() {
                      AppInjector.resolve<AppRoutes>()
                          .navigatorKey
                          .currentState!
                          .pop();
                    });
                  })
            ],
          ),
        ));
  }

  Future<dynamic> createChannelApi(
      channelName, channelBio, profilePhoto, coverPhoto, categories) async {
    setState(() {
      isButtonLoader = true;
    });

    FormData formData = new FormData.fromMap({
      "data": convert.jsonEncode({
        "name": channelName,
        "bio": channelBio,
        "categories": List<dynamic>.from(categories.map((x) => x))
      }),
      'files.profilePhoto': profilePhoto == null
          ? null
          : await MultipartFile.fromFile(
        profilePhoto.path,
        filename: profilePhoto.path.split('/').last,
      ),
      "files.coverPhoto": coverPhoto == null
          ? null
          : await MultipartFile.fromFile(
        coverPhoto.path,
        filename: coverPhoto.path.split('/').last,
      )
    });

    var response = await ApiService().createChannel(formData);

    if (response.status == ApiResponseStatus.completed) {
      var jsonResponse =
      convert.jsonDecode(response.data) as Map<String, dynamic>;
      return [true, jsonResponse];
    } else if (response.status == ApiResponseStatus.error) {
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }

  Future<dynamic> updateChannelApi(
      channelName, channelBio, profilePhoto, coverPhoto, categories) async {
    setState(() {
      isButtonLoader = true;
    });

    FormData formData = new FormData.fromMap({
      "data": convert.jsonEncode({
        "name": channelName,
        "bio": channelBio,
        "categories": List<dynamic>.from(categories.map((x) => x))
      }),
      'files.profilePhoto': profilePhoto == null
          ? null
          : await MultipartFile.fromFile(
        profilePhoto.path,
        filename: profilePhoto.path.split('/').last,
      ),
      "files.coverPhoto": coverPhoto == null
          ? null
          : await MultipartFile.fromFile(
        coverPhoto.path,
        filename: coverPhoto.path.split('/').last,
      )
    });

    var response = await ApiService()
        .updateChannel(formData, widget.editChannelResponse['id']);

    if (response.status == ApiResponseStatus.completed) {
      var jsonResponse = convert.jsonDecode(response.data);

      return [true, jsonResponse['id']];
    } else if (response.status == ApiResponseStatus.error) {
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }

  /// Get from gallery
  Future<void> _getProfileFromGallery(
      {required double width, required double height}) async {
    var pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 640,
      maxHeight: 480,
    );
    if (pickedFile != null) {
      setState(() {
        _profileImageFile = File(pickedFile.path);
        isImageLoader = false;
      });
    }
  }

  /// Get from Camera
  Future<void> _getProfileFromCamera(
      {required double width, required double height}) async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 640,
      maxHeight: 480,
    );
    if (pickedFile != null) {
      setState(() {
        _profileImageFile = File(pickedFile.path);
        isImageLoader = false;
      });
    }
  }

  /// Get from gallery
  Future<void> _getCoverFromGallery(
      {required double width, required double height}) async {
    var pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 640,
      maxHeight: 480,
    );
    if (pickedFile != null) {
      setState(() {
        _coverImageFile = File(pickedFile.path);
        isImageLoader = false;
      });
    }
  }

  /// Get from Camera
  Future<void> _getCoverFromCamera(
      {required double width, required double height}) async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 640,
      maxHeight: 480,
    );
    if (pickedFile != null) {
      setState(() {
        _coverImageFile = File(pickedFile.path);
        isImageLoader = false;
      });
    }
  }

  Iterable<Widget> get getInterestTypes sync* {
    for (int i = 0; i < interestTypesSelectedName.length; i++) {
      yield Row(
        children: [
          FilterChip(
            label: Row(
              children: [
                Text(
                  interestTypesSelectedName[i],
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Container(
                  width: 16,
                  height: 16,
                  child: Image.asset(
                    "assets/images/cancel_icon.png",
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
            // selected: interestTypesSelected[i],
            disabledColor: Color(0xff3a3a3c),
            backgroundColor: Color(0xff3a3a3c),
            selectedShadowColor: Color(0xff3a3a3c),
            shadowColor: Color(0xff3a3a3c),
            checkmarkColor: Color(0xff3a3a3c),
            shape: StadiumBorder(
                side: BorderSide(color: Color(0xff3a3a3c), width: 1)),
            showCheckmark: false,
            onSelected: (bool value) {
              setState(() {
                interestTypesSelectedName.removeAt(i);
                interestTypesSelectedId.removeAt(i);
              });
            },
          ),
          SizedBox(
            width: 5,
          ),
        ],
      );
    }
  }
}
