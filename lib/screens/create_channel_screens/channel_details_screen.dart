import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/screens/channel_settings_screens/channel_setting_screen.dart';
import 'package:pre_seasoned/screens/create_channel_screens/invite_member_screen.dart';
import 'package:pre_seasoned/screens/create_thread_screens/create_thread_screen.dart';
import 'package:pre_seasoned/screens/create_thread_screens/create_thread_video_screen.dart';
import 'package:pre_seasoned/screens/main_container/main_container_screen.dart';
import 'package:pre_seasoned/screens/thread_screens/general_thread_screen.dart';
import 'package:pre_seasoned/screens/thread_screens/non_general_thread_screen.dart';
import 'package:pre_seasoned/screens/thread_screens/non_general_thread_user_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';

class ChannelDetailsScreen extends StatefulWidget {
  final int createChannelId;
  final bool isFromCreateChannel;
  final String? profilePhoto;
  final String? coverPhoto;
  final bool isChannelManaged;
  final bool isFromExplore;
  const ChannelDetailsScreen({
    Key? key,
    required this.createChannelId,
    required this.isFromCreateChannel,
    required this.profilePhoto,
    required this.coverPhoto,
    required this.isChannelManaged,
    required this.isFromExplore,
  }) : super(key: key);
  static const String routeName = "/ChannelCreateMainScreen";

  @override
  _ChannelDetailsScreenState createState() => _ChannelDetailsScreenState();
}

class _ChannelDetailsScreenState extends State<ChannelDetailsScreen>
    with WidgetsBindingObserver {
  Map<String, dynamic>? createChannelDetailResponse;
  bool isGeneralCancelPressed = false;
  bool isThreadCancelPressed = false;
  List? topicsList;
  bool detailsLoader = false;
  bool topicLoader = false;
  bool dialogLoader = false;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  List _item = [
    'Hate speech against the protected group',
    'Content is irrelavant',
    'Harassment and abuse ',
    'Spam/Misleading ',
    'Others'
  ];
  var _selectedIndex = -1;
  bool showTextField = false;
  bool _submitted = false;

  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    detailsLoader = true;
    getChannelDetailsApi(widget.createChannelId);
  }

  void getChannelDetailsApi(createChannelId) async {
    detailsLoader = true;
    AppInjector.resolve<ApiService>()
        .channelDetails(createChannelId)
        .then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          detailsLoader = false;
          createChannelDetailResponse = convert.jsonDecode(value.data);
          topicLoader = true;
        });
        getTopicsDetailsApi(widget.createChannelId);
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          detailsLoader = false;
          BotToast.showText(text: value.message[1]);
        });
      } else {
        setState(() {
          detailsLoader = false;
        });
        return [false, "Something went wrong.Please try later"];
      }
    });
  }

  void getTopicsDetailsApi(createChannelId) async {
    topicLoader = true;
    AppInjector.resolve<ApiService>()
        .topicDetails(createChannelId)
        .then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          topicLoader = false;
          topicsList = convert.jsonDecode(value.data);
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          topicLoader = false;
          BotToast.showText(text: value.message[1]);
        });
      } else {
        setState(() {
          topicLoader = false;
        });
        return [false, "Something went wrong.Please try later"];
      }
    });
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 2));
    refreshKey.currentState?.show();
    setState(() {
      getChannelDetailsApi(widget.createChannelId);
      // getTopicsDetailsApi(widget.createChannelId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
      ),
      backgroundColor: AppWidgets.titleColor5,
      body: _getBody(),
    );
  }

  Widget _getBody() {
    return RefreshIndicator(
      displacement: 50,
      strokeWidth: 2,
      backgroundColor: Colors.white,
      color: AppWidgets.buttonTextColor,
      onRefresh: _refresh,
      child: SingleChildScrollView(
          child: Stack(
              children: [
          Column(
          children: [
          Stack(
          children: [
          Container(
          child: Row(
          children: [
              Container(
              width: MediaQuery.of(context).size.width,
          height: 120,
          child: createChannelDetailResponse == null
              ? Image.asset(
            "assets/images/channel_cover.png",
            fit: BoxFit.fill,
          )
              : createChannelDetailResponse![
          "coverPhoto"] ==
              null
              ? Image.asset(
            "assets/images/channel_cover.png",
            fit: BoxFit.fill,
          )
              : CachedNetworkImage(
            imageUrl:
            createChannelDetailResponse![
            "coverPhoto"]["url"],
            fit: BoxFit.fill,
            placeholder: (context, url) =>
                Image.asset(
                  "assets/images/channel_cover.png",
                  fit: BoxFit.fill,
                ),

            // placeholder: (context, url) =>
            //     CircularProgressIndicator(),
            errorWidget: (context, url, error) {
              print(
                  'errorWidget ${error.toString()} \n$url');
              return Image.asset(
                "assets/images/channel_cover.png",
                fit: BoxFit.fill,
              );
            },
          )),
      ],
    ),
    ),
    Positioned(
    top: 16,
    left: 20,
    child: InkWell(
    focusColor: Colors.transparent,
    highlightColor: Colors.transparent,
    hoverColor: Colors.transparent,
    splashColor: Colors.transparent,
    onTap: () {
    widget.isFromCreateChannel
    ? AppInjector.resolve<AppRoutes>()
        .navigatorKey
        .currentState!
        .popAndPushNamed(
    MainContainerScreen.routeName)
        : AppInjector.resolve<AppRoutes>()
        .navigatorKey
        .currentState!
        .pop();
    },
    child: Container(
    decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(100),
    color: Color(0x9918191f),
    ),
    padding: const EdgeInsets.all(7),
    child: Row(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
    Container(
    width: 18,
    height: 18,
    child: Image.asset(
    "assets/images/arrow_left_icon.png",
    fit: BoxFit.fill,
    ),
    ),
    ],
    ),
    )),
    ),
    Positioned(
    top: 16,
    right: 20,
    child: widget.isChannelManaged
    ? InkWell(
    focusColor: Colors.transparent,
    highlightColor: Colors.transparent,
    hoverColor: Colors.transparent,
    splashColor: Colors.transparent,
    onTap: () {
    AppInjector.resolve<AppRoutes>()
        .navigatorKey
        .currentState!
        .pushNamed(
    ChannelSettingsScreen.routeName,
    arguments: [
    createChannelDetailResponse!['id'],
    ]).then((_) {
    setState(() {
    if (widget.isChannelManaged) {
    getChannelDetailsApi(
    widget.createChannelId);
    }
    });
    });
    },
    child: Container(
    decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(100),
    color: Color(0x9918191f),
    ),
    padding: const EdgeInsets.all(7),
    child: Row(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment:
    MainAxisAlignment.start,
    crossAxisAlignment:
    CrossAxisAlignment.center,
    children: [
    Container(
    width: 18,
    height: 18,
    child: Image.asset(
    "assets/images/settings_icon.png",
    fit: BoxFit.fill,
    ),
    ),
    ],
    ),
    ),
    )
        : PopupMenuButton<int>(
    child: Container(
    decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(6),
    color: Color(0x9918191f),
    ),
    padding: const EdgeInsets.all(8),
    child: Row(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment:
    MainAxisAlignment.start,
    crossAxisAlignment:
    CrossAxisAlignment.center,
    children: [
    Container(
    width: 18,
    height: 18,
    child: Image.asset(
    "assets/images/more_vertical.png",
    fit: BoxFit.fill,
    ),
    ),
    ],
    ),
    ),
    shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(12)),
    color: Colors.black,
    itemBuilder: (context) => widget.isFromExplore
    ? [
    PopupMenuItem<int>(
    value: 0,
    child: Text("Report Channel",
    style: TextStyle(
    fontFamily:
    'Proxima Nova',
    fontSize: 14,
    color: Colors.white,
    fontWeight:
    FontWeight.w600))),
    ]
        : [
    PopupMenuItem<int>(
    value: 0,
    child: Text("Report Channel",
    style: TextStyle(
    fontFamily:
    'Proxima Nova',
    fontSize: 14,
    color: Colors.white,
    fontWeight:
    FontWeight.w600))),
    PopupMenuItem<int>(
    value: 1,
    child: Text("Leave Channel",
    style: TextStyle(
    fontFamily:
    'Proxima Nova',
    fontSize: 14,
    color: Colors.red,
    fontWeight:
    FontWeight.w600))),
    ],
    onSelected: (item) =>
    selectedItem(context, item),
    )),
    ],
    ),
    Padding(
    padding: EdgeInsets.only(top: 40),
    child: Container(
    margin: EdgeInsets.only(
    left: 20, right: 20, bottom: 40, top: 20),
    child: Column(
    children: [
    Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
    Container(
    // margin: EdgeInsets.only(left: 20, right: 20),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
    SizedBox(
    child: Text(
    createChannelDetailResponse == null
    ? ""
        : capitalize(
    createChannelDetailResponse![
    "name"]),
    style: TextStyle(
    color: Colors.white,
    fontSize: 24,
    fontFamily: "Proxima Nova",
    fontWeight: FontWeight.w600,
    ),
    ),
    ),
    SizedBox(height: 4),
    SizedBox(
    child: Text(
    createChannelDetailResponse == null
    ? ""
        : "${createChannelDetailResponse!["membersCount"]} members",
    style: TextStyle(
    fontSize: 12,
    fontFamily: "Proxima Nova",
    fontWeight: FontWeight.w600,
    color: AppWidgets.titleColor3,
    ),
    ),
    ),
    ],
    ),
    ),
    SizedBox(height: 8),
    SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child: Row(
    children: getCategoryList.toList(),
    ),
    ),
    SizedBox(height: 8),
    SizedBox(
    child: Text(
    createChannelDetailResponse == null
    ? ""
        : createChannelDetailResponse!['bio'] ==
    null
    ? ""
        : createChannelDetailResponse!['bio'],
    style: TextStyle(
    color: Color(0xfff2f2f7),
    fontSize: 14,
    ),
    ),
    ),
    widget.isChannelManaged
    ? Column(children: [
    SizedBox(height: 16),
    Container(
    width:
    MediaQuery.of(context).size.width,
    child: Row(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment:
    MainAxisAlignment.start,
    crossAxisAlignment:
    CrossAxisAlignment.start,
    children: [
    Expanded(
    child: InkWell(
    focusColor: Colors.transparent,
    highlightColor:
    Colors.transparent,
    hoverColor: Colors.transparent,
    splashColor: Colors.transparent,
    onTap: () {
    AppInjector.resolve<
    AppRoutes>()
        .navigatorKey
        .currentState!
        .pushNamed(
    CreateThreadVideoScreen
        .routeName,
    arguments: [
    widget.createChannelId
    ]).then((value) {
    setState(() {
    getChannelDetailsApi(
    widget
        .createChannelId);
    //
    // getTopicsDetailsApi(widget
    //     .createChannelId);
    });
    });
    },
    child: Container(
    decoration: BoxDecoration(
    borderRadius:
    BorderRadius.circular(
    6),
    color: Color(0xff0080ff),
    ),
    padding: const EdgeInsets
        .symmetric(
    horizontal: 20,
    vertical: 8,
    ),
    child: Row(
    mainAxisSize:
    MainAxisSize.max,
    mainAxisAlignment:
    MainAxisAlignment
        .center,
    crossAxisAlignment:
    CrossAxisAlignment
        .center,
    children: [
    Text(
    "Create Thread",
    textAlign:
    TextAlign.center,
    style: TextStyle(
    color: Colors.white,
    fontSize: 14,
    fontFamily:
    "Proxima Nova",
    fontWeight:
    FontWeight.w600,
    ),
    ),
    ],
    ),
    ),
    ),
    ),
    SizedBox(width: 16),
    Expanded(
    child: InkWell(
    focusColor: Colors.transparent,
    highlightColor:
    Colors.transparent,
    hoverColor: Colors.transparent,
    splashColor: Colors.transparent,
    onTap: () {
    AppInjector.resolve<AppRoutes>()
        .navigatorKey
        .currentState!
        .pushNamed(
    InviteMemberScreen
        .routeName,
    arguments: [
    createChannelDetailResponse![
    'code']
    ]);
    },
    child: Container(
    decoration: BoxDecoration(
    borderRadius:
    BorderRadius.circular(6),
    border: Border.all(
    color: Color(0xff5cabff),
    width: 1,
    ),
    ),
    padding:
    const EdgeInsets.symmetric(
    horizontal: 20,
    vertical: 8,
    ),
    child: Row(
    mainAxisSize:
    MainAxisSize.max,
    mainAxisAlignment:
    MainAxisAlignment.center,
    crossAxisAlignment:
    CrossAxisAlignment.center,
    children: [
    Text(
    "Invite Member",
    textAlign:
    TextAlign.center,
    style: TextStyle(
    color:
    Color(0xff5cabff),
    fontSize: 14,
    fontFamily:
    "Proxima Nova",
    fontWeight:
    FontWeight.w600,
    ),
    ),
    ],
    ),
    ),
    )),
    ],
    ),
    ),
    SizedBox(
    height: 4,
    ),
    isGeneralCancelPressed &&
    !isThreadCancelPressed
    ? AppWidgets.getInfoCard(
    "If you want to have specific conversation on a short lived/current events, you can create a thread.",
    "It could be current news, specific events etc.",
    () {
    setState(() {
    AppInjector.resolve<
    AppPreferences>()
        .isLogged("LoggedInState");
    isThreadCancelPressed = true;
    });
    }, context)
        : Container()
    ])
        : Container()
    ],
    ),
    SizedBox(height: 32),
    Container(
    child: Column(
    children: [
    Row(
    children: [
    Expanded(
    child: Text(
    createChannelDetailResponse == null
    ? "Community"
        : createChannelDetailResponse![
    "generalThread"] ==
    null
    ? "Community"
        : createChannelDetailResponse![
    "generalThread"]["topic"]
    ["name"],
    textAlign: TextAlign.left,
    style: TextStyle(
    color: Color(0xfff2f2f7),
    fontSize: 14,
    fontFamily: "Proxima Nova",
    fontWeight: FontWeight.w700,
    letterSpacing: 0.70,
    ),
    )),
    ],
    ),
    SizedBox(height: 8),
    Container(
    color: AppWidgets.titleColor4,
    width: double.infinity,
    height: 1,
    ),
    SizedBox(height: 8),
    InkWell(
    focusColor: Colors.transparent,
    highlightColor: Colors.transparent,
    hoverColor: Colors.transparent,
    splashColor: Colors.transparent,
    onTap: widget.isFromExplore
    ? () {
    BotToast.showText(
    text:
    "Please Join the channel to see the thread details");
    }
        : () async {
    if (createChannelDetailResponse !=
    null) {
    if (createChannelDetailResponse![
    "generalThread"] !=
    null) {
    if (createChannelDetailResponse![
    "generalThread"]
    ["feed_id"] ==
    null) {
    BotToast.showText(
    text: "Feed_id is null");
    return;
    }
    setState(() {
    dialogLoader = true;
    });
    joinDialog(
    createChannelDetailResponse![
    "generalThread"],
    true,
    );
    } else {
    BotToast.showText(
    text: "Thread is null");
    }
    }
    },
    child: Container(
    child: Row(
    children: [
    Container(
    width: 20,
    height: 20,
    child: Image.asset(
    "assets/images/message_icon.png",
    fit: BoxFit.fill,
    ),
    ),
    SizedBox(width: 8),
    Expanded(
    child: SizedBox(
    child: Text(
    createChannelDetailResponse ==
    null
    ? "General"
        : createChannelDetailResponse![
    "generalThread"] ==
    null
    ? "General"
        : createChannelDetailResponse![
    "generalThread"]
    ["name"],
    style: TextStyle(
    color: Colors.white,
    fontSize: 16,
    ),
    ),
    ),
    ),
    ],
    ),
    )),
    SizedBox(height: 4),
    widget.isFromCreateChannel &&
    !isGeneralCancelPressed
    ? AppWidgets.getInfoCard(
    "Here you can share general updates with all your audience",
    "It could be announcements, news etc.",
    () {
    setState(() {
    isGeneralCancelPressed = true;
    });
    }, context)
        : Container(),
    ],
    ),
    ),
    SizedBox(height: 32),
    Stack(
    children: [
    ListView.builder(
    itemCount: (topicsList == null
    ? []
        : topicsList as List)
        .length,
    physics: NeverScrollableScrollPhysics(),
    shrinkWrap: true,
    itemBuilder:
    (BuildContext context, topicIndex) {
    return topicsList == null ||
    createChannelDetailResponse == null
    ? Container()
        : topicsList![topicIndex]["topic_id"] !=
    (createChannelDetailResponse![
    "generalThread"] ==
    null
    ? topicsList![topicIndex]
    ["topic_id"]
        : createChannelDetailResponse![
    "generalThread"]
    ["topic"]["topic_id"])
    ? Container(
    child: Column(
    mainAxisAlignment:
    MainAxisAlignment.start,
    crossAxisAlignment:
    CrossAxisAlignment.start,
    children: [
    Row(
    mainAxisAlignment:
    MainAxisAlignment.end,
    crossAxisAlignment:
    CrossAxisAlignment
        .end,
    children: [
    Expanded(
    child: Text(
    topicsList![
    topicIndex]
    ['name'],
    textAlign:
    TextAlign.start,
    style: TextStyle(
    color: Color(
    0xfff2f2f7),
    fontSize: 14,
    fontFamily:
    "Proxima Nova",
    fontWeight:
    FontWeight
        .w700,
    letterSpacing:
    0.70,
    ),
    ),
    ),
    widget.isChannelManaged
    ? InkWell(
    focusColor: Colors
        .transparent,
    highlightColor:
    Colors
        .transparent,
    hoverColor: Colors
        .transparent,
    splashColor: Colors
        .transparent,
    onTap: () {
    AppInjector.resolve<
    AppRoutes>()
        .navigatorKey
        .currentState!
        .pushNamed(
    CreateThreadScreen
        .routeName,
    arguments: [
    widget
        .createChannelId,
    topicsList![topicIndex]
    [
    'id'],
    topicsList![topicIndex]
    [
    'name']
    ]).then(
    (value) {
    setState(
    () {
    getChannelDetailsApi(
    widget
        .createChannelId);
    // getTopicsDetailsApi(
    //     widget
    //         .createChannelId);
    });
    });
    },
    child:
    Container(
    child: Row(
    mainAxisAlignment:
    MainAxisAlignment.end,
    crossAxisAlignment: CrossAxisAlignment.end,
    children: [
    Container(
    width:
    18,
    height:
    18,
    child: Image
        .asset(
    "assets/images/plus_blue_icon.png",
    fit: BoxFit
        .fill,
    ),
    ),
    SizedBox(
    width:
    4),
    Text(
    "Add Threads",
    textAlign:
    TextAlign.start,
    style:
    TextStyle(
    color:
    AppWidgets.buttonTextColor,
    fontSize:
    14,
    fontFamily:
    "Proxima Nova",
    fontWeight:
    FontWeight.w700,
    ),
    ),
    ])),
    )
        : Container()
    ],
    ),
    SizedBox(height: 8),
    Container(
    color: AppWidgets
        .titleColor4,
    width: double.infinity,
    height: 1,
    ),
    SizedBox(height: 8),
    ListView.builder(
    itemCount: topicsList![
    topicIndex]
    [
    'threads'] ==
    null
    ? 0
        : (topicsList![topicIndex]
    [
    'threads']
    as List)
        .length,
    physics:
    NeverScrollableScrollPhysics(),
    shrinkWrap: true,
    itemBuilder:
    (BuildContext
    context,
    threadIndex) {
    return Column(
    children: [
    InkWell(
    focusColor: Colors
        .transparent,
    highlightColor:
    Colors
        .transparent,
    hoverColor: Colors
        .transparent,
    splashColor:
    Colors
        .transparent,
    onTap: widget
        .isFromExplore
    ? () {
    BotToast.showText(
    text: "Please Join the channel to see the thread details");
    }
        : () {
    if (topicsList![topicIndex]['threads'] !=
    null) {
    if (topicsList![topicIndex]['threads']![threadIndex]["feed_id"] ==
    null) {
    BotToast.showText(text: "Feed_id is null");
    return;
    }
    setState(() {
    dialogLoader = true;
    });
    joinDialog(
    topicsList![topicIndex]['threads'][threadIndex],
    false,
    );
    }
    },
    child:
    Container(
    child: Row(
    children: [
    Container(
    width:
    20,
    height:
    20,
    child:
    Image.asset(
    "assets/images/message_icon.png",
    fit:
    BoxFit.fill,
    ),
    ),
    SizedBox(
    width:
    8),
    Expanded(
    child:
    SizedBox(
    child:
    Text(
    capitalize(topicsList![topicIndex]['threads'][threadIndex]['name']),
    style: TextStyle(
    color: Colors.white,
    fontSize: 16,
    ),
    ),
    ),
    ),
    ],
    ),
    )),
    SizedBox(
    height: 16,
    )
    ],
    );
    }),
    SizedBox(height: 32),
    ],
    ),
    )
        : Container();
    }),
    topicLoader
    ? Align(
    alignment: Alignment.center,
    child: Container(
    alignment: Alignment.center,
    height: 30,
    width: 30,
    child:
    CircularProgressIndicator.adaptive(
    strokeWidth: 2,
    backgroundColor:
    AppWidgets.buttonTextColor,
    valueColor:
    AlwaysStoppedAnimation<
    Color>(Colors.white)),
    ))
        : Container()
    ],
    )
    ],
    ),
    ),
    )
    ],
    ),
    Positioned(
    left: 20,
    top: 80,
    child: Container(
    width: 82,
    height: 82,
    decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(8),
    border: Border.all(
    color: AppWidgets.backgroundColor,
    width: 1,
    ),
    ),
    child: Row(
    mainAxisAlignment: MainAxisAlignment.end,
    crossAxisAlignment: CrossAxisAlignment.end,
    children: [
    createChannelDetailResponse == null
    ? Container(
    width: 80,
    height: 80,
    child: Image.asset(
    "assets/images/suggested_channel_cover.png",
    fit: BoxFit.fill,
    ),
    )
        : createChannelDetailResponse!["profilePhoto"] != null
    ? Container(
    width: 80,
    height: 80,
    child: CachedNetworkImage(
    imageUrl: createChannelDetailResponse![
    "profilePhoto"]['url'],
    fit: BoxFit.fill,
    placeholder: (context, url) => Image.asset(
    "assets/images/suggested_channel_cover.png",
    fit: BoxFit.fill,
    ),
    //
    // placeholder: (context, url) =>
    //     CircularProgressIndicator(),
    errorWidget: (context, url, error) {
    print(
    'errorWidget ${error.toString()} \n$url');
    return Image.asset(
    "assets/images/suggested_channel_cover.png",
    fit: BoxFit.fill,
    );
    },
    ))
        : Container(
    width: 80,
    height: 80,
    child: Image.asset(
    "assets/images/suggested_channel_cover.png",
    fit: BoxFit.fill,
    ),
    ),
    ],
    ),
    ),
    ),
    detailsLoader || dialogLoader
    ? Positioned.fill(
    child: Align(
    alignment: Alignment.center,
    child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
    Container(
    alignment: Alignment.center,
    child: SizedBox(
    height: 50,
    width: 50,
    child: CircularProgressIndicator.adaptive(
    strokeWidth: 2,
    backgroundColor:
    AppWidgets.buttonTextColor,
    valueColor:
    AlwaysStoppedAnimation<Color>(
    Colors.white))),
    )
    ],
    )),
    )
        : Container()
    ],
    ),
    ));
  }

  void selectedItem(BuildContext context, item) {
    switch (item) {
      case 0:
        setState(() {
          _selectedIndex = -1;
        });
        showModalBottomSheet(
          isDismissible: true,
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          builder: ((builder) {
            return reportChannel();
          }),
        );
        break;
      case 1:
        showDialog(
          context: context,
          builder: (context1) {
            return AppWidgets.getAlertDialog(
                title: "Alert",
                content: "Are you sure you want to leave Channel?",
                confirmButton: "Leave",
                cancelButton: "Cancel",
                onCancelTap: () {
                  Navigator.of(context1).pop();
                },
                onConfirmTap: () {
                  Navigator.of(context1).pop();
                  setState(() {
                    detailsLoader = true;
                  });
                  joinChannelApi(
                      AppInjector.resolve<AppPreferences>().getUserId()!,
                      widget.createChannelId,
                      "unfollow")
                      .then((value) {
                    if (value[0]) {
                      setState(() {
                        detailsLoader = false;
                      });
                      Navigator.of(context).pop();
                    } else {
                      BotToast.showText(text: "Unable to leave channel");
                    }
                  });
                });
          },
        );

        break;
    }
  }

  Future<List> joinChannelApi(int userId, int channelId, type) async {
    var response = await AppInjector.resolve<ApiService>().joinChannel({
      "channel_id": channelId,
      "user_id": userId,
    }, type);

    if (response.status == ApiResponseStatus.completed) {
      return [true, ""];
    } else if (response.status == ApiResponseStatus.error) {
      print("response data init ${response.message[1]}");
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }

  Iterable<Widget> get getCategoryList sync* {
    if (createChannelDetailResponse != null) {
      for (int i = 0;
      i < createChannelDetailResponse!['categories'].length;
      i++) {
        yield Row(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: Color(0xff3a3a3c),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Color(0xff3a3a3c),
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 8,
                      vertical: 6,
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          createChannelDetailResponse!['categories'][i]['name'],
                          style: TextStyle(
                            color: Color(0xffa4a4ab),
                            fontSize: 12,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 5,
            ),
          ],
        );
      }
    }
  }

  Future<void> joinDialog(
      threadDetails,
      isGeneralThread,
      ) async {
    AppInjector.resolve<QuickbloxApiService>()
        .joinDialog(threadDetails['feed_id'])
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(value.data);
        setState(() {
          dialogLoader = false;
        });

        checkJoinDialog(threadDetails['feed_id']);
        isGeneralThread
            ? AppInjector.resolve<AppRoutes>()
            .navigatorKey
            .currentState!
            .pushNamed(GeneralThreadScreen.routeName,
            arguments: [threadDetails, widget.isChannelManaged])
            : widget.isChannelManaged
            ? AppInjector.resolve<AppRoutes>()
            .navigatorKey
            .currentState!
            .pushNamed(NonGeneralThreadScreen.routeName, arguments: [
          threadDetails,
          createChannelDetailResponse!["generalThread"]["feed_id"],
          false
        ])
            : AppInjector.resolve<AppRoutes>()
            .navigatorKey
            .currentState!
            .pushNamed(NonGeneralThreadUserScreen.routeName,
            arguments: [threadDetails]);
      } else {
        setState(() {
          dialogLoader = false;
        });
        BotToast.showText(text: "Couldn't join the thread,Try again later");
      }
    });
  }

  Widget reportChannel() {
    return Padding(
        padding:
        EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: StatefulBuilder(
            builder: (BuildContext context, StateSetter setModalState) {
              return SingleChildScrollView(
                child: _submitted
                    ? Container(
                  padding: EdgeInsets.all(20),
                  color: AppWidgets.backgroundColor,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 42,
                        width: 42,
                        child: Image.asset(
                            'assets/images/confirmation_sent_icon.png'),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        'Channel Reported',
                        style: TextStyle(
                          fontFamily: 'Proxima nova',
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      AppWidgets.getWtaButton(
                          buttonName: 'Ok, Got it',
                          fontSize: 16,
                          width: double.infinity,
                          height: 36,
                          isLoader: false,
                          onPressed: () {
                            Navigator.of(context).pop();
                          })
                    ],
                  ),
                )
                    : Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: AppWidgets.backgroundColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Report Channel',
                            style: TextStyle(
                                fontFamily: 'Proxima nova',
                                fontSize: 20,
                                color: Colors.white,
                                fontWeight: FontWeight.w600),
                          ),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                _selectedIndex = -1;
                                showTextField = false;
                              });
                              Navigator.of(context).pop();
                            },
                            icon: Icon(Icons.close, color: Colors.white),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: _item.length,
                        itemBuilder: (BuildContext ctx, i) {
                          var item = _item[i];
                          return Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  if (_selectedIndex == i) {
                                    _selectedIndex = -1;
                                  } else
                                    _selectedIndex = i;
                                  if (_selectedIndex == 4) {
                                    showTextField = true;
                                  } else
                                    showTextField = false;
                                });
                                setModalState(() {});
                              },
                              child: Row(
                                children: [
                                  SizedBox(
                                    height: 20,
                                    width: 20,
                                    child: Image.asset(_selectedIndex == i
                                        ? 'assets/images/single_circle_select.png'
                                        : 'assets/images/single_circle_deselect.png'),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    child: Text(
                                      item,
                                      style: TextStyle(
                                          fontFamily: 'Proxima Nova ',
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.white),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                      showTextField
                          ? Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          AppWidgets.getEditText(
                              controller: _controller,
                              onChanged: (_) {},
                              keyboardType: TextInputType.name,
                              maxLength: 100,
                              hint: 'Write your issue',
                              inputFormatter: [],
                              isObscureText: false),
                          SizedBox(
                            height: 15,
                          ),
                        ],
                      )
                          : Container(),
                      AppWidgets.getWtaButton(
                          buttonName: 'Submit',
                          fontSize: 16,
                          width: double.infinity,
                          height: 48,
                          isLoader: false,
                          onPressed: () {
                            setState(() {
                              if (showTextField && _controller.text.isEmpty) {
                                BotToast.showText(
                                    text: 'Please select an issue');
                              } else
                                _submitted = true;
                            });
                            setModalState(() {});
                          })
                    ],
                  ),
                ),
              );
            }));
  }
}

Future<void> checkJoinDialog(feedId) async {
  QBFilter filter = QBFilter();
  filter.field = "_id";
  filter.operator = QBChatDialogFilterOperators.ALL;
  filter.value = feedId;

  QBSort sort = QBSort();
  sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
  sort.ascending = false;

  int limit = 0;
  int skip = 0;

  await AppInjector.resolve<QuickbloxApiService>()
      .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit)
      .then((value) {
    if (value.status == QuickbloxApiResponseStatus.completed) {
      print(
          "Before send msg dialog unread ${(value.data[0] as QBDialog).unreadMessagesCount}");
    } else {
      BotToast.showText(text: "Before send msg dialog status,Try again later");
    }
  });
}
