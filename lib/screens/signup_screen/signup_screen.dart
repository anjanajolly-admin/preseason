import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_cubit.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_state.dart';
import 'package:pre_seasoned/screens/login_screen/login_screen.dart';
import 'package:pre_seasoned/screens/username_screen/username_details_screen.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);
  static const String routeName = "/SignUpScreen";

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final TextEditingController _createEmailId = TextEditingController();
  final TextEditingController _createPassword = TextEditingController();
  var isLoader = false;
  bool isObscureText = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        title: Text("Signup"),
        elevation: 0,
        brightness: Brightness.dark,
        backgroundColor: AppWidgets.backgroundColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () =>
              AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pop(),
        ),
      ),
      body: InkWell(
          focusColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () {
            AppConstants.hideKeyboard(context);
          },
          child: BlocListener<BaseCubit, BaseState>(
            bloc: BlocProvider.of<BaseCubit>(context),
            listener: (context, BaseState state) {
              print("BlocListener=> Login Screen");
              if (state is EmailAvailable) {
                setState(() {
                  isLoader = false;
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pushNamed(UsernameDetailsScreen.routeName, arguments: [
                    _createEmailId.text,
                    _createPassword.text
                  ]);
                });
              } else if (state is LoadingState) {
                setState(() {
                  isLoader = true;
                });
                return;
              } else if (state is EmailUnAvailable) {
                setState(() {
                  isLoader = false;
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AppWidgets.getAlertDialog(
                        title: "Alert",
                        content: "The given email is available",
                        confirmButton: "Ok",
                        cancelButton: "Cancel",
                        onCancelTap: () {
                          Navigator.of(context).pop();
                        },
                        onConfirmTap: () {
                          Navigator.of(context).pop();
                        },
                      );
                    },
                  );
                });
              } else {
                setState(() {
                  isLoader = false;
                  BotToast.showText(
                      text: "Something went wrong.Please try later");
                });
              }
            },
            child: _getBody(),
          )),
    );
  }

  Widget _getBody() {
    return SingleChildScrollView(
        child: Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: [
          SizedBox(height: 24),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                Container(
                  width: 80,
                  height: 80,
                  child: Image.asset(
                    "assets/images/app_icon.png",
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(height: 16),
                SizedBox(
                  width: double.infinity,
                  child: Text(
                    "Easy way to talk every day and hang out more often.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xfff2f2f7),
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 32),
          Container(
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "E-mail",
                        style: TextStyle(
                          color: Color(0xfff2f2f7),
                          fontSize: 16,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: 4),
                      AppWidgets.getEditText(
                          isObscureText: false,
                          controller: _createEmailId,
                          onChanged: (value) {
                            // _createEmailId.text = value;
                          },
                          keyboardType: TextInputType.emailAddress,
                          maxLength: 100,
                          hint: " Enter your mail id ",
                          inputFormatter: [
                            FilteringTextInputFormatter.deny(RegExp(" "))
                          ])
                    ],
                  ),
                ),
                SizedBox(height: 24),
                Container(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Create Password",
                        style: TextStyle(
                          color: Color(0xfff2f2f7),
                          fontSize: 16,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: 4),
                      AppWidgets.getEditPasswordText(
                        isObscureText: !isObscureText,
                        controller: _createPassword,
                        onChanged: (value) {
                          // _password.text = value;
                        },
                        hint: " Password ",
                        inputFormatter: [],
                        suffixIcon: IconButton(
                            color: AppWidgets.titleColor4,
                            icon: Icon(isObscureText
                                ? Icons.visibility_off
                                : Icons.visibility),
                            onPressed: () {
                              setState(() {
                                isObscureText = !isObscureText;
                              });
                            }),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 24),
                AppWidgets.getWtaButton(
                    isLoader: isLoader,
                    buttonName: "Continue",
                    fontSize: 16,
                    width: double.infinity,
                    height: 48,
                    onPressed: () {
                      isValidEmail(_createEmailId)
                          ? _createPassword.text.length < 8
                              ? showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AppWidgets.getAlertDialog(
                                      title: "Alert",
                                      content:
                                          "Password must be 8 characters at least",
                                      confirmButton: "Ok",
                                      cancelButton: "Cancel",
                                      onCancelTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      onConfirmTap: () {
                                        Navigator.of(context).pop();
                                      },
                                    );
                                  },
                                )
                              : context
                                  .read<BaseCubit>()
                                  .availableEmail(_createEmailId.text)
                          : showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AppWidgets.getAlertDialog(
                                  title: "Alert",
                                  content: "Please enter valid email Id",
                                  confirmButton: "Ok",
                                  cancelButton: "Cancel",
                                  onCancelTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  onConfirmTap: () {
                                    Navigator.of(context).pop();
                                  },
                                );
                              },
                            );

                      print("SignUp");
                    })
              ],
            ),
          ),
          SizedBox(height: 16),
          Row(
            children: [
              Text(
                "Already have an account ?",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: "Proxima Nova",
                  fontWeight: FontWeight.w600,
                ),
              ),
              AppWidgets.getTextButton("Login", AppWidgets.buttonTextColor, 16,
                  () {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamedAndRemoveUntil(
                        LoginScreen.routeName, (route) => false);
              })
            ],
          ),
        ],
      ),
    ));
  }
}

bool isValidEmail(TextEditingController emailId) {
  return RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(emailId.text);
}
