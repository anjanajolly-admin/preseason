import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mixpanel_flutter/mixpanel_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_cubit.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_state.dart';
import 'package:pre_seasoned/screens/interest_screen/interest_screen.dart';
import 'package:pre_seasoned/screens/login_screen/login_screen.dart';
import 'package:pre_seasoned/screens/main_container/main_container_screen.dart';
import 'package:pre_seasoned/screens/suggest_channels_screen/suggested_channel_illustration_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);
  static const String routeName = "/WelcomeScreen";

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  late Mixpanel mixPanel;

  @override
  void initState() {
    super.initState();
    // initMixpanel();

    Future.delayed(Duration(seconds: 3)).then((value) {
      context.read<BaseCubit>().isLoggedIn();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> initMixpanel() async {
    mixPanel = await Mixpanel.init("c4b6cba1cc4506afee6ae57cacc328cf",
        optOutTrackingDefault: false);
    var distinctId = await mixPanel.getDistinctId();
    print(distinctId);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  Future _getVersionNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF000000),
        appBar: AppBar(
          toolbarHeight: 0,
          backgroundColor: Colors.black,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        body: _getBody());
  }

  Widget _getBody() {
    return Center(
      child: Container(
        child: BlocListener<BaseCubit, BaseState>(
          bloc: BlocProvider.of<BaseCubit>(context),
          listener: (context, state) {
            print("BlocListener Login");
            if (state is LoginSuccessState) {
              if (state.isLogin) {
                Future.delayed(Duration(seconds: 1)).then((value) {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pushNamedAndRemoveUntil(
                          MainContainerScreen.routeName, (route) => false);
                });
              } else {
                Future.delayed(Duration(seconds: 1)).then((value) {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pushNamedAndRemoveUntil(
                          LoginScreen.routeName, (route) => false);
                });
              }
            } else if (state is LoginInterestState) {
              Future.delayed(Duration(seconds: 1)).then((value) {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamedAndRemoveUntil(
                        InterestScreen.routeName, (route) => false);
              });
            } else if (state is LoginSuggestedState) {
              Future.delayed(Duration(seconds: 1)).then((value) {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamedAndRemoveUntil(
                        SuggestedChannelIllustrationScreen.routeName,
                        (route) => false);
              });
            } else {
              Future.delayed(Duration(seconds: 1)).then((value) {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamedAndRemoveUntil(
                        LoginScreen.routeName, (route) => false);
              });
            }
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(),
              Container(
                width: 150,
                height: 150,
                child: Image.asset(
                  "assets/images/preseasoned_altletic_logo.png",
                  fit: BoxFit.fill,
                ),
              ),
              Spacer(),
              Container(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Loading",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      SizedBox(
                          height: 25,
                          width: 25,
                          child: CircularProgressIndicator.adaptive(
                              strokeWidth: 2,
                              backgroundColor: AppWidgets.buttonTextColor,
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(Colors.white))),
                    ],
                  )),
              SizedBox(
                height: 20,
              ),
              Container(
                child: FutureBuilder(
                  future: _getVersionNumber(),
                  builder:
                      (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                    return Text(
                      "Version Number: ${snapshot.data.toString()}",
                      style: TextStyle(color: Colors.white54, fontSize: 15),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 20,
              )
            ],
          ),
        ),
      ),
    );
  }
}
