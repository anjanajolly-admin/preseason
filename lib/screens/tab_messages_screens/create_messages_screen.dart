import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/network/model/debounce.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/friends_screen/add_friends_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';

import 'group_messages_screens/select_members_screen.dart';
import 'messages_details_screen.dart';

class CreateMessageScreen extends StatefulWidget {
  const CreateMessageScreen({Key? key}) : super(key: key);
  static const String routeName = "/CreateMessageScreen";

  @override
  _CreateMessageScreenState createState() => _CreateMessageScreenState();
}

class _CreateMessageScreenState extends State<CreateMessageScreen> {
  final TextEditingController _searchController = TextEditingController();
  bool _friendsScreenLoader = false;
  List? _allFriends;
  bool isFriendsLoading = false;
  final _debounce = Debounce();
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _getAllFriends();
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 2));
    refreshKey.currentState?.show();

    _getAllFriends();
  }

  onTextChanged(String query) {
    _debounce.call(() {
      setState(() {
        isFriendsLoading = true;
      });
      query == "" || query.isEmpty ? _getAllFriends() : _searchUsers(query);
    });
  }

  _searchUsers(query) {
    setState(() {
      isFriendsLoading = true;
    });
    AppInjector.resolve<ApiService>().searchUsers(query).then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          isFriendsLoading = false;
          Map<String, dynamic> map = convert.jsonDecode(value.data);
          print("Map Data: ${map["data"]}");
          _allFriends = map["data"];
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          isFriendsLoading = false;
          BotToast.showText(text: value.message[1]);
        });
      } else {
        setState(() {
          isFriendsLoading = false;
          BotToast.showText(text: "Something went wrong.Please try later");
        });
      }
    });
  }

  Future<void> _getAllFriends() async {
    setState(() {
      _friendsScreenLoader = true;
    });
    AppInjector.resolve<ApiService>().getAllFriends().then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          _friendsScreenLoader = false;
          isFriendsLoading = false;
          _allFriends = convert.jsonDecode(value.data);

          print("My Friends $_allFriends");
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          _allFriends = [];
          _friendsScreenLoader = false;
          isFriendsLoading = false;
        });
        BotToast.showText(text: value.message[1]);
      } else {
        setState(() {
          _allFriends = [];
          _friendsScreenLoader = false;
          isFriendsLoading = false;
        });
        BotToast.showText(text: "Something went wrong.Please try later");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
        title: Text('New Message'),
        actions: [
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: AppWidgets.getTextButton(
                  "Refresh", AppWidgets.buttonTextColor, 16, () {
                _refresh();
                setState(() {
                  _friendsScreenLoader = true;
                });
              })),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
            color: AppWidgets.backgroundColor,
            padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
            child: _body()),
      ),
    );
  }

  Widget _body() {
    return Column(
      children: [
        Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: Color(0xff464649), width: 1),
              color: Color(0xff3a3a3c),
            ),
            child: TextField(
              controller: _searchController,
              onChanged: (String query) => onTextChanged(query),
              cursorColor: Colors.white,
              decoration: InputDecoration(
                hintText: "Find or start a conversation",
                hintStyle: TextStyle(
                  color: Color(0xffa4a4ab),
                  fontSize: 14,
                ),
                border: InputBorder.none,
                prefixIcon: new IconButton(
                  icon: Container(
                    width: 24,
                    height: 24,
                    child: Image.asset(
                      "assets/images/search_icon.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  onPressed: null,
                ),
                suffixIcon: new IconButton(
                  icon: new Icon(
                    Icons.clear,
                    color: Color(0xffa4a4ab),
                  ),
                  onPressed: () {
                    setState(() {
                      _searchController.clear();
                    });
                  },
                ),
              ),
              style: TextStyle(
                color: Color(0xffa4a4ab),
                fontSize: 16,
              ),
            )),
        SizedBox(
          height: 16,
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Color(0x1e000000),
                blurRadius: 30,
                offset: Offset(0, 10),
              ),
            ],
            color: Color(0xff3a3a3c),
          ),
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 8,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              AppWidgets.getMyFriendsCard(
                  leadingIcon: "assets/images/group_placeholder.png",
                  title: "Create New Group",
                  onTap: () {
                    AppInjector.resolve<AppRoutes>()
                        .navigatorKey
                        .currentState!
                        .pushNamed(SelectMembersScreen.routeName,
                        arguments: [_allFriends, null]);
                  }),
              SizedBox(height: 8),
              Container(
                color: AppWidgets.titleColor5,
                width: double.infinity,
                height: 1,
              ),
              SizedBox(height: 8),
              AppWidgets.getMyFriendsCard(
                  leadingIcon: "assets/images/add_friend_icon.png",
                  title: "Invite Friends",
                  onTap: () {
                    AppInjector.resolve<AppRoutes>()
                        .navigatorKey
                        .currentState!
                        .pushNamed(AddFriendsScreen.routeName);
                  }),
            ],
          ),
        ),
        SizedBox(height: 16),
        _allFriends == null
            ? Container(
          padding: EdgeInsets.all(30),
          child: Center(
            child: CircularProgressIndicator.adaptive(
                strokeWidth: 2,
                backgroundColor: AppWidgets.buttonTextColor,
                valueColor: AlwaysStoppedAnimation<Color>(
                    Colors.white)
            ),
          ),
        )
            : Stack(
          children: [
            _allFriends!.isEmpty
                ? Container(
              padding:
              EdgeInsets.only(top: 30, left: 20, right: 20),
              child: Center(
                child: Column(
                  children: [
                    Text(
                      'No Friends yet ',
                      style: TextStyle(
                        color: Colors.white60,
                        fontSize: 16,
                        fontFamily: "Proxima Nova",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            )
                : Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x1e000000),
                    blurRadius: 30,
                    offset: Offset(0, 10),
                  ),
                ],
                color: Color(0xff3a3a3c),
              ),
              padding: const EdgeInsets.only(
                left: 16,
              ),
              child: ListView.builder(
                itemCount: _allFriends!.length,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, myFriendIndex) {
                  return InkWell(
                      focusColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        createPrivateDialog(
                            _allFriends![myFriendIndex]);
                      },
                      child: Container(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              SizedBox(
                                height: 16,
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  _allFriends![myFriendIndex]
                                  ['profilePhoto'] !=
                                      null
                                      ? CachedNetworkImage(
                                    width: 48,
                                    height: 48,
                                    imageUrl: _allFriends![
                                    myFriendIndex]
                                    ["profilePhoto"]["url"],
                                    fit: BoxFit.fill,
                                    placeholder: (context, url) =>
                                        Image.asset(
                                          "assets/images/channel_cover.png",
                                          fit: BoxFit.fill,
                                        ),

                                    // placeholder: (context, url) =>
                                    //     CircularProgressIndicator(),
                                    errorWidget:
                                        (context, url, error) {
                                      print(
                                          'errorWidget ${error.toString()} \n$url');
                                      return Image.asset(
                                        "assets/images/channel_cover.png",
                                        fit: BoxFit.fill,
                                      );
                                    },
                                  )
                                      : Container(
                                    width: 48,
                                    height: 48,
                                    child: Image.asset(
                                      "assets/images/default_profile_photo.png",
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  SizedBox(width: 16),
                                  Expanded(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                      MainAxisAlignment.start,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          width: double.infinity,
                                          child: Text(
                                            _allFriends![myFriendIndex]
                                            ["firstName"] +
                                                " " +
                                                _allFriends![
                                                myFriendIndex]
                                                ["lastName"],
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontFamily:
                                              "Proxima Nova",
                                              fontWeight:
                                              FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 4),
                                        SizedBox(
                                          width: double.infinity,
                                          child: Text(
                                            _allFriends![myFriendIndex]
                                            ["username"],
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12,
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 16),
                                        myFriendIndex != 2
                                            ? Container(
                                          color: AppWidgets
                                              .titleColor5,
                                          width: double.infinity,
                                          height: 1,
                                        )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )));
                },
              ),
            ),
            isFriendsLoading || _friendsScreenLoader
                ? Container(
              padding: EdgeInsets.all(30),
              child: Center(
                child: CircularProgressIndicator.adaptive(
                    strokeWidth: 2,
                    backgroundColor: AppWidgets.buttonTextColor,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Colors.white)
                ),
              ),
            )
                : Container()
          ],
        ),
        SizedBox(
          height: 16,
        )
      ],
    );
  }

  Future<void> createPrivateDialog(friendsDetails) async {
    List<int> occupantsIds = [
      int.parse(friendsDetails['occupant_id']),
      AppInjector.resolve<AppPreferences>().getQbUserId()!
    ];

    await AppInjector.resolve<QuickbloxApiService>()
        .createDialog(occupantsIds,
        dialogType: QBChatDialogTypes.CHAT,
        dialogName: " ",
        dialogPhoto: " ")
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print((value.data as QBDialog).id);
        AppInjector.resolve<AppRoutes>()
            .navigatorKey
            .currentState!
            .pushNamed(MessageDetailsScreen.routeName, arguments: [value.data]);
      } else {
        BotToast.showText(text: "Couldn't connect to chat ${value.message}");
      }
    });
  }
}
