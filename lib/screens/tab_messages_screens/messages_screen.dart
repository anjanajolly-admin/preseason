import 'dart:async';

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/network/model/debounce.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/group_messages_screens/group_message_details_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_attachment.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

import 'create_messages_screen.dart';
import 'messages_details_screen.dart';

class MessagesScreen extends StatefulWidget {
  const MessagesScreen({Key? key}) : super(key: key);
  static const String routeName = "/InboxScreen";

  @override
  _MessagesScreenState createState() => _MessagesScreenState();
}

class _MessagesScreenState extends State<MessagesScreen> {
  bool searchChannelsLoader = false;

  List<QBDialog?>? _allMessagesList;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  final TextEditingController _searchController = TextEditingController();
  bool dialogLoader = false;
  final _debounce = Debounce();
  StreamSubscription? _newMessageSubscription;

  @override
  void initState() {
    // setState(() {
    //   dialogLoader = true;
    // });
    getSubscribe();
    super.initState();
    getMessagesList();
  }

  @override
  void dispose() {
    // if (_newMessageSubscription != null) {
    //   _newMessageSubscription!.cancel();
    //   _newMessageSubscription = null;
    // }
    super.dispose();
  }

  Future<Null> refresh() async {
    setState(() {
      dialogLoader = true;
    });
    await Future.delayed(Duration(seconds: 2));
    refreshKey.currentState?.show();

    getMessagesList();
  }

  Future<void> getMessagesList() async {
    // QBFilter filter = QBFilter();
    // filter.field = "name";
    // filter.operator = QBChatDialogFilterOperators.NE;
    // filter.value =
    //     AppInjector.resolve<AppPreferences>().getQbUserId()!.toString();
    // print("Filter ----type ${filter.operator}----field  ${filter.field}");
    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: null, limit: limit)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        List<QBDialog?>? tempMessagesList = [];
        for (var index = 0; index < (value.data as List).length; index++) {
          if (!(value.data[index] as QBDialog).name!.contains("_\$\$CH\$\$")) {
            tempMessagesList.add(value.data[index]);
          }
        }
        // print(
        //     "Messages List Check ${(value.data[0] as QBDialog).unreadMessagesCount}");
        setState(() {
          _allMessagesList = tempMessagesList;
          dialogLoader = false;
          searchChannelsLoader = false;
        });
      } else {
        setState(() {
          _allMessagesList = <QBDialog?>[];
          dialogLoader = false;
          searchChannelsLoader = false;
        });
        BotToast.showText(
            text: "Couldn't retrieve the messages,Try again later");
      }
    });
  }

  _searchChannels(name) {
    QBFilter filter = QBFilter();
    filter.field = QBChatDialogFilterFields.NAME;
    filter.operator = QBChatDialogFilterOperators.CTN;
    filter.value = name;

    print("Filter ----type ${filter.type}----field  ${filter.field}");
    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print("Messages List Check ${value.data}");
        List<QBDialog?> tempMsgList = [];
        for (var index = 0; index < (value.data as List).length; index++) {
          for (var checkId
          in (value.data[index] as QBDialog).occupantsIds != null
              ? (value.data[index] as QBDialog).occupantsIds!
              : []) {
            if (checkId ==
                AppInjector.resolve<AppPreferences>().getQbUserId() &&
                !(value.data[index] as QBDialog)
                    .name!
                    .contains("_\$\$CH\$\$")) {
              tempMsgList.add(value.data[index]);
            }
          }
        }
        setState(() {
          _allMessagesList = tempMsgList;
          searchChannelsLoader = false;
        });
      } else {
        setState(() {
          searchChannelsLoader = false;
        });
        BotToast.showText(
            text: "Couldn't retrieve the messages,Try again later");
      }
    });
  }

  onTextChanged(String query) {
    _debounce.call(() {
      setState(() {
        searchChannelsLoader = true;
      });
      query == "" || query.isEmpty ? getMessagesList() : _searchChannels(query);
    });
  }

  getSubscribe() async {
    try {
      _newMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.RECEIVED_NEW_MESSAGE, (data) {
        print("Subscribed: " + QBChatEvents.RECEIVED_NEW_MESSAGE);
        Map<dynamic, dynamic> payload =
        Map<dynamic, dynamic>.from(data["payload"]);
        print("Received message : \n $payload");

        for (var index = 0; index < _allMessagesList!.length; index++) {
          if (payload["dialogId"] == _allMessagesList![index]!.id) {
            getMessagesList();
            setState(() {});
          }
        }
      }, onErrorMethod: (error) {
        print("Sub Error $error");
      });
    } catch (e) {
      print("Main Error $e");
    }
  }

  List<QBAttachment?> getAttachments(attachmentReceived) {
    var qbAttachment = QBAttachment();
    List<QBAttachment> qbAttachmentList = [];
    qbAttachment.id = (attachmentReceived['id'].toString());
    qbAttachment.contentType = attachmentReceived['contentType'];
    qbAttachment.type = attachmentReceived['type'];
    qbAttachmentList.add(qbAttachment);
    return qbAttachmentList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
          title: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text('Messages'),
          ),
          actions: [
            // InkWell(
            //   onTap: () {
            //     AppInjector.resolve<AppRoutes>()
            //         .navigatorKey
            //         .currentState!
            //         .pushNamed(AddFriendsScreen.routeName);
            //   },
            //   child: Container(
            //       margin: EdgeInsets.only(right: 10),
            //       height: 20,
            //       width: 20,
            //       child: Image.asset('assets/images/add_friend_icon.png')),
            // ),
            IconButton(
              icon: Icon(Icons.note_alt_outlined),
              onPressed: () {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamed(CreateMessageScreen.routeName)
                    .then((value) {
                  setState(() {
                    getMessagesList();
                  });
                });
              },
            )
          ],
        ),
        body: _allMessagesList == null
            ? Align(
            alignment: Alignment.center,
            child: Container(
              alignment: Alignment.center,
              height: 30,
              width: 30,
              child: CircularProgressIndicator.adaptive(
                  strokeWidth: 2,
                  backgroundColor: AppWidgets.buttonTextColor,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
            ))
            : RefreshIndicator(
            displacement: 50,
            strokeWidth: 2,
            backgroundColor: Colors.grey,
            color: AppWidgets.buttonTextColor,
            onRefresh: refresh,
            child: Container(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                color: AppWidgets.backgroundColor,
                width: double.infinity,
                child: messageListWidget())));
  }

  Widget messageListWidget() {
    return Stack(
      children: [
        Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: Color(0xff464649), width: 1),
              color: Color(0xff3a3a3c),
            ),
            child: TextField(
              controller: _searchController,
              onChanged: (String query) => onTextChanged(query),
              cursorColor: Colors.white,
              decoration: InputDecoration(
                hintText: "Find or start a conversation",
                hintStyle: TextStyle(
                  color: Color(0xffa4a4ab),
                  fontSize: 14,
                ),
                border: InputBorder.none,
                prefixIcon: new IconButton(
                  icon: Container(
                    width: 24,
                    height: 24,
                    child: Image.asset(
                      "assets/images/search_icon.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  onPressed: null,
                ),
                suffixIcon: new IconButton(
                  icon: new Icon(
                    Icons.clear,
                    color: Color(0xffa4a4ab),
                  ),
                  onPressed: () {
                    setState(() {
                      _searchController.clear();
                    });
                  },
                ),
              ),
              style: TextStyle(
                color: Color(0xffa4a4ab),
                fontSize: 16,
              ),
            )),
        _allMessagesList!.isEmpty || _allMessagesList!.length == 0
            ? Container(
            padding: EdgeInsets.only(
                top: 54, bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 180,
                      height: 180,
                      child: Image.asset(
                        "assets/images/messages_empty_image.png",
                        fit: BoxFit.contain,
                      ),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: SizedBox(
                        width: double.infinity,
                        child: Text(
                          "Messages interaction are empty.Can't find any group or direct conversations of you.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                  ],
                )))
            : Container(
          padding: EdgeInsets.only(top: 54),
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: _allMessagesList!.length,
              physics: AlwaysScrollableScrollPhysics(),
              itemBuilder: (BuildContext ctx, messageIndex) {
                return InkWell(
                  onTap: () async {
                    setState(() {
                      dialogLoader = true;
                    });
                    _debounce.call(() {
                      _allMessagesList![messageIndex]!.type == 3
                          ? AppInjector.resolve<AppRoutes>()
                          .navigatorKey
                          .currentState!
                          .pushNamed(MessageDetailsScreen.routeName,
                          arguments: [
                            _allMessagesList![messageIndex]
                          ]).then((value) {
                        setState(() {
                          dialogLoader = true;
                          getMessagesList();
                        });
                      })
                          : joinDialog(
                          _allMessagesList![messageIndex]!.id!,
                          _allMessagesList![messageIndex]);
                    });
                  },
                  child: _allMessagesList![messageIndex]!.lastMessage ==
                      null &&
                      _allMessagesList![messageIndex]!.type ==
                          QBChatDialogTypes.CHAT
                      ? Container()
                      : Container(
                    width: double.infinity,
                    padding: const EdgeInsets.only(
                      top: 10,
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _allMessagesList![messageIndex]!.photo !=
                            null
                            ? CachedNetworkImage(
                          height: 48,
                          width: 48,
                          imageUrl:
                          _allMessagesList![messageIndex]!
                              .photo!,
                          fit: BoxFit.fill,
                          placeholder: (context, url) =>
                              Image.asset(
                                "assets/images/profile_default_image.png",
                                fit: BoxFit.fill,
                              ),

                          // placeholder: (context, url) =>
                          //     CircularProgressIndicator(),
                          errorWidget: (context, url, error) {
                            print(
                                'errorWidget ${error.toString()} \n$url');
                            return Image.asset(
                              "assets/images/profile_default_image.png",
                              fit: BoxFit.fill,
                            );
                          },
                        )
                            : Container(
                          width: 48,
                          height: 48,
                          child: Image.asset(
                            "assets/images/default_profile_photo.png",
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(width: 16),
                        Expanded(
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment:
                            MainAxisAlignment.start,
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: double.infinity,
                                child: Text(
                                  _allMessagesList![messageIndex]!
                                      .name !=
                                      null
                                      ? _allMessagesList![
                                  messageIndex]!
                                      .type ==
                                      3
                                      ? _allMessagesList![
                                  messageIndex]!
                                      .name!
                                      : _allMessagesList![
                                  messageIndex]!
                                      .name!
                                      .substring(
                                      0,
                                      _allMessagesList![
                                      messageIndex]!
                                          .name!
                                          .length -
                                          7)
                                      : "",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontFamily: "Proxima Nova",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              SizedBox(height: 4),
                              _allMessagesList![messageIndex]!
                                  .lastMessage ==
                                  null
                                  ? Container()
                                  : _allMessagesList![messageIndex]!
                                  .lastMessage ==
                                  " "
                                  ? _allMessagesList![messageIndex]!
                                  .unreadMessagesCount !=
                                  0 &&
                                  _allMessagesList![messageIndex]!
                                      .unreadMessagesCount !=
                                      null
                                  ? Container(
                                  height: 12,
                                  width: 12,
                                  child: Icon(
                                      Icons
                                          .image_outlined,
                                      color:
                                      Colors.white))
                                  : Container(
                                  height: 12,
                                  width: 12,
                                  child: Icon(
                                      Icons
                                          .image_outlined,
                                      color: Colors.grey))
                                  : SizedBox(
                                width: double.infinity,
                                child: Text(
                                  _allMessagesList![
                                  messageIndex]!
                                      .lastMessage!,
                                  style: TextStyle(
                                    color: _allMessagesList![
                                    messageIndex]!
                                        .unreadMessagesCount !=
                                        0 &&
                                        _allMessagesList![
                                        messageIndex]!
                                            .unreadMessagesCount !=
                                            null
                                        ? Colors.white
                                        : Colors.grey,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                              SizedBox(height: 16),
                            ],
                          ),
                        ),
                        _allMessagesList![messageIndex]!
                            .lastMessageDateSent !=
                            null
                            ? Column(
                          children: [
                            DateFormat('yyyy-MM-dd').format(DateTime
                                .fromMillisecondsSinceEpoch(
                                _allMessagesList![
                                messageIndex]!
                                    .lastMessageDateSent!)) !=
                                DateFormat('yyyy-MM-dd')
                                    .format(
                                    DateTime.now())
                                ? Text(
                                DateFormat('dd-MM-yyyy').format(
                                    DateTime.fromMillisecondsSinceEpoch(
                                        _allMessagesList![
                                        messageIndex]!
                                            .lastMessageDateSent!)),
                                style: TextStyle(
                                  color: AppWidgets
                                      .titleColor3,
                                  fontSize: 10,
                                  fontFamily:
                                  "Proxima Nova",
                                  fontWeight:
                                  FontWeight.w700,
                                ))
                                : Container(),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              DateFormat('hh:mm a').format(DateTime
                                  .fromMillisecondsSinceEpoch(
                                  _allMessagesList![
                                  messageIndex]!
                                      .lastMessageDateSent!)),
                              style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: 'Proxima Nova',
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white60),
                            ),
                            _allMessagesList![messageIndex]!
                                .unreadMessagesCount !=
                                null
                                ? _allMessagesList![
                            messageIndex]!
                                .unreadMessagesCount !=
                                0
                                ? Container(
                              decoration:
                              BoxDecoration(
                                borderRadius:
                                BorderRadius
                                    .circular(
                                    100),
                                color: AppWidgets
                                    .buttonTextColor,
                              ),
                              padding:
                              const EdgeInsets
                                  .all(4),
                              // child: Column(
                              //   mainAxisSize:
                              //       MainAxisSize
                              //           .min,
                              //   mainAxisAlignment:
                              //       MainAxisAlignment
                              //           .start,
                              //   crossAxisAlignment:
                              //       CrossAxisAlignment
                              //           .start,
                              //   children: [
                              child: Text(
                                "${_allMessagesList![messageIndex]!.unreadMessagesCount}",
                                textAlign: TextAlign
                                    .center,
                                style: TextStyle(
                                  color:
                                  Colors.white,
                                  fontSize: 10,
                                  fontFamily:
                                  "Proxima Nova",
                                  fontWeight:
                                  FontWeight
                                      .w600,
                                ),
                              ),
                              //   ],
                              // ),
                            )
                                : Container()
                                : Container()
                          ],
                        )
                            : Container(),
                      ],
                    ),
                  ),
                );
              }),
        ),
        searchChannelsLoader || dialogLoader
            ? Positioned(
            child: Align(
                alignment: Alignment.center,
                child: Container(
                  alignment: Alignment.center,
                  height: 30,
                  width: 30,
                  child: CircularProgressIndicator.adaptive(
                      strokeWidth: 2,
                      backgroundColor: AppWidgets.buttonTextColor,
                      valueColor:
                      AlwaysStoppedAnimation<Color>(Colors.white)),
                )))
            : Container(),
      ],
    );
  }

  Future<void> joinDialog(dialogId, messageDetails) async {
    AppInjector.resolve<QuickbloxApiService>()
        .joinDialog(dialogId)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(value.data);
        setState(() {
          dialogLoader = false;
        });
        AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pushNamed(
            GroupMessageDetailsScreen.routeName,
            arguments: [messageDetails]).then((value) {
          setState(() {
            dialogLoader = true;
            getMessagesList();
          });
        });
      } else {
        setState(() {
          dialogLoader = false;
        });
        BotToast.showText(text: "Couldn't join the group.Try again later");
      }
    });
  }
}
