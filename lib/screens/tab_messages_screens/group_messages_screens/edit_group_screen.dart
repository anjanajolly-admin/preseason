import 'dart:io';
import 'dart:math';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/models/qb_custom_object.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';

class EditGroupScreen extends StatefulWidget {
  final QBDialog groupDetails;
  final QBCustomObject groupDescriptionDetails;
  EditGroupScreen(
      {Key? key,
        required this.groupDetails,
        required this.groupDescriptionDetails})
      : super(key: key);
  static const String routeName = "/EditGroupScreen";

  @override
  _EditGroupScreenState createState() => _EditGroupScreenState();
}

class _EditGroupScreenState extends State<EditGroupScreen> {
  File? _profileImageFile;
  bool isImageLoader = false;
  TextEditingController _groupName = TextEditingController();
  TextEditingController _groupDescription = TextEditingController();
  bool buttonLoader = false;

  @override
  void initState() {
    super.initState();
    _groupName.text = widget.groupDetails.name!
        .substring(0, widget.groupDetails.name!.length - 7);
    _groupDescription.text =
        widget.groupDescriptionDetails.fields!['groupDescription'].toString();
    if (widget.groupDetails.photo != null) {
      urlToFile(widget.groupDetails.photo!);
    }
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> urlToFile(String imageUrl) async {
    var rng = new Random();
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File file = new File('$tempPath' + (rng.nextInt(100)).toString() + '.png');
    http.Response response = await http.get(Uri.parse(imageUrl));
    await file.writeAsBytes(response.bodyBytes);
    _profileImageFile = file;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
        title: Text('Edit Group'),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
          child: Column(
            children: [
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      InkWell(
                        child: _profileImageFile != null
                            ? Container(
                            height: 120,
                            width: 120,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(12),
                              child: Image.file(
                                _profileImageFile!,
                                fit: BoxFit.fill,
                              ),
                            ))
                            : Container(
                          width: 120,
                          height: 120,
                          child: Image.asset(
                            "assets/images/default_profile_photo.png",
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      InkWell(
                        focusColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        onTap: () {
                          showModalBottomSheet(
                              isDismissible: true,
                              enableDrag: false,
                              context: context,
                              builder: ((builder) {
                                return Container(
                                  color: AppWidgets.backgroundColor,
                                  width: MediaQuery.of(context).size.width,
                                  height: MediaQuery.of(context).size.width * .3,
                                  child: Column(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(
                                        'Choose Profile photo',
                                        style: TextStyle(
                                            color: AppWidgets.buttonTextColor,
                                            fontSize: 18),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                        children: [
                                          // Column(
                                          //   mainAxisAlignment:
                                          //       MainAxisAlignment.center,
                                          //   children: [
                                          //     IconButton(
                                          //         onPressed: () {
                                          //           isImageLoader = true;
                                          //           Navigator.pop(context);
                                          //           _getProfileFromCamera()();
                                          //         },
                                          //         icon: Icon(
                                          //           Icons.camera,
                                          //           size: 36,
                                          //           color:
                                          //               AppWidgets.buttonTextColor,
                                          //         )),
                                          //     SizedBox(
                                          //       height: 8,
                                          //     ),
                                          //     Text(
                                          //       'Camera',
                                          //       style: TextStyle(
                                          //           color:
                                          //               AppWidgets.buttonTextColor,
                                          //           fontSize: 16),
                                          //     )
                                          //   ],
                                          // ),
                                          Column(
                                            mainAxisAlignment:
                                            MainAxisAlignment.center,
                                            children: [
                                              IconButton(
                                                  onPressed: () {
                                                    // isImageLoader = true;
                                                    Navigator.pop(context);
                                                    _getProfileFromGallery();
                                                  },
                                                  icon: Icon(
                                                    Icons.image,
                                                    size: 36,
                                                    color:
                                                    AppWidgets.buttonTextColor,
                                                  )),
                                              SizedBox(
                                                height: 8,
                                              ),
                                              Text(
                                                'Gallery',
                                                style: TextStyle(
                                                    color:
                                                    AppWidgets.buttonTextColor,
                                                    fontSize: 16),
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                );
                              }));
                        },
                        child: Text('Change photo',
                            style: TextStyle(
                                fontFamily: 'Proxima Nova',
                                fontSize: 16,
                                color: AppWidgets.buttonTextColor,
                                fontWeight: FontWeight.w600)),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Group Name',
                    style: TextStyle(
                      color: Color(0xfff2f2f7),
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 4),
                  AppWidgets.getEditText(
                      controller: _groupName,
                      onChanged: (_) {},
                      keyboardType: TextInputType.name,
                      maxLength: 100,
                      hint: "",
                      inputFormatter: [],
                      isObscureText: false),
                  SizedBox(height: 24),
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Group Description",
                          style: TextStyle(
                            color: Color(0xfff2f2f7),
                            fontSize: 16,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(height: 4),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                child: Container(
                                  height: 120,
                                  padding: EdgeInsets.only(left: 10, right: 10),
                                  decoration: BoxDecoration(
                                    color: AppWidgets.backgroundColor,
                                    border: Border.all(
                                      color: AppWidgets.titleColor4,
                                      width: 1,
                                    ), // set border width
                                    borderRadius: BorderRadius.all(Radius.circular(
                                        8.0)), // set rounded corner radius
                                  ),
                                  child: TextField(
                                    controller: _groupDescription,
                                    inputFormatters: [],
                                    obscureText: false,
                                    onChanged: (groupDescription) {},
                                    maxLines: null,
                                    style: TextStyle(
                                      color: AppWidgets.titleColor3,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Proxima Nova",
                                    ),
                                    decoration: InputDecoration(
                                      hintText: "",
                                      border: InputBorder.none,
                                      hintStyle: TextStyle(
                                        color: AppWidgets.titleColor3,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        fontFamily: "Proxima Nova",
                                      ),
                                    ),
                                  ),
                                )),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              AppWidgets.getWtaButton(
                isLoader: buttonLoader,
                buttonName: "Save",
                fontSize: 16,
                width: double.infinity,
                height: 48,
                onPressed: () {
                  setState(() {
                    buttonLoader = true;
                  });
                  updateGroupDialog(_groupName.text, _groupDescription.text,
                      _profileImageFile == null ? " " : _profileImageFile);
                },
              ),
            ],
          ),
        ));
  }

  Future<void> updateGroupDialog(
      groupName, groupDescription, dialogPhoto) async {
    var imagePath = await uploadImageToContentQB(dialogPhoto!.path);
    var url = await getQbImageMessage(id: imagePath.id!);

    await AppInjector.resolve<QuickbloxApiService>()
        .updateDialog(widget.groupDetails.id!,
        dialogPhoto: url, dialogName: "${groupName}_\$\$GP\$\$")
        .then((value) async {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(value.data);
        Map<String, Object> fieldsMap = Map();
        fieldsMap["dialogId"] = (value.data as QBDialog).id!;
        fieldsMap["groupDescription"] = groupDescription;

        var groupDescriptionApiResponse =
        await AppInjector.resolve<QuickbloxApiService>()
            .updateGroupDescriptionRecord(
            fieldsMap, widget.groupDescriptionDetails.id);
        if (groupDescriptionApiResponse.status ==
            QuickbloxApiResponseStatus.completed) {
          setState(() {
            buttonLoader = false;
          });
          AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pop();
        } else {
          setState(() {
            buttonLoader = false;
          });
          BotToast.showText(text: "Couldn't update the group.Try again later");
        }
      } else {
        setState(() {
          buttonLoader = false;
        });
        BotToast.showText(text: "Couldn't update the group.Try again later");
      }
    });
  }

  _getProfileFromGallery() async {
    var pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 640,
      maxHeight: 480,
    );
    if (pickedFile != null) {
      setState(() {
        _profileImageFile = File(pickedFile.path);
        isImageLoader = false;
      });
    }
  }

// /// Get from Camera
// _getProfileFromCamera() async {
//   XFile? pickedFile = await ImagePicker().pickImage(
//     source: ImageSource.camera,
//     maxWidth: 640,
//     maxHeight: 480,
//   );
//   if (pickedFile != null) {
//     setState(() {
//       _profileImageFile = File(pickedFile.path);
//       isImageLoader = false;
//     });
//   }
// }

}
