import 'dart:async';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';

import 'group_message_details_screen.dart';

class CreateGroupScreen extends StatefulWidget {
  final List<dynamic> selectedMembers;
  CreateGroupScreen({Key? key, required this.selectedMembers})
      : super(key: key);
  static const String routeName = "/NameGroupScreen";

  @override
  _CreateGroupScreenState createState() => _CreateGroupScreenState();
}

class _CreateGroupScreenState extends State<CreateGroupScreen> {
  final TextEditingController groupNameController = TextEditingController();
  final TextEditingController groupDescriptionController =
  TextEditingController();

  File? profileImageFile;
  bool isImageLoader = false;
  bool isButtonLoader = false;
  List<int> selectedMembersId = [];
  List<dynamic> selectedMembers = [];

  @override
  void initState() {
    super.initState();
    print("Selected members ${widget.selectedMembers.length}");

    for (var selectedMember in widget.selectedMembers) {
      print("Selected Members ${selectedMember["occupant_id"]}");
      selectedMembersId.add(int.parse(selectedMember["occupant_id"] == null
          ? 0
          : selectedMember["occupant_id"]));
      selectedMembers.add(selectedMember);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
        title: Text('Name Group'),
        actions: [
          TextButton(
              onPressed: () {
                groupNameController.text.trim() == ""
                    ? showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AppWidgets.getAlertDialog(
                      title: "Alert",
                      content: "Group Name should not be empty",
                      confirmButton: "Ok",
                      cancelButton: "Cancel",
                      onCancelTap: () {
                        Navigator.of(context).pop();
                      },
                      onConfirmTap: () {
                        Navigator.of(context).pop();
                      },
                    );
                  },
                )
                    : groupDescriptionController.text.trim() == ""
                    ? showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AppWidgets.getAlertDialog(
                      title: "Alert",
                      content:
                      "Group Description should not be empty",
                      confirmButton: "Ok",
                      cancelButton: "Cancel",
                      onCancelTap: () {
                        Navigator.of(context).pop();
                      },
                      onConfirmTap: () {
                        Navigator.of(context).pop();
                      },
                    );
                  },
                )
                    : profileImageFile == null
                    ? showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AppWidgets.getAlertDialog(
                      title: "Alert",
                      content: "Profile photo is not uploaded",
                      confirmButton: "Ok",
                      cancelButton: "Cancel",
                      onCancelTap: () {
                        Navigator.of(context).pop();
                      },
                      onConfirmTap: () {
                        Navigator.of(context).pop();
                      },
                    );
                  },
                )
                    : setState(() {
                  isButtonLoader = true;
                  createGroupDialog(
                      groupNameController.text,
                      groupDescriptionController.text,
                      selectedMembersId,
                      profileImageFile);
                });
              },
              child: Text(
                "Create",
                style: TextStyle(color: AppWidgets.buttonTextColor),
              ))
        ],
      ),
      body: SingleChildScrollView(child: _body()),
    );
  }

  Widget _body() {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: Column(
            children: [
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      InkWell(
                          focusColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          hoverColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onTap: () {
                            showModalBottomSheet(
                                isDismissible: false,
                                enableDrag: false,
                                context: context,
                                builder: ((builder) {
                                  return Container(
                                    color: AppWidgets.backgroundColor,
                                    width: MediaQuery.of(context).size.width,
                                    height: 110,
                                    child: Column(
                                      children: [
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          'Choose Profile photo',
                                          style: TextStyle(
                                              color: AppWidgets.buttonTextColor,
                                              fontSize: 18),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                          children: [
                                            // Column(
                                            //   mainAxisAlignment:
                                            //       MainAxisAlignment.center,
                                            //   children: [
                                            //     IconButton(
                                            //         onPressed: () {
                                            //           isImageLoader = true;
                                            //           Navigator.pop(context);
                                            //           _getProfileFromCamera();
                                            //         },
                                            //         icon: Icon(
                                            //           Icons.camera,
                                            //           size: 36,
                                            //           color:
                                            //               AppWidgets.buttonTextColor,
                                            //         )),
                                            //     SizedBox(
                                            //       height: 8,
                                            //     ),
                                            //     Text(
                                            //       'Camera',
                                            //       style: TextStyle(
                                            //           color:
                                            //               AppWidgets.buttonTextColor,
                                            //           fontSize: 16),
                                            //     )
                                            //   ],
                                            // ),
                                            Column(
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: [
                                                IconButton(
                                                    onPressed: () {
                                                      setState(() {
                                                        isImageLoader = true;
                                                      });

                                                      Navigator.pop(context);
                                                      _getProfileFromGallery();
                                                    },
                                                    icon: Icon(
                                                      Icons.image,
                                                      size: 36,
                                                      color: AppWidgets
                                                          .buttonTextColor,
                                                    )),
                                                SizedBox(
                                                  height: 8,
                                                ),
                                                Text(
                                                  'Gallery',
                                                  style: TextStyle(
                                                      color: AppWidgets
                                                          .buttonTextColor,
                                                      fontSize: 16),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  );
                                }));
                          },
                          child: SizedBox(
                              height: 120,
                              width: 120,
                              child: (profileImageFile == null)
                                  ? DottedBorder(
                                child: Center(
                                  child: Icon(
                                    Icons.add_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                                borderType: BorderType.RRect,
                                radius: Radius.circular(12),
                                color: Colors.white,
                                dashPattern: [10, 5, 10, 5, 10, 5],
                              )
                                  : ClipRRect(
                                borderRadius: BorderRadius.circular(12),
                                child: Image.file(
                                  profileImageFile!,
                                  fit: BoxFit.fill,
                                ),
                              ))),
                      SizedBox(
                        height: 16,
                      ),
                      InkWell(
                          focusColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          hoverColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onTap: () {
                            showModalBottomSheet(
                                isDismissible: false,
                                enableDrag: false,
                                context: context,
                                builder: ((builder) {
                                  return Container(
                                    color: AppWidgets.backgroundColor,
                                    width: MediaQuery.of(context).size.width,
                                    height: 120,
                                    child: Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                      children: [
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          'Choose Profile photo',
                                          style: TextStyle(
                                              color: AppWidgets.buttonTextColor,
                                              fontSize: 18),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                          children: [
                                            // Column(
                                            //   mainAxisAlignment:
                                            //       MainAxisAlignment.center,
                                            //   children: [
                                            //     IconButton(
                                            //         onPressed: () {
                                            //           isImageLoader = true;
                                            //           Navigator.pop(context);
                                            //           _getProfileFromCamera();
                                            //         },
                                            //         icon: Icon(
                                            //           Icons.camera,
                                            //           size: 36,
                                            //           color:
                                            //               AppWidgets.buttonTextColor,
                                            //         )),
                                            //     SizedBox(
                                            //       height: 8,
                                            //     ),
                                            //     Text(
                                            //       'Camera',
                                            //       style: TextStyle(
                                            //           color:
                                            //               AppWidgets.buttonTextColor,
                                            //           fontSize: 16),
                                            //     )
                                            //   ],
                                            // ),
                                            Column(
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: [
                                                IconButton(
                                                    onPressed: () {
                                                      setState(() {
                                                        isImageLoader = true;
                                                      });

                                                      Navigator.pop(context);
                                                      _getProfileFromGallery();
                                                    },
                                                    icon: Icon(
                                                      Icons.image,
                                                      size: 36,
                                                      color: AppWidgets
                                                          .buttonTextColor,
                                                    )),
                                                SizedBox(
                                                  height: 8,
                                                ),
                                                Text(
                                                  'Gallery',
                                                  style: TextStyle(
                                                      color: AppWidgets
                                                          .buttonTextColor,
                                                      fontSize: 16),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  );
                                }));
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              profileImageFile == null
                                  ? Text('Upload Photo',
                                  style: TextStyle(
                                      fontFamily: 'Proxima Nova',
                                      fontSize: 16,
                                      color: Colors.white60,
                                      fontWeight: FontWeight.w600))
                                  : Text('Change photo',
                                  style: TextStyle(
                                      fontFamily: 'Proxima Nova',
                                      fontSize: 16,
                                      color: AppWidgets.buttonTextColor,
                                      fontWeight: FontWeight.w600)),
                              SizedBox(
                                width: 8,
                              ),
                              isImageLoader
                                  ? SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator.adaptive(
                                      strokeWidth: 2,
                                      backgroundColor:
                                      AppWidgets.buttonTextColor,
                                      valueColor:
                                      AlwaysStoppedAnimation<Color>(
                                          Colors.white)))
                                  : Container()
                            ],
                          )),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Text(
                    'Group Name',
                    style: TextStyle(
                      color: Color(0xfff2f2f7),
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
              SizedBox(
                height: 6,
              ),
              AppWidgets.getEditText(
                controller: groupNameController,
                keyboardType: TextInputType.name,
                maxLength: 100,
                hint: "",
                inputFormatter: [],
                isObscureText: false,
                onChanged: (value) {},
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Group Description ",
                      style: TextStyle(
                        color: Color(0xfff2f2f7),
                        fontSize: 16,
                        fontFamily: "Proxima Nova",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(height: 4),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Container(
                              height: 120,
                              padding: EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                color: AppWidgets.backgroundColor,
                                border: Border.all(
                                  color: AppWidgets.titleColor4,
                                  width: 1,
                                ), // set border width
                                borderRadius: BorderRadius.all(Radius.circular(
                                    8.0)), // set rounded corner radius
                              ),
                              child: TextField(
                                controller: groupDescriptionController,
                                inputFormatters: [],
                                obscureText: false,
                                onChanged: (channelBio) {},
                                maxLines: null,
                                style: TextStyle(
                                  color: AppWidgets.titleColor3,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: "Proxima Nova",
                                ),
                                decoration: InputDecoration(
                                  hintText: "",
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                    color: AppWidgets.titleColor3,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "Proxima Nova",
                                  ),
                                ),
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 32,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x1e000000),
                      blurRadius: 30,
                      offset: Offset(0, 10),
                    ),
                  ],
                  color: Color(0xff3a3a3c),
                ),
                padding: const EdgeInsets.only(
                  left: 16,
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text(
                          '${selectedMembers.length.toString()} MEMBERS',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListView.builder(
                      itemCount: selectedMembers.length,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, myFriendIndex) {
                        return Container(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Container(
                                width: double.infinity,
                                padding: const EdgeInsets.symmetric(
                                  vertical: 8,
                                ),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    selectedMembers[myFriendIndex]
                                    ['profilePhoto'] !=
                                        null
                                        ? CachedNetworkImage(
                                      width: 48,
                                      height: 48,
                                      imageUrl:
                                      selectedMembers[myFriendIndex]
                                      ["profilePhoto"]["url"],
                                      fit: BoxFit.fill,
                                      placeholder: (context, url) =>
                                          Image.asset(
                                            "assets/images/channel_cover.png",
                                            fit: BoxFit.fill,
                                          ),

                                      // placeholder: (context, url) =>
                                      //     CircularProgressIndicator(),
                                      errorWidget: (context, url, error) {
                                        print(
                                            'errorWidget ${error.toString()} \n$url');
                                        return Image.asset(
                                          "assets/images/channel_cover.png",
                                          fit: BoxFit.fill,
                                        );
                                      },
                                    )
                                        : Container(
                                      width: 48,
                                      height: 48,
                                      child: Image.asset(
                                        "assets/images/default_profile_photo.png",
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    SizedBox(width: 16),
                                    Expanded(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                        MainAxisAlignment.start,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            width: double.infinity,
                                            child: Text(
                                              selectedMembers[myFriendIndex]
                                              ["firstName"] +
                                                  " " +
                                                  selectedMembers[myFriendIndex]
                                                  ["lastName"],
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16,
                                                fontFamily: "Proxima Nova",
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          SizedBox(height: 4),
                                          SizedBox(
                                            width: double.infinity,
                                            child: Text(
                                              selectedMembers[myFriendIndex]
                                              ["username"],
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ),
                                          SizedBox(height: 16),
                                          Container(
                                            color: AppWidgets.titleColor5,
                                            width: double.infinity,
                                            height: 1,
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 16,
              ),
            ],
          ),
        ),
        isButtonLoader
            ? Positioned.fill(
          child: Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: SizedBox(
                        height: 50,
                        width: 50,
                        child: CircularProgressIndicator.adaptive(
                            strokeWidth: 2,
                            backgroundColor: AppWidgets.buttonTextColor,
                            valueColor: AlwaysStoppedAnimation<Color>(
                                Colors.white))),
                  )
                ],
              )),
        )
            : Container(),
      ],
    );
  }

  Future<void> createGroupDialog(
      groupName, groupDescription, occupantsIds, dialogPhoto) async {
    var imagePath = await uploadImageToContentQB(dialogPhoto!.path);
    var url = await getQbImageMessage(id: imagePath.id!);

    await AppInjector.resolve<QuickbloxApiService>()
        .createDialog(occupantsIds,
        dialogPhoto: url,
        dialogName: "${groupName}_\$\$GP\$\$",
        dialogType: QBChatDialogTypes.GROUP_CHAT)
        .then((value) async {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        Map<String, Object> fieldsMap = Map();
        fieldsMap["dialogId"] = (value.data as QBDialog).id!;
        fieldsMap["groupDescription"] = groupDescription;

        var groupDescriptionApiResponse =
        await AppInjector.resolve<QuickbloxApiService>()
            .createGroupDescriptionRecord(fieldsMap);
        if (groupDescriptionApiResponse.status ==
            QuickbloxApiResponseStatus.completed) {
          setState(() {
            isButtonLoader = false;
          });

          AppInjector.resolve<AppRoutes>()
              .navigatorKey
              .currentState!
              .popAndPushNamed(GroupMessageDetailsScreen.routeName,
              arguments: [value.data]);
          print(
              "groupDescriptionApiResponse ${groupDescriptionApiResponse.data}");
        } else {
          setState(() {
            isButtonLoader = false;
          });
          BotToast.showText(text: "Couldn't create the group.Try again later");
        }
      } else {
        setState(() {
          isButtonLoader = false;
        });
        BotToast.showText(text: "Couldn't create the group.Try again later");
      }
    });
  }

  _getProfileFromGallery() async {
    var pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 640,
      maxHeight: 480,
    );
    if (pickedFile != null) {
      setState(() {
        profileImageFile = File(pickedFile.path);
        isImageLoader = false;
      });
    }
  }

// /// Get from Camera
// _getProfileFromCamera() async {
//   XFile? pickedFile = await ImagePicker().pickImage(
//     source: ImageSource.camera,
//     maxWidth: 640,
//     maxHeight: 480,
//   );
//   if (pickedFile != null) {
//     setState(() {
//       profileImageFile = File(pickedFile.path);
//       isImageLoader = false;
//     });
//   }
// }

}
