import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/group_messages_screens/select_members_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/messages_details_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/friends_screen/friend_profile_view_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/customobjects/constants.dart';
import 'package:quickblox_sdk/models/qb_custom_object.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';

import 'edit_group_screen.dart';

class GroupInfoScreen extends StatefulWidget {
  final String dialogId;
  GroupInfoScreen({
    Key? key,
    required this.dialogId,
  }) : super(key: key);
  static const String routeName = "/GroupInfoScreen";
  @override
  _GroupInfoScreenState createState() => _GroupInfoScreenState();
}

class _GroupInfoScreenState extends State<GroupInfoScreen> {
  List nonGroupMembers = [];
  List groupMembers = [];
  bool infoLoader = false;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  QBDialog? groupDialog;
  QBCustomObject? descriptionObject;

  @override
  void initState() {
    super.initState();
    getGroupDescription();
    getGroupDetails();

    infoLoader = true;
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> getGroupDescription() async {
    QBFilter filter = QBFilter();
    filter.field = "dialogId";
    filter.operator = QBCustomObjectsStringSearchTypes.CTN;
    filter.value = widget.dialogId;
    await AppInjector.resolve<QuickbloxApiService>()
        .getGroupDescriptionRecord(
      filter: filter,
    )
        .then((value) {
      setState(() {
        descriptionObject = value.data[0];
      });
      print("Group des ${descriptionObject!.fields}");
    });
  }

  Future<void> getGroupDetails() async {
    QBFilter filter = QBFilter();
    filter.field = "_id";
    filter.operator = QBChatDialogFilterOperators.ALL;
    filter.value = widget.dialogId;
    print("Filter ----type ${filter.type}----field  ${filter.field}");
    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print("Messages List Check ${value.data}");
        setState(() {
          groupDialog = value.data[0];
          _getAllFriends();
        });
      } else {
        BotToast.showText(
            text: "Couldn't retrieve the messages,Try again later");
      }
      setState(() {
        infoLoader = false;
      });
    });
  }

  Future<void> _getAllFriends() async {
    AppInjector.resolve<ApiService>().getAllFriends().then((value) {
      if (value.status == ApiResponseStatus.completed) {
        var allFriends = convert.jsonDecode(value.data);
        groupMembers = [];
        nonGroupMembers = [];
        for (var friend in allFriends) {
          if ((friend["occupant_id"]) != null) {
            if (groupDialog!.occupantsIds!
                .contains(int.parse(friend["occupant_id"]))) {
              groupMembers.add(friend);
            } else {
              nonGroupMembers.add(friend);
            }
          } else {
            nonGroupMembers.add(friend);
          }
        }
        _getUserDetails();
        setState(() {
          print(
              "Group -- ${groupMembers.toSet().toList().length} Non Group--  ${nonGroupMembers.toSet().toList().length}");
        });
      } else if (value.status == ApiResponseStatus.error) {
        BotToast.showText(text: value.message[1]);
      } else {
        BotToast.showText(text: "Something went wrong.Please try later");
      }
    });
  }

  Future<void> _getUserDetails() async {
    AppInjector.resolve<ApiService>().getUserDetails().then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          groupMembers.add(convert.jsonDecode(value.data));

          print("User Details ${convert.jsonDecode(value.data)}");
        });
      } else if (value.status == ApiResponseStatus.error) {
        BotToast.showText(text: value.message[1]);
      } else {
        BotToast.showText(text: "Something went wrong.Please try later");
      }
    });
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 2));
    refreshKey.currentState?.show();
    getGroupDetails();
    getGroupDescription();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
        title: Text('Group Info'),
      ),
      body: RefreshIndicator(
        displacement: 50,
        strokeWidth: 2,
        backgroundColor: Colors.white,
        color: AppWidgets.buttonTextColor,
        onRefresh: _refresh,
        child: _getBody(),
      ),
    );
  }

  Widget _getBody() {
    return Stack(
      children: [
        infoLoader
            ? Align(
            alignment: Alignment.center,
            child: Container(
              alignment: Alignment.center,
              height: 50,
              width: 50,
              child: CircularProgressIndicator.adaptive(
                  strokeWidth: 2,
                  backgroundColor: AppWidgets.buttonTextColor,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
            ))
            : Container(),
        Container(
          padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: groupDialog == null
              ? Container()
              : ListView(
            children: [
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      InkWell(
                        child: groupDialog!.photo != null
                            ? Container(
                            height: 120,
                            width: 120,
                            child: ClipRRect(
                                borderRadius:
                                BorderRadius.circular(12),
                                child: CachedNetworkImage(
                                  width: 48,
                                  height: 48,
                                  imageUrl: groupDialog!.photo!,
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) =>
                                      Image.asset(
                                        "assets/images/profile_default_image.png",
                                        fit: BoxFit.fill,
                                      ),
                                  errorWidget: (context, url, error) {
                                    print(
                                        'errorWidget ${error.toString()} \n$url');
                                    return Image.asset(
                                      "assets/images/profile_default_image.png",
                                      fit: BoxFit.fill,
                                    );
                                  },
                                )))
                            : Container(
                          width: 120,
                          height: 120,
                          child: Image.asset(
                            "assets/images/default_profile_photo.png",
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Color(0xff3a3a3c),
                ),
                width: double.infinity,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              groupDialog!.name!.substring(
                                  0, groupDialog!.name!.length - 7),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontFamily: "Proxima Nova",
                                fontWeight: FontWeight.w600,
                              ),
                            )),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      color: AppWidgets.titleColor5,
                      width: double.infinity,
                      height: 1,
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                'Description',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          descriptionObject == null
                              ? Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  AppInjector.resolve<AppRoutes>()
                                      .navigatorKey
                                      .currentState!
                                      .pushNamed(
                                      EditGroupScreen.routeName,
                                      arguments: [
                                        groupDialog,
                                        descriptionObject
                                      ]).then((value) {
                                    setState(() {
                                      getGroupDetails();
                                      getGroupDescription();
                                    });
                                  });
                                },
                                child: Text(
                                  'Add group description',
                                  style: TextStyle(
                                    color:
                                    AppWidgets.buttonTextColor,
                                    fontSize: 14,
                                    fontFamily: "Proxima Nova",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ],
                          )
                              : descriptionObject!.fields![
                          'groupDescription'] !=
                              null ||
                              descriptionObject!.fields![
                              'groupDescription'] !=
                                  ""
                              ? Row(
                            children: [
                              Expanded(
                                  child: Text(
                                    descriptionObject!
                                        .fields!['groupDescription']
                                        .toString(),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ))
                            ],
                          )
                              : Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  AppInjector.resolve<
                                      AppRoutes>()
                                      .navigatorKey
                                      .currentState!
                                      .pushNamed(
                                      EditGroupScreen
                                          .routeName,
                                      arguments: [
                                        groupDialog,
                                        descriptionObject
                                      ]).then((value) {
                                    setState(() {
                                      getGroupDetails();
                                      getGroupDescription();
                                    });
                                  });
                                },
                                child: Text(
                                  'Add group description',
                                  style: TextStyle(
                                    color: AppWidgets
                                        .buttonTextColor,
                                    fontSize: 14,
                                    fontFamily: "Proxima Nova",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      color: AppWidgets.titleColor5,
                      width: double.infinity,
                      height: 1,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pushNamed(EditGroupScreen.routeName,
                            arguments: [
                              groupDialog,
                              descriptionObject
                            ]).then((value) {
                          setState(() {
                            getGroupDetails();
                            getGroupDescription();
                          });
                        });
                      },
                      child: Text(
                        'Edit',
                        style: TextStyle(
                          color: AppWidgets.buttonTextColor,
                          fontSize: 14,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x1e000000),
                      blurRadius: 30,
                      offset: Offset(0, 10),
                    ),
                  ],
                  color: Color(0xff3a3a3c),
                ),
                padding: const EdgeInsets.only(
                  left: 16,
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text(
                          '${groupMembers.length.toString()} MEMBERS',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListView.builder(
                      itemCount: groupMembers.length + 1,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, myFriendIndex) {
                        return InkWell(
                          onTap: () {
                            myFriendIndex == 0
                                ? AppInjector.resolve<AppRoutes>()
                                .navigatorKey
                                .currentState!
                                .pushNamed(
                                SelectMembersScreen.routeName,
                                arguments: [
                                  nonGroupMembers,
                                  widget.dialogId
                                ]).then((value) {
                              setState(() {
                                getGroupDetails();
                                getGroupDescription();
                              });
                            })
                                : showModalBottomSheet(
                                isDismissible: true,
                                context: context,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20),
                                      topRight: Radius.circular(20)),
                                ),
                                builder: ((builder) {
                                  return SingleChildScrollView(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Color(0xff3a3a3c),
                                        borderRadius:
                                        BorderRadius.only(
                                            topLeft:
                                            Radius.circular(
                                                15),
                                            topRight:
                                            Radius.circular(
                                                15)),
                                      ),
                                      padding: EdgeInsets.fromLTRB(
                                          20, 20, 20, 20),
                                      child: Column(
                                        children: [
                                          groupMembers[myFriendIndex -
                                              1][
                                          "occupant_id"] ==
                                              AppInjector.resolve<
                                                  AppPreferences>()
                                                  .getQbUserId()
                                                  .toString()
                                              ? Container()
                                              : InkWell(
                                            focusColor: Colors
                                                .transparent,
                                            highlightColor:
                                            Colors
                                                .transparent,
                                            hoverColor: Colors
                                                .transparent,
                                            splashColor: Colors
                                                .transparent,
                                            onTap: () {
                                              Navigator.of(
                                                  context)
                                                  .pop();
                                              createPrivateDialog(
                                                  groupMembers[
                                                  myFriendIndex -
                                                      1]);
                                            },
                                            child: Row(
                                              children: [
                                                SizedBox(
                                                  height: 20,
                                                  width: 20,
                                                  child: Image
                                                      .asset(
                                                    'assets/images/message_home_deactivate_icon.png',
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                  'Message ${groupMembers[myFriendIndex - 1]['firstName']} ${groupMembers[myFriendIndex - 1]['lastName']}',
                                                  style:
                                                  TextStyle(
                                                    color: Colors
                                                        .white,
                                                    fontSize:
                                                    16,
                                                    fontFamily:
                                                    "Proxima Nova",
                                                    fontWeight:
                                                    FontWeight
                                                        .w600,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          groupMembers[myFriendIndex -
                                              1][
                                          "occupant_id"] ==
                                              AppInjector.resolve<
                                                  AppPreferences>()
                                                  .getQbUserId()
                                                  .toString()
                                              ? Container()
                                              : SizedBox(
                                            height: 20,
                                          ),
                                          InkWell(
                                            focusColor:
                                            Colors.transparent,
                                            highlightColor:
                                            Colors.transparent,
                                            hoverColor:
                                            Colors.transparent,
                                            splashColor:
                                            Colors.transparent,
                                            onTap: () {
                                              Navigator.of(context)
                                                  .pop();
                                              AppInjector.resolve<
                                                  AppRoutes>()
                                                  .navigatorKey
                                                  .currentState!
                                                  .pushNamed(
                                                  FriendProfileViewScreen
                                                      .routeName,
                                                  arguments: [
                                                    groupMembers[
                                                    myFriendIndex -
                                                        1]
                                                  ]);
                                            },
                                            child: Row(
                                              children: [
                                                SizedBox(
                                                  height: 20,
                                                  width: 20,
                                                  child: Image.asset(
                                                    'assets/images/message_read_view.png',
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                  "View ${groupMembers[myFriendIndex - 1]['firstName']} ${groupMembers[myFriendIndex - 1]['lastName']}",
                                                  style: TextStyle(
                                                    color:
                                                    Colors.white,
                                                    fontSize: 16,
                                                    fontFamily:
                                                    "Proxima Nova",
                                                    fontWeight:
                                                    FontWeight
                                                        .w600,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          InkWell(
                                            focusColor:
                                            Colors.transparent,
                                            highlightColor:
                                            Colors.transparent,
                                            hoverColor:
                                            Colors.transparent,
                                            splashColor:
                                            Colors.transparent,
                                            onTap: () {
                                              List<int> removeUsers =
                                              [];
                                              removeUsers.add(int
                                                  .parse(groupMembers[
                                              myFriendIndex -
                                                  1][
                                              "occupant_id"]));
                                              removeMember(
                                                  dialogId:
                                                  widget.dialogId,
                                                  removeUsers:
                                                  removeUsers);
                                            },
                                            child: Row(
                                              children: [
                                                SizedBox(
                                                  height: 20,
                                                  width: 20,
                                                  child: Image.asset(
                                                    'assets/images/info_outline_white.png',
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                  'Remove from Group',
                                                  style: TextStyle(
                                                    color:
                                                    Colors.white,
                                                    fontSize: 16,
                                                    fontFamily:
                                                    "Proxima Nova",
                                                    fontWeight:
                                                    FontWeight
                                                        .w600,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Container(
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                BorderRadius
                                                    .circular(
                                                    10)),
                                            height: 2,
                                            width:
                                            MediaQuery.of(context)
                                                .size
                                                .width /
                                                3,
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                }));
                          },
                          child: myFriendIndex == 0
                              ? Container(
                            width: double.infinity,
                            padding: const EdgeInsets.symmetric(
                              vertical: 8,
                            ),
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 48,
                                  height: 48,
                                  child: Image.asset(
                                    "assets/images/add_group_members.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                SizedBox(width: 16),
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 12),
                                      Container(
                                        width: double.infinity,
                                        child: Text(
                                          "Add Members",
                                          style: TextStyle(
                                            color: AppWidgets
                                                .buttonTextColor,
                                            fontSize: 16,
                                            fontFamily:
                                            "Proxima Nova",
                                            fontWeight:
                                            FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 24),
                                      Container(
                                        color:
                                        AppWidgets.titleColor5,
                                        width: double.infinity,
                                        height: 1,
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                              : Container(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.end,
                              children: [
                                Container(
                                  width: double.infinity,
                                  padding:
                                  const EdgeInsets.symmetric(
                                    vertical: 8,
                                  ),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      groupMembers[myFriendIndex -
                                          1][
                                      'profilePhoto'] !=
                                          null
                                          ? CachedNetworkImage(
                                        width: 48,
                                        height: 48,
                                        imageUrl: groupMembers[
                                        myFriendIndex -
                                            1][
                                        'profilePhoto']['url'],
                                        fit: BoxFit.fill,
                                        placeholder:
                                            (context, url) =>
                                            Image.asset(
                                              "assets/images/profile_default_image.png",
                                              fit: BoxFit.fill,
                                            ),
                                        errorWidget: (context,
                                            url, error) {
                                          print(
                                              'errorWidget ${error.toString()} \n$url');
                                          return Image.asset(
                                            "assets/images/profile_default_image.png",
                                            fit: BoxFit.fill,
                                          );
                                        },
                                      )
                                          : Container(
                                        width: 48,
                                        height: 48,
                                        child: Image.asset(
                                          "assets/images/default_profile_photo.png",
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                      SizedBox(width: 16),
                                      Expanded(
                                        child: Column(
                                          mainAxisSize:
                                          MainAxisSize.max,
                                          mainAxisAlignment:
                                          MainAxisAlignment
                                              .start,
                                          crossAxisAlignment:
                                          CrossAxisAlignment
                                              .start,
                                          children: [
                                            SizedBox(
                                              width:
                                              double.infinity,
                                              child: Text(
                                                "${groupMembers[myFriendIndex - 1]['firstName']} ${groupMembers[myFriendIndex - 1]['lastName']}",
                                                style: TextStyle(
                                                  color:
                                                  Colors.white,
                                                  fontSize: 16,
                                                  fontFamily:
                                                  "Proxima Nova",
                                                  fontWeight:
                                                  FontWeight
                                                      .w600,
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: 4),
                                            SizedBox(
                                              width:
                                              double.infinity,
                                              child: Text(
                                                "${groupMembers[myFriendIndex - 1]['username']}",
                                                style: TextStyle(
                                                  color:
                                                  Colors.white,
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: 16),
                                            Container(
                                              color: AppWidgets
                                                  .titleColor5,
                                              width:
                                              double.infinity,
                                              height: 1,
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 16,
              ),
              InkWell(
                  focusColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  onTap: () {
                    // show the dialog
                    showDialog(
                      context: context,
                      builder: (context1) {
                        return AppWidgets.getAlertDialog(
                          title: "Alert",
                          content: "Do you want to Leave the group?",
                          confirmButton: "Leave",
                          cancelButton: "Cancel",
                          onCancelTap: () {
                            Navigator.of(context1).pop();
                          },
                          onConfirmTap: () {
                            print("Left group succes");
                            leaveGroup(dialogId: widget.dialogId);
                            Navigator.of(context1).pop();
                          },
                        );
                      },
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x1e000000),
                          blurRadius: 30,
                          offset: Offset(0, 10),
                        ),
                      ],
                      color: Color(0xffE53535).withOpacity(0.2),
                    ),
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                width: 24,
                                height: 24,
                                child: Image.asset(
                                  "assets/images/logout_icon.png",
                                  fit: BoxFit.fill,
                                ),
                              ),
                              SizedBox(width: 8),
                              Expanded(
                                child: SizedBox(
                                  child: Text(
                                    "Leave Group",
                                    style: TextStyle(
                                      color: Color(0xfff2f2f7),
                                      fontSize: 16,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )),
              SizedBox(
                height: 16,
              ),
            ],
          ),
        )
      ],
    );
  }

  Future<void> createPrivateDialog(friendsDetails) async {
    List<int> occupantsIds = [
      int.parse(friendsDetails['occupant_id']),
      AppInjector.resolve<AppPreferences>().getQbUserId()!
    ];

    await AppInjector.resolve<QuickbloxApiService>()
        .createDialog(occupantsIds,
        dialogType: QBChatDialogTypes.CHAT,
        dialogName: " ",
        dialogPhoto: " ")
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(value.data);
        AppInjector.resolve<AppRoutes>()
            .navigatorKey
            .currentState!
            .pushNamed(MessageDetailsScreen.routeName, arguments: [value.data]);
      } else {
        BotToast.showText(text: "Couldn't connect to chat ${value.message}");
      }
    });
  }

  Future<void> removeMember(
      {required String dialogId, required List<int> removeUsers}) async {
    await AppInjector.resolve<QuickbloxApiService>()
        .removeGroupMember(dialogId, removeUsers: removeUsers)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        getGroupDetails();
        print(
            "Value after removing ${(value.data as QBDialog).occupantsIds!.length}");
        AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pop();
      } else {
        BotToast.showText(text: "Couldn't add users${value.message}");
      }
    });
  }

  Future<void> leaveGroup({required String dialogId}) async {
    await AppInjector.resolve<QuickbloxApiService>()
        .leaveDialog(dialogId)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pop();
      } else {
        BotToast.showText(text: "Couldn't add users${value.message}");
      }
    });
  }
}
