import 'dart:async';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_attachment.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_file.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_message.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';
import 'package:quickblox_sdk/models/qb_user.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:uuid/uuid.dart';

import 'group_info_screen.dart';

class GroupMessageDetailsScreen extends StatefulWidget {
  final QBDialog groupDetails;

  GroupMessageDetailsScreen({
    Key? key,
    required this.groupDetails,
  }) : super(key: key);
  static const String routeName = "/GroupMessageScreen";
  @override
  _GroupMessageDetailsScreenState createState() =>
      _GroupMessageDetailsScreenState();
}

class _GroupMessageDetailsScreenState extends State<GroupMessageDetailsScreen> {
  List<QBMessage?>? messagesList;
  final TextEditingController _sendMessageController = TextEditingController();

  File? _profileImageFile;
  StreamSubscription? _newMessageSubscription;
  StreamSubscription? _deliveredMessageSubscription;
  bool isMessagesLoader = false;
  ScrollController scrollController = ScrollController();
  String userName = "";
  @override
  void initState() {
    isMessagesLoader = true;
    getSubscribe();
    getSubscribeDelivered();
    super.initState();
    getMessageHistory();
  }

  @override
  void dispose() {
    // if (_newMessageSubscription != null) {
    //   _newMessageSubscription!.cancel();
    //   _newMessageSubscription = null;
    // }
    // if (_deliveredMessageSubscription != null) {
    //   _deliveredMessageSubscription!.cancel();
    //   _deliveredMessageSubscription = null;
    // }
    super.dispose();
  }

  getSubscribe() async {
    try {
      _newMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.RECEIVED_NEW_MESSAGE, (data) {
        print("Subscribed: " + QBChatEvents.RECEIVED_NEW_MESSAGE);
        Map<dynamic, dynamic> payload =
        Map<dynamic, dynamic>.from(data["payload"]);

        print("Received message : \n $payload");
        if (payload["dialogId"] == widget.groupDetails.id!) {
          setMessages(payload);
          setState(() {});
        }
      }, onErrorMethod: (error) {
        print("Sub Error $error");
      });
    } catch (e) {
      print("Main Error $e");
    }
  }

  void setMessages(payload) {
    int count = 0;
    for (var msgObj in messagesList!) {
      if (msgObj!.properties!['message_id'] ==
          payload["properties"]!['message_id']) {
        setState(() {
          count = count + 1;
          msgObj.id = payload["id"];
          if (payload['attachments'] != null && payload['attachments'] != []) {
            msgObj.attachments = getAttachments(payload["attachments"][0]);
          } else {
            msgObj.attachments = [];
          }
        });
      }
    }
    if (count == 0) {
      QBMessage? receivedMessage = QBMessage();
      receivedMessage.id = payload["id"];
      receivedMessage.body = payload['body'];
      receivedMessage.dateSent = payload['dateSent'];
      receivedMessage.dialogId = payload['dialogId'];
      receivedMessage.senderId = payload['senderId'];

      receivedMessage.properties = {
        "message_id": payload["properties"]["message_id"],
        "image_path": payload["properties"]["image_path"] != null
            ? payload["properties"]["image_path"]
            : "",
        "original_sender_name":
        payload["properties"]["original_sender_name"] != null
            ? payload["properties"]["original_sender_name"]
            : null
      };
      if (payload['attachments'] != null && payload['attachments'] != []) {
        receivedMessage.attachments = getAttachments(payload["attachments"][0]);
      } else {
        receivedMessage.attachments = [];
      }
      AppInjector.resolve<QuickbloxApiService>()
          .markMessageRead(receivedMessage);
      setState(() {
        messagesList!.add(receivedMessage);
        count = 0;
      });
    } else {}
  }

  getSubscribeDelivered() async {
    try {
      _deliveredMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.MESSAGE_DELIVERED, (data) {
        // LinkedHashMap<dynamic, dynamic> messageStatusHashMap = data;
        // Map<dynamic, dynamic> messageStatusMap =
        //     Map<dynamic, dynamic>.from(messageStatusHashMap);
        // Map<dynamic, dynamic> payloadMap =
        //     Map<String, Object>.from(messageStatusHashMap["payload"]);
        print("MESSAGE_DELIVERED message: \n $data");
      });
    } catch (e) {
      // Some error occurred, look at the exception message for more details
    }
  }

  List<QBAttachment?> getAttachments(attachmentReceived) {
    var qbAttachment = QBAttachment();
    List<QBAttachment> qbAttachmentList = [];
    qbAttachment.id = (attachmentReceived['id'].toString());
    qbAttachment.contentType = attachmentReceived['contentType'];
    qbAttachment.type = attachmentReceived['type'];
    qbAttachmentList.add(qbAttachment);
    return qbAttachmentList;
  }

  Future<void> getMessageHistory() async {
    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;
    int limit = 0;
    int skip = 0;
    userName =
    await getUsersName(AppInjector.resolve<AppPreferences>().getQbUserId());
    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogMessages(widget.groupDetails.id!,
        sort: sort, limit: limit, skip: skip)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        setState(() {
          messagesList = value.data;
          isMessagesLoader = false;
        });
      } else {
        setState(() {
          messagesList = [];
          isMessagesLoader = false;
        });
        BotToast.showText(text: "Couldn't Retrieve messages");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
        title: Text(widget.groupDetails.name!
            .substring(0, widget.groupDetails.name!.length - 7)),
        actions: [
          SizedBox(
              width: 20, child: Image.asset('assets/images/phone_icon.png')),
          SizedBox(
            width: 8,
          ),
          SizedBox(
            width: 20,
            child: Image.asset('assets/images/video_icon.png'),
          ),
          SizedBox(
            width: 8,
          ),
          PopupMenuButton<int>(
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            color: Colors.black,
            itemBuilder: (context) => [
              PopupMenuItem<int>(
                  value: 0,
                  child: Text("Group Info",
                      style: TextStyle(
                          fontFamily: 'Proxima Nova',
                          fontSize: 14,
                          color: Colors.white,
                          fontWeight: FontWeight.w600))),
              PopupMenuItem<int>(
                  value: 1,
                  child: Text("Clear Chat",
                      style: TextStyle(
                          fontFamily: 'Proxima Nova',
                          fontSize: 14,
                          color: Colors.white,
                          fontWeight: FontWeight.w600))),
              PopupMenuItem<int>(
                  value: 2,
                  child: Text("Leave Group",
                      style: TextStyle(
                          fontFamily: 'Proxima Nova',
                          fontSize: 14,
                          color: Colors.red,
                          fontWeight: FontWeight.w600))),
            ],
            onSelected: (item) => selectedItem(context, item),
          ),
        ],
      ),
      body: _getBody(),
    );
  }

  Widget _getBody() {
    return Stack(
      children: [
        isMessagesLoader
            ? new Positioned.fill(
          child: Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      height: 50,
                      width: 50,
                      alignment: Alignment.center,
                      child: CircularProgressIndicator.adaptive(
                          strokeWidth: 2,
                          backgroundColor: AppWidgets.buttonTextColor,
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Colors.white))),
                ],
              )),
        )
            : new Positioned(
            child: Container(
              margin: EdgeInsets.only(left: 20, top: 16, right: 20),
              padding: EdgeInsets.only(bottom: 68),
              child: Stack(
                children: [
                  ListView.builder(
                      controller: scrollController,
                      itemCount: messagesList!.length,
                      physics: AlwaysScrollableScrollPhysics(),
                      shrinkWrap: true,
                      reverse: true,
                      itemBuilder: (context, index1) {
                        return Row(
                          mainAxisAlignment: messagesList![
                          messagesList!.length -
                              1 -
                              index1]!
                              .senderId ==
                              AppInjector.resolve<AppPreferences>()
                                  .getQbUserId() ||
                              messagesList![messagesList!.length -
                                  1 -
                                  index1]!
                                  .senderId ==
                                  null
                              ? MainAxisAlignment.end
                              : MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppWidgets.messageDetailsConversation(
                              messageObj: messagesList![
                              messagesList!.length - 1 - index1]!,
                              context: context,
                              isFromGroup: true,
                              isFromReplyMessage: false,
                            )
                          ],
                        );
                      }),
                ],
              ),
            )),
        new Positioned(
            child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: Color(0xff171717),
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(
                                  color: Color(0xff464649),
                                  width: 1,
                                ),
                                color: Color(0xff1c1c1e),
                              ),
                              padding: const EdgeInsets.symmetric(
                                horizontal: 8,
                              ),
                              child: TextField(
                                controller: _sendMessageController,
                                onChanged: (value) {},
                                style: TextStyle(
                                  color: AppWidgets.titleColor3,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: "Proxima Nova",
                                ),
                                decoration: InputDecoration(
                                  hintText: "Send your first message",
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                    color: AppWidgets.titleColor3,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "Proxima Nova",
                                  ),
                                  suffixIcon: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: [
                                      IconButton(
                                          onPressed: () async {
                                            _sendMessageController
                                                .text.isEmpty ||
                                                _sendMessageController
                                                    .text ==
                                                    ""
                                                ? BotToast.showText(
                                                text:
                                                "Can't send empty messages")
                                                : sendMessage(
                                                _sendMessageController.text,
                                                <QBAttachment>[],
                                                null,
                                                "");

                                            _sendMessageController.text = "";
                                            AppConstants.hideKeyboard(context);
                                          },
                                          icon: Container(
                                            width: 24,
                                            height: 24,
                                            child: Image.asset(
                                              "assets/images/logout_icon.png",
                                              fit: BoxFit.fill,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          IconButton(
                            onPressed: () async {
                              AppConstants.hideKeyboard(context);
                              showModalBottomSheet(
                                  isDismissible: false,
                                  enableDrag: false,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  backgroundColor: AppWidgets.backgroundColor,
                                  context: context,
                                  builder: (context) {
                                    return StatefulBuilder(builder: (BuildContext
                                    context,
                                        StateSetter
                                        setState /*You can rename this!*/) {
                                      return Container(
                                        height: 50,
                                        margin: EdgeInsets.all(10),
                                        child: Column(
                                          children: [
                                            AppWidgets.getWtaButton(
                                                buttonName: "Select Images",
                                                fontSize: 14,
                                                width: double.infinity,
                                                height: 48,
                                                isLoader: false,
                                                onPressed: () async {
                                                  AppConstants.hideKeyboard(
                                                      context);

                                                  Navigator.pop(context);
                                                  _getProfileFromGallery();
                                                }),
                                            // SizedBox(
                                            //   height: 10,
                                            // ),
                                            // AppWidgets.getWtaButton(
                                            //     buttonName: "Camera",
                                            //     fontSize: 14,
                                            //     width: double.infinity,
                                            //     height: 48,
                                            //     isLoader: false,
                                            //     onPressed: () {
                                            //
                                            //       Navigator.pop(
                                            //           context);
                                            //       _getProfileFromCamera(
                                            //           width: 80,
                                            //           height: 80);
                                            //     }),
                                          ],
                                        ),
                                      );
                                    });
                                  });
                            },
                            icon: Container(
                              width: 24,
                              height: 24,
                              child: Image.asset(
                                "assets/images/camera_blue_icon.png",
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          // SizedBox(width: 8),
                          // IconButton(
                          //   onPressed: () {},
                          //   icon: Container(
                          //     width: 24,
                          //     height: 24,
                          //     child: Image.asset(
                          //       "assets/images/mic_blue_icon.png",
                          //       fit: BoxFit.fill,
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    ],
                  ),
                )))
      ],
    );
  }

  Future<void> sendMessage(
      messageBody, attachment, message_id, image_path) async {
    if (message_id == null) {
      message_id = Uuid().v1();
      QBMessage messageObj = QBMessage();
      messageObj.id = null;
      messageObj.body = messageBody;
      messageObj.properties = {
        "message_id": message_id,
        "image_path": image_path,
        "original_sender_name": userName
      };
      messageObj.dateSent = DateTime.now().millisecondsSinceEpoch;
      messageObj.attachments = attachment;
      setState(() {
        messagesList!.add(messageObj);
      });
    }
    _newMessageSubscription!.resume();
    AppInjector.resolve<QuickbloxApiService>()
        .sendMessage(widget.groupDetails.id!,
        body: messageBody,
        attachments: attachment,
        properties: {
          "message_id": message_id,
          "image_path": image_path,
          "original_sender_name": userName
        },
        saveToHistory: true,
        markable: true)
        .then((value) async {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        List<String> recipientsIds = await getOccupantIds();
        setState(() {
          sendPushNotification(
              messageBody,
              widget.groupDetails.name!
                  .substring(0, widget.groupDetails.name!.length - 7),
              userName,
              recipientsIds);
        });
      } else {
        BotToast.showText(text: "Couldn't send message. Try again later.");
      }
    });
  }

  /// Get from gallery
  Future<void> _getProfileFromGallery() async {
    var pickedFile = await FilePicker.platform.pickFiles();

    if (pickedFile != null) {
      _profileImageFile = File(pickedFile.files.single.path!);

      var message_id = Uuid().v1();

      QBMessage messageObj = QBMessage();
      messageObj.id = null;
      messageObj.body = " ";
      messageObj.attachments = <QBAttachment>[];
      messageObj.dateSent = DateTime.now().millisecondsSinceEpoch;
      messageObj.properties = {
        "message_id": message_id,
        "image_path": _profileImageFile!.path,
        "original_sender_name":
        AppInjector.resolve<AppPreferences>().getQbUserId().toString(),
      };
      setState(() {
        messagesList!.add(messageObj);
      });
      List<QBAttachment> attachmentsResponse =
      await uploadImageToContentQB(_profileImageFile!.path);
      if (attachmentsResponse != []) {
        sendMessage(
            " ", attachmentsResponse, message_id, _profileImageFile!.path);
      } else {}
    }
  }

  // /// Get from Camera
  // Future<void> _getProfileFromCamera(
  //     {required double width, required double height}) async {
  //   XFile? pickedFile = await ImagePicker().pickImage(
  //     source: ImageSource.camera,
  //     maxWidth: 640,
  //     maxHeight: 480,
  //   );
  //   if (pickedFile != null) {
  //     setState(() {
  //       _profileImageFile = File(pickedFile.path);
  //     });
  //   }
  // }
  void selectedItem(BuildContext context, item) {
    switch (item) {
      case 0:
        AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pushNamed(
            GroupInfoScreen.routeName,
            arguments: [widget.groupDetails.id]);
        break;
      case 1:
        setState(() {
          messagesList!.clear();
        });

        print("Clear chat");
        break;
      case 2:
        showDialog(
          context: context,
          builder: (context1) {
            return AppWidgets.getAlertDialog(
              title: "Alert",
              content: "Are you sure you want to leave from this group?",
              confirmButton: "Leave",
              cancelButton: "Cancel",
              onCancelTap: () {
                Navigator.of(context1).pop();
              },
              onConfirmTap: () {
                leaveGroup(dialogId: widget.groupDetails.id!);
                Navigator.of(context1).pop();
              },
            );
          },
        );

        break;
    }
  }

  Future<String> getUsersName(senderId) async {
    var allUsers =
    await AppInjector.resolve<QuickbloxApiService>().getQBUsers();
    String fullName = "";
    if (allUsers.status == QuickbloxApiResponseStatus.completed) {
      print(
          "Before send msg dialog status ${(allUsers.data[0] as QBUser).fullName}");
      for (var index = 0; index < allUsers.data.length; index++) {
        if ((allUsers.data[index] as QBUser).id == senderId) {
          fullName = (allUsers.data[index] as QBUser).fullName!;
        }
      }

      return fullName;
    } else {
      BotToast.showText(text: "Before send msg dialog status,Try again later");
      return fullName;
    }
  }

  Future<List<QBAttachment>> uploadImageToContentQB(_profileImageFile) async {
    try {
      QBFile? file = await QB.content.upload(_profileImageFile, public: true);
      if (file != null) {
        int? id = file.id;
        String? contentType = file.contentType;

        QBAttachment attachment = QBAttachment();

        attachment.id = id.toString();
        attachment.contentType = contentType;
        attachment.type = "PHOTO";

        List<QBAttachment> attachmentsList = [];
        attachmentsList.add(attachment);
        print("Attachment list added $attachmentsList");
        return attachmentsList;
      } else {
        return [];
      }
    } catch (error) {
      print("------------Error is $error");
      return [];
    }
  }

  Future<void> leaveGroup({required String dialogId}) async {
    await AppInjector.resolve<QuickbloxApiService>()
        .leaveDialog(dialogId)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pop();
      } else {
        BotToast.showText(text: "Couldn't add users${value.message}");
      }
    });
  }

  Future<List<String>> getOccupantIds() async {
    List<String> occupantsIds = [];
    QBFilter filter = QBFilter();
    filter.field = "_id";
    filter.operator = QBChatDialogFilterOperators.ALL;
    filter.value = widget.groupDetails.id;

    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(
            "Before send msg dialog status ${(value.data[0] as QBDialog).occupantsIds}");
        List<String> tempOccupantId = [];
        for (int i = 0;
        i < (value.data[0] as QBDialog).occupantsIds!.length;
        i++) {
          if (((value.data[0] as QBDialog).occupantsIds![i]) !=
              AppInjector.resolve<AppPreferences>().getQbUserId()) {
            tempOccupantId
                .add((value.data[0] as QBDialog).occupantsIds![i].toString());
          }
        }
        occupantsIds = tempOccupantId;
      } else {
        occupantsIds = [];
      }
    });
    return occupantsIds;
  }

  Future<void> sendPushNotification(String message, String notificationName,
      String senderName, List<String> recipientsIds) async {
    Map<String, dynamic> navigationObject = Map();
    navigationObject['isJoined'] = widget.groupDetails.isJoined;
    navigationObject['createdAt'] = widget.groupDetails.createdAt;
    navigationObject['lastMessage'] = widget.groupDetails.lastMessage;
    navigationObject['lastMessageDateSent'] =
        widget.groupDetails.lastMessageDateSent;
    navigationObject['lastMessageUserId'] =
        widget.groupDetails.lastMessageUserId;
    navigationObject['name'] = widget.groupDetails.name;
    navigationObject['photo'] = widget.groupDetails.photo;
    navigationObject['type'] = widget.groupDetails.type;
    navigationObject['unreadMessagesCount'] =
        widget.groupDetails.unreadMessagesCount;
    navigationObject['updatedAt'] = widget.groupDetails.updatedAt;
    navigationObject['userId'] = widget.groupDetails.userId;
    navigationObject['roomJid'] = widget.groupDetails.roomJid;
    navigationObject['id'] = widget.groupDetails.id;
    navigationObject['occupantsIds'] = widget.groupDetails.occupantsIds;
    navigationObject['customData'] = widget.groupDetails.customData;

    AppInjector.resolve<QuickbloxApiService>()
        .sendPushNotification(
        notificationName: notificationName,
        message: message,
        senderName: senderName,
        recipientsIds: recipientsIds,
        navigationObject: navigationObject,
        screen: "3")
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        setState(() {
          print('Successful sent');
        });
      } else {
        setState(() {
          print('Unsuccessful');
        });
      }
    });
  }
}
