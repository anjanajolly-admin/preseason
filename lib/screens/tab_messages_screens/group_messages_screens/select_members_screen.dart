import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/network/model/debounce.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

import 'create_group_screen.dart';

class SelectMembersScreen extends StatefulWidget {
  final List allFriends;
  final dialogId;

  SelectMembersScreen(
      {Key? key, required this.allFriends, required this.dialogId})
      : super(key: key);
  static const String routeName = "/SelectMembersScreen";
  @override
  _SelectMembersScreenState createState() => _SelectMembersScreenState();
}

class _SelectMembersScreenState extends State<SelectMembersScreen> {
  final TextEditingController _searchController = TextEditingController();
  List _allFriends = [];
  bool _noFriends = false;
  List<dynamic> selectedMembers = [];
  bool isMembersLoading = false;
  final _debounce = Debounce();

  @override
  void initState() {
    super.initState();
    setState(() {
      _allFriends = widget.allFriends;
    });
  }

  @override
  void dispose() {
    selectedMembers.clear();
    _searchController.dispose();
    super.dispose();
  }

  onTextChanged(String query) {
    _debounce.call(() {
      setState(() {
        isMembersLoading = true;
      });
      query == "" || query.isEmpty ? _getAllFriends() : _searchUsers(query);
    });
  }

  Future<void> _getAllFriends() async {
    AppInjector.resolve<ApiService>().getAllFriends().then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          isMembersLoading = false;
          _allFriends = convert.jsonDecode(value.data);

          print("My Friends $_allFriends");
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          _allFriends = [];
          isMembersLoading = false;
        });
        BotToast.showText(text: value.message[1]);
      } else {
        setState(() {
          _allFriends = [];
          isMembersLoading = false;
        });
        BotToast.showText(text: "Something went wrong.Please try later");
      }
    });
  }

  _searchUsers(query) {
    setState(() {
      isMembersLoading = true;
    });
    AppInjector.resolve<ApiService>().searchUsers(query).then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          isMembersLoading = false;
          Map<String, dynamic> map = convert.jsonDecode(value.data);
          print("Map Data: ${map["data"]}");
          _allFriends = map["data"];
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          isMembersLoading = false;
          BotToast.showText(text: value.message[1]);
        });
      } else {
        setState(() {
          isMembersLoading = false;
          BotToast.showText(text: "Something went wrong.Please try later");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
        title: widget.dialogId == null
            ? Text('Select Members')
            : Text('Add Members'),
        actions: [
          widget.dialogId == null
              ? AppWidgets.getTextButton("Next", AppWidgets.buttonTextColor, 16,
                  () {
                selectedMembers.length > 1
                    ? AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .popAndPushNamed(CreateGroupScreen.routeName,
                    arguments: [selectedMembers])
                    : BotToast.showText(
                    text:
                    "Please have at least 2 friends before trying to create a group");
              })
              : AppWidgets.getTextButton("Add", AppWidgets.buttonTextColor, 16,
                  () {
                List<int> addUsers = [];
                for (var selectedMember in selectedMembers) {
                  addUsers.add(int.parse(selectedMember["occupant_id"]));
                }
                addMembers(dialogId: widget.dialogId, addUsers: addUsers);
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
            color: AppWidgets.backgroundColor,
            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: _getBody()),
      ),
    );
  }

  Widget _getBody() {
    return Column(
      children: [
        Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: Color(0xff464649), width: 1),
              color: Color(0xff3a3a3c),
            ),
            child: TextField(
              controller: _searchController,
              onChanged: (String query) => onTextChanged(query),
              cursorColor: Colors.white,
              decoration: InputDecoration(
                hintText: "Find or start a conversation",
                hintStyle: TextStyle(
                  color: Color(0xffa4a4ab),
                  fontSize: 14,
                ),
                border: InputBorder.none,
                prefixIcon: new IconButton(
                  icon: Container(
                    width: 24,
                    height: 24,
                    child: Image.asset(
                      "assets/images/search_icon.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  onPressed: null,
                ),
                suffixIcon: new IconButton(
                  icon: new Icon(
                    Icons.clear,
                    color: Color(0xffa4a4ab),
                  ),
                  onPressed: () {
                    setState(() {
                      _searchController.clear();
                    });
                  },
                ),
              ),
              style: TextStyle(
                color: Color(0xffa4a4ab),
                fontSize: 16,
              ),
            )),
        SizedBox(
          height: 16,
        ),
        (selectedMembers.isNotEmpty) ? selectedMemberList() : Container(),
        SizedBox(
          height: 16,
        ),
        Stack(
          children: [
            _noFriends
                ? Container(
              padding: EdgeInsets.only(top: 30, left: 20, right: 20),
              child: Center(
                child: Column(
                  children: [
                    Text(
                      'No Friends yet ',
                      style: TextStyle(
                        color: Colors.white60,
                        fontSize: 16,
                        fontFamily: "Proxima Nova",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            )
                : Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x1e000000),
                    blurRadius: 30,
                    offset: Offset(0, 10),
                  ),
                ],
                color: Color(0xff3a3a3c),
              ),
              padding: const EdgeInsets.only(
                left: 16,
              ),
              child: ListView.builder(
                itemCount: _allFriends.length,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, myFriendIndex) {
                  return Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        SizedBox(
                          height: 16,
                        ),
                        InkWell(
                            focusColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            onTap: () {
                              setState(() {
                                if (selectedMembers.contains(
                                    _allFriends[myFriendIndex])) {
                                  selectedMembers
                                      .remove(_allFriends[myFriendIndex]);
                                } else
                                  selectedMembers
                                      .add(_allFriends[myFriendIndex]);
                              });
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                _allFriends[myFriendIndex]
                                ['profilePhoto'] !=
                                    null
                                    ? CachedNetworkImage(
                                  width: 48,
                                  height: 48,
                                  imageUrl:
                                  _allFriends[myFriendIndex]
                                  ["profilePhoto"]["url"],
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) =>
                                      Image.asset(
                                        "assets/images/channel_cover.png",
                                        fit: BoxFit.fill,
                                      ),

                                  // placeholder: (context, url) =>
                                  //     CircularProgressIndicator(),
                                  errorWidget:
                                      (context, url, error) {
                                    print(
                                        'errorWidget ${error.toString()} \n$url');
                                    return Image.asset(
                                      "assets/images/channel_cover.png",
                                      fit: BoxFit.fill,
                                    );
                                  },
                                )
                                    : Container(
                                  width: 48,
                                  height: 48,
                                  child: Image.asset(
                                    "assets/images/default_profile_photo.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                SizedBox(width: 16),
                                Expanded(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: double.infinity,
                                        child: Text(
                                          _allFriends[myFriendIndex]
                                          ["firstName"] +
                                              " " +
                                              _allFriends[myFriendIndex]
                                              ["lastName"],
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontFamily: "Proxima Nova",
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 4),
                                      SizedBox(
                                        width: double.infinity,
                                        child: Text(
                                          _allFriends[myFriendIndex]
                                          ["username"],
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 16),
                                      Container(
                                        color: AppWidgets.titleColor5,
                                        width: double.infinity,
                                        height: 1,
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 20,
                                  width: 20,
                                  child: selectedMembers.contains(
                                      _allFriends[myFriendIndex])
                                      ? Image.asset(
                                      'assets/images/single_circle_select.png')
                                      : Image.asset(
                                      'assets/images/single_circle_deselect.png'),
                                ),
                                SizedBox(
                                  width: 10,
                                )
                              ],
                            )),
                      ],
                    ),
                  );
                },
              ),
            ),
            isMembersLoading
                ? Container(
              padding: EdgeInsets.all(30),
              child: Center(
                child: CircularProgressIndicator.adaptive(
                    strokeWidth: 2,
                    backgroundColor: AppWidgets.buttonTextColor,
                    valueColor:
                    AlwaysStoppedAnimation<Color>(Colors.white)),
              ),
            )
                : Container()
          ],
        ),
      ],
    );
  }

  Widget selectedMemberList() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: [
              Text(
                '${selectedMembers.length} ',
                style: TextStyle(
                  color: Colors.white60,
                  fontSize: 16,
                  fontFamily: "Proxima Nova",
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text(
                (selectedMembers.length == 1) ? 'Member' : 'Members',
                style: TextStyle(
                  color: Colors.white60,
                  fontSize: 16,
                  fontFamily: "Proxima Nova",
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 50,
            child: ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: selectedMembers.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    margin: EdgeInsets.all(3),
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    decoration: BoxDecoration(
                        color: Color(0xff3a3a3c),
                        borderRadius: BorderRadius.circular(20)),
                    child: Row(
                      children: [
                        SizedBox(
                          height: 20,
                          width: 20,
                          child: selectedMembers[index]['profilePhoto'] != null
                              ? CachedNetworkImage(
                            width: 48,
                            height: 48,
                            imageUrl: selectedMembers[index]
                            ["profilePhoto"]["url"],
                            fit: BoxFit.fill,
                            placeholder: (context, url) => Image.asset(
                              "assets/images/channel_cover.png",
                              fit: BoxFit.fill,
                            ),

                            // placeholder: (context, url) =>
                            //     CircularProgressIndicator(),
                            errorWidget: (context, url, error) {
                              print(
                                  'errorWidget ${error.toString()} \n$url');
                              return Image.asset(
                                "assets/images/channel_cover.png",
                                fit: BoxFit.fill,
                              );
                            },
                          )
                              : Container(
                            width: 48,
                            height: 48,
                            child: Image.asset(
                              "assets/images/default_profile_photo.png",
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          selectedMembers[index]["firstName"] +
                              " " +
                              selectedMembers[index]["lastName"],
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 13,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              selectedMembers.removeAt(index);
                            });
                          },
                          child: Icon(
                            Icons.close,
                            size: 15,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  Future<void> addMembers(
      {required String dialogId, required List<int> addUsers}) async {
    await AppInjector.resolve<QuickbloxApiService>()
        .addGroupMember(dialogId, addUsers: addUsers)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print("Value after adding ${value.data}");
        AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pop();
      } else {
        BotToast.showText(text: "Couldn't add users${value.message}");
      }
    });
  }
}
