import 'dart:convert' as convert;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class ManagedPreferencesScreen extends StatefulWidget {
  final List interestsList;
  const ManagedPreferencesScreen({Key? key, required this.interestsList})
      : super(key: key);
  static const String routeName = "/ManagedPreferencesScreen";

  @override
  _ManagedPreferencesScreenState createState() =>
      _ManagedPreferencesScreenState();
}

class _ManagedPreferencesScreenState extends State<ManagedPreferencesScreen> {
  bool isCategoryLoader = false;
  List<String> interestTypesSelectedName = [];
  List<int> interestTypesSelectedId = [];
  bool isUpdateLoader = false;
  List<bool> interestTypesSelected = [];
  late List<dynamic> interestTypes = [];

  @override
  void initState() {
    super.initState();
    interestTypes = widget.interestsList;
    interestTypesSelectedName = List.generate(widget.interestsList.length,
            (index) => widget.interestsList[index]['name']);
    interestTypesSelectedId = List.generate(widget.interestsList.length,
            (index) => widget.interestsList[index]['id']);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          title: Text(
            "Managed Preferences",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        body: _getBody());
  }

  Widget _getBody() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 16),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                child: Text(
                  "Channel Category",
                  style: TextStyle(
                    color: Color(0xfff2f2f7),
                    fontSize: 16,
                    fontFamily: "Proxima Nova",
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              isCategoryLoader
                  ? SizedBox(
                  height: 20,
                  width: 20,
                  child: CircularProgressIndicator.adaptive(
                      strokeWidth: 2,
                      backgroundColor: AppWidgets.buttonTextColor,
                      valueColor:
                      AlwaysStoppedAnimation<Color>(Colors.white)))
                  : InkWell(
                focusColor: Colors.transparent,
                highlightColor: Colors.transparent,
                hoverColor: Colors.transparent,
                splashColor: Colors.transparent,
                onTap: () {
                  setState(() {
                    isCategoryLoader = true;
                  });

                  AppInjector.resolve<ApiService>()
                      .getAllInterest()
                      .then((response) {
                    if (response.status == ApiResponseStatus.completed) {
                      setState(() {
                        isCategoryLoader = false;
                        showModalBottomSheet(
                            isDismissible: false,
                            enableDrag: false,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            backgroundColor: AppWidgets.backgroundColor,
                            context: context,
                            builder: (context) {
                              return StatefulBuilder(builder: (BuildContext
                              context,
                                  StateSetter
                                  setModalState /*You can rename this!*/) {
                                return _getCategoryWidget(
                                    convert
                                        .jsonDecode(response.data)
                                        .length,
                                    convert.jsonDecode(response.data),
                                    setModalState);
                              });
                            });
                      });
                    } else if (response.status ==
                        ApiResponseStatus.error) {
                      setState(() {
                        isCategoryLoader = false;
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AppWidgets.getAlertDialog(
                              title: "Alert",
                              content: response.message[1],
                              confirmButton: "Ok",
                              cancelButton: "Cancel",
                              onCancelTap: () {
                                Navigator.of(context).pop();
                              },
                              onConfirmTap: () {
                                Navigator.of(context).pop();
                              },
                            );
                          },
                        );
                      });
                    } else {
                      return [
                        false,
                        "Something went wrong.Please try later"
                      ];
                    }
                  });
                },
                child: Icon(
                  Icons.edit,
                  color: Colors.white,
                  size: 20.0,
                  semanticLabel:
                  'Text to announce in accessibility modes',
                ),
              )
            ],
          ),
          SizedBox(height: 4),
          InkWell(
            focusColor: Colors.transparent,
            highlightColor: Colors.transparent,
            hoverColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              setState(() {
                isCategoryLoader = true;
              });

              AppInjector.resolve<ApiService>()
                  .getAllInterest()
                  .then((response) {
                if (response.status == ApiResponseStatus.completed) {
                  setState(() {
                    isCategoryLoader = false;
                    showModalBottomSheet(
                        isDismissible: false,
                        enableDrag: false,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        backgroundColor: AppWidgets.backgroundColor,
                        context: context,
                        builder: (context) {
                          return StatefulBuilder(builder: (BuildContext context,
                              StateSetter
                              setModalState /*You can rename this!*/) {
                            return _getCategoryWidget(
                                convert.jsonDecode(response.data).length,
                                convert.jsonDecode(response.data),
                                setModalState);
                          });
                        });
                  });
                } else if (response.status == ApiResponseStatus.error) {
                  setState(() {
                    isCategoryLoader = false;
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AppWidgets.getAlertDialog(
                          title: "Alert",
                          content: response.message[1],
                          confirmButton: "Ok",
                          cancelButton: "Cancel",
                          onCancelTap: () {
                            Navigator.of(context).pop();
                          },
                          onConfirmTap: () {
                            Navigator.of(context).pop();
                          },
                        );
                      },
                    );
                  });
                } else {
                  return [false, "Something went wrong.Please try later"];
                }
              });
            },
            child: Container(
              padding: EdgeInsets.only(left: 10),
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                color: AppWidgets.backgroundColor,
                border: Border.all(
                  color: AppWidgets.titleColor4,
                  width: 1,
                ), // set border width
                borderRadius: BorderRadius.all(
                    Radius.circular(8.0)), // set rounded corner radius
              ),
              child: Container(
                  height: 100.0,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: getInterestTypes.toList(),
                    ),
                  )),
            ),
          ),
          Spacer(),
          AppWidgets.getWtaButton(
              isLoader: isUpdateLoader,
              buttonName: "Save",
              fontSize: 16,
              width: double.infinity,
              height: 48,
              onPressed: () {
                for (int i = 0; i < interestTypesSelected.length; i++) {
                  if (interestTypesSelected[i]) {
                    interestTypesSelectedId.add(interestTypes[i]["id"] as int);
                  }
                }
                if (interestTypesSelectedId.isEmpty) {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AppWidgets.getAlertDialog(
                        title: "Alert",
                        content: "Please Select at least one Interest",
                        confirmButton: "Ok",
                        cancelButton: "Cancel",
                        onCancelTap: () {
                          Navigator.of(context).pop();
                        },
                        onConfirmTap: () {
                          Navigator.of(context).pop();
                        },
                      );
                    },
                  );
                } else {
                  interestUpdateApi(
                      AppInjector.resolve<AppPreferences>().getUserName(),
                      interestTypesSelectedId)
                      .then((value) {
                    if (value[0]) {
                      isUpdateLoader = false;

                      AppInjector.resolve<AppRoutes>()
                          .navigatorKey
                          .currentState!
                          .pop(context);
                    } else {
                      setState(() {
                        isUpdateLoader = false;
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AppWidgets.getAlertDialog(
                              title: "Alert",
                              content: value[1],
                              confirmButton: "Ok",
                              cancelButton: "Cancel",
                              onCancelTap: () {
                                Navigator.of(context).pop();
                              },
                              onConfirmTap: () {
                                Navigator.of(context).pop();
                              },
                            );
                          },
                        );
                      });
                    }
                  });
                }
              }),
          SizedBox(height: 40),
        ],
      ),
    );
  }

  Widget _getCategoryWidget(length, value, setModalState) {
    return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        "Select Category",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    InkWell(
                      focusColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        interestTypesSelectedName = [];
                        interestTypesSelectedId = [];
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pop();
                      },
                      child: Container(
                        width: 24,
                        height: 24,
                        child: Image.asset(
                          "assets/images/cancel_icon.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 16),
              ListView.builder(
                itemCount: length,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index1) {
                  return Container(
                      child: Column(
                        children: [
                          InkWell(
                            focusColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            onTap: () {
                              if (interestTypesSelectedId
                                  .contains(value[index1]["id"])) {
                                setModalState(() {
                                  interestTypesSelectedName
                                      .remove(value[index1]["name"]);
                                  interestTypesSelectedId.remove(value[index1]["id"]);
                                });
                              } else {
                                setModalState(() {
                                  interestTypesSelectedName.add(value[index1]["name"]);
                                  interestTypesSelectedId.add(value[index1]["id"]);
                                });
                              }
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                interestTypesSelectedName
                                    .contains(value[index1]['name'])
                                    ? Container(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                    "assets/images/single_square_select.png",
                                    fit: BoxFit.fill,
                                  ),
                                )
                                    : Container(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                    "assets/images/single_square_deselect.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  child: Text(
                                    value[index1]["name"],
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 12)
                        ],
                      ));
                },
              ),
              SizedBox(height: 16),
              AppWidgets.getWtaButton(
                  buttonName: "Done",
                  isLoader: false,
                  fontSize: 16,
                  width: double.infinity,
                  height: 48,
                  onPressed: () {
                    setState(() {
                      AppInjector.resolve<AppRoutes>()
                          .navigatorKey
                          .currentState!
                          .pop();
                    });
                  })
            ],
          ),
        ));
  }

  Iterable<Widget> get getInterestTypes sync* {
    for (int i = 0; i < interestTypesSelectedName.length; i++) {
      yield Row(
        children: [
          FilterChip(
            label: Row(
              children: [
                Text(
                  interestTypesSelectedName[i],
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Container(
                  width: 16,
                  height: 16,
                  child: Image.asset(
                    "assets/images/cancel_icon.png",
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
            // selected: interestTypesSelected[i],
            disabledColor: Color(0xff3a3a3c),
            backgroundColor: Color(0xff3a3a3c),
            selectedShadowColor: Color(0xff3a3a3c),
            shadowColor: Color(0xff3a3a3c),
            checkmarkColor: Color(0xff3a3a3c),
            shape: StadiumBorder(
                side: BorderSide(color: Color(0xff3a3a3c), width: 1)),
            showCheckmark: false,
            onSelected: (bool value) {
              setState(() {
                interestTypesSelectedName.removeAt(i);
                interestTypesSelectedId.removeAt(i);
              });
            },
          ),
          SizedBox(
            width: 5,
          ),
        ],
      );
    }
  }

  Future<List> interestUpdateApi(username, interests) async {
    setState(() {
      isUpdateLoader = true;
    });

    var response = await AppInjector.resolve<ApiService>().interestUpdate({
      "username": username,
      "interests": interests,
    });

    if (response.status == ApiResponseStatus.completed) {
      setState(() {
        isUpdateLoader = false;
      });
      return [true, ""];
    } else if (response.status == ApiResponseStatus.error) {
      print("response data init ${response.message[1]}");
      setState(() {
        isUpdateLoader = false;
      });
      return [false, response.message[1]];
    } else {
      setState(() {
        isUpdateLoader = false;
      });
      return [false, "Something went wrong.Please try later"];
    }
  }
}
