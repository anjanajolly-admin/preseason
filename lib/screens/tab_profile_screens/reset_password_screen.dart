import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class ResetPasswordScreen extends StatefulWidget {
  const ResetPasswordScreen({Key? key}) : super(key: key);
  static const String routeName = "/ResetPasswordScreen";

  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  final TextEditingController _currentPassword = TextEditingController();
  final TextEditingController _newPassword = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();

  bool isLoader = false;
  bool isObscureText = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
          title: Text(
            "Change Password",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        body: _getBody());
  }

  Widget _getBody() {
    return SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 16,
                ),
                Container(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Current Password",
                        style: TextStyle(
                          color: Color(0xfff2f2f7),
                          fontSize: 16,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: 4),
                      AppWidgets.getEditPasswordText(
                        isObscureText: isObscureText,
                        controller: _currentPassword,
                        onChanged: (value) {
                          // _password.text = value;
                        },
                        hint: " Password ",
                        inputFormatter: [],
                        suffixIcon: IconButton(
                            color: AppWidgets.titleColor4,
                            icon: Icon(isObscureText
                                ? Icons.visibility_off
                                : Icons.visibility),
                            onPressed: () {
                              setState(() {
                                isObscureText = !isObscureText;
                              });
                            }),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 16),
                Container(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "New Password",
                        style: TextStyle(
                          color: Color(0xfff2f2f7),
                          fontSize: 16,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: 4),
                      AppWidgets.getEditPasswordText(
                        isObscureText: isObscureText,
                        controller: _newPassword,
                        onChanged: (value) {
                          // _password.text = value;
                        },
                        hint: " Password ",
                        inputFormatter: [],
                        suffixIcon: IconButton(
                            color: AppWidgets.titleColor4,
                            icon: Icon(isObscureText
                                ? Icons.visibility_off
                                : Icons.visibility),
                            onPressed: () {
                              setState(() {
                                isObscureText = !isObscureText;
                              });
                            }),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 16),
                Container(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Confirmed Password",
                        style: TextStyle(
                          color: Color(0xfff2f2f7),
                          fontSize: 16,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: 4),
                      AppWidgets.getEditPasswordText(
                        isObscureText: isObscureText,
                        controller: _confirmPassword,
                        onChanged: (value) {
                          // _password.text = value;
                        },
                        hint: " Password ",
                        inputFormatter: [],
                        suffixIcon: IconButton(
                            color: AppWidgets.titleColor4,
                            icon: Icon(isObscureText
                                ? Icons.visibility_off
                                : Icons.visibility),
                            onPressed: () {
                              setState(() {
                                isObscureText = !isObscureText;
                              });
                            }),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 16),
                AppWidgets.getWtaButton(
                    isLoader: false,
                    buttonName: "Save",
                    fontSize: 16,
                    width: double.infinity,
                    height: 48,
                    onPressed: () {})
              ],
            )));
  }
}
