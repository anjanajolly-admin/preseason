import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_cubit.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

import 'edit_profile_screen.dart';
import 'friends_screen/my_friends_screen.dart';
import 'friends_screen/pending_invitation_screen.dart';
import 'manage_preferences_screen.dart';

class ProfileScreen extends StatefulWidget {
  static const String routeName = "/ProfileScreen";
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  List<Map<String, dynamic>?>? _userDetails;
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    _getUserDetails();
    super.initState();
  }

  Future<Null> refresh() async {
    await Future.delayed(Duration(seconds: 2));
    refreshKey.currentState?.show();
    _getUserDetails();
  }

  _getUserDetails() {
    AppInjector.resolve<ApiService>().getUserDetails().then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          _userDetails = [convert.jsonDecode(value.data)];
          print("User Details $_userDetails");
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          _userDetails = [];
        });
        BotToast.showText(text: value.message[1]);
      } else {
        setState(() {
          _userDetails = [];
        });
        BotToast.showText(text: "Something went wrong.Please try later");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          title: Text(
            "Profile",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        body: RefreshIndicator(
            displacement: 50,
            strokeWidth: 2,
            backgroundColor: Colors.white,
            color: AppWidgets.buttonTextColor,
            onRefresh: refresh,
            child: _getBody()));
  }

  Widget _getBody() {
    return _userDetails == null
        ? Center(
      child: Container(
          height: 30,
          width: 30,
          child: CircularProgressIndicator.adaptive(
              strokeWidth: 2,
              backgroundColor: AppWidgets.buttonTextColor,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white))),
    )
        : _userDetails!.isEmpty || _userDetails!.length == 0
        ? Container()
        : ListView(
      children: [
        Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 16,
                ),
                Container(
                  width: double.infinity,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              width: 100,
                              height: 100,
                              child:
                              _userDetails![0]!["profilePhoto"] ==
                                  null
                                  ? Image.asset(
                                "assets/images/profile_default_image.png",
                                fit: BoxFit.fill,
                              )
                                  : CachedNetworkImage(
                                imageUrl: _userDetails![0]![
                                "profilePhoto"]["url"],
                                fit: BoxFit.fill,
                                placeholder:
                                    (context, url) =>
                                    Image.asset(
                                      "assets/images/profile_default_image.png",
                                      fit: BoxFit.fill,
                                    ),

                                // placeholder: (context, url) =>
                                //     CircularProgressIndicator(),
                                errorWidget:
                                    (context, url, error) {
                                  print(
                                      'errorWidget ${error.toString()} \n$url');
                                  return Image.asset(
                                    "assets/images/profile_default_image.png",
                                    fit: BoxFit.fill,
                                  );
                                },
                              )),
                          // SizedBox(height: 8),
                          // SizedBox(
                          //   width: 107,
                          //   height: 20,
                          //   child: Text(
                          //     " Change Photo",
                          //     style: TextStyle(
                          //       color: Color(0xff5cabff),
                          //       fontSize: 16,
                          //       fontFamily: "Proxima Nova",
                          //       fontWeight: FontWeight.w600,
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 16),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0x1e000000),
                        blurRadius: 30,
                        offset: Offset(0, 10),
                      ),
                    ],
                    color: Color(0xff3a3a3c),
                  ),
                  padding: const EdgeInsets.only(
                      left: 16, right: 16, top: 16, bottom: 0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: double.infinity,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: double.infinity,
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: SizedBox(
                                      child: Text(
                                        " ${_userDetails![0]!["firstName"]} ${_userDetails![0]!["lastName"]}",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontFamily: "Proxima Nova",
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 16),
                      Container(
                        color: AppWidgets.titleColor5,
                        width: double.infinity,
                        height: 1,
                      ),
                      SizedBox(height: 16),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment:
                            MainAxisAlignment.start,
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Username",
                                style: TextStyle(
                                  color: Color(0xfff2f2f7),
                                  fontSize: 14,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          SizedBox(
                            width: double.infinity,
                            child: Text(
                              _userDetails![0]!["username"],
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 16),
                      Container(
                        color: AppWidgets.titleColor5,
                        width: double.infinity,
                        height: 1,
                      ),
                      SizedBox(height: 16),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment:
                            MainAxisAlignment.start,
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Email Address",
                                style: TextStyle(
                                  color: Color(0xfff2f2f7),
                                  fontSize: 14,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          SizedBox(
                            width: double.infinity,
                            child: Text(
                              _userDetails![0]!["email"],
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 16),
                      Container(
                        color: AppWidgets.titleColor5,
                        width: double.infinity,
                        height: 1,
                      ),
                      // SizedBox(height: 16),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment:
                          CrossAxisAlignment.center,
                          children: [
                            AppWidgets.getTextButton("Edit Profile",
                                AppWidgets.buttonTextColor, 14, () {
                                  print(_userDetails);
                                  AppInjector.resolve<AppRoutes>()
                                      .navigatorKey
                                      .currentState!
                                      .pushNamed(
                                      EditProfileScreen.routeName,
                                      arguments: [
                                        _userDetails![0]
                                      ]).then((value) {
                                    setState(() {
                                      _getUserDetails();
                                    });
                                  });
                                }),
                          ]),
                    ],
                  ),
                ),
                SizedBox(height: 16),
                _getOptionsCard("Managed Preferences", () {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pushNamed(ManagedPreferencesScreen.routeName,
                      arguments: [
                        _userDetails![0]!["interests"]
                      ]).then((value) {
                    setState(() {
                      _getUserDetails();
                    });
                  });
                }),
                SizedBox(height: 16),
                _getOptionsCard("My Friends", () {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pushNamed(MyFriendsScreen.routeName);
                }),
                SizedBox(height: 16),
                _getOptionsCard("Pending Requests", () {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pushNamed(PendingInvitationScreen.routeName);
                }),
                // SizedBox(height: 16),
                // _getOptionsCard("Change Password", () {
                //   AppInjector.resolve<AppRoutes>()
                //       .navigatorKey
                //       .currentState!
                //       .pushNamed(ResetPasswordScreen.routeName);
                // }),
                SizedBox(height: 16),
                InkWell(
                    focusColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    onTap: () {
                      // show the dialog
                      showDialog(
                        context: context,
                        builder: (context1) {
                          return AppWidgets.getAlertDialog(
                            title: "Alert",
                            content: "Do you want to Logout?",
                            confirmButton: "Logout",
                            cancelButton: "Cancel",
                            onCancelTap: () {
                              Navigator.of(context1).pop();
                            },
                            onConfirmTap: () {
                              Navigator.of(context1).pop();
                              setState(() {
                                context.read<BaseCubit>().logout();
                              });
                            },
                          );
                        },
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                            color: Color(0x1e000000),
                            blurRadius: 30,
                            offset: Offset(0, 10),
                          ),
                        ],
                        color: Color(0xff3a3a3c),
                      ),
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              children: [
                                Container(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                    "assets/images/logout_icon.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  child: SizedBox(
                                    child: Text(
                                      "Logout",
                                      style: TextStyle(
                                        color: Color(0xfff2f2f7),
                                        fontSize: 16,
                                        fontFamily: "Proxima Nova",
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )),
                SizedBox(height: 16),
              ],
            ))
      ],
    );
  }

  Widget _getOptionsCard(String title, onTap) {
    return InkWell(
        focusColor: Colors.transparent,
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Color(0x1e000000),
                blurRadius: 30,
                offset: Offset(0, 10),
              ),
            ],
            color: Color(0xff3a3a3c),
          ),
          padding: const EdgeInsets.all(16),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: SizedBox(
                        child: Text(
                          title,
                          style: TextStyle(
                            color: Color(0xfff2f2f7),
                            fontSize: 16,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    Container(
                      width: 24,
                      height: 24,
                      child: Image.asset(
                        "assets/images/chevron_right_icon.png",
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
