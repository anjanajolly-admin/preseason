import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class PendingInvitationScreen extends StatefulWidget {
  const PendingInvitationScreen({Key? key}) : super(key: key);
  static const String routeName = "/PendingInvitationScreen";

  @override
  _PendingInvitationScreenState createState() =>
      _PendingInvitationScreenState();
}

class _PendingInvitationScreenState extends State<PendingInvitationScreen> {
  bool pendingReqLoader = false;

  List? pendingRequestsList;
  var acceptedRequests = [];
  var canceledRequests = [];
  int selectedFriendIndex = -1;
  int deletingIndex = -1;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  @override
  void initState() {
    super.initState();
    setState(() {
      pendingReqLoader = true;
    });
    _pendingFriendRequests();
  }

  Future<void> _pendingFriendRequests() async {
    setState(() {
      pendingReqLoader = true;
    });
    await AppInjector.resolve<ApiService>()
        .pendingFriendRequests()
        .then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          pendingReqLoader = false;
          pendingRequestsList = convert.jsonDecode(value.data);

          print("Pending Requests $pendingRequestsList");
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          pendingRequestsList = [];
          pendingReqLoader = false;
        });
        BotToast.showText(text: value.message[1]);
      } else {
        setState(() {
          pendingRequestsList = [];
          pendingReqLoader = false;
        });
        BotToast.showText(text: "Something went wrong.Please try later");
      }
    });
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 2));
    refreshKey.currentState?.show();
    setState(() {
      _pendingFriendRequests();
      acceptedRequests.clear();
      canceledRequests.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        title: Text(
          "Pending Requests",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontFamily: "Proxima Nova",
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
      ),
      body: RefreshIndicator(
        displacement: 50,
        strokeWidth: 2,
        backgroundColor: Colors.white,
        color: AppWidgets.buttonTextColor,
        onRefresh: _refresh,
        child: _getBody(),
      ),
    );
  }

  Widget _getBody() {
    return Stack(
      children: [
        pendingReqLoader
            ? Positioned.fill(
          child: Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: SizedBox(
                        height: 50,
                        width: 50,
                        child: CircularProgressIndicator.adaptive(
                            strokeWidth: 2,
                            backgroundColor: AppWidgets.buttonTextColor,
                            valueColor: AlwaysStoppedAnimation<Color>(
                                Colors.white))),
                  )
                ],
              )),
        )
            : Container(),
        pendingRequestsList == null
            ? Container()
            : pendingRequestsList!.isEmpty
            ? Container(
          margin: EdgeInsets.only(left: 10, right: 10),
          height: double.infinity,
          width: double.infinity,
          child: Center(
            child: Text(
              ' You have no pending Friend requests',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white60,
                fontSize: 25,
                fontFamily: "Proxima Nova",
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        )
            : Container(
          margin: EdgeInsets.only(left: 20, right: 20, top: 20),
          child: ListView.builder(
              itemCount: pendingRequestsList!.length,
              // physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (BuildContext context, index) {
                return canceledRequests
                    .contains(pendingRequestsList![index]["id"])
                    ? Container()
                    : Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment:
                        MainAxisAlignment.start,
                        crossAxisAlignment:
                        CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            child: Image.asset(
                              "assets/images/default_profile_photo.png",
                              fit: BoxFit.fill,
                            ),
                          ),
                          SizedBox(width: 8),
                          Expanded(
                              child: Container(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment:
                                  MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        pendingRequestsList![index]
                                        ["firstName"] +
                                            " " +
                                            pendingRequestsList![
                                            index]["lastName"],
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14,
                                          fontFamily:
                                          "Proxima Nova",
                                          fontWeight:
                                          FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        pendingRequestsList![index]
                                        ["username"],
                                        style: TextStyle(
                                          color: Color(0xfff2f2f7),
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceEvenly,
                            children: [
                              InkWell(
                                focusColor: Colors.transparent,
                                highlightColor:
                                Colors.transparent,
                                hoverColor: Colors.transparent,
                                splashColor: Colors.transparent,
                                onTap: acceptedRequests.contains(
                                    pendingRequestsList![
                                    index]["id"])
                                    ? null
                                    : () {
                                  setState(() {
                                    selectedFriendIndex =
                                        index;
                                    acceptFriendRequestApi(
                                        userId: pendingRequestsList![
                                        index]
                                        ["id"])
                                        .then((value) {
                                      if (value[0]) {
                                        setState(() {
                                          selectedFriendIndex =
                                          -1;
                                        });
                                      } else {
                                        setState(() {
                                          selectedFriendIndex =
                                          -1;
                                          BotToast.showText(
                                              text:
                                              'Something went wrong');
                                        });
                                      }
                                    });
                                  });
                                },
                                child: Container(
                                  width: 70,
                                  height: 28,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                    BorderRadius.circular(
                                        4),
                                    color: Color(0xff0080ff),
                                  ),
                                  child: FittedBox(
                                    child: Padding(
                                      padding:
                                      const EdgeInsets.all(
                                          5.0),
                                      child: Row(
                                        mainAxisSize:
                                        MainAxisSize.min,
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment
                                            .center,
                                        children: [
                                          Text(
                                            acceptedRequests.contains(
                                                pendingRequestsList![
                                                index]
                                                ["id"])
                                                ? "Friends"
                                                : "Accept",
                                            textAlign: TextAlign
                                                .center,
                                            style: TextStyle(
                                              color:
                                              Colors.white,
                                              fontSize: 14,
                                              fontFamily:
                                              "Proxima Nova",
                                              fontWeight:
                                              FontWeight
                                                  .w600,
                                            ),
                                          ),
                                          selectedFriendIndex ==
                                              index
                                              ? Row(
                                            children: [
                                              SizedBox(
                                                width: 5,
                                              ),
                                              SizedBox(
                                                  height:
                                                  12,
                                                  width:
                                                  12,
                                                  child: CircularProgressIndicator.adaptive(
                                                      strokeWidth:
                                                      2,
                                                      backgroundColor:
                                                      AppWidgets.buttonTextColor,
                                                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white))),
                                            ],
                                          )
                                              : Container()
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              acceptedRequests.contains(
                                  pendingRequestsList![
                                  index]["id"])
                                  ? Container(
                                height: 0,
                                width: 0,
                              )
                                  : Row(
                                children: [
                                  SizedBox(width: 8),
                                  InkWell(
                                    focusColor: Colors
                                        .transparent,
                                    highlightColor: Colors
                                        .transparent,
                                    hoverColor: Colors
                                        .transparent,
                                    splashColor: Colors
                                        .transparent,
                                    onTap: () {
                                      setState(() {
                                        deletingIndex =
                                            index;
                                        deleteFriendRequestApi(
                                            userId: pendingRequestsList![
                                            index]
                                            [
                                            "id"])
                                            .then(
                                                (value) {
                                              if (value[0]) {
                                                setState(() {
                                                  deletingIndex =
                                                  -1;
                                                });
                                              } else {
                                                setState(() {
                                                  deletingIndex =
                                                  -1;
                                                  BotToast.showText(
                                                      text:
                                                      'Something went wrong');
                                                });
                                              }
                                            });
                                      });
                                    },
                                    child: Container(
                                      width: 70,
                                      height: 28,
                                      decoration:
                                      BoxDecoration(
                                        borderRadius:
                                        BorderRadius
                                            .circular(
                                            4),
                                        border:
                                        Border.all(
                                          color: Color(
                                              0xff5cabff),
                                          width: 1,
                                        ),
                                      ),
                                      child: Row(
                                        mainAxisSize:
                                        MainAxisSize
                                            .min,
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment
                                            .center,
                                        children: [
                                          Text(
                                            "Cancel",
                                            textAlign:
                                            TextAlign
                                                .center,
                                            style:
                                            TextStyle(
                                              color: Colors
                                                  .white,
                                              fontSize:
                                              14,
                                              fontFamily:
                                              "Proxima Nova",
                                              fontWeight:
                                              FontWeight
                                                  .w600,
                                            ),
                                          ),
                                          deletingIndex ==
                                              index
                                              ? Row(
                                            children: [
                                              SizedBox(
                                                width:
                                                5,
                                              ),
                                              SizedBox(
                                                  height: 12,
                                                  width: 12,
                                                  child: CircularProgressIndicator.adaptive(strokeWidth: 2, backgroundColor: AppWidgets.buttonTextColor, valueColor: AlwaysStoppedAnimation<Color>(Colors.white))),
                                            ],
                                          )
                                              : Container()
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 24,
                    )
                  ],
                );
              }),
        )
      ],
    );
  }

  Future<dynamic> acceptFriendRequestApi({
    required int userId,
  }) async {
    var response = await AppInjector.resolve<ApiService>().acceptFriendRequest({
      "friend": userId,
    });

    if (response.status == ApiResponseStatus.completed) {
      var jsonResponse =
      convert.jsonDecode(response.data) as Map<String, dynamic>;
      setState(() {
        acceptedRequests.add(userId);
      });
      return [true, jsonResponse];
    } else if (response.status == ApiResponseStatus.error) {
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }

  Future<dynamic> deleteFriendRequestApi({
    required int userId,
  }) async {
    setState(() {});

    var response =
    await AppInjector.resolve<ApiService>().deleteFriendRequest(userId);

    if (response.status == ApiResponseStatus.completed) {
      var jsonResponse =
      convert.jsonDecode(response.data) as Map<String, dynamic>;
      setState(() {
        canceledRequests.add(userId);
      });

      return [true, jsonResponse];
    } else if (response.status == ApiResponseStatus.error) {
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }
}
