import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/friends_screen/pending_invitation_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

import 'add_friends_screen.dart';
import 'friend_profile_view_screen.dart';

class MyFriendsScreen extends StatefulWidget {
  const MyFriendsScreen({Key? key}) : super(key: key);
  static const String routeName = "/MyFriendsScreen";

  @override
  _MyFriendsScreenState createState() => _MyFriendsScreenState();
}

class _MyFriendsScreenState extends State<MyFriendsScreen> {
  bool _friendsScreenLoader = false;
  List _allFriends = [];
  bool _noFriends = false;
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _getAllFriends();
    setState(() {
      _friendsScreenLoader = true;
    });
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 2));
    refreshKey.currentState?.show();
    setState(() {
      _getAllFriends();
    });
  }

  Future<void> _getAllFriends() async {
    AppInjector.resolve<ApiService>().getAllFriends().then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          _friendsScreenLoader = false;
          _allFriends = convert.jsonDecode(value.data);
          if (_allFriends.isEmpty) {
            _noFriends = true;
          }
          print("My Friends $_allFriends");
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          _friendsScreenLoader = false;
        });
        BotToast.showText(text: value.message[1]);
      } else {
        setState(() {
          _friendsScreenLoader = false;
        });
        BotToast.showText(text: "Something went wrong.Please try later");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          title: Text(
            "My Friends",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        body: RefreshIndicator(
            displacement: 50,
            strokeWidth: 2,
            backgroundColor: Colors.white,
            color: AppWidgets.buttonTextColor,
            onRefresh: _refresh,
            child: _getBody()));
  }

  Widget _getBody() {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x1e000000),
                    blurRadius: 30,
                    offset: Offset(0, 10),
                  ),
                ],
                color: Color(0xff3a3a3c),
              ),
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 8,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  AppWidgets.getMyFriendsCard(
                      leadingIcon: "assets/images/pending_friend_icon.png",
                      title: "Pending Requests",
                      onTap: () {
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pushNamed(PendingInvitationScreen.routeName);
                      }),
                  SizedBox(height: 8),
                  Container(
                    color: AppWidgets.titleColor5,
                    width: double.infinity,
                    height: 1,
                  ),
                  SizedBox(height: 8),
                  AppWidgets.getMyFriendsCard(
                      leadingIcon: "assets/images/add_friend_icon.png",
                      title: "Add Friends",
                      onTap: () {
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pushNamed(AddFriendsScreen.routeName);
                      }),
                ],
              ),
            ),
            SizedBox(height: 16),
            Stack(
              children: [
                _noFriends
                    ? Container(
                  padding: EdgeInsets.only(top: 30, left: 20, right: 20),
                  child: Center(
                    child: Column(
                      children: [
                        Text(
                          'No Friends yet ',
                          style: TextStyle(
                            color: Colors.white60,
                            fontSize: 16,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
                    : Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0x1e000000),
                        blurRadius: 30,
                        offset: Offset(0, 10),
                      ),
                    ],
                    color: Color(0xff3a3a3c),
                  ),
                  padding: const EdgeInsets.only(
                    left: 16,
                  ),
                  child: ListView.builder(
                    itemCount: _allFriends.length,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, myFriendIndex) {
                      return InkWell(
                          focusColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          hoverColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onTap: () {
                            AppInjector.resolve<AppRoutes>()
                                .navigatorKey
                                .currentState!
                                .pushNamed(
                                FriendProfileViewScreen.routeName,
                                arguments: [
                                  _allFriends[myFriendIndex]
                                ]);
                          },
                          child: Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Container(
                                    width: double.infinity,
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 8,
                                    ),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                      MainAxisAlignment.start,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        _allFriends[myFriendIndex]
                                        ['profilePhoto'] !=
                                            null
                                            ? CachedNetworkImage(
                                          height: 48,
                                          width: 48,
                                          imageUrl:
                                          _allFriends[myFriendIndex]
                                          ["profilePhoto"]
                                          ["url"],
                                          fit: BoxFit.fill,
                                          placeholder: (context, url) =>
                                              Image.asset(
                                                "assets/images/channel_cover.png",
                                                fit: BoxFit.fill,
                                              ),

                                          // placeholder: (context, url) =>
                                          //     CircularProgressIndicator(),
                                          errorWidget:
                                              (context, url, error) {
                                            print(
                                                'errorWidget ${error.toString()} \n$url');
                                            return Image.asset(
                                              "assets/images/channel_cover.png",
                                              fit: BoxFit.fill,
                                            );
                                          },
                                        )
                                            : Container(
                                          width: 48,
                                          height: 48,
                                          child: Image.asset(
                                            "assets/images/default_profile_photo.png",
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                        SizedBox(width: 16),
                                        Expanded(
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                width: double.infinity,
                                                child: Text(
                                                  _allFriends[myFriendIndex]
                                                  ["firstName"] +
                                                      " " +
                                                      _allFriends[
                                                      myFriendIndex]
                                                      ["lastName"],
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16,
                                                    fontFamily:
                                                    "Proxima Nova",
                                                    fontWeight:
                                                    FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(height: 4),
                                              SizedBox(
                                                width: double.infinity,
                                                child: Text(
                                                  _allFriends[myFriendIndex]
                                                  ["username"],
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(height: 16),
                                              myFriendIndex != 2
                                                  ? Container(
                                                color: AppWidgets
                                                    .titleColor5,
                                                width: double.infinity,
                                                height: 1,
                                              )
                                                  : Container(),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              )));
                    },
                  ),
                ),
                _friendsScreenLoader
                    ? Container(
                  padding: EdgeInsets.all(30),
                  child: Center(
                    child: CircularProgressIndicator.adaptive(
                        strokeWidth: 2,
                        backgroundColor: AppWidgets.buttonTextColor,
                        valueColor:
                        AlwaysStoppedAnimation<Color>(Colors.white)),
                  ),
                )
                    : Container()
              ],
            ),
          ],
        ),
      ),
    );
  }
}
