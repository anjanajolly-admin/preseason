import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/network/model/debounce.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

import 'friend_profile_view_screen.dart';

class AddFriendsScreen extends StatefulWidget {
  const AddFriendsScreen({Key? key}) : super(key: key);
  static const String routeName = "/AddFriendsScreen";

  @override
  _AddFriendsScreenState createState() => _AddFriendsScreenState();
}

class _AddFriendsScreenState extends State<AddFriendsScreen> {
  final TextEditingController _searchController = TextEditingController();
  bool isAddFriendLoading = false;
  List<dynamic>? friendsList;
  late String filter;
  final _debounce = Debounce();
  int selectedFriendIndex = -1;
  bool friendRequestSent = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  onTextChanged(String query) {
    _debounce.call(() {
      setState(() {
        isAddFriendLoading = true;
      });
      _searchUsers(query);
    });
  }

  _searchUsers(query) {
    // setState(() {
    //   isAddFriendLoading = true;
    // });
    AppInjector.resolve<ApiService>().searchUsers(query).then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          isAddFriendLoading = false;
          Map<String, dynamic> map = convert.jsonDecode(value.data);
          print("Map Data: ${map["data"]}");
          friendsList = map["data"];
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          isAddFriendLoading = false;
          BotToast.showText(text: value.message[1]);
        });
      } else {
        setState(() {
          isAddFriendLoading = false;
          BotToast.showText(text: "Something went wrong.Please try later");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          title: Text(
            "Add Friend",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        body: _getBody());
  }

  Widget _getBody() {
    return Stack(
      children: [
        SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(
                          color: Color(0xff464649),
                          width: 1,
                        ),
                        color: Color(0xff3a3a3c),
                      ),
                      child: TextField(
                        controller: _searchController,
                        onChanged: (String query) => onTextChanged(query),
                        cursorColor: Colors.white,
                        decoration: InputDecoration(
                          hintText: "Search by name, username or email",
                          hintStyle: TextStyle(
                            color: Color(0xffa4a4ab),
                            fontSize: 14,
                          ),
                          border: InputBorder.none,
                          prefixIcon: new IconButton(
                            icon: Container(
                              width: 24,
                              height: 24,
                              child: Image.asset(
                                "assets/images/search_icon.png",
                                fit: BoxFit.contain,
                              ),
                            ),
                            onPressed: null,
                          ),
                          suffixIcon: new IconButton(
                            icon: new Icon(
                              Icons.clear,
                              color: Color(0xffa4a4ab),
                            ),
                            onPressed: () {
                              setState(() {
                                _searchController.clear();
                              });
                            },
                          ),
                        ),
                        style: TextStyle(
                          color: Color(0xffa4a4ab),
                          fontSize: 16,
                        ),
                      )),
                  Container(
                    child: (friendsList == null)
                        ? Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(height: 64),
                              Container(
                                width: 180,
                                height: 180,
                                child: Image.asset(
                                  "assets/images/add_friends_empty_image.png",
                                  fit: BoxFit.fill,
                                ),
                              ),
                              SizedBox(height: 24),
                              Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "Add your Friends",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(height: 8),
                                  SizedBox(
                                    width: double.infinity,
                                    height: 60,
                                    child: Text(
                                      "You need to enter username to find your friends. Let’s start.",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )
                        : friendsList!.isEmpty
                            ? Container(
                                // padding: EdgeInsets.only(top: 50),
                                child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(height: 48),
                                  Text(
                                    "Could not find the Users with that search",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(height: 12),
                                  SizedBox(
                                    width: double.infinity,
                                    height: 60,
                                    child: Text(
                                      "Enter username to find your friends.",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                ],
                              ))
                            : Container(
                                child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: friendsList!.length,
                                    itemBuilder:
                                        (BuildContext context, addFriendIndex) {
                                      return InkWell(
                                        onTap: friendsList![addFriendIndex][
                                                        "isFriendRequestSent"] ==
                                                    true ||
                                                friendsList![addFriendIndex]
                                                        ["isFriend"] ==
                                                    true
                                            ? null
                                            : () {
                                                AppInjector.resolve<AppRoutes>()
                                                    .navigatorKey
                                                    .currentState!
                                                    .pushNamed(
                                                        FriendProfileViewScreen
                                                            .routeName,
                                                        arguments: [
                                                      friendsList![
                                                          addFriendIndex]
                                                    ]);
                                              },
                                        child: Container(
                                          padding: EdgeInsets.only(top: 20),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      height: 50,
                                                      width: 50,
                                                      child: Image.asset(
                                                        "assets/images/default_profile_photo.png",
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Expanded(
                                                        child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          friendsList![
                                                                      addFriendIndex]
                                                                  [
                                                                  "firstName"] +
                                                              " " +
                                                              friendsList![
                                                                      addFriendIndex]
                                                                  ["lastName"],
                                                          style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 16,
                                                            fontFamily:
                                                                "Proxima Nova",
                                                            fontWeight:
                                                                FontWeight.w600,
                                                          ),
                                                        ),
                                                        Text(
                                                          friendsList![
                                                                  addFriendIndex]
                                                              ["username"],
                                                          style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 14,
                                                            fontFamily:
                                                                "Proxima Nova",
                                                            fontWeight:
                                                                FontWeight.w600,
                                                          ),
                                                        ),
                                                      ],
                                                    ))
                                                  ],
                                                ),
                                              ),
                                              InkWell(
                                                  focusColor:
                                                      Colors.transparent,
                                                  highlightColor:
                                                      Colors.transparent,
                                                  hoverColor:
                                                      Colors.transparent,
                                                  splashColor:
                                                      Colors.transparent,
                                                  onTap: () {
                                                    setState(() {
                                                      friendsList![addFriendIndex]
                                                                      [
                                                                      "isFriendRequestSent"] ==
                                                                  true ||
                                                              friendsList![
                                                                          addFriendIndex]
                                                                      [
                                                                      "isFriend"] ==
                                                                  true
                                                          ? print("Tapped")
                                                          : setState(() {
                                                              sendPushNotification(
                                                                  "Sent from ${friendsList![addFriendIndex]["firstName"]} ${friendsList![addFriendIndex]["lastName"]}",
                                                                  "Friend Request",
                                                                  "",
                                                                  friendsList![
                                                                          addFriendIndex]
                                                                      [
                                                                      "occupant_id"]);
                                                              selectedFriendIndex =
                                                                  addFriendIndex;
                                                              sendFriendRequestApi(
                                                                      userId: friendsList![
                                                                              addFriendIndex]
                                                                          [
                                                                          "id"])
                                                                  .then(
                                                                      (value) {
                                                                if (value[0]) {
                                                                  setState(() {
                                                                    _searchUsers(
                                                                        _searchController
                                                                            .text);
                                                                    selectedFriendIndex =
                                                                        -1;
                                                                  });
                                                                } else {
                                                                  setState(() {
                                                                    selectedFriendIndex =
                                                                        -1;
                                                                    BotToast.showText(
                                                                        text:
                                                                            'Something went wrong');
                                                                  });
                                                                }
                                                              });
                                                            });
                                                    });
                                                  },
                                                  child: friendsList![addFriendIndex]["isFriendRequestSent"] ==
                                                          true
                                                      ? _button(
                                                          text: "Invited",
                                                          backgroundColor:
                                                              Color(0xff5cabff),
                                                          borderColor:
                                                              Color(0xff5cabff),
                                                          textColor:
                                                              Colors.white,
                                                          isLoader: selectedFriendIndex ==
                                                                  addFriendIndex
                                                              ? true
                                                              : false)
                                                      : friendsList![addFriendIndex]["isFriend"] ==
                                                              true
                                                          ? _button(
                                                              text: "Friends",
                                                              backgroundColor: Color(
                                                                  0xff5cabff),
                                                              borderColor: Color(
                                                                  0xff5cabff),
                                                              textColor:
                                                                  Colors.white,
                                                              isLoader: selectedFriendIndex == addFriendIndex
                                                                  ? true
                                                                  : false)
                                                          : _button(
                                                              text: "Add",
                                                              backgroundColor: AppWidgets.backgroundColor,
                                                              borderColor: Color(0xff5cabff),
                                                              textColor: Color(0xff5cabff),
                                                              isLoader: selectedFriendIndex == addFriendIndex ? true : false))
                                            ],
                                          ),
                                        ),
                                      );
                                    }),
                              ),
                  ),
                ],
              )),
        ),
        isAddFriendLoading
            ? Container(
                child: Center(
                  child: CircularProgressIndicator.adaptive(
                      strokeWidth: 2,
                      backgroundColor: AppWidgets.buttonTextColor,
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
                ),
              )
            : Container(),
      ],
    );
  }

  Widget _button(
      {required String text,
      required Color backgroundColor,
      required Color borderColor,
      required Color textColor,
      required bool isLoader}) {
    return Container(
      width: 90,
      height: 28,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(
          color: borderColor,
          width: 1,
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Text(
                    text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: textColor,
                      fontSize: 14,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                isLoader
                    ? SizedBox(
                        height: 14,
                        width: 14,
                        child: CircularProgressIndicator.adaptive(
                            strokeWidth: 2,
                            backgroundColor: AppWidgets.buttonTextColor,
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white)))
                    : Container()
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<dynamic> sendFriendRequestApi({
    required int userId,
  }) async {
    setState(() {});

    var response = await AppInjector.resolve<ApiService>().sentFriendRequest({
      "friend": userId,
    });

    if (response.status == ApiResponseStatus.completed) {
      var jsonResponse =
          convert.jsonDecode(response.data) as Map<String, dynamic>;

      return [true, jsonResponse];
    } else if (response.status == ApiResponseStatus.error) {
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }

  Future<void> sendPushNotification(String message, String notificationName,
      String senderName, List<String> recipientsIds) async {
    AppInjector.resolve<QuickbloxApiService>()
        .sendPushNotification(
            notificationName: notificationName,
            message: message,
            senderName: senderName,
            recipientsIds: recipientsIds,
            navigationObject: "",
            screen: "4")
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        setState(() {
          print('Successful sent');
        });
      } else {
        setState(() {
          print('Unsuccessful');
        });
      }
    });
  }
}
