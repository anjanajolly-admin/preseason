import 'dart:convert' as convert;
import 'dart:io';
import 'dart:math';

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class EditProfileScreen extends StatefulWidget {
  final Map<String, dynamic> userDetails;
  EditProfileScreen({required this.userDetails, Key? key}) : super(key: key);
  static const String routeName = "/EditProfileScreen";

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  final TextEditingController _firstName = TextEditingController();
  final TextEditingController _lastName = TextEditingController();

  bool _buttonLoader = false;
  File? _profileImageFile;
  bool isImageLoader = false;

  @override
  void initState() {
    super.initState();
    if (widget.userDetails != null) {
      editScreenInitialize();
    }
  }

  Future<void> editScreenInitialize() async {
    _firstName.text = widget.userDetails['firstName'];
    _lastName.text = widget.userDetails['lastName'];
    _profileImageFile = widget.userDetails["profilePhoto"] == null
        ? null
        : await urlToFile(widget.userDetails["profilePhoto"]["url"]);
  }

  Future<File> urlToFile(String imageUrl) async {
    var rng = new Random();
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File file = new File('$tempPath' + (rng.nextInt(100)).toString() + '.png');
    http.Response response = await http.get(Uri.parse(imageUrl));
    await file.writeAsBytes(response.bodyBytes);
    return file;
  }

  @override
  void dispose() {
    _firstName.dispose();
    _lastName.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          title: Text(
            "Edit Profile",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        body: _getBody());
  }

  Widget _getBody() {
    return SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(left: 20, right: 20, top: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      width: 100,
                      height: 100,
                      child: _profileImageFile != null
                          ? CircleAvatar(
                          radius: 80.0,
                          backgroundImage: FileImage(_profileImageFile!))
                          : widget.userDetails["profilePhoto"] == null
                          ? Image.asset(
                        "assets/images/profile_default_image.png",
                        fit: BoxFit.fill,
                      )
                          : CachedNetworkImage(
                        imageUrl: widget.userDetails["profilePhoto"]
                        ["url"],
                        fit: BoxFit.fill,
                        placeholder: (context, url) => Image.asset(
                          "assets/images/profile_default_image.png",
                          fit: BoxFit.fill,
                        ),

                        // placeholder: (context, url) =>
                        //     CircularProgressIndicator(),
                        errorWidget: (context, url, error) {
                          print('errorWidget ${error.toString()} \n$url');
                          return Image.asset(
                            "assets/images/profile_default_image.png",
                            fit: BoxFit.fill,
                          );
                        },
                      )),
                  SizedBox(height: 8),
                  InkWell(
                      onTap: () {
                        showModalBottomSheet(
                            isDismissible: false,
                            enableDrag: false,
                            context: context,
                            builder: ((builder) {
                              return Container(
                                color: AppWidgets.backgroundColor,
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.width * .3,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      'Choose Profile photo',
                                      style: TextStyle(
                                          color: AppWidgets.buttonTextColor,
                                          fontSize: 18),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: [
                                            IconButton(
                                                onPressed: () async {
                                                  isImageLoader = true;
                                                  Navigator.pop(context);
                                                  await _getProfileFromCamera()();
                                                },
                                                icon: Icon(
                                                  Icons.camera,
                                                  size: 36,
                                                  color: AppWidgets.buttonTextColor,
                                                )),
                                            SizedBox(
                                              height: 8,
                                            ),
                                            Text(
                                              'Camera',
                                              style: TextStyle(
                                                  color: AppWidgets.buttonTextColor,
                                                  fontSize: 16),
                                            )
                                          ],
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: [
                                            IconButton(
                                                onPressed: () {
                                                  isImageLoader = true;
                                                  Navigator.pop(context);
                                                  _getProfileFromGallery();
                                                },
                                                icon: Icon(
                                                  Icons.image,
                                                  size: 36,
                                                  color: AppWidgets.buttonTextColor,
                                                )),
                                            SizedBox(
                                              height: 8,
                                            ),
                                            Text(
                                              'Gallery',
                                              style: TextStyle(
                                                  color: AppWidgets.buttonTextColor,
                                                  fontSize: 16),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            }));
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            " Change Photo",
                            style: TextStyle(
                              color: Color(0xff5cabff),
                              fontSize: 16,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          isImageLoader
                              ? SizedBox(
                              height: 25,
                              width: 25,
                              child: CircularProgressIndicator.adaptive(
                                  strokeWidth: 2,
                                  backgroundColor: AppWidgets.buttonTextColor,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.white)))
                              : Container()
                        ],
                      )),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'First Name',
                    style: TextStyle(
                      color: Color(0xfff2f2f7),
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 4),
                  AppWidgets.getEditText(
                      controller: _firstName,
                      onChanged: (_) {},
                      keyboardType: TextInputType.name,
                      maxLength: 50,
                      hint: '',
                      inputFormatter: [
                        FilteringTextInputFormatter.deny(RegExp(" ")),
                      ],
                      isObscureText: false)
                ],
              ),
              SizedBox(height: 24),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Last name',
                    style: TextStyle(
                      color: Color(0xfff2f2f7),
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 4),
                  AppWidgets.getEditText(
                      controller: _lastName,
                      onChanged: (_) {},
                      keyboardType: TextInputType.name,
                      maxLength: 50,
                      hint: '',
                      inputFormatter: [
                        FilteringTextInputFormatter.deny(RegExp(" ")),
                      ],
                      isObscureText: false)
                ],
              ),
              SizedBox(
                height: 24,
              ),
              AppWidgets.getWtaButton(
                isLoader: _buttonLoader,
                buttonName: "Save",
                fontSize: 16,
                width: double.infinity,
                height: 48,
                onPressed: () {
                  if (_firstName.text.trim().isEmpty) {
                    BotToast.showText(text: 'First name cannot be empty');
                  } else if (_lastName.text.trim().isEmpty) {
                    BotToast.showText(text: 'Last name cannot be empty');
                  } else
                    updateUserDetails(_firstName.text, _lastName.text,
                        widget.userDetails["id"], _profileImageFile)
                        .then((value) {
                      if (value[0]) {
                        setState(() {
                          _buttonLoader = false;
                        });
                        BotToast.showText(text: 'Profile updated successfully');
                        Navigator.of(context).pop();
                      } else {
                        setState(() {
                          _buttonLoader = false;
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AppWidgets.getAlertDialog(
                                title: "Alert",
                                content: value[1],
                                confirmButton: "Ok",
                                cancelButton: "Cancel",
                                onCancelTap: () {
                                  Navigator.of(context).pop();
                                },
                                onConfirmTap: () {
                                  Navigator.of(context).pop();
                                },
                              );
                            },
                          );
                        });
                      }
                    });
                },
              ),
            ],
          ),
        ));
  }

  _getProfileFromGallery() async {
    var pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 640,
      maxHeight: 480,
    );
    if (pickedFile != null) {
      setState(() {
        _profileImageFile = File(pickedFile.path);
        isImageLoader = false;
      });
    }
  }

  /// Get from Camera
  _getProfileFromCamera() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 640,
      maxHeight: 480,
    );
    if (pickedFile != null) {
      setState(() {
        _profileImageFile = File(pickedFile.path);
        isImageLoader = false;
      });
    }
  }

  Future<dynamic> updateUserDetails(
      firstName, lastName, userId, profilePhoto) async {
    FormData formData = new FormData.fromMap({
      "data": convert.jsonEncode({
        "firstName": firstName,
        "lastName": lastName,
      }),
      'files.profilePhoto': (profilePhoto == null)
          ? null
          : await MultipartFile.fromFile(
        profilePhoto.path,
        filename: profilePhoto.path.split('/').last,
      )
    });
    setState(() {
      _buttonLoader = true;
    });

    var response = await ApiService().updateUserDetails(formData, userId);

    if (response.status == ApiResponseStatus.completed) {
      var jsonResponse = convert.jsonDecode(response.data);

      return [true, jsonResponse['id']];
    } else if (response.status == ApiResponseStatus.error) {
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }
}
