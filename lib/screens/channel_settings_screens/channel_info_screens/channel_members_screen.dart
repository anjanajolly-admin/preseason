import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class ChannelMembersScreen extends StatefulWidget {
  const ChannelMembersScreen({Key? key}) : super(key: key);
  static const String routeName = "/ChannelMembersScreen";

  @override
  _ChannelMembersScreenState createState() => _ChannelMembersScreenState();
}

class _ChannelMembersScreenState extends State<ChannelMembersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Channel Members",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        backgroundColor: AppWidgets.backgroundColor,
        body: _getBody());
  }

  Widget _getBody() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20, top: 20),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 48,
            height: 48,
            child: Image.asset(
              "assets/images/placeholder_contact.png",
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(width: 8),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: double.infinity,
                  child: Text(
                    "Aravind",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                SizedBox(height: 4),
                SizedBox(
                  width: double.infinity,
                  child: Text(
                    "Admin",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 8),
          Container(
            height: 28,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              border: Border.all(
                color: Color(0xff464649),
                width: 1,
              ),
            ),
            padding: const EdgeInsets.only(
              left: 11,
              right: 10,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Remove",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0xffff8686),
                    fontSize: 14,
                    fontFamily: "Proxima Nova",
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
