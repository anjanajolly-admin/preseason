import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/screens/create_channel_screens/create_channel_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class ChannelInfoScreen extends StatefulWidget {
  final int channelId;

  const ChannelInfoScreen({Key? key, required this.channelId})
      : super(key: key);
  static const String routeName = "/ChannelInfoScreen";

  @override
  _ChannelInfoScreenState createState() => _ChannelInfoScreenState();
}

class _ChannelInfoScreenState extends State<ChannelInfoScreen> {
  Map<String, dynamic>? channelInfoResponse;
  bool channelInfoLoader = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      channelInfoLoader = true;
      getChannelDetailsApi(widget.channelId);
    });
  }

  Future<void> getChannelDetailsApi(createChannelId) async {
    await AppInjector.resolve<ApiService>()
        .channelDetails(createChannelId)
        .then((value) {
      if (value.status == ApiResponseStatus.completed) {
        if (mounted)
          setState(() {
            channelInfoLoader = false;
            channelInfoResponse = convert.jsonDecode(value.data);
          });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          channelInfoLoader = false;
          BotToast.showText(text: value.message[1]);
        });
      } else {
        setState(() {
          channelInfoLoader = false;
        });
        return [false, "Something went wrong.Please try later"];
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => AppInjector.resolve<AppRoutes>()
                  .navigatorKey
                  .currentState!
                  .pop(context)),
          title: Text(
            "Channel Info",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        backgroundColor: AppWidgets.backgroundColor,
        body: _getBody());
  }

  Widget _getBody() {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width - 40,
                    height: 176,
                    child: Stack(
                      children: [
                        Container(
                            width: MediaQuery.of(context).size.width - 40,
                            height: 136,
                            child: channelInfoResponse == null
                                ? Container()
                                : channelInfoResponse!["coverPhoto"] == null
                                ? Image.asset(
                              "assets/images/channel_cover.png",
                              fit: BoxFit.fill,
                            )
                                : CachedNetworkImage(
                              imageUrl:
                              channelInfoResponse!["coverPhoto"]
                              ["url"],
                              fit: BoxFit.fill,
                              placeholder: (context, url) =>
                                  Image.asset(
                                    "assets/images/channel_cover.png",
                                    fit: BoxFit.fill,
                                  ),

                              // placeholder: (context, url) =>
                              //     CircularProgressIndicator(),
                              errorWidget: (context, url, error) {
                                print(
                                    'errorWidget ${error.toString()} \n$url');
                                return Image.asset(
                                  "assets/images/channel_cover.png",
                                  fit: BoxFit.fill,
                                );
                              },
                            )),
                        Positioned(
                          left: 20,
                          top: 96,
                          child: Container(
                            width: 80,
                            height: 80,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                color: Color(0xff2c2c2e),
                                width: 1,
                              ),
                            ),
                            child: channelInfoResponse == null
                                ? Container()
                                : channelInfoResponse!["profilePhoto"] != null
                                ? Container(
                                width: 80,
                                height: 80,
                                child: CachedNetworkImage(
                                  imageUrl: channelInfoResponse![
                                  "profilePhoto"]["url"],
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) =>
                                      Image.asset(
                                        "assets/images/channel_cricbuzz_icon.png",
                                        fit: BoxFit.fill,
                                      ),

                                  // placeholder: (context, url) =>
                                  //     CircularProgressIndicator(),
                                  errorWidget: (context, url, error) {
                                    print(
                                        'errorWidget ${error.toString()} \n$url');
                                    return Image.asset(
                                      "assets/images/channel_cricbuzz_icon.png",
                                      fit: BoxFit.fill,
                                    );
                                  },
                                ))
                                : Container(
                              width: 80,
                              height: 80,
                              child: Image.asset(
                                "assets/images/channel_cricbuzz_icon.png",
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 16),
                  // Container(
                  //   child: SizedBox(
                  //     child: Text(
                  //       " Change Photo",
                  //       style: TextStyle(
                  //         color: Color(0xff5cabff),
                  //         fontSize: 16,
                  //         fontFamily: "Proxima Nova",
                  //         fontWeight: FontWeight.w600,
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  // SizedBox(height: 16),
                  Container(
                    decoration: BoxDecoration(
                      color: Color(0xff1C1C1E),
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x1e000000),
                          blurRadius: 30,
                          offset: Offset(0, 10),
                        ),
                      ],
                    ),
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 16, right: 16, top: 16, bottom: 8),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Text(
                                      channelInfoResponse == null
                                          ? ""
                                          : channelInfoResponse!["name"],
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontFamily: "Proxima Nova",
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 16),
                            Container(
                              color: AppWidgets.titleColor5,
                              width: double.infinity,
                              height: 1,
                            ),
                            SizedBox(height: 16),
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Categories",
                                  style: TextStyle(
                                    color: Color(0xfff2f2f7),
                                    fontSize: 14,
                                    fontFamily: "Proxima Nova",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                SizedBox(height: 8),
                                SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    children: getCategoryList.toList(),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 16),
                            Container(
                              color: AppWidgets.titleColor5,
                              width: double.infinity,
                              height: 1,
                            ),
                            SizedBox(height: 16),
                            Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Channel Bio",
                                    style: TextStyle(
                                      color: Color(0xfff2f2f7),
                                      fontSize: 14,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  SizedBox(height: 8),
                                  SizedBox(
                                    child: Text(
                                      channelInfoResponse == null
                                          ? ""
                                          : channelInfoResponse!['bio'] == null
                                          ? ""
                                          : channelInfoResponse!['bio'],
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 16),
                            Container(
                              color: AppWidgets.titleColor5,
                              width: double.infinity,
                              height: 1,
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  AppWidgets.getTextButton("Edit Channel",
                                      AppWidgets.buttonTextColor, 14, () {
                                        AppInjector.resolve<AppRoutes>()
                                            .navigatorKey
                                            .currentState!
                                            .pushNamed(
                                            CreateChannelScreen.routeName,
                                            arguments: [
                                              channelInfoResponse
                                            ]).then((channelInfoResponse) {
                                          setState(() {
                                            getChannelDetailsApi(widget.channelId);
                                          });
                                        });
                                      })
                                ],
                              ),
                            )
                          ],
                        )),
                  ),
                  SizedBox(height: 16),
                  InkWell(
                      focusColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        // AppInjector.resolve<AppRoutes>()
                        //     .navigatorKey
                        //     .currentState!
                        //     .pushNamed(ChannelMembersScreen.routeName);
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Color(
                            0xff1c1c1e,
                          ),
                          borderRadius: BorderRadius.circular(
                            8,
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Color(
                                0x1e000000,
                              ),
                              offset: Offset(
                                0,
                                10,
                              ),
                              blurRadius: 30,
                            ),
                          ],
                        ),
                        child: Container(
                          margin: EdgeInsets.all(12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                width: 24,
                                height: 24,
                                child: Image.asset(
                                  "assets/images/members_channel_info_icon.png",
                                  fit: BoxFit.fill,
                                ),
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Expanded(
                                child: Text(
                                  "Members",
                                  style: TextStyle(
                                    color: Color(
                                      0xfff2f2f7,
                                    ),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "Proxima Nova",
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Container(
                                child: Text(
                                  channelInfoResponse == null
                                      ? ""
                                      : "${channelInfoResponse!["membersCount"]} members",
                                  style: TextStyle(
                                    color: Color(
                                      0xfff2f2f7,
                                    ),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "Proxima Nova",
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              // Container(
                              //     width: 24,
                              //     height: 24,
                              //     child: Icon(Icons.keyboard_arrow_right,
                              //         color: Colors.white))
                            ],
                          ),
                        ),
                      )),
                  SizedBox(height: 16),
                ],
              )),
          channelInfoLoader
              ? Positioned.fill(
            child: Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: SizedBox(
                          height: 50,
                          width: 50,
                          child: CircularProgressIndicator.adaptive(
                              strokeWidth: 2,
                              backgroundColor: AppWidgets.buttonTextColor,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Colors.white))),
                    )
                  ],
                )),
          )
              : Container()
        ],
      ),
    );
  }

  Iterable<Widget> get getCategoryList sync* {
    if (channelInfoResponse != null) {
      for (int i = 0; i < channelInfoResponse!['categories'].length; i++) {
        yield Row(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: Color(0xff3a3a3c),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Color(0xff3a3a3c),
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 12,
                      vertical: 5,
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          channelInfoResponse!['categories'][i]['name'],
                          style: TextStyle(
                            color: Color(0xffa4a4ab),
                            fontSize: 12,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 5,
            ),
          ],
        );
      }
    }
  }
}
