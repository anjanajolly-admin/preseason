import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/screens/channel_settings_screens/role_management_screen/edit_role_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class AddRoleManagement extends StatefulWidget {
  const AddRoleManagement({Key? key}) : super(key: key);
  static const String routeName = "/AddRoleManagement";

  @override
  _AddRoleManagementState createState() => _AddRoleManagementState();
}

class _AddRoleManagementState extends State<AddRoleManagement> {
  final TextEditingController _userName = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Add Channel Role",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        backgroundColor: AppWidgets.backgroundColor,
        body: _getBody());
  }

  Widget _getBody() {
    return Container(
        margin: EdgeInsets.only(left: 20, right: 20, top: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Enter username",
                    style: TextStyle(
                      color: Color(0xfff2f2f7),
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 4),
                  AppWidgets.getEditText(
                    controller: _userName,
                    keyboardType: TextInputType.name,
                    maxLength: 100,
                    hint: "Type the name or username",
                    inputFormatter: [],
                    isObscureText: false,
                    onChanged: (value) {},
                  )
                ],
              ),
            ),
            SizedBox(height: 16),
            Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Select Role",
                    style: TextStyle(
                      color: Color(0xfff2f2f7),
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 4),
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    width: double.infinity,
                    height: 48,
                    decoration: BoxDecoration(
                      color: AppWidgets.backgroundColor,
                      border: Border.all(
                        color: AppWidgets.titleColor4,
                        width: 1,
                      ), // set border width
                      borderRadius: BorderRadius.all(
                          Radius.circular(8.0)), // set rounded corner radius
                    ),
                    child: InkWell(
                        focusColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        onTap: () {
                          showModalBottomSheet(
                              isDismissible: false,
                              enableDrag: false,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              backgroundColor: AppWidgets.backgroundColor,
                              context: context,
                              builder: (context) {
                                return StatefulBuilder(builder:
                                    (BuildContext context,
                                        StateSetter
                                            setState /*You can rename this!*/) {
                                  return getPopupWidget();
                                });
                              });
                        },
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                "",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 24,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(right: 8),
                              width: 24,
                              height: 24,
                              child: Image.asset(
                                "assets/images/message_icon.png",
                                fit: BoxFit.fill,
                              ),
                            ),
                          ],
                        )),
                  ),
                ],
              ),
            ),
            Spacer(),
            AppWidgets.getWtaButton(
                buttonName: "Assign Role",
                fontSize: 16,
                width: double.infinity,
                height: 48,
                isLoader: false,
                onPressed: () {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pushNamed(EditRoleManagement.routeName);
                }),
            SizedBox(
              height: 20,
            )
          ],
        ));
  }

  Widget getPopupWidget() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
          bottomLeft: Radius.circular(0),
          bottomRight: Radius.circular(0),
        ),
        color: Color(0xff3a3a3c),
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 24,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Text(
                    "Select Role",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Container(
                  width: 24,
                  height: 24,
                  child: Image.asset(
                    "assets/images/cancel_icon.png",
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 16),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 24,
                      height: 24,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: FlutterLogo(size: 24),
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      child: SizedBox(
                        child: Text(
                          "Admin (Channel access with full control)",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 16),
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 24,
                      height: 24,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: FlutterLogo(size: 24),
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        "Editor (Channel access with partial control)",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 16),
          AppWidgets.getWtaButton(
              buttonName: "Done",
              fontSize: 16,
              width: double.infinity,
              height: 48,
              isLoader: false,
              onPressed: () {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pop();
              })
        ],
      ),
    );
  }
}
