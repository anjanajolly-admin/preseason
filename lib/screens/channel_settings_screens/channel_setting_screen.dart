import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/screens/channel_settings_screens/channel_info_screens/channel_info_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class ChannelSettingsScreen extends StatefulWidget {
  final int channelId;

  const ChannelSettingsScreen({
    Key? key,
    required this.channelId,
  }) : super(key: key);
  static const String routeName = "/ChannelSettingsScreen";

  @override
  _ChannelSettingsScreenState createState() => _ChannelSettingsScreenState();
}

class _ChannelSettingsScreenState extends State<ChannelSettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => AppInjector.resolve<AppRoutes>()
                  .navigatorKey
                  .currentState!
                  .pop(context)),
          title: Text(
            "Channel Settings",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
        ),
        backgroundColor: AppWidgets.backgroundColor,
        body: _getBody());
  }

  Widget _getBody() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20, top: 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 8,
            ),
            child: InkWell(
              focusColor: Colors.transparent,
              highlightColor: Colors.transparent,
              hoverColor: Colors.transparent,
              splashColor: Colors.transparent,
              onTap: () {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamed(ChannelInfoScreen.routeName, arguments: [
                  widget.channelId,
                ]).then((value) {
                  setState(() {});
                });
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 24,
                    height: 24,
                    child: Image.asset(
                      "assets/images/channel_info_icon.png",
                      fit: BoxFit.fill,
                    ),
                  ),
                  SizedBox(width: 16),
                  Expanded(
                    child: Text(
                      "Channel Info",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontFamily: "Proxima Nova",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(width: 16),
                  Container(
                      width: 24,
                      height: 24,
                      child: Icon(Icons.keyboard_arrow_right,
                          color: Colors.white)),
                ],
              ),
            ),
          ),
          // SizedBox(height: 16),
          // Padding(
          //   padding: const EdgeInsets.symmetric(
          //     vertical: 8,
          //   ),
          //   child: InkWell(
          //       focusColor: Colors.transparent,
          //       highlightColor: Colors.transparent,
          //       hoverColor: Colors.transparent,
          //       splashColor: Colors.transparent,
          //       onTap: () {
          //         // AppInjector.resolve<AppRoutes>()
          //         //     .navigatorKey
          //         //     .currentState!
          //         //     .pushNamed(RoleManagementScreen.routeName);
          //       },
          //       child: Row(
          //         mainAxisSize: MainAxisSize.min,
          //         mainAxisAlignment: MainAxisAlignment.center,
          //         crossAxisAlignment: CrossAxisAlignment.center,
          //         children: [
          //           Container(
          //             width: 24,
          //             height: 24,
          //             child: Image.asset(
          //               "assets/images/role_management_icon.png",
          //               fit: BoxFit.fill,
          //             ),
          //           ),
          //           SizedBox(width: 16),
          //           Expanded(
          //             child: Text(
          //               "Role Management",
          //               style: TextStyle(
          //                 color: Colors.white,
          //                 fontSize: 16,
          //                 fontFamily: "Proxima Nova",
          //                 fontWeight: FontWeight.w600,
          //               ),
          //             ),
          //           ),
          //           SizedBox(width: 16),
          //           Container(
          //               width: 24,
          //               height: 24,
          //               child: Icon(Icons.keyboard_arrow_right,
          //                   color: Colors.white)),
          //         ],
          //       )),
          // ),
        ],
      ),
    );
  }
}
