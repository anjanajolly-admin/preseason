import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class CreateThreadScreen extends StatefulWidget {
  dynamic channelId;
  dynamic topicId;
  dynamic topicName;
  CreateThreadScreen(
      {Key? key,
        required this.channelId,
        required this.topicId,
        required this.topicName})
      : super(key: key);
  static const String routeName = '/CreateThreadScreen';

  @override
  _CreateThreadScreenState createState() => _CreateThreadScreenState();
}

class _CreateThreadScreenState extends State<CreateThreadScreen> {
  final TextEditingController _threadName = TextEditingController();
  final TextEditingController _threadBio = TextEditingController();
  final TextEditingController _topicName = TextEditingController();

  String expiryTime = "";
  String expiryDate = "";
  String _selectedDate = "";

  List listOfTopics = [];
  String topicName = "";
  int topicId = 0;
  bool isCreateThreadLoader = false;
  bool isCreateTopicLoader = false;
  bool allFilled = false;
  dynamic threadDetails;

  @override
  void initState() {
    super.initState();
    if (widget.topicId > 0) {
      topicName = widget.topicName;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff2c2c2e),
      appBar: AppBar(
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => AppInjector.resolve<AppRoutes>()
                .navigatorKey
                .currentState!
                .pop(context)),
        title: Text(
          "Create Thread",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontFamily: "Proxima Nova",
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Container(
              width: 24,
              height: 24,
              child: Image.asset(
                "assets/images/info_filled_gray.png",
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
      ),
      body: InkWell(
        focusColor: Colors.transparent,
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: () {
          AppConstants.hideKeyboard(context);
        },
        child: SingleChildScrollView(
          child: _getBody(),
        ),
      ),
    );
  }

  Widget _getBody() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Topic Name",
                    style: TextStyle(
                        fontFamily: 'Proxima Nova',
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  InkWell(
                    focusColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    onTap: () {
                      widget.topicId > 0
                          ? print("Nothing is possible")
                          : setState(() {
                        showModalBottomSheet(
                            isDismissible: false,
                            enableDrag: false,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            backgroundColor: AppWidgets.backgroundColor,
                            context: context,
                            builder: (context) {
                              return StatefulBuilder(builder: (BuildContext
                              context,
                                  StateSetter
                                  setSelectTopicState /*You can rename this!*/) {
                                return _getTopicName(setSelectTopicState);
                              });
                            });
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: 10),
                      width: double.infinity,
                      height: 48,
                      decoration: BoxDecoration(
                        color: widget.topicId > 0
                            ? AppWidgets.titleColor5
                            : AppWidgets.backgroundColor,
                        border: Border.all(
                          color: AppWidgets.titleColor4,
                          width: 1,
                        ), // set border width
                        borderRadius: BorderRadius.all(
                            Radius.circular(8.0)), // set rounded corner radius
                      ),
                      child: Container(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Text(
                                  topicName,
                                  style: TextStyle(
                                    color: widget.topicId > 0
                                        ? AppWidgets.titleColor3
                                        : Colors.white,
                                    fontSize: 16,
                                    fontFamily: "Proxima Nova",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              widget.topicId > 0
                                  ? Container()
                                  : Container(
                                width: 24,
                                height: 24,
                                child: Image.asset(
                                  "assets/images/chevron_down_icon.png",
                                  fit: BoxFit.fill,
                                ),
                              ),
                              SizedBox(
                                width: 12,
                              )
                            ]),
                      ),
                    ),
                  )
                ],
              )),
          SizedBox(
            height: 16,
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Thread Name",
                  style: TextStyle(
                    color: Color(0xfff2f2f7),
                    fontSize: 16,
                    fontFamily: "Proxima Nova",
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 4),
                AppWidgets.getEditText(
                    controller: _threadName,
                    onChanged: (threadName) {},
                    keyboardType: TextInputType.text,
                    maxLength: 100,
                    hint: "",
                    inputFormatter: [],
                    isObscureText: false),
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Thread Bio",
                  style: TextStyle(
                    color: Color(0xfff2f2f7),
                    fontSize: 16,
                    fontFamily: "Proxima Nova",
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 4),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        child: Container(
                          height: 120,
                          padding: EdgeInsets.only(left: 10, right: 10),
                          decoration: BoxDecoration(
                            color: AppWidgets.backgroundColor,
                            border: Border.all(
                              color: AppWidgets.titleColor4,
                              width: 1,
                            ), // set border width
                            borderRadius: BorderRadius.all(
                                Radius.circular(8.0)), // set rounded corner radius
                          ),
                          child: TextField(
                            controller: _threadBio,
                            inputFormatters: [],
                            obscureText: false,
                            onChanged: (channelBio) {},
                            maxLines: null,
                            style: TextStyle(
                              color: AppWidgets.titleColor3,
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              fontFamily: "Proxima Nova",
                            ),
                            decoration: InputDecoration(
                              hintText: "",
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                color: AppWidgets.titleColor3,
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                fontFamily: "Proxima Nova",
                              ),
                            ),
                          ),
                        )),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Color(0xff3a3a3c),
            ),
            padding: const EdgeInsets.all(12),
            child: Column(children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'AUTO DELETE',
                      style: TextStyle(
                          fontFamily: 'Proxima Nova',
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Container(
                      width: 16,
                      height: 16,
                      child: Image.asset(
                        "assets/images/info_filled_white.png",
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                child:
                /* Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [*/
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Date",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          color: Color(0xfff2f2f7),
                          fontSize: 16,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: 4),
                      InkWell(
                        focusColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        onTap: () {
                          List listOfDates = new List<DateTime>.generate(7,
                                  (i) => DateTime.now().add(Duration(days: i + 1)));
                          print(listOfDates);
                          setState(() {
                            showModalBottomSheet(
                                isDismissible: false,
                                enableDrag: false,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                backgroundColor: AppWidgets.backgroundColor,
                                context: context,
                                builder: (context) {
                                  return StatefulBuilder(builder: (BuildContext
                                  context,
                                      StateSetter
                                      setModalState /*You can rename this!*/) {
                                    return _getDateWidget(
                                        listOfDates, setModalState);
                                  });
                                });
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 10),
                          // width: MediaQuery.of(context).size.width / 2 - 36,
                          width: MediaQuery.of(context).size.width,

                          height: 48,
                          decoration: BoxDecoration(
                            color: Color(0xff3a3a3c),
                            border: Border.all(
                              color: AppWidgets.titleColor4,
                              width: 1,
                            ), // set border width
                            borderRadius: BorderRadius.all(Radius.circular(
                                8.0)), // set rounded corner radius
                          ),
                          child: Container(
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Text(
                                      expiryDate,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontFamily: "Proxima Nova",
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 24,
                                    height: 24,
                                    child: Image.asset(
                                      "assets/images/chevron_down_icon.png",
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 12,
                                  )
                                ]),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                /* Spacer(),
                    SizedBox(
                      width: 8,
                    ),
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Time",
                            style: TextStyle(
                              color: Color(0xfff2f2f7),
                              fontSize: 16,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 4),
                          InkWell(
                            focusColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            onTap: () {
                              _selectTime(context);
                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 10),
                              width: MediaQuery.of(context).size.width / 2 - 36,
                              height: 48,
                              decoration: BoxDecoration(
                                color: Color(0xff3a3a3c),
                                border: Border.all(
                                  color: AppWidgets.titleColor4,
                                  width: 1,
                                ), // set border width
                                borderRadius: BorderRadius.all(Radius.circular(
                                    8.0)), // set rounded corner radius
                              ),
                              child: Container(
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        child: Text(
                                          expiryTime,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontFamily: "Proxima Nova",
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 24,
                                        height: 24,
                                        child: Image.asset(
                                          "assets/images/chevron_down_icon.png",
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 12,
                                      )
                                    ]),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Spacer(),*/
                // ],
                //),
              ),
            ]),
          ),
          SizedBox(
            height: 16,
          ),
          topicName.isEmpty ||
              _threadName.text.trim().isEmpty ||
              _threadBio.text.trim().isEmpty ||
              expiryDate.isEmpty
          // expiryTime.isEmpty
              ? AppWidgets.getButton(
              buttonName: "Create",
              isLoader: isCreateThreadLoader,
              fontSize: 16,
              width: double.infinity,
              height: 48,
              color: AppWidgets.titleColor5,
              textColor: AppWidgets.titleColor3,
              onPressed: () {
                topicName.trim().isEmpty || topicName.trim() == ""
                    ? BotToast.showText(text: "Topic Name cant be empty")
                    : _threadName.text.trim() == "" ||
                    _threadName.text.trim().isEmpty
                    ? BotToast.showText(
                    text: "Thread Name cant be empty")
                    : _threadBio.text.trim() == "" ||
                    _threadBio.text.trim().isEmpty
                    ? BotToast.showText(
                    text: "Thread Bio cant be empty")
                    : expiryDate.isEmpty || expiryDate == ""
                    ? BotToast.showText(
                    text: "Expiry Date cant be empty")
                /*: expiryTime.isEmpty || expiryTime == ""
                                        ? BotToast.showText(
                                            text: "Expiry Time cant be empty")*/
                    : print("");
              })
              : AppWidgets.getWtaButton(
              buttonName: "Create",
              fontSize: 16,
              width: double.infinity,
              height: 48,
              isLoader: isCreateThreadLoader,
              onPressed: () {
                DateTime now = DateTime.now();
                var time =
                ("${now.hour.toString().padLeft(2, '0')}:${now.minute.toString().padLeft(2, '0')}:${now.second.toString().padLeft(2, '0')}")
                    .split(":");

                String _expiryDate =
                    '${_selectedDate}T${time[0].trim()}:${time[1].trim()}:00.000Z';
                print('dateTimex $_expiryDate');
                setState(() {
                  createThreadApi(
                      threadName: _threadName.text,
                      threadBio: _threadBio.text,
                      topicId:
                      widget.topicId > 0 ? widget.topicId : topicId,
                      channelId: widget.channelId,
                      expirationTime: _expiryDate)
                      .then((value) {
                    if (value[0]) {
                      isCreateThreadLoader = false;
                      showModalBottomSheet(
                          isDismissible: false,
                          enableDrag: false,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          backgroundColor: AppWidgets.backgroundColor,
                          context: context,
                          builder: (context) {
                            return _autoDeleteInfoCard();
                          });
                    } else {
                      setState(() {
                        isCreateThreadLoader = false;
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AppWidgets.getAlertDialog(
                              title: "Alert",
                              content: value[1],
                              confirmButton: "Ok",
                              cancelButton: "Cancel",
                              onCancelTap: () {
                                Navigator.of(context).pop();
                              },
                              onConfirmTap: () {
                                Navigator.of(context).pop();
                              },
                            );
                          },
                        );
                      });
                    }
                  });
                });
              }),
          SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? pickedTime = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
        builder: (BuildContext context, Widget? child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
            child: child!,
          );
        });
    if (pickedTime != null)
      setState(() {
        print(pickedTime);
        expiryTime =
        "${pickedTime.hour.toString().length == 1 ? "0${pickedTime.hour}" : "${pickedTime.hour}"} : ${pickedTime.minute.toString().length == 1 ? "0${pickedTime.minute}" : "${pickedTime.minute}"} ${pickedTime.period.index == 0 ? "AM" : "PM"}";
      });
  }

  Widget _getDateWidget(listOfDates, setModalState) {
    return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        "Select Date",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    InkWell(
                      focusColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pop();
                      },
                      child: Container(
                        width: 24,
                        height: 24,
                        child: Image.asset(
                          "assets/images/cancel_icon.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 16),
              ListView.builder(
                itemCount: 7,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                reverse: true,
                itemBuilder: (context, dateIndex) {
                  String _dateItem =
                      "${dateIndex + 1} ${dateIndex + 1 == 1 ? "day" : "days"} (${(listOfDates[dateIndex] as DateTime).day} ${DateFormat.MMM().format(listOfDates[dateIndex])})";

                  return Container(
                      child: Column(
                        children: [
                          InkWell(
                            focusColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            onTap: () {
                              setState(() {
                                expiryDate =
                                "${dateIndex + 1} ${dateIndex + 1 == 1 ? "day" : "days"} (${(listOfDates[dateIndex] as DateTime).day} ${DateFormat.MMM().format(listOfDates[dateIndex])})";
                                _selectedDate = DateFormat("yyyy-MM-dd")
                                    .format(listOfDates[dateIndex]);
                              });
                              setModalState(() {});
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                _dateItem == expiryDate
                                    ? Container(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                    "assets/images/single_circle_select.png",
                                    fit: BoxFit.fill,
                                  ),
                                )
                                    : Container(
                                  width: 24,
                                  height: 24,
                                  child: Image.asset(
                                    "assets/images/single_circle_deselect.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  child: Text(
                                    _dateItem,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 12)
                        ],
                      ));
                },
              ),
              SizedBox(height: 16),
              AppWidgets.getWtaButton(
                  buttonName: "Done",
                  isLoader: false,
                  fontSize: 16,
                  width: double.infinity,
                  height: 48,
                  onPressed: () {
                    setState(() {
                      AppInjector.resolve<AppRoutes>()
                          .navigatorKey
                          .currentState!
                          .pop();
                    });
                  })
            ],
          ),
        ));
  }

  Widget _getTopicName(
      setSelectTopicState,
      ) {
    return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        "Select Topic",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    InkWell(
                      focusColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          topicName = "";
                        });
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pop();
                      },
                      child: Container(
                        width: 24,
                        height: 24,
                        child: Image.asset(
                          "assets/images/cancel_icon.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 16),
              listOfTopics.isEmpty
                  ? Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                        child: Text(
                          "There are no topics available. Create new topic.",
                          style: TextStyle(
                            color: Color(0xfff2f2f7),
                            fontSize: 14,
                          ),
                        )),
                  ])
                  : Container(
                  child: ListView.builder(
                      itemCount: listOfTopics.length,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (context, topicsIndex) {
                        return InkWell(
                            focusColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            onTap: () {
                              setState(() {
                                topicName = listOfTopics[topicsIndex];
                              });
                              setSelectTopicState(() {});
                            },
                            child: Container(
                              child: Column(
                                children: [
                                  Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.start,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      children: [
                                        topicName == listOfTopics[topicsIndex]
                                            ? Container(
                                          width: 24,
                                          height: 24,
                                          child: Image.asset(
                                            "assets/images/single_circle_select.png",
                                            fit: BoxFit.fill,
                                          ),
                                        )
                                            : Container(
                                          width: 24,
                                          height: 24,
                                          child: Image.asset(
                                            "assets/images/single_circle_deselect.png",
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                        SizedBox(width: 16),
                                        Expanded(
                                          child: Text(
                                            listOfTopics[topicsIndex],
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontFamily: "Proxima Nova",
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                      ]),
                                  SizedBox(
                                    height: 8,
                                  )
                                ],
                              ),
                            ));
                      })),
              SizedBox(height: 16),
              Container(
                  width: double.infinity, height: 1, color: AppWidgets.titleColor5),
              SizedBox(height: 16),
              Container(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 24,
                          height: 24,
                          child: Image.asset(
                            "assets/images/plus_blue_icon.png",
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(width: 16),
                        AppWidgets.getTextButton(
                            "Create Topic", AppWidgets.buttonTextColor, 16, () {
                          setSelectTopicState(() {
                            showModalBottomSheet(
                                isDismissible: false,
                                enableDrag: false,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                backgroundColor: AppWidgets.backgroundColor,
                                context: context,
                                isScrollControlled: true,
                                builder: (context) {
                                  return StatefulBuilder(builder: (BuildContext context,
                                      StateSetter setCreateTopicState) {
                                    return _createTopicName(setCreateTopicState);
                                  });
                                }).then((value) => setSelectTopicState(() {
                              setState(() {
                                topicName = _topicName.text;
                              });
                            }));
                          });
                        }),
                      ])),
              listOfTopics.isEmpty
                  ? Container()
                  : Column(children: [
                SizedBox(
                  height: 16,
                ),
                AppWidgets.getWtaButton(
                    buttonName: "Done",
                    isLoader: isCreateTopicLoader,
                    fontSize: 16,
                    width: double.infinity,
                    height: 48,
                    onPressed: () {
                      setSelectTopicState(() {
                        createTopicApi(
                          topicName: topicName,
                          channelId: widget.channelId,
                        ).then((value) {
                          if (value[0]) {
                            isCreateTopicLoader = false;
                            topicId = value[1]["id"];
                            AppInjector.resolve<AppRoutes>()
                                .navigatorKey
                                .currentState!
                                .pop();
                          } else {
                            setState(() {
                              topicName = "";
                            });
                            setSelectTopicState(() {
                              isCreateTopicLoader = false;
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AppWidgets.getAlertDialog(
                                    title: "Alert",
                                    content: value[1],
                                    confirmButton: "Ok",
                                    cancelButton: "Cancel",
                                    onCancelTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    onConfirmTap: () {
                                      Navigator.of(context).pop();
                                    },
                                  );
                                },
                              );
                            });
                          }
                        });
                      });
                    })
              ]),
            ],
          ),
        ));
  }

  Widget _createTopicName(
      setModalState,
      ) {
    return Padding(
        padding:
        EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        "Create Topic",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    InkWell(
                      focusColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pop();
                      },
                      child: Container(
                        width: 24,
                        height: 24,
                        child: Image.asset(
                          "assets/images/cancel_icon.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 16),
              Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        "Topic",
                        style: TextStyle(
                          color: Color(0xfff2f2f7),
                          fontSize: 16,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ]),
              SizedBox(height: 4),
              AppWidgets.getEditText(
                  controller: _topicName,
                  onChanged: (topicName) {},
                  keyboardType: TextInputType.text,
                  maxLength: 100,
                  hint: "",
                  inputFormatter: [],
                  isObscureText: false),
              SizedBox(height: 16),
              AppWidgets.getWtaButton(
                  buttonName: "Done",
                  isLoader: false,
                  fontSize: 16,
                  width: double.infinity,
                  height: 48,
                  onPressed: () {
                    setState(() {
                      listOfTopics.add(_topicName.text);
                      AppInjector.resolve<AppRoutes>()
                          .navigatorKey
                          .currentState!
                          .pop();
                    });
                  }),
            ],
          ),
        ));
  }

  Widget _autoDeleteInfoCard() {
    return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        "Auto Delete Thread",
                        style: TextStyle(
                          color: Color(0xfff2f2f7),
                          fontSize: 16,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ]),
              SizedBox(height: 16),
              Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        "Auto delete is mandatory by default. Messages will delete automatically after seven days from day it starts, if you want to delete message in prior adjust the date in the auto delete.",
                        style: TextStyle(
                          color: Color(0xfff2f2f7),
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ]),
              SizedBox(height: 16),
              AppWidgets.getWtaButton(
                  buttonName: "Ok, Got it!",
                  isLoader: false,
                  fontSize: 16,
                  width: double.infinity,
                  height: 48,
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).pop();
                      /*AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pop(context);*/
                      AppInjector.resolve<AppRoutes>()
                          .navigatorKey
                          .currentState!
                          .pop(context);
                      // AppInjector.resolve<AppRoutes>()
                      //     .navigatorKey
                      //     .currentState!
                      //     .pushNamed(NonGeneralThreadScreen.routeName, arguments: [
                      //   threadDetails,
                      //   "",
                      //   true
                      // ]);
                    });
                  }),
            ],
          ),
        ));
  }

  Future<dynamic> createTopicApi(
      {required String topicName, required int channelId}) async {
    setState(() {
      isCreateTopicLoader = true;
    });

    var response = await AppInjector.resolve<ApiService>().createTopic({
      "name": topicName,
      "channel": channelId.toString(),
    });

    if (response.status == ApiResponseStatus.completed) {
      var jsonResponse =
      convert.jsonDecode(response.data) as Map<String, dynamic>;

      return [true, jsonResponse];
    } else if (response.status == ApiResponseStatus.error) {
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }

  Future<dynamic> createThreadApi(
      {required String threadName,
        required String threadBio,
        required int topicId,
        required int channelId,
        required String expirationTime}) async {
    setState(() {
      isCreateThreadLoader = true;
    });

    var response = await AppInjector.resolve<ApiService>().createThread({
      "name": threadName,
      "bio": threadBio,
      "topic": topicId,
      "channel": channelId,
      "expiration_time": expirationTime
    });

    if (response.status == ApiResponseStatus.completed) {
      var jsonResponse =
      convert.jsonDecode(response.data) as Map<String, dynamic>;
      threadDetails = jsonResponse;
      print('threadDetails $threadDetails');
      return [true, jsonResponse];
    } else if (response.status == ApiResponseStatus.error) {
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }
}
