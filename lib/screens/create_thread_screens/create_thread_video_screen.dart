import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/screens/create_thread_screens/create_thread_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:video_player/video_player.dart';

class CreateThreadVideoScreen extends StatefulWidget {
  final int channelId;
  const CreateThreadVideoScreen({Key? key, required this.channelId})
      : super(key: key);
  static const String routeName = '/CreateThreadVideoScreen';

  @override
  _CreateThreadVideoScreenState createState() =>
      _CreateThreadVideoScreenState();
}

class _CreateThreadVideoScreenState extends State<CreateThreadVideoScreen> {
  VideoPlayerController _videoPlayerController = VideoPlayerController.network(
      'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4');
  late bool isPlayButton;

  @override
  void initState() {
    super.initState();
    isPlayButton = false;

    _videoPlayerController = VideoPlayerController.asset('assets/demo.mp4')
      ..initialize().then((_) {
        setState(() {});
      });
  }

  @override
  void dispose() {
    super.dispose();
    _videoPlayerController.dispose();
    _videoPlayerController.pause();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(
                Icons.close,
                size: 28,
              ))
        ],
      ),
      body: _getBody(),
    );
  }

  Widget _getBody() {
    return SafeArea(
        child: Stack(children: [
      new Positioned(
        child: InkWell(
            focusColor: Colors.transparent,
            highlightColor: Colors.transparent,
            hoverColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              print("on tap");
              if (_videoPlayerController.value.isPlaying) {
                setState(() {
                  isPlayButton = false;
                  _videoPlayerController.pause();
                });
              } else {
                setState(() {
                  isPlayButton = true;
                  _videoPlayerController.play();
                });
              }
            },
            child: Container(
              height: MediaQuery.of(context).size.height / 3,
              alignment: Alignment.topCenter,
              child: _videoPlayerController.value.isInitialized
                  ? Stack(
                      alignment: Alignment.center,
                      children: [
                        AspectRatio(
                          aspectRatio: _videoPlayerController.value.aspectRatio,
                          child: VideoPlayer(
                            _videoPlayerController,
                          ),
                        ),
                        isPlayButton
                            ? Container()
                            : Center(
                                child: _videoPlayerController.value.isPlaying
                                    ? Icon(Icons.pause_circle_filled,
                                        color: Color(0xffD86F33), size: 44.0)
                                    : IconButton(
                                        icon: Container(
                                          color: Colors.white,
                                          alignment: Alignment.center,
                                          width: 44,
                                          height: 44,
                                          child: Image.asset(
                                            "assets/images/play_button.png",
                                          ),
                                        ), // set your color here
                                        onPressed: () {
                                          print("on tap");
                                          if (_videoPlayerController
                                              .value.isPlaying) {
                                            setState(() {
                                              isPlayButton = false;
                                              _videoPlayerController.pause();
                                            });
                                          } else {
                                            setState(() {
                                              isPlayButton = false;
                                              _videoPlayerController.play();
                                              setState(() {
                                                isPlayButton = true;
                                              });
                                            });
                                          }
                                        },
                                      ))
                      ],
                    )
                  : Stack(
                      alignment: Alignment.center,
                      children: [
                        SizedBox(
                            width: 40,
                            height: 40,
                            child: CircularProgressIndicator.adaptive(
                                strokeWidth: 2,
                                backgroundColor: AppWidgets.buttonTextColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.white))),
                      ],
                    ),
            )),
      ),
      Positioned(
          child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height / 2,
            padding: EdgeInsets.only(left: 20, right: 20, top: 24),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16),
                  topRight: Radius.circular(16),
                ),
                color: AppWidgets.widgetBackgroundColor),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Getting started with Threads",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: 'Proxima Nova',
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        "Threads help you to organize conversations by topic in a channel",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: 'Proxima Nova',
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.circle,
                        size: 5,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      displayText(
                          'Threads is an option which comes in the category',
                          16,
                          FontWeight.w400),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.circle,
                        size: 5,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      displayText('In a category we can have multiple threads',
                          16, FontWeight.w400),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.circle,
                        size: 5,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      displayText('Thread category - Thread name - Thread bio',
                          16, FontWeight.w400),
                    ],
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  AppWidgets.getWtaButton(
                      buttonName: "Got it!",
                      fontSize: 16,
                      width: double.infinity,
                      height: 48,
                      isLoader: false,
                      onPressed: () {
                        AppInjector.resolve<AppRoutes>()
                            .navigatorKey
                            .currentState!
                            .pushReplacementNamed(CreateThreadScreen.routeName,
                                arguments: [widget.channelId, -1, ""]);
                      }),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            )),
      )),
    ]));
  }

  Widget displayText(String name, double fontSize, FontWeight fontWeight) {
    return Expanded(
        child: Text(
      name,
      style: TextStyle(
          fontFamily: 'Proxima Nova',
          fontSize: fontSize,
          fontWeight: fontWeight,
          color: Colors.white),
    ));
  }
}
