import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/screens/login_screen/login_screen.dart';
import 'package:pre_seasoned/screens/suggest_channels_screen/suggested_channel_illustration_screen.dart';
import 'package:pre_seasoned/utils/app_fonts.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class InterestScreen extends StatefulWidget {
  const InterestScreen({Key? key}) : super(key: key);
  static const String routeName = "/InterestScreen";

  @override
  _InterestScreenState createState() => _InterestScreenState();
}

class _InterestScreenState extends State<InterestScreen> {
  String interestType = "";
  List<bool> interestTypesSelected = [];
  bool isUpdateLoader = false;
  bool isGetInterestLoader = false;

  late List<dynamic> interestTypes = [];

  @override
  void initState() {
    super.initState();
    isGetInterestLoader = true;
    getAllInterestApi();
  }

  getAllInterestApi() async {
    AppInjector.resolve<ApiService>().getAllInterest().then((value) {
      print("Value All Interest $value");
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          isGetInterestLoader = false;
          interestTypes = convert.jsonDecode(value.data);
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          isGetInterestLoader = false;
          BotToast.showText(text: value.message[1]);
        });
      } else {
        setState(() {
          isGetInterestLoader = false;
          BotToast.showText(text: "Something went wrong.Please try later");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          brightness: Brightness.dark,
          backgroundColor: AppWidgets.backgroundColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamedAndRemoveUntil(
                    LoginScreen.routeName, (route) => false);
              }),
        ),
        backgroundColor: Color(0xff2c2c2e),
        body: Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    SizedBox(
                      child: Text(
                        "Select your interests to find more channels",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 28,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(height: 24),
                    isGetInterestLoader
                        ? SizedBox(
                        height: 50,
                        width: 50,
                        child: CircularProgressIndicator.adaptive(
                            strokeWidth: 2,
                            backgroundColor: AppWidgets.buttonTextColor,
                            valueColor: AlwaysStoppedAnimation<Color>(
                                Colors.white)))
                        : Wrap(
                      children: getInterestTypes.toList(),
                    ),
                  ],
                ),
              ),
              AppWidgets.getWtaButton(
                  isLoader: isUpdateLoader,
                  buttonName: "Continue",
                  fontSize: 16,
                  width: double.infinity,
                  height: 48,
                  onPressed: () {
                    List<int> interestTypesSelectedId = [];
                    for (int i = 0; i < interestTypesSelected.length; i++) {
                      if (interestTypesSelected[i]) {
                        interestTypesSelectedId
                            .add(interestTypes[i]["id"] as int);
                      }
                    }
                    if (interestTypesSelectedId.isEmpty) {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AppWidgets.getAlertDialog(
                            title: "Alert",
                            content: "Please Select at least one Interest",
                            confirmButton: "Ok",
                            cancelButton: "Cancel",
                            onCancelTap: () {
                              Navigator.of(context).pop();
                            },
                            onConfirmTap: () {
                              Navigator.of(context).pop();
                            },
                          );
                        },
                      );
                    } else {
                      interestUpdateApi(
                          AppInjector.resolve<AppPreferences>()
                              .getUserName(),
                          interestTypesSelectedId)
                          .then((value) {
                        if (value[0]) {
                          isUpdateLoader = false;
                          AppInjector.resolve<AppPreferences>()
                              .isLogged("LoggedInState");
                          AppInjector.resolve<AppRoutes>()
                              .navigatorKey
                              .currentState!
                              .pushNamedAndRemoveUntil(
                              SuggestedChannelIllustrationScreen.routeName,
                                  (route) => false);
                        } else {
                          setState(() {
                            isUpdateLoader = false;
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AppWidgets.getAlertDialog(
                                  title: "Alert",
                                  content: value[1],
                                  confirmButton: "Ok",
                                  cancelButton: "Cancel",
                                  onCancelTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  onConfirmTap: () {
                                    Navigator.of(context).pop();
                                  },
                                );
                              },
                            );
                          });
                        }
                      });
                    }
                  }),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ));
  }

  Future<List> interestUpdateApi(username, interests) async {
    setState(() {
      isUpdateLoader = true;
    });

    var response = await AppInjector.resolve<ApiService>().interestUpdate({
      "username": username,
      "interests": interests,
    });

    if (response.status == ApiResponseStatus.completed) {
      return [true, ""];
    } else if (response.status == ApiResponseStatus.error) {
      print("response data init ${response.message[1]}");
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }

  String getSelectedInterestType() {
    String temp = "";
    for (int i = 0; i < interestTypesSelected.length; i++) {
      if (interestTypesSelected[i]) {
        if (temp.isEmpty) {
          temp += interestTypes[i]['name'];
        } else {
          temp += ",${interestTypes[i]['name']}";
        }
      }
    }
    return temp;
  }

  Iterable<Widget> get getInterestTypes sync* {
    interestTypesSelected = [];
    for (int i = 0; i < interestTypes.length; i++) {
      var interest = interestTypes[i]['name'];
      interestTypesSelected.add(interestType.contains(interest));
      yield Container(
        height: 32,
        margin: const EdgeInsets.only(right: 10, bottom: 10),
        child: FilterChip(
          label: Text(
            interest,
            style: TextStyle(
              color: Colors.white,
              fontFamily: AppFonts.siemensSansBold,
              fontSize: 14,
            ),
          ),
          selected: interestTypesSelected[i],
          disabledColor: AppWidgets.backgroundColor,
          backgroundColor: (interestTypesSelected[i])
              ? AppWidgets.primaryColor
              : AppWidgets.backgroundColor,
          selectedShadowColor: Colors.transparent,
          shadowColor: Colors.transparent,
          checkmarkColor: Colors.transparent,
          shape: StadiumBorder(
            side: (interestTypesSelected[i])
                ? BorderSide(color: AppWidgets.primaryColor, width: 1)
                : BorderSide(color: Colors.white, width: 1),
          ),
          selectedColor: (interestTypesSelected[i])
              ? AppWidgets.primaryColor
              : Colors.white,
          showCheckmark: false,
          onSelected: (bool selected) {
            setState(() {
              interestTypesSelected[i] = selected;
              interestType = getSelectedInterestType();
            });
          },
        ),
      );
    }
  }
}
