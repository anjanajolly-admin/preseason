import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_cubit.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_state.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/screens/email_verification_screen/email_verification_screen.dart';
import 'package:pre_seasoned/screens/forgot_password_screen/forgot_password_screen.dart';
import 'package:pre_seasoned/screens/interest_screen/interest_screen.dart';
import 'package:pre_seasoned/screens/main_container/main_container_screen.dart';
import 'package:pre_seasoned/screens/signup_screen/signup_screen.dart';
import 'package:pre_seasoned/screens/suggest_channels_screen/suggested_channel_illustration_screen.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  static const String routeName = "/LoginScreen";

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailId = TextEditingController();
  final TextEditingController _password = TextEditingController();
  bool isLoader = false;
  bool isObscureText = false;

  @override
  void initState() {
    super.initState();
  }

  Future tokenGenerated() async {
    FirebaseMessaging.instance.getToken().then(
          (token) {
        print('Token Generated :$token');
        String? userId =
        AppInjector.resolve<AppPreferences>().getUserId().toString();
        print('userId :$userId');
        if (userId != null)
          sendTokenApi(token, userId);
        else
          return;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
        elevation: 0,
      ),
      body: BlocListener<BaseCubit, BaseState>(
          bloc: BlocProvider.of<BaseCubit>(context),
          listener: (context, BaseState state) {
            print("BlocListener=> Login Screen");
            if (state is LoginSuccessState) {
              setState(() {
                tokenGenerated();
                isLoader = false;
              });
              AppInjector.resolve<AppRoutes>()
                  .navigatorKey
                  .currentState!
                  .pushNamedAndRemoveUntil(
                  MainContainerScreen.routeName, (route) => false);
            } else if (state is LoginInterestState) {
              setState(() {
                tokenGenerated();
                isLoader = false;
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamed(InterestScreen.routeName);
              });
            } else if (state is LoginSuggestedState) {
              setState(() {
                tokenGenerated();
                isLoader = false;
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamed(SuggestedChannelIllustrationScreen.routeName);
              });
            } else if (state is LoadingState) {
              setState(() {
                isLoader = true;
              });
              return;
            } else if (state is LoginFailureState) {
              setState(() {
                isLoader = false;
              });
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AppWidgets.getAlertDialog(
                    title: "Login Failed",
                    content: "${state.failureMsg}",
                    confirmButton: "Ok",
                    cancelButton: "Cancel",
                    onCancelTap: () {
                      Navigator.of(context).pop();
                    },
                    onConfirmTap: () {
                      Navigator.of(context).pop();
                    },
                  );
                },
              );
            } else if (state is LoginVerifyState) {
              setState(() {
                isLoader = false;
              });
              AppInjector.resolve<AppRoutes>()
                  .navigatorKey
                  .currentState!
                  .pushNamedAndRemoveUntil(
                  EmailVerificationScreen.routeName, (route) => false);
            }
          },
          child: InkWell(
            focusColor: Colors.transparent,
            highlightColor: Colors.transparent,
            hoverColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              AppConstants.hideKeyboard(context);
            },
            child: _getBody(),
          )),
    );
  }

  Widget _getBody() {
    return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            children: [
              SizedBox(height: 24),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  children: [
                    Container(
                      width: 80,
                      height: 80,
                      child: Image.asset(
                        "assets/images/app_icon.png",
                        fit: BoxFit.fill,
                      ),
                    ),
                    SizedBox(height: 16),
                    SizedBox(
                      width: double.infinity,
                      child: Text(
                        "Easy way to talk every day and hang out more often.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xfff2f2f7),
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 32),
              Container(
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "E-mail",
                            style: TextStyle(
                              color: Color(0xfff2f2f7),
                              fontSize: 16,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 4),
                          AppWidgets.getEditText(
                            isObscureText: false,
                            controller: _emailId,
                            onChanged: (value) {
                              // _emailId.text = value;
                            },
                            keyboardType: TextInputType.emailAddress,
                            maxLength: 100,
                            hint: " Enter your mail id ",
                            inputFormatter: [
                              FilteringTextInputFormatter.deny(RegExp(" ")),
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 24),
                    Container(
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Password",
                            style: TextStyle(
                              color: Color(0xfff2f2f7),
                              fontSize: 16,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 4),
                          AppWidgets.getEditPasswordText(
                            isObscureText: !isObscureText,
                            controller: _password,
                            onChanged: (value) {
                              // _password.text = value;
                            },
                            hint: " Password ",
                            inputFormatter: [],
                            suffixIcon: IconButton(
                                color: AppWidgets.titleColor4,
                                icon: Icon(isObscureText
                                    ? Icons.visibility_off
                                    : Icons.visibility),
                                onPressed: () {
                                  setState(() {
                                    isObscureText = !isObscureText;
                                  });
                                }),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 24),
                    AppWidgets.getWtaButton(
                        isLoader: isLoader,
                        buttonName: "Login",
                        fontSize: 16,
                        width: double.infinity,
                        height: 48,
                        onPressed: () {
                          setState(() {
                            {
                              isValidEmail(_emailId)
                                  ? _password.text.length < 8
                                  ? showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AppWidgets.getAlertDialog(
                                    title: "Alert",
                                    content:
                                    "Password must be 8 characters at least",
                                    confirmButton: "Ok",
                                    cancelButton: "Cancel",
                                    onCancelTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    onConfirmTap: () {
                                      Navigator.of(context).pop();
                                    },
                                  );
                                },
                              )
                                  : context
                                  .read<BaseCubit>()
                                  .emailLogin(_emailId.text, _password.text)
                                  : showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AppWidgets.getAlertDialog(
                                    title: "Alert",
                                    content: "Please enter valid email Id",
                                    confirmButton: "Ok",
                                    cancelButton: "Cancel",
                                    onCancelTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    onConfirmTap: () {
                                      Navigator.of(context).pop();
                                    },
                                  );
                                },
                              );
                            }
                          });
                        })
                  ],
                ),
              ),
              SizedBox(height: 16),
              AppWidgets.getTextButton(
                  "Forgot Password ?", AppWidgets.buttonTextColor, 16, () {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamed(ForgotPasswordScreen.routeName,
                    arguments: (route) => false);
              }),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Don't have an account ?",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  AppWidgets.getTextButton(
                      "Sign up", AppWidgets.buttonTextColor, 16, () {
                    AppInjector.resolve<AppRoutes>()
                        .navigatorKey
                        .currentState!
                        .pushNamed(SignUpScreen.routeName);
                  })
                ],
              ),
            ],
          ),
        ));
  }

  Future<void> sendTokenApi(token, userId) async {
    var response = await AppInjector.resolve<ApiService>().sendTokenApi({
      "token": token,
      "user": userId,
    });

    if (response.status == ApiResponseStatus.completed) {
      print("response data init ${response.data}");
      return;
    } else if (response.status == ApiResponseStatus.error) {
      print("response data init ${response.message[1]}");
      return;
    } else {
      return;
    }
  }
}
