import 'dart:async';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_attachment.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_file.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_message.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';
import 'package:quickblox_sdk/models/qb_user.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:uuid/uuid.dart';

class NonGeneralThreadUserScreen extends StatefulWidget {
  final dynamic threadDetails;

  NonGeneralThreadUserScreen({Key? key, this.threadDetails}) : super(key: key);
  static const String routeName = "/NonGeneralThreadUserScreen";

  @override
  _NonGeneralThreadUserScreenState createState() =>
      _NonGeneralThreadUserScreenState();
}

class _NonGeneralThreadUserScreenState extends State<NonGeneralThreadUserScreen>
    with SingleTickerProviderStateMixin {
  bool _isAgreed = false;
  bool _buttonLoader = false;
  bool _isMessageSent = false;
  File? _profileImageFile;
  bool isSendingLoader = false;
  List<QBMessage?>? messagesList;
  List<QBMessage?>? inboxList;
  final TextEditingController _sendMessageController = TextEditingController();
  final List<Tab> tabs = <Tab>[
    Tab(
      child: Text(
        'Feed',
        style: TextStyle(
            fontFamily: 'Proxima nova',
            fontSize: 16,
            fontWeight: FontWeight.w600),
      ),
    ),
    Tab(
      child: Text(
        'My Questions',
        style: TextStyle(
            fontFamily: 'Proxima nova',
            fontSize: 16,
            fontWeight: FontWeight.w600),
      ),
    )
  ];
  StreamSubscription? _newMessageSubscription;
  StreamSubscription? _deliveredMessageSubscription;
  bool isMessagesLoader = false;
  ScrollController scrollController = ScrollController();
  TabController? tabController;

  @override
  void initState() {
    isMessagesLoader = true;
    getSubscribe();
    getSubscribeDelivered();
    super.initState();
    getMessageHistory();
    getInboxHistory();
    tabController = new TabController(length: tabs.length, vsync: this);
  }

  @override
  void dispose() {
    // if (_newMessageSubscription != null) {
    //   _newMessageSubscription!.cancel();
    //   _newMessageSubscription = null;
    // }
    tabController!.dispose();
    super.dispose();
  }

  getSubscribe() async {
    try {
      _newMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.RECEIVED_NEW_MESSAGE, (data) {
        print("Subscribed: " + QBChatEvents.RECEIVED_NEW_MESSAGE);
        Map<dynamic, dynamic> payload =
        Map<dynamic, dynamic>.from(data["payload"]);
        print("Received message : \n $payload");

        if (data["payload"]["dialogId"] == widget.threadDetails['feed_id']) {
          refreshList(payload, messagesList);
        } else if ((data["payload"]["dialogId"] ==
            widget.threadDetails['inbox_id'])) {
          refreshList(payload, inboxList);
        }
      }, onErrorMethod: (error) {
        print("Sub Error $error");
      });
    } catch (e) {
      print("Main Error $e");
    }
  }

  Future<void> refreshList(payload, listMessages) async {
    int count = 0;
    for (var msgObj in messagesList!) {
      if (msgObj!.properties!['message_id'] ==
          payload["properties"]!['message_id']) {
        setState(() {
          count = count + 1;
          msgObj.id = payload["id"];
          if (payload['attachments'] != null && payload['attachments'] != []) {
            msgObj.attachments = getAttachments(payload["attachments"][0]);
          } else {
            msgObj.attachments = [];
          }
        });
      }
    }
    if (count == 0) {
      QBMessage? receivedMessage = QBMessage();
      receivedMessage.id = payload["id"];
      receivedMessage.body = payload['body'];
      receivedMessage.dateSent = payload['dateSent'];
      receivedMessage.dialogId = payload['dialogId'];
      receivedMessage.properties = {
        "message_id": payload["properties"]["message_id"],
        "image_path": payload["properties"]["image_path"] != null
            ? payload["properties"]["image_path"]
            : "",
        "original_sender_name":
        payload["properties"]["original_sender_name"] != null
            ? payload["properties"]["original_sender_name"]
            : ""
      };
      if (payload['attachments'] != null && payload['attachments'] != []) {
        receivedMessage.attachments = getAttachments(payload["attachments"][0]);
      } else {
        receivedMessage.attachments = [];
      }

      setState(() {
        listMessages!.add(receivedMessage);
        count = 0;
      });
    } else {}
  }

  List<QBAttachment?> getAttachments(attachmentReceived) {
    var qbAttachment = QBAttachment();
    List<QBAttachment> qbAttachmentList = [];
    qbAttachment.id = (attachmentReceived['id'].toString());
    qbAttachment.contentType = attachmentReceived['contentType'];
    qbAttachment.type = attachmentReceived['type'];
    qbAttachmentList.add(qbAttachment);
    return qbAttachmentList;
  }

  getSubscribeDelivered() async {
    try {
      _deliveredMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.MESSAGE_DELIVERED, (data) {
        // LinkedHashMap<dynamic, dynamic> messageStatusHashMap = data;
        // Map<dynamic, dynamic> messageStatusMap =
        //     Map<dynamic, dynamic>.from(messageStatusHashMap);
        // Map<dynamic, dynamic> payloadMap =
        //     Map<String, Object>.from(messageStatusHashMap["payload"]);
        print("MESSAGE_DELIVERED message: \n $data");
      });
    } catch (e) {
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<void> getMessageHistory() async {
    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;
    int limit = 0;
    int skip = 0;
    print("Dialog Id got is ${widget.threadDetails['feed_id']}");
    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogMessages(widget.threadDetails['feed_id'],
        sort: sort, limit: limit, skip: skip)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        messagesList = value.data;
      } else {
        messagesList = [];
        BotToast.showText(text: "Couldn't Retrieve messages");
      }
      isMessagesLoader = false;
      setState(() {});
    });
  }

  Future<void> getInboxHistory() async {
    QBFilter filter = QBFilter();
    filter.field = "sender_id";
    filter.operator = QBChatDialogFilterOperators.IN;
    filter.value =
        AppInjector.resolve<AppPreferences>().getQbUserId()!.toString();

    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;
    int limit = 0;
    int skip = 0;

    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogMessages(widget.threadDetails['inbox_id'],
        sort: sort, limit: limit, skip: skip)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        inboxList = [];
        for (var index = 0; index < (value.data as List).length; index++) {
          print("Got All sender id ${(value.data as List)[index].senderId!}");
          if ((value.data as List)[index].senderId! ==
              AppInjector.resolve<AppPreferences>().getQbUserId()) {
            inboxList!.add((value.data as List)[index]);
          }
        }
      } else {
        inboxList = [];
        BotToast.showText(text: "Couldn't Retrieve messages");
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pop(context);
                // var leaveDialog =
                //     await AppInjector.resolve<QuickbloxApiService>()
                //         .leaveDialog(widget.threadDetails['feed_id']);
                // if (leaveDialog.status ==
                //     QuickbloxApiResponseStatus.completed) {
                //   print("Dialog Left ${widget.threadDetails['feed_id']}");
                //   AppInjector.resolve<AppRoutes>()
                //       .navigatorKey
                //       .currentState!
                //       .pop(context);
                // } else {
                //   BotToast.showText(text: "Cant leave the thread");
                // }
              }),
          title: Text(
            widget.threadDetails['name'],
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
          bottom: TabBar(
            labelColor: AppWidgets.buttonTextColor,
            unselectedLabelColor: Colors.white,
            controller: tabController,
            tabs: tabs,
          ),
        ),
        body: TabBarView(controller: tabController, children: [
          Stack(children: [
            feedTab(),
            Container(
              width: double.infinity,
              height: 2,
              color: AppWidgets.titleColor4,
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      color: Color(0xFF1C1C1E),
                      height: 50,
                      child: InkWell(
                        focusColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          showModalBottomSheet(
                            isDismissible: _isMessageSent ? false : true,
                            context: context,
                            isScrollControlled: true,
                            backgroundColor: Colors.black,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              ),
                            ),
                            builder: ((builder) {
                              return Padding(
                                padding: MediaQuery.of(context).viewInsets,
                                child: StatefulBuilder(builder:
                                    (BuildContext context,
                                    StateSetter setModalState) {
                                  return Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(12),
                                          topRight: Radius.circular(12),
                                          bottomLeft: Radius.circular(0),
                                          bottomRight: Radius.circular(0),
                                        ),
                                        color: Colors.black,
                                      ),
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 20,
                                        vertical: 16,
                                      ),
                                      child: _bottomSheet(setModalState));
                                }),
                              );
                            }),
                          );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.add,
                              color: AppWidgets.buttonTextColor,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Send message to host',
                              style: TextStyle(
                                  color: AppWidgets.buttonTextColor,
                                  fontSize: 16,
                                  fontFamily: 'Proxima nova'),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ))
          ]),
          Stack(children: [
            questionTab(),
            Container(
              width: double.infinity,
              height: 2,
              color: AppWidgets.titleColor4,
            ),
          ]),
        ]),
      ),
    );
  }

  Widget feedTab() {
    return messagesList == null
        ? Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              child: SizedBox(
                  height: 50,
                  width: 50,
                  child: CircularProgressIndicator.adaptive(
                      strokeWidth: 2,
                      backgroundColor: AppWidgets.buttonTextColor,
                      valueColor:
                      AlwaysStoppedAnimation<Color>(Colors.white))),
            )
          ],
        ))
        : messagesList!.isEmpty || messagesList!.length == 0
        ? Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xff464649),
                ),
                borderRadius: BorderRadius.circular(25),
                color: Color(0xff3a3a3c),
              ),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 160,
                      width: 160,
                      margin: EdgeInsets.all(20),
                      child: Align(
                          alignment: Alignment.center,
                          child: Image.asset(
                              'assets/images/thread_empty_image.png')),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * .8,
                      child: SizedBox(
                        child: Text(
                          "Send you first message and start the engagement with your audience",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Messages in Feed can be seen by everyone.",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Color(0xffa4a4ab),
                        fontSize: 12,
                      ),
                    ),
                  ]),
            )
          ],
        ))
        : Stack(
      children: [
        // Container(
        //   padding: EdgeInsets.all(10),
        //   margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
        //   color: Color(0xff3a3a3c),
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //     children: [
        //       Row(
        //         children: [
        //           Container(
        //             width: 25,
        //             height: 25,
        //             child: Image.asset('assets/images/broadcast_go_live.png'),
        //           ),
        //           SizedBox(
        //             width: 10,
        //           ),
        //           Text(
        //             'Host is on live',
        //             style: TextStyle(
        //                 fontFamily: 'Proxima nova',
        //                 fontSize: 18,
        //                 color: Colors.white),
        //           )
        //         ],
        //       ),
        //       InkWell(
        //         focusColor: Colors.transparent,
        //         highlightColor: Colors.transparent,
        //         hoverColor: Colors.transparent,
        //         splashColor: Colors.transparent,
        //         onTap: () {},
        //         child: Container(
        //           width: 90,
        //           height: 30,
        //           decoration: BoxDecoration(
        //             borderRadius: BorderRadius.circular(4),
        //             border: Border.all(
        //               color: AppWidgets.buttonTextColor,
        //               width: 1,
        //             ),
        //           ),
        //           child: Row(
        //             mainAxisSize: MainAxisSize.min,
        //             mainAxisAlignment: MainAxisAlignment.center,
        //             crossAxisAlignment: CrossAxisAlignment.center,
        //             children: [
        //               Expanded(
        //                 child: Text(
        //                   "Join now",
        //                   textAlign: TextAlign.center,
        //                   style: TextStyle(
        //                     color: AppWidgets.buttonTextColor,
        //                     fontSize: 14,
        //                     fontFamily: "Proxima Nova",
        //                     fontWeight: FontWeight.w600,
        //                   ),
        //                 ),
        //               ),
        //             ],
        //           ),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        Container(
          margin: EdgeInsets.only(left: 20, right: 20, top: 16),
          padding: EdgeInsets.only(bottom: 54),
          child: ListView.builder(
              controller: scrollController,
              itemCount: messagesList!.length,
              physics: AlwaysScrollableScrollPhysics(),
              shrinkWrap: true,
              reverse: true,
              itemBuilder: (context, index1) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppWidgets.adminInboxConversation(
                        messageObj: messagesList![
                        messagesList!.length - 1 - index1]!,
                        context: context,
                        isFromReplyMessage: false)
                  ],
                );
              }),
        ),
      ],
    );
  }

  Widget questionTab() {
    return inboxList == null
        ? Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              child: SizedBox(
                  height: 50,
                  width: 50,
                  child: CircularProgressIndicator.adaptive(
                      strokeWidth: 2,
                      backgroundColor: AppWidgets.buttonTextColor,
                      valueColor:
                      AlwaysStoppedAnimation<Color>(Colors.white))),
            )
          ],
        ))
        : inboxList!.isEmpty || inboxList!.length == 0
        ? Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xff464649),
                ),
                borderRadius: BorderRadius.circular(25),
                color: Color(0xff3a3a3c),
              ),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 160,
                      width: 160,
                      margin: EdgeInsets.all(20),
                      child: Align(
                          alignment: Alignment.center,
                          child: Image.asset(
                              'assets/images/thread_empty_image.png')),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * .8,
                      child: SizedBox(
                        child: Text(
                          "Send you first message and start the engagement with your audience",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Messages in Feed can be seen by everyone.",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Color(0xffa4a4ab),
                        fontSize: 12,
                      ),
                    ),
                  ]),
            )
          ],
        ))
        : Container(
      margin: EdgeInsets.only(left: 20, right: 20, top: 16),
      // padding: EdgeInsets.only(bottom: 54),
      child: ListView.builder(
          controller: scrollController,
          itemCount: inboxList!.length,
          physics: AlwaysScrollableScrollPhysics(),
          shrinkWrap: true,
          reverse: true,
          itemBuilder: (context, index1) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppWidgets.adminMessageSent(
                    messageObj:
                    inboxList![inboxList!.length - 1 - index1]!,
                    context: context,
                    isGeneralThread: true,
                    isFromInboxShared: false,
                    isFromReplyMessage: false)
              ],
            );
          }),
    );
  }

  Widget _bottomSheet(StateSetter setModalState) {
    if (AppInjector.resolve<AppPreferences>()
        .getAgreementStatus(widget.threadDetails['id'].toString()) ==
        null) {
      AppInjector.resolve<AppPreferences>()
          .setAgreementStatus(false, widget.threadDetails['id'].toString());
    }
    return _isMessageSent
        ? Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          width: 40,
          height: 40,
          child: Image.asset('assets/images/confirmation_sent_icon.png'),
        ),
        SizedBox(
          height: 8,
        ),
        SizedBox(
          width: double.infinity,
          child: Text(
            "Message Sent",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0xfff2f2f7),
              fontSize: 14,
            ),
          ),
        ),
        SizedBox(
          height: 8,
        ),
        SizedBox(
          width: double.infinity,
          child: Text(
            "Please note that the message sent will not appear in live feed until admin address it. You can view your send messages in “My Question” tab.",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0xfff2f2f7),
              fontSize: 14,
            ),
          ),
        ),
        SizedBox(
          height: 16,
        ),
        AppWidgets.getWtaButton(
            buttonName: 'Ok, Got it',
            fontSize: 16,
            width: double.infinity,
            height: 40,
            isLoader: false,
            onPressed: () {
              Navigator.of(context).pop();
              setState(() {
                _isMessageSent = false;
                _isAgreed = false;
                tabController!.animateTo(1);
                _sendMessageController.clear();
              });
              setModalState(() {});
            }),
      ],
    )
        : AppInjector.resolve<AppPreferences>()
        .getAgreementStatus(widget.threadDetails['id'].toString())!
        ? Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Post questions / thoughts",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontFamily: "Proxima Nova",
                    fontWeight: FontWeight.w600,
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Icon(
                    Icons.close,
                    color: Colors.white,
                  ),
                )
              ],
            ),
            SizedBox(height: 4),
            Text(
              "Messages will not appear in live until admin address it",
              style: TextStyle(
                color: Colors.white,
                fontSize: 12,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          height: 1,
          width: double.infinity,
          color: AppWidgets.titleColor4,
        ),
        SizedBox(
          height: 8,
        ),
        isSendingLoader
            ? CircularProgressIndicator.adaptive(
            strokeWidth: 2,
            backgroundColor: AppWidgets.buttonTextColor,
            valueColor:
            AlwaysStoppedAnimation<Color>(Colors.white))
            : Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: Container(
                  height: 120,
                  padding: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: AppWidgets.titleColor4,
                      width: 1,
                    ), // set border width
                    borderRadius: BorderRadius.all(Radius.circular(
                        8.0)), // set rounded corner radius
                  ),
                  child: TextField(
                    controller: _sendMessageController,
                    inputFormatters: [],
                    obscureText: false,
                    onChanged: (_) {},
                    maxLines: null,
                    style: TextStyle(
                      color: AppWidgets.titleColor3,
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontFamily: "Proxima Nova",
                    ),
                    decoration: InputDecoration(
                      hintText: "Enter your message",
                      border: InputBorder.none,
                      suffixIcon: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            alignment: Alignment.bottomRight,
                            padding: EdgeInsets.only(bottom: 5),
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.end,
                              children: [
                                InkWell(
                                  onTap: () {
                                    AppConstants.hideKeyboard(
                                        context);
                                    showModalBottomSheet(
                                        isDismissible: false,
                                        enableDrag: false,
                                        shape:
                                        RoundedRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(
                                              10.0),
                                        ),
                                        backgroundColor: AppWidgets
                                            .backgroundColor,
                                        context: context,
                                        builder: (context) {
                                          return StatefulBuilder(
                                              builder: (BuildContext
                                              context,
                                                  StateSetter
                                                  setState /*You can rename this!*/) {
                                                return Container(
                                                  height: 50,
                                                  margin:
                                                  EdgeInsets.all(
                                                      10),
                                                  child: Column(
                                                    children: [
                                                      AppWidgets
                                                          .getWtaButton(
                                                          buttonName:
                                                          "Select Images",
                                                          fontSize:
                                                          14,
                                                          width: double
                                                              .infinity,
                                                          height:
                                                          48,
                                                          isLoader:
                                                          false,
                                                          onPressed:
                                                              () async {
                                                            AppConstants.hideKeyboard(
                                                                context);

                                                            Navigator.pop(
                                                                context);
                                                            _getProfileFromGallery();
                                                          }),
                                                      // SizedBox(
                                                      //   height: 10,
                                                      // ),
                                                      // AppWidgets.getWtaButton(
                                                      //     buttonName: "Camera",
                                                      //     fontSize: 14,
                                                      //     width: double.infinity,
                                                      //     height: 48,
                                                      //     isLoader: false,
                                                      //     onPressed: () {
                                                      //
                                                      //       Navigator.pop(
                                                      //           context);
                                                      //       _getProfileFromCamera(
                                                      //           width: 80,
                                                      //           height: 80);
                                                      //     }),
                                                    ],
                                                  ),
                                                );
                                              });
                                        });
                                  },
                                  child: SizedBox(
                                    height: 25,
                                    width: 25,
                                    child: Image.asset(
                                        'assets/images/camera_blue_icon.png'),
                                  ),
                                ),
                                // SizedBox(
                                //   width: 10,
                                // ),
                                // InkWell(
                                //   onTap: () {},
                                //   child: SizedBox(
                                //     height: 25,
                                //     width: 25,
                                //     child: Image.asset(
                                //         'assets/images/mic_blue_icon.png'),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      hintStyle: TextStyle(
                        color: AppWidgets.titleColor3,
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        fontFamily: "Proxima Nova",
                      ),
                    ),
                  ),
                )),
          ],
        ),
        SizedBox(
          height: 8,
        ),
        AppWidgets.getWtaButton(
            buttonName: "Post",
            fontSize: 16,
            width: double.infinity,
            height: 40,
            isLoader: _buttonLoader,
            onPressed: () async {
              if (_sendMessageController.text.isEmpty ||
                  _sendMessageController.text == "") {
                BotToast.showText(text: 'Enter your message');
              } else {
                await sendMessage(_sendMessageController.text,
                    <QBAttachment>[], null, "")
                    .then((value) {
                  setState(() {
                    isSendingLoader = false;
                    _isMessageSent = true;
                  });
                });
                _sendMessageController.text = "";
                AppConstants.hideKeyboard(context);
              }
            }),
        SizedBox(
          height: 24,
        ),
        Container(
          height: 2,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white),
          width: MediaQuery.of(context).size.width * .3,
        )
      ],
    )
        : Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Communication Guidelines",
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontFamily: "Proxima Nova",
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: 16),
            SizedBox(
              width: double.infinity,
              child: Text(
                "Don’t use the content that promote or violate against the Communication guidelines",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              ),
            ),
            SizedBox(height: 16),
            Container(
              child: InkWell(
                focusColor: Colors.transparent,
                hoverColor: Colors.transparent,
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                onTap: () {
                  setState(() {
                    _isAgreed ? _isAgreed = false : _isAgreed = true;
                  });
                  setModalState(() {});
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: _isAgreed
                          ? Image.asset(
                          'assets/images/single_square_select.png')
                          : Image.asset(
                          'assets/images/single_square_deselect.png'),
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      child: FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Container(
                          child: Row(
                            children: [
                              Text(
                                "I hereby agree to the ",
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Text(
                                "communication guidelines",
                                style: TextStyle(
                                  fontSize: 14,
                                  color: AppWidgets.buttonTextColor,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 15),
            AppWidgets.getButton(
                buttonName: 'Continue',
                fontSize: 16,
                width: double.infinity,
                height: 40,
                isLoader: _buttonLoader,
                color: _isAgreed
                    ? AppWidgets.primaryColor
                    : Color(0xff3a3a3c),
                onPressed: _isAgreed
                    ? () {
                  setState(() {
                    AppInjector.resolve<AppPreferences>()
                        .setAgreementStatus(
                        true,
                        widget.threadDetails['id']
                            .toString());
                  });
                  setModalState(() {});
                }
                    : () {
                  setState(() {
                    BotToast.showText(
                        text:
                        'Please agree to the communication guidelines to continue;');
                  });
                  setModalState(() {});
                },
                textColor: _isAgreed
                    ? Colors.white
                    : AppWidgets.buttonTextColor),
            SizedBox(
              height: 30,
            ),
          ],
        ),
        Container(
          alignment: Alignment.center,
          height: 3,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white),
          width: MediaQuery.of(context).size.width * .3,
        )
      ],
    );
  }

  Future<void> sendMessage(
      messageBody,
      attachment,
      message_id,
      image_path,
      ) async {
    setState(() {
      isSendingLoader = true;
    });
    checkJoinDialog();

    var original_sender_name =
    await getUsersName(AppInjector.resolve<AppPreferences>().getQbUserId());
    if (message_id == null) {
      message_id = Uuid().v1();
      QBMessage messageObj = QBMessage();
      messageObj.id = null;
      messageObj.body = messageBody;
      messageObj.properties = {
        "message_id": message_id,
        "image_path": image_path,
        "original_sender_name": original_sender_name
      };
      messageObj.dateSent = DateTime.now().millisecondsSinceEpoch;
      messageObj.attachments = attachment;
      // setState(() {
      //   inboxList!.add(messageObj);
      // });
    }
    _newMessageSubscription!.resume();
    AppInjector.resolve<QuickbloxApiService>()
        .sendMessage(widget.threadDetails['inbox_id'],
        body: messageBody,
        attachments: attachment,
        properties: {
          "message_id": message_id,
          "image_path": image_path,
          "original_sender_name": original_sender_name
        },
        saveToHistory: true,
        markable: true);
  }

  Future<void> _getProfileFromGallery() async {
    var pickedFile = await FilePicker.platform.pickFiles();

    if (pickedFile != null) {
      _profileImageFile = File(pickedFile.files.single.path!);

      var message_id = Uuid().v1();
      QBMessage messageObj = QBMessage();
      messageObj.id = null;
      messageObj.body = " ";
      messageObj.attachments = <QBAttachment>[];
      messageObj.dateSent = DateTime.now().millisecondsSinceEpoch;
      messageObj.properties = {
        "message_id": message_id,
        "image_path": _profileImageFile!.path
      };
      setState(() {
        inboxList!.add(messageObj);
        isSendingLoader = true;
      });
      List<QBAttachment> attachmentsResponse =
      await uploadImageToContentQB(_profileImageFile!.path);
      if (attachmentsResponse != []) {
        await sendMessage(
            " ", attachmentsResponse, message_id, _profileImageFile!.path)
            .then((value) {
          setState(() {
            isSendingLoader = false;
            _isMessageSent = true;
          });
        });
      } else {}
    }
  }

  Future<void> checkJoinDialog() async {
    QBFilter filter = QBFilter();
    filter.field = "_id";
    filter.operator = QBChatDialogFilterOperators.ALL;
    filter.value = widget.threadDetails['feed_id'];

    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(
            "Before send msg dialog status ${(value.data[0] as QBDialog).unreadMessagesCount}");
      } else {
        BotToast.showText(
            text: "Before send msg dialog status,Try again later");
      }
    });
  }

  Future<List<QBAttachment>> uploadImageToContentQB(_profileImageFile) async {
    try {
      QBFile? file = await QB.content.upload(_profileImageFile, public: true);
      if (file != null) {
        int? id = file.id;
        String? contentType = file.contentType;

        QBAttachment attachment = QBAttachment();

        attachment.id = id.toString();
        attachment.contentType = contentType;
        attachment.type = "PHOTO";

        List<QBAttachment> attachmentsList = [];
        attachmentsList.add(attachment);
        print("Attachment list added $attachmentsList");
        return attachmentsList;
      } else {
        return [];
      }
    } catch (error) {
      print("------------Error is $error");
      return [];
    }
  }

  Future<String> getUsersName(senderId) async {
    var allUsers =
    await AppInjector.resolve<QuickbloxApiService>().getQBUsers();
    String fullName = "";
    if (allUsers.status == QuickbloxApiResponseStatus.completed) {
      print(
          "Before send msg dialog status ${(allUsers.data[0] as QBUser).fullName}");
      for (var index = 0; index < allUsers.data.length; index++) {
        if ((allUsers.data[index] as QBUser).id == senderId) {
          fullName = (allUsers.data[index] as QBUser).fullName!;
        }
      }

      return fullName;
    } else {
      BotToast.showText(text: "Before send msg dialog status,Try again later");
      return fullName;
    }
  }
}
