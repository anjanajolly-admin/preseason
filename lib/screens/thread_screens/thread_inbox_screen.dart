import 'dart:async';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_attachment.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_file.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_message.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';
import 'package:quickblox_sdk/models/qb_user.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

class InboxThreadScreen extends StatefulWidget {
  final dynamic threadDetails;
  final String generalDialogId;
  final bool isNewThread;
  const InboxThreadScreen(
      {Key? key,
        this.threadDetails,
        required this.generalDialogId,
        required this.isNewThread})
      : super(key: key);
  static const String routeName = "/InboxThreadScreen";
  @override
  _InboxThreadScreenState createState() => _InboxThreadScreenState();
}

class _InboxThreadScreenState extends State<InboxThreadScreen> {
  List<QBMessage?>? inboxMessagesList;

  File? profileImageFile;
  StreamSubscription? _newMessageSubscription;
  StreamSubscription? _deliveredMessageSubscription;

  ScrollController scrollController = ScrollController();
  int isFromInboxShared = -1;
  bool isMessageSent = false;
  bool isSendingLoader = false;
  bool showBottomSheet = true;

  @override
  void initState() {
    getSubscribe();
    getSubscribeDelivered();
    super.initState();
    getMessageHistory();
  }

  @override
  void dispose() {
    // if (_newMessageSubscription != null) {
    //   _newMessageSubscription!.cancel();
    //   _newMessageSubscription = null;
    // }
    // if (_deliveredMessageSubscription != null) {
    //   _deliveredMessageSubscription!.cancel();
    //   _deliveredMessageSubscription = null;
    // }
    super.dispose();
  }

  getSubscribe() async {
    try {
      _newMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.RECEIVED_NEW_MESSAGE, (data) {
        print("Subscribed: " + QBChatEvents.RECEIVED_NEW_MESSAGE);
        Map<dynamic, dynamic> payload =
        Map<dynamic, dynamic>.from(data["payload"]);
        if (payload["dialogId"] == widget.threadDetails['inbox_id']) {
          setMessages(payload);
          setState(() {});
        }
        print("Received message : \n $payload");
      }, onErrorMethod: (error) {
        print("Sub Error $error");
      });
    } catch (e) {
      print("Main Error $e");
    }
  }

  void setMessages(payload) {
    int count = 0;
    for (var msgObj in inboxMessagesList!) {
      if (msgObj!.properties!['message_id'] ==
          payload["properties"]!['message_id']) {
        setState(() {
          count = count + 1;
          msgObj.id = payload["id"];
          if (payload['attachments'] != null && payload['attachments'] != []) {
            msgObj.attachments = getAttachments(payload["attachments"][0]);
          } else {
            msgObj.attachments = [];
          }
        });
      }
    }
    if (count == 0) {
      QBMessage? receivedMessage = QBMessage();
      receivedMessage.id = payload["id"];
      receivedMessage.body = payload['body'];
      receivedMessage.dateSent = payload['dateSent'];
      receivedMessage.dialogId = payload['dialogId'];
      receivedMessage.properties = {
        "message_id": payload["properties"]["message_id"],
        "image_path": payload["properties"]["image_path"] != null
            ? payload["properties"]["image_path"]
            : "",
        "orginal_sender_name":
        payload["properties"]["orginal_sender_name"] != null
            ? payload["properties"]["orginal_sender_name"]
            : "",
        "orginal_sender_id": payload["properties"]["orginal_sender_id"] != null
            ? payload["properties"]["orginal_sender_id"]
            : ""
      };
      if (payload['attachments'] != null && payload['attachments'] != []) {
        receivedMessage.attachments = getAttachments(payload["attachments"][0]);
      } else {
        receivedMessage.attachments = [];
      }
      setState(() {
        inboxMessagesList!.add(receivedMessage);
        count = 0;
      });
    } else {}
  }

  List<QBAttachment?> getAttachments(attachmentReceived) {
    var qbAttachment = QBAttachment();
    List<QBAttachment> qbAttachmentList = [];
    qbAttachment.id = (attachmentReceived['id'].toString());
    qbAttachment.contentType = attachmentReceived['contentType'];
    qbAttachment.type = attachmentReceived['type'];
    qbAttachmentList.add(qbAttachment);
    return qbAttachmentList;
  }

  getSubscribeDelivered() async {
    try {
      _deliveredMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.MESSAGE_DELIVERED, (data) {
        // LinkedHashMap<dynamic, dynamic> messageStatusHashMap = data;
        // Map<dynamic, dynamic> messageStatusMap =
        //     Map<dynamic, dynamic>.from(messageStatusHashMap);
        // Map<dynamic, dynamic> payloadMap =
        //     Map<String, Object>.from(messageStatusHashMap["payload"]);
        print("MESSAGE_DELIVERED message: \n $data");
      });
    } catch (e) {
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<void> getMessageHistory() async {
    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;
    int limit = 0;
    int skip = 0;
    print("Dialog Id got is ${widget.threadDetails['inbox_id']}");
    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogMessages(widget.threadDetails['inbox_id'],
        sort: sort, limit: limit, skip: skip)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        setState(() {
          inboxMessagesList = value.data;
        });
      } else {
        setState(() {
          inboxMessagesList = [];
        });
        BotToast.showText(text: "Couldn't Retrieve messages");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppWidgets.backgroundColor,
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pop(context);
                // var leaveDialog =
                //     await AppInjector.resolve<QuickbloxApiService>()
                //         .leaveDialog(widget.threadDetails['inbox_id']);
                // if (leaveDialog.status ==
                //     QuickbloxApiResponseStatus.completed) {
                //   print("Dialog Left ${widget.threadDetails['inbox_id']}");
                //   AppInjector.resolve<AppRoutes>()
                //       .navigatorKey
                //       .currentState!
                //       .pop(context);
                // } else {
                //   BotToast.showText(text: "Cant leave the thread");
                // }
              }),
          title: Text(
            widget.threadDetails['name'],
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
          actions: [
            IconButton(
                onPressed: () async {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pop(context);
                  // var leaveDialog =
                  //     await AppInjector.resolve<QuickbloxApiService>()
                  //         .leaveDialog(widget.threadDetails['inbox_id']);
                  // if (leaveDialog.status ==
                  //     QuickbloxApiResponseStatus.completed) {
                  //   print("Dialog Left ${widget.threadDetails['inbox_id']}");
                  //   AppInjector.resolve<AppRoutes>()
                  //       .navigatorKey
                  //       .currentState!
                  //       .pop(context);
                  // } else {
                  //   BotToast.showText(text: "Cant leave the thread");
                  // }
                },
                iconSize: 72,
                icon: Container(
                  height: 24,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(
                      color: Color(0xff0080ff),
                      width: 1,
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "View Feed",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xff5cabff),
                          fontSize: 14,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
        body: InkWell(
          focusColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () {
            AppConstants.hideKeyboard(context);
          },
          child: _getBody(),
        ),
        bottomSheet: widget.isNewThread && showBottomSheet
            ? Container(
          width: double.infinity,
          decoration: BoxDecoration(
            //borderRadius: BorderRadius.only(topLeft: Radius.circular(12)),
            color: Color(0xff3a3a3c),
          ),
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 24,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                child: Text(
                  "How Inbox Works?",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontFamily: "Proxima Nova",
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(height: 10),
              SizedBox(
                child: Row(
                  children: [
                    Flexible(
                      child: Container(
                        child: Text(
                          "After creating a thread, you are directed with Inbox and Feed. "
                              "In Inbox you can see all messages sent by the audience in the thread."
                              " You can reply or post messages sent by the audience in the live feed .",
                          style: TextStyle(
                            color: Color(0xfff2f2f7),
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 16),
              AppWidgets.getWtaButton(
                  buttonName: 'Ok, Got it!',
                  fontSize: 16,
                  width: double.infinity,
                  height: 40,
                  isLoader: false,
                  onPressed: () {
                    setState(() {
                      showBottomSheet = false;
                    });
                    // Navigator.of(context).pop();
                  })
            ],
          ),
        )
            : Container(
          height: 0,
          width: 0,
        ));
  }

  Widget _getBody() {
    return Stack(
      children: [
        Container(
          height: 48,
          color: Color(0xff2c2c2e),
          padding: const EdgeInsets.only(
            top: 12,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Inbox",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(width: 12),
                  Container(
                    width: 16,
                    height: 16,
                    child: Image.asset(
                      "assets/images/info_filled_gray.png",
                      fit: BoxFit.fill,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 12),
              Container(
                color: AppWidgets.titleColor4,
                width: double.infinity,
                height: 1,
              ),
            ],
          ),
        ),
        inboxMessagesList == null
            ? Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: SizedBox(
                      height: 50,
                      width: 50,
                      child: CircularProgressIndicator.adaptive(
                          strokeWidth: 2,
                          backgroundColor: AppWidgets.buttonTextColor,
                          valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.white))),
                )
              ],
            ))
            : inboxMessagesList!.isEmpty || inboxMessagesList!.length == 0
            ? Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xff464649),
                    ),
                    borderRadius: BorderRadius.circular(25),
                    color: Color(0xff3a3a3c),
                  ),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 160,
                          width: 160,
                          margin: EdgeInsets.all(20),
                          child: Align(
                              alignment: Alignment.center,
                              child: Image.asset(
                                  'assets/images/thread_empty_image.png')),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * .8,
                          child: SizedBox(
                            child: Text(
                              "Messages sent by your audience will be shown here",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "You can reply or share the messages to the feed.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0xffa4a4ab),
                            fontSize: 12,
                          ),
                        ),
                      ]),
                )
              ],
            ))
            : Container(
            margin: EdgeInsets.only(left: 20, top: 54),
            child: ListView.builder(
                controller: scrollController,
                itemCount: inboxMessagesList!.length,
                physics: AlwaysScrollableScrollPhysics(),
                shrinkWrap: true,
                reverse: true,
                itemBuilder: (context, index1) {
                  return InkWell(
                      onTap: () {
                        showModalBottomSheet(
                          isDismissible: isMessageSent ? false : true,
                          context: context,
                          isScrollControlled: true,
                          backgroundColor: Colors.black,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                          ),
                          builder: ((builder) {
                            return Padding(
                              padding:
                              MediaQuery.of(context).viewInsets,
                              child: StatefulBuilder(builder:
                                  (BuildContext context,
                                  StateSetter setModalState) {
                                return Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(12),
                                        topRight: Radius.circular(12),
                                        bottomLeft: Radius.circular(0),
                                        bottomRight: Radius.circular(0),
                                      ),
                                      color: Colors.black,
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 20,
                                      vertical: 16,
                                    ),
                                    child: shareMessagesSheet(
                                        setModalState,
                                        index1,
                                        inboxMessagesList));
                              }),
                            );
                          }),
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppWidgets.adminInboxConversation(
                              messageObj: inboxMessagesList![
                              inboxMessagesList!.length -
                                  1 -
                                  index1]!,
                              context: context,
                              isFromReplyMessage: false)
                        ],
                      ));
                })),
      ],
    );
  }

  Widget shareMessagesSheet(
      StateSetter setShareState, index1, inboxMessagesList) {
    return isMessageSent
        ? Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          width: 40,
          height: 40,
          child: Image.asset('assets/images/confirmation_sent_icon.png'),
        ),
        SizedBox(
          height: 8,
        ),
        SizedBox(
          width: double.infinity,
          child: Text(
            "Message sent to Feed!",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0xfff2f2f7),
              fontSize: 14,
            ),
          ),
        ),
        SizedBox(
          height: 16,
        ),
        AppWidgets.getWtaButton(
            buttonName: 'Ok, Got it',
            fontSize: 16,
            width: double.infinity,
            height: 40,
            isLoader: false,
            onPressed: () {
              setState(() {
                isMessageSent = false;
              });
              Navigator.of(context).pop();
              setShareState(() {});
            }),
      ],
    )
        : Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
          focusColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () async {
            setState(() {
              isSendingLoader = true;
            });
            setShareState(() {});
            var userName = await getUsersName(inboxMessagesList[
            (inboxMessagesList.length - 1) - index1]!
                .senderId);
            List<QBAttachment>? attachments = [];
            if (inboxMessagesList[inboxMessagesList.length - 1 - index1]!
                .attachments !=
                null) {
              for (var i in (inboxMessagesList[
              inboxMessagesList.length - 1 - index1]!
                  .attachments)) {
                attachments.add(i);
              }
            }
            sendMessage(
                widget.threadDetails['feed_id'],
                inboxMessagesList[inboxMessagesList.length - 1 - index1]!
                    .body,
                attachments,
                inboxMessagesList[inboxMessagesList.length - 1 - index1]!
                    .properties!['message_id'],
                inboxMessagesList[inboxMessagesList.length - 1 - index1]!
                    .properties!['image_path'],
                inboxMessagesList[inboxMessagesList.length - 1 - index1]!
                    .senderId
                    .toString(),
                userName);
            setShareState(() {});
          },
          child: Row(
            children: [
              SizedBox(
                height: 20,
                width: 20,
                child: Image.asset(
                  'assets/images/share_icon.png',
                ),
              ),
              SizedBox(
                width: 12,
              ),
              Expanded(
                child: Text(
                  "Share to feed",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontFamily: "Proxima Nova",
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              isSendingLoader
                  ? SizedBox(
                  height: 20,
                  width: 20,
                  child: CircularProgressIndicator.adaptive(
                      strokeWidth: 2,
                      backgroundColor: AppWidgets.buttonTextColor,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Colors.white)))
                  : Container()
            ],
          ),
        ),
        SizedBox(
          height: 16,
        ),
        InkWell(
          focusColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () async {
            // var userName = await getUsersName(
            //     inboxMessagesList[
            //             inboxMessagesList
            //                     .length -
            //                 1 -
            //                 index1]!
            //         .senderId);
            // sendMessage(
            //     widget
            //         .generalDialogId,
            //     inboxMessagesList[
            //             inboxMessagesList
            //                     .length -
            //                 1 -
            //                 index1]!
            //         .body,
            //     inboxMessagesList[
            //             inboxMessagesList
            //                     .length -
            //                 1 -
            //                 index1]!
            //         .attachments,
            //     inboxMessagesList[
            //                 inboxMessagesList
            //                         .length -
            //                     1 -
            //                     index1]!
            //             .properties![
            //         'message_id'],
            //     inboxMessagesList[inboxMessagesList
            //                     .length -
            //                 1 -
            //                 index1]!
            //             .properties![
            //         'image_path'],
            //     inboxMessagesList[
            //             inboxMessagesList
            //                     .length -
            //                 1 -
            //                 index1]!
            //         .senderId
            //         .toString(),
            //     userName).then((value){});
          },
          child: Row(
            children: [
              SizedBox(
                height: 20,
                width: 20,
                child: Image.asset(
                  'assets/images/share_icon.png',
                ),
              ),
              SizedBox(
                width: 12,
              ),
              Text(
                "Share to General",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: "Proxima Nova",
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 16,
        ),
        InkWell(
          focusColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () {},
          child: Row(
            children: [
              SizedBox(
                height: 20,
                width: 20,
                child: Image.asset(
                  'assets/images/reply_icon.png',
                ),
              ),
              SizedBox(
                width: 12,
              ),
              Text(
                'Reply Message',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: "Proxima Nova",
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future<void> sendMessage(feedId, messageBody, attachment, message_id,
      image_path, original_sender_id, original_sender_name) async {
    checkJoinDialog();
    // if (message_id == null) {
    //   message_id = Uuid().v1();
    //   QBMessage messageObj = QBMessage();
    //   messageObj.id = null;
    //   messageObj.body = messageBody;
    //   messageObj.properties = {
    //     "message_id": message_id,
    //     "image_path": image_path,
    //     "original_sender_id": original_sender_id
    //   };
    //   messageObj.dateSent = DateTime.now().millisecondsSinceEpoch;
    //   messageObj.attachments = attachment;
    //   setState(() {
    //     inboxMessagesList.add(messageObj);
    //   });
    // }
    _newMessageSubscription!.resume();
    AppInjector.resolve<QuickbloxApiService>().sendMessage(feedId,
        body: messageBody,
        attachments: attachment,
        properties: {
          "message_id": message_id,
          "image_path": image_path,
          "original_sender_id": original_sender_id,
          "original_sender_name": original_sender_name,
        },
        saveToHistory: true,
        markable: true);
    setState(() {
      isMessageSent = true;
      isSendingLoader = false;
    });
  }

  /// Get from gallery
  // Future<void> _getProfileFromGallery() async {
  //   var pickedFile = await FilePicker.platform.pickFiles();
  //
  //   if (pickedFile != null) {
  //     _profileImageFile = File(pickedFile.files.single.path!);
  //
  //     var message_id = Uuid().v1();
  //
  //     QBMessage messageObj = QBMessage();
  //     messageObj.id = null;
  //     messageObj.body = " ";
  //     messageObj.attachments = <QBAttachment>[];
  //     messageObj.dateSent = DateTime.now().millisecondsSinceEpoch;
  //     messageObj.properties = {
  //       "message_id": message_id,
  //       "image_path": _profileImageFile!.path
  //     };
  //     setState(() {
  //       inboxMessagesList.add(messageObj);
  //     });
  //     List<QBAttachment> attachmentsResponse =
  //         await uploadImageToContentQB(_profileImageFile!.path);
  //
  //     if (attachmentsResponse != []) {
  //       // sendMessage(
  //       //     " ", attachmentsResponse, message_id, _profileImageFile!.path);
  //     } else {}
  //   }
  // }

  // /// Get from Camera
  // Future<void> _getProfileFromCamera(
  //     {required double width, required double height}) async {
  //   XFile? pickedFile = await ImagePicker().pickImage(
  //     source: ImageSource.camera,
  //     maxWidth: 640,
  //     maxHeight: 480,
  //   );
  //   if (pickedFile != null) {
  //     setState(() {
  //       _profileImageFile = File(pickedFile.path);
  //     });
  //   }
  // }
  Future<void> checkJoinDialog() async {
    QBFilter filter = QBFilter();
    filter.field = "_id";
    filter.operator = QBChatDialogFilterOperators.ALL;
    filter.value = widget.threadDetails['inbox_id'];

    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(
            "Before send msg dialog status ${(value.data[0] as QBDialog).unreadMessagesCount}");
      } else {
        BotToast.showText(
            text: "Before send msg dialog status,Try again later");
      }
    });
  }

  Future<String> getUsersName(senderId) async {
    var allUsers =
    await AppInjector.resolve<QuickbloxApiService>().getQBUsers();
    String fullName = "";
    if (allUsers.status == QuickbloxApiResponseStatus.completed) {
      print(
          "Before send msg dialog status ${(allUsers.data[0] as QBUser).fullName}");
      for (var index = 0; index < allUsers.data.length; index++) {
        if ((allUsers.data[index] as QBUser).id == senderId) {
          fullName = (allUsers.data[index] as QBUser).fullName!;
        }
      }

      return fullName;
    } else {
      BotToast.showText(text: "Before send msg dialog status,Try again later");
      return fullName;
    }
  }

  Future<List<QBAttachment>> uploadImageToContentQB(_profileImageFile) async {
    try {
      QBFile? file = await QB.content.upload(_profileImageFile, public: true);
      if (file != null) {
        int? id = file.id;
        String? contentType = file.contentType;

        QBAttachment attachment = QBAttachment();

        attachment.id = id.toString();
        attachment.contentType = contentType;
        attachment.type = "PHOTO";

        List<QBAttachment> attachmentsList = [];
        attachmentsList.add(attachment);
        print("Attachment list added $attachmentsList");
        return attachmentsList;
      } else {
        return [];
      }
    } catch (error) {
      print("------------Error is $error");
      return [];
    }
  }
}
