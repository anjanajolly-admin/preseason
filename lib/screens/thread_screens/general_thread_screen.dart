import 'dart:async';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_attachment.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_file.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_message.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:uuid/uuid.dart';

class GeneralThreadScreen extends StatefulWidget {
  final dynamic threadDetails;
  final bool isChannelManaged;
  const GeneralThreadScreen(
      {Key? key, required this.isChannelManaged, required this.threadDetails})
      : super(key: key);
  static const String routeName = "/GeneralThreadScreen";

  @override
  _GeneralThreadScreenState createState() => _GeneralThreadScreenState();
}

class _GeneralThreadScreenState extends State<GeneralThreadScreen>
    with WidgetsBindingObserver {
  List<QBMessage?>? messagesList;
  final TextEditingController _sendMessageController = TextEditingController();

  File? _profileImageFile;
  StreamSubscription? _newMessageSubscription;
  StreamSubscription? _deliveredMessageSubscription;

  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    if (WidgetsBinding.instance != null) {
      WidgetsBinding.instance!.addObserver(this);
    }

    getSubscribe();
    getSubscribeDelivered();
    super.initState();
    getMessageHistory();
  }

  @override
  void dispose() {
    // if (_newMessageSubscription != null) {
    //   _newMessageSubscription!.cancel();
    //   _newMessageSubscription = null;
    // }
    // if (_deliveredMessageSubscription != null) {
    //   _deliveredMessageSubscription!.cancel();
    //   _deliveredMessageSubscription = null;
    // }
    super.dispose();
  }

  getSubscribe() async {
    try {
      _newMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.RECEIVED_NEW_MESSAGE, (data) {
        print("Subscribed: " + QBChatEvents.RECEIVED_NEW_MESSAGE);
        Map<dynamic, dynamic> payload =
        Map<dynamic, dynamic>.from(data["payload"]);

        print("Received message : \n $payload");

        if (payload["dialogId"] == widget.threadDetails['feed_id']) {
          setMessages(payload);
          setState(() {});
        }
      }, onErrorMethod: (error) {
        print("Sub Error $error");
      });
    } catch (e) {
      print("Main Error $e");
    }
  }

  void setMessages(payload) {
    int count = 0;
    for (var msgObj in messagesList!) {
      if (msgObj!.properties!['message_id'] ==
          payload["properties"]!['message_id']) {
        setState(() {
          count = count + 1;
          msgObj.id = payload["id"];
          if (payload['attachments'] != null && payload['attachments'] != []) {
            msgObj.attachments = getAttachments(payload["attachments"][0]);
          } else {
            msgObj.attachments = [];
          }
        });
      }
    }
    if (count == 0) {
      QBMessage? receivedMessage = QBMessage();
      receivedMessage.id = payload["id"];
      receivedMessage.body = payload['body'];
      receivedMessage.dateSent = payload['dateSent'];
      receivedMessage.dialogId = payload['dialogId'];
      receivedMessage.properties = {
        "message_id": payload["properties"]["message_id"],
        "image_path": payload["properties"]["image_path"] != null
            ? payload["properties"]["image_path"]
            : ""
      };
      if (payload['attachments'] != null && payload['attachments'] != []) {
        receivedMessage.attachments = getAttachments(payload["attachments"][0]);
      } else {
        receivedMessage.attachments = [];
      }
      setState(() {
        messagesList!.add(receivedMessage);
        count = 0;
      });
    } else {}
  }

  List<QBAttachment?> getAttachments(attachmentReceived) {
    var qbAttachment = QBAttachment();
    List<QBAttachment> qbAttachmentList = [];
    qbAttachment.id = (attachmentReceived['id'].toString());
    qbAttachment.contentType = attachmentReceived['contentType'];
    qbAttachment.type = attachmentReceived['type'];
    qbAttachmentList.add(qbAttachment);
    return qbAttachmentList;
  }

  getSubscribeDelivered() async {
    try {
      _deliveredMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.MESSAGE_DELIVERED, (data) {
        // LinkedHashMap<dynamic, dynamic> messageStatusHashMap = data;
        // Map<dynamic, dynamic> messageStatusMap =
        //     Map<dynamic, dynamic>.from(messageStatusHashMap);
        // Map<dynamic, dynamic> payloadMap =
        //     Map<String, Object>.from(messageStatusHashMap["payload"]);
        print("MESSAGE_DELIVERED message: \n $data");
      });
    } catch (e) {
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<void> getMessageHistory() async {
    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;
    int limit = 0;
    int skip = 0;
    print("Dialog Id got is ${widget.threadDetails['feed_id']}");
    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogMessages(widget.threadDetails['feed_id'],
        sort: sort, limit: limit, skip: skip)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        messagesList = value.data;
      } else {
        messagesList = [];

        BotToast.showText(text: "Couldn't Retrieve messages");
      }

      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          AppInjector.resolve<AppRoutes>()
              .navigatorKey
              .currentState!
              .pop(context);
          return true;
          // print(
          //     'Back button pressed (device or appbar button), do whatever you want.');
          // var leaveDialog = await AppInjector.resolve<QuickbloxApiService>()
          //     .leaveDialog(widget.threadDetails['feed_id']);
          // if (leaveDialog.status == QuickbloxApiResponseStatus.completed) {
          //   print("Dialog Left ${widget.threadDetails['feed_id']}");
          //   AppInjector.resolve<AppRoutes>()
          //       .navigatorKey
          //       .currentState!
          //       .pop(context);
          //   return true;
          // } else {
          //   BotToast.showText(text: "Cant leave the thread");
          //   return false;
          // }
        },
        child: Scaffold(
            backgroundColor: AppWidgets.titleColor5,
            appBar: AppBar(
              backgroundColor: AppWidgets.backgroundColor,
              brightness: Brightness.dark,
              elevation: 0,
              leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () async {
                    AppInjector.resolve<AppRoutes>()
                        .navigatorKey
                        .currentState!
                        .pop(context);
                    print("Dialog Left ${widget.threadDetails}");
                    // var leaveDialog =
                    //     await AppInjector.resolve<QuickbloxApiService>()
                    //         .leaveDialog(widget.threadDetails['feed_id']);
                    // if (leaveDialog.status ==
                    //     QuickbloxApiResponseStatus.completed) {
                    //   print("Dialog Left ${widget.threadDetails['feed_id']}");
                    //   AppInjector.resolve<AppRoutes>()
                    //       .navigatorKey
                    //       .currentState!
                    //       .pop(context);
                    // } else {
                    //   BotToast.showText(text: "Cant leave the thread");
                    // }
                  }),
              title: Text(
                "General",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontFamily: "Proxima Nova",
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            body: InkWell(
              focusColor: Colors.transparent,
              highlightColor: Colors.transparent,
              hoverColor: Colors.transparent,
              splashColor: Colors.transparent,
              onTap: () {
                AppConstants.hideKeyboard(context);
              },
              child: _getBody(),
            )));
  }

  Widget _getBody() {
    return Stack(
      children: [
        messagesList == null
            ? Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 50,
                  width: 50,
                  alignment: Alignment.center,
                  child: CircularProgressIndicator.adaptive(
                      strokeWidth: 2,
                      backgroundColor: AppWidgets.buttonTextColor,
                      valueColor:
                      AlwaysStoppedAnimation<Color>(Colors.white)),
                )
              ],
            ))
            : messagesList!.isEmpty || messagesList!.length == 0
            ? Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xff464649),
                    ),
                    borderRadius: BorderRadius.circular(25),
                    color: Color(0xff3a3a3c),
                  ),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 160,
                          width: 160,
                          margin: EdgeInsets.all(20),
                          child: Align(
                              alignment: Alignment.center,
                              child: Image.asset(
                                  'assets/images/thread_empty_image.png')),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * .8,
                          child: SizedBox(
                            child: Text(
                              "Here you can share general updates with all your audience",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "It could be announcements, news etc.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0xffa4a4ab),
                            fontSize: 12,
                          ),
                        ),
                      ]),
                )
              ],
            ))
            : new Positioned(
            child: Container(
              margin: EdgeInsets.only(left: 20),
              padding: widget.isChannelManaged
                  ? EdgeInsets.only(bottom: 68, top: 8)
                  : EdgeInsets.only(bottom: 0, top: 8),
              child: Stack(
                children: [
                  ListView.builder(
                      controller: scrollController,
                      itemCount: messagesList!.length,
                      physics: AlwaysScrollableScrollPhysics(),
                      shrinkWrap: true,
                      reverse: true,
                      itemBuilder: (context, index1) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppWidgets.adminMessageSent(
                                messageObj: messagesList![
                                messagesList!.length - 1 - index1]!,
                                context: context,
                                isGeneralThread: widget.isChannelManaged
                                    ? true
                                    : false,
                                isFromInboxShared: false,
                                isFromReplyMessage: false)
                          ],
                        );
                      }),
                ],
              ),
            )),
        widget.isChannelManaged
            ? new Positioned(
            child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: Color(0xff171717),
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(
                                  color: Color(0xff464649),
                                  width: 1,
                                ),
                                color: Color(0xff1c1c1e),
                              ),
                              padding: const EdgeInsets.symmetric(
                                horizontal: 8,
                              ),
                              child: TextField(
                                controller: _sendMessageController,
                                onChanged: (value) {},
                                style: TextStyle(
                                  color: AppWidgets.titleColor3,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: "Proxima Nova",
                                ),
                                decoration: InputDecoration(
                                  hintText: "Send your first message",
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                    color: AppWidgets.titleColor3,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "Proxima Nova",
                                  ),
                                  suffixIcon: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: [
                                      IconButton(
                                          onPressed: () {
                                            _sendMessageController
                                                .text.isEmpty ||
                                                _sendMessageController
                                                    .text ==
                                                    ""
                                                ? BotToast.showText(
                                                text:
                                                "Can't send empty messages")
                                                : sendMessage(
                                                _sendMessageController
                                                    .text,
                                                <QBAttachment>[],
                                                null,
                                                "");
                                            _sendMessageController.text =
                                            "";
                                            AppConstants.hideKeyboard(
                                                context);
                                          },
                                          icon: Container(
                                            width: 24,
                                            height: 24,
                                            child: Image.asset(
                                              "assets/images/logout_icon.png",
                                              fit: BoxFit.fill,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          IconButton(
                            onPressed: () async {
                              AppConstants.hideKeyboard(context);
                              showModalBottomSheet(
                                  isDismissible: false,
                                  enableDrag: false,
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                    BorderRadius.circular(10.0),
                                  ),
                                  backgroundColor:
                                  AppWidgets.backgroundColor,
                                  context: context,
                                  builder: (context) {
                                    return StatefulBuilder(builder:
                                        (BuildContext context,
                                        StateSetter
                                        setState /*You can rename this!*/) {
                                      return Container(
                                        height: 50,
                                        margin: EdgeInsets.all(10),
                                        child: Column(
                                          children: [
                                            AppWidgets.getWtaButton(
                                                buttonName: "Select Images",
                                                fontSize: 14,
                                                width: double.infinity,
                                                height: 48,
                                                isLoader: false,
                                                onPressed: () async {
                                                  AppConstants.hideKeyboard(
                                                      context);

                                                  Navigator.pop(context);
                                                  _getProfileFromGallery();
                                                }),
                                            // SizedBox(
                                            //   height: 10,
                                            // ),
                                            // AppWidgets.getWtaButton(
                                            //     buttonName: "Camera",
                                            //     fontSize: 14,
                                            //     width: double.infinity,
                                            //     height: 48,
                                            //     isLoader: false,
                                            //     onPressed: () {
                                            //
                                            //       Navigator.pop(
                                            //           context);
                                            //       _getProfileFromCamera(
                                            //           width: 80,
                                            //           height: 80);
                                            //     }),
                                          ],
                                        ),
                                      );
                                    });
                                  });
                            },
                            icon: Container(
                              width: 24,
                              height: 24,
                              child: Image.asset(
                                "assets/images/camera_blue_icon.png",
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          // SizedBox(width: 8),
                          // IconButton(
                          //   onPressed: () {},
                          //   icon: Container(
                          //     width: 24,
                          //     height: 24,
                          //     child: Image.asset(
                          //       "assets/images/mic_blue_icon.png",
                          //       fit: BoxFit.fill,
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    ],
                  ),
                )))
            : Container()
      ],
    );
  }

  Future<void> sendMessage(
      messageBody, attachment, message_id, image_path) async {
    if (message_id == null) {
      message_id = Uuid().v1();
      QBMessage messageObj = QBMessage();
      messageObj.id = null;
      messageObj.body = messageBody;
      messageObj.properties = {
        "message_id": message_id,
        "image_path": image_path
      };
      messageObj.dateSent = DateTime.now().millisecondsSinceEpoch;
      messageObj.attachments = attachment;
      setState(() {
        messagesList!.add(messageObj);
      });
    }
    _newMessageSubscription!.resume();
    AppInjector.resolve<QuickbloxApiService>()
        .sendMessage(widget.threadDetails['feed_id'],
        body: messageBody,
        attachments: attachment,
        properties: {"message_id": message_id, "image_path": image_path},
        saveToHistory: true,
        markable: true)
        .then((value) async {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        List<String> recipientsIds = await getOccupantIds();
        setState(() {
          sendPushNotification(messageBody,
              widget.threadDetails['channel']['name'], "Admin", recipientsIds);
        });
      } else {
        BotToast.showText(text: "Couldn't send message. Try again later.");
      }
    });
  }

  Future<void> sendPushNotification(String message, String threadName,
      String senderName, List<String> recipientsIds) async {
    AppInjector.resolve<QuickbloxApiService>()
        .sendPushNotification(
        notificationName: threadName,
        message: message,
        senderName: senderName,
        recipientsIds: recipientsIds,
        navigationObject: widget.threadDetails,
        screen: "1")
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        setState(() {
          print('Successful sent');
        });
      } else {
        setState(() {
          print('Unsuccessful');
        });
      }
    });
  }

  /// Get from gallery
  Future<void> _getProfileFromGallery() async {
    var pickedFile = await FilePicker.platform.pickFiles();

    if (pickedFile != null) {
      _profileImageFile = File(pickedFile.files.single.path!);

      var message_id = Uuid().v1();

      QBMessage messageObj = QBMessage();
      messageObj.id = null;
      messageObj.body = " ";
      messageObj.attachments = <QBAttachment>[];
      messageObj.dateSent = DateTime.now().millisecondsSinceEpoch;
      messageObj.properties = {
        "message_id": message_id,
        "image_path": _profileImageFile!.path
      };
      setState(() {
        messagesList!.add(messageObj);
      });
      List<QBAttachment> attachmentsResponse =
      await uploadImageToContentQB(_profileImageFile!.path);

      if (attachmentsResponse != []) {
        sendMessage(
            " ", attachmentsResponse, message_id, _profileImageFile!.path);
      } else {}
    }
  }

  // /// Get from Camera
  // Future<void> _getProfileFromCamera(
  //     {required double width, required double height}) async {
  //   XFile? pickedFile = await ImagePicker().pickImage(
  //     source: ImageSource.camera,
  //     maxWidth: 640,
  //     maxHeight: 480,
  //   );
  //   if (pickedFile != null) {
  //     setState(() {
  //       _profileImageFile = File(pickedFile.path);
  //     });
  //   }
  // }
  Future<List<String>> getOccupantIds() async {
    List<String> occupantsIds = [];
    QBFilter filter = QBFilter();
    filter.field = "_id";
    filter.operator = QBChatDialogFilterOperators.ALL;
    filter.value = widget.threadDetails['feed_id'];

    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(
            "Before send msg dialog status ${(value.data[0] as QBDialog).occupantsIds}");
        List<String> tempOccupantId = [];
        for (int i = 0;
        i < (value.data[0] as QBDialog).occupantsIds!.length;
        i++) {
          if (((value.data[0] as QBDialog).occupantsIds![i]) !=
              AppInjector.resolve<AppPreferences>().getQbUserId()) {
            tempOccupantId
                .add((value.data[0] as QBDialog).occupantsIds![i].toString());
          }
        }
        occupantsIds = tempOccupantId;
      } else {
        occupantsIds = [];
      }
    });
    return occupantsIds;
  }

  Future<List<QBAttachment>> uploadImageToContentQB(_profileImageFile) async {
    try {
      QBFile? file = await QB.content.upload(_profileImageFile, public: true);
      if (file != null) {
        int? id = file.id;
        String? contentType = file.contentType;

        QBAttachment attachment = QBAttachment();

        attachment.id = id.toString();
        attachment.contentType = contentType;
        attachment.type = "PHOTO";

        List<QBAttachment> attachmentsList = [];
        attachmentsList.add(attachment);
        print("Attachment list added $attachmentsList");
        return attachmentsList;
      } else {
        return [];
      }
    } catch (error) {
      print("------------Error is $error");
      return [];
    }
  }
}
