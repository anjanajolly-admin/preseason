import 'dart:async';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/screens/thread_screens/thread_inbox_screen.dart';
import 'package:pre_seasoned/utils/app_constants.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_attachment.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_file.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_message.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';
import 'package:quickblox_sdk/models/qb_user.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:uuid/uuid.dart';

class NonGeneralThreadScreen extends StatefulWidget {
  final dynamic threadDetails;
  final String generalDialogId;
  final bool newThread;

  const NonGeneralThreadScreen(
      {Key? key,
        required this.threadDetails,
        required this.generalDialogId,
        required this.newThread})
      : super(key: key);
  static const String routeName = "/NonGeneralThreadScreen";

  @override
  _NonGeneralThreadScreenState createState() => _NonGeneralThreadScreenState();
}

class _NonGeneralThreadScreenState extends State<NonGeneralThreadScreen>
    with WidgetsBindingObserver {
  List<QBMessage?>? messagesList;
  final TextEditingController _sendMessageController = TextEditingController();

  File? _profileImageFile;
  StreamSubscription? _newMessageSubscription;
  StreamSubscription? _deliveredMessageSubscription;

  ScrollController scrollController = ScrollController();
  bool showDialogBox = false;
  bool showBottomSheet = false;
  int? unreadInboxMessageCount = 0;
  @override
  void initState() {
    if (WidgetsBinding.instance != null) {
      WidgetsBinding.instance!.addObserver(this);
    }

    getUnreadMessagesCount(widget.threadDetails['inbox_id']);
    getSubscribe();
    getSubscribeDelivered();
    super.initState();
    getMessageHistory();
    showBottomSheet = true;
  }

  @override
  void dispose() {
    // if (_newMessageSubscription != null) {
    //   _newMessageSubscription!.cancel();
    //   _newMessageSubscription = null;
    // }
    // if (_deliveredMessageSubscription != null) {
    //   _deliveredMessageSubscription!.cancel();
    //   _deliveredMessageSubscription = null;
    // }
    super.dispose();
  }

  Future<void> getUnreadMessagesCount(inboxId) async {
    QBFilter filter = QBFilter();
    filter.field = "_id";
    filter.operator = QBChatDialogFilterOperators.ALL;
    filter.value = inboxId;

    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    var getDialogDetails = await AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit);
    if (getDialogDetails.status == QuickbloxApiResponseStatus.completed) {
      setState(() {
        unreadInboxMessageCount =
        ((getDialogDetails.data[0] as QBDialog).unreadMessagesCount == null
            ? 0
            : (getDialogDetails.data[0] as QBDialog).unreadMessagesCount)!;
      });
    } else {
      unreadInboxMessageCount = 0;
    }
    setState(() {});
  }

  getSubscribe() async {
    try {
      _newMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.RECEIVED_NEW_MESSAGE, (data) {
        print("Subscribed: " + QBChatEvents.RECEIVED_NEW_MESSAGE);
        Map<dynamic, dynamic> payload =
        Map<dynamic, dynamic>.from(data["payload"]);

        print("Received message : \n $payload");
        if (payload["dialogId"] == widget.threadDetails['feed_id']) {
          refreshList(payload);
          // setState(() {});
        } else if (payload["dialogId"] == widget.threadDetails['inbox_id']) {
          getUnreadMessagesCount(widget.threadDetails['inbox_id']);
          setState(() {});
        }
      }, onErrorMethod: (error) {
        print("Sub Error $error");
      });
    } catch (e) {
      print("Main Error $e");
    }
  }

  Future<void> refreshList(payload) async {
    int count = 0;
    for (var msgObj in messagesList!) {
      if (msgObj!.properties!['message_id'] ==
          payload["properties"]!['message_id']) {
        setState(() {
          count = count + 1;
          msgObj.id = payload["id"];
          if (payload['attachments'] != null && payload['attachments'] != []) {
            msgObj.attachments = getAttachments(payload["attachments"][0]);
          } else {
            msgObj.attachments = [];
          }
        });
      }
    }
    if (count == 0) {
      QBMessage? receivedMessage = QBMessage();
      receivedMessage.id = payload["id"];
      receivedMessage.body = payload['body'];
      receivedMessage.dateSent = payload['dateSent'];
      receivedMessage.dialogId = payload['dialogId'];
      receivedMessage.senderId = payload['senderId'];
      receivedMessage.properties = {
        "message_id": payload["properties"]["message_id"],
        "image_path": payload["properties"]["image_path"] != null
            ? payload["properties"]["image_path"]
            : "",
      };
      if (payload['attachments'] != null && payload['attachments'] != []) {
        receivedMessage.attachments = getAttachments(payload["attachments"][0]);
      } else {
        receivedMessage.attachments = [];
      }
      AppInjector.resolve<QuickbloxApiService>()
          .markMessageRead(receivedMessage);
      setState(() {
        messagesList!.add(receivedMessage);
        count = 0;
      });
    } else {}
  }

  List<QBAttachment?> getAttachments(attachmentReceived) {
    var qbAttachment = QBAttachment();
    List<QBAttachment> qbAttachmentList = [];
    qbAttachment.id = (attachmentReceived['id'].toString());
    qbAttachment.contentType = attachmentReceived['contentType'];
    qbAttachment.type = attachmentReceived['type'];
    qbAttachmentList.add(qbAttachment);
    return qbAttachmentList;
  }

  getSubscribeDelivered() async {
    try {
      _deliveredMessageSubscription = await QB.chat
          .subscribeChatEvent(QBChatEvents.MESSAGE_DELIVERED, (data) {
        // LinkedHashMap<dynamic, dynamic> messageStatusHashMap = data;
        // Map<dynamic, dynamic> messageStatusMap =
        //     Map<dynamic, dynamic>.from(messageStatusHashMap);
        // Map<dynamic, dynamic> payloadMap =
        //     Map<String, Object>.from(messageStatusHashMap["payload"]);
        print("MESSAGE_DELIVERED message: \n $data");
      });
    } catch (e) {
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<void> getMessageHistory() async {
    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;
    int limit = 0;
    int skip = 0;
    print("Dialog Id got is ${widget.threadDetails['feed_id']}");
    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogMessages(widget.threadDetails['feed_id'],
        sort: sort, limit: limit, skip: skip)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        setState(() {
          messagesList = value.data;
        });
      } else {
        setState(() {
          messagesList = [];
        });
        BotToast.showText(text: "Couldn't Retrieve messages");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          AppInjector.resolve<AppRoutes>()
              .navigatorKey
              .currentState!
              .pop(context);
          // print(
          //     'Back button pressed (device or appbar button), do whatever you want.');
          // var leaveDialog = await AppInjector.resolve<QuickbloxApiService>()
          //     .leaveDialog(widget.threadDetails['feed_id']);
          // if (leaveDialog.status == QuickbloxApiResponseStatus.completed) {
          //   print("Dialog Left ${widget.threadDetails['feed_id']}");
          //   AppInjector.resolve<AppRoutes>()
          //       .navigatorKey
          //       .currentState!
          //       .pop(context);
          //   return true;
          // } else {
          //   BotToast.showText(text: "Cant leave the thread");
          //   return false;
          // }
          return true;
        },
        child: Scaffold(
          backgroundColor: AppWidgets.titleColor5,
          appBar: AppBar(
            backgroundColor: AppWidgets.backgroundColor,
            brightness: Brightness.dark,
            elevation: 0,
            leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () async {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .pop(context);
                  // var leaveDialog =
                  //     await AppInjector.resolve<QuickbloxApiService>()
                  //         .leaveDialog(widget.threadDetails['feed_id']);
                  // if (leaveDialog.status ==
                  //     QuickbloxApiResponseStatus.completed) {
                  //   print("Dialog Left ${widget.threadDetails['feed_id']}");
                  //   AppInjector.resolve<AppRoutes>()
                  //       .navigatorKey
                  //       .currentState!
                  //       .pop(context);
                  // } else {
                  //   BotToast.showText(text: "Cant leave the thread");
                  // }
                }),
            title: Text(
              widget.threadDetails['name'],
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontFamily: "Proxima Nova",
                fontWeight: FontWeight.w600,
              ),
            ),
            actions: [
              Row(
                children: [
                  IconButton(
                    onPressed: () async {
                      AppConstants.hideKeyboard(context);
                      showModalBottomSheet(
                          isDismissible: false,
                          enableDrag: false,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          backgroundColor: AppWidgets.backgroundColor,
                          context: context,
                          builder: (context) {
                            return StatefulBuilder(builder: (BuildContext
                            context,
                                StateSetter
                                setModalState /*You can rename this!*/) {
                              return Container(
                                  margin: EdgeInsets.all(20),
                                  child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                      MainAxisAlignment.start,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                            children: [
                                              Expanded(
                                                child: Text(
                                                  "Create",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                    fontFamily: "Proxima Nova",
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                              InkWell(
                                                focusColor: Colors.transparent,
                                                highlightColor:
                                                Colors.transparent,
                                                hoverColor: Colors.transparent,
                                                splashColor: Colors.transparent,
                                                onTap: () {
                                                  AppInjector.resolve<
                                                      AppRoutes>()
                                                      .navigatorKey
                                                      .currentState!
                                                      .pop();
                                                },
                                                child: Container(
                                                  width: 24,
                                                  height: 24,
                                                  child: Image.asset(
                                                    "assets/images/cancel_icon.png",
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Container(
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                width: 24,
                                                height: 24,
                                                child: Image.asset(
                                                  "assets/images/broadcast_go_live.png",
                                                  fit: BoxFit.fill,
                                                ),
                                              ),
                                              SizedBox(width: 16),
                                              Expanded(
                                                child: SizedBox(
                                                  child: Text(
                                                    " Go Live ",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                      fontFamily:
                                                      "Proxima Nova",
                                                      fontWeight:
                                                      FontWeight.w600,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ]));
                            });
                          });
                    },
                    icon: Container(
                      width: 24,
                      height: 24,
                      child: Image.asset(
                        "assets/images/add_notification_icon.png",
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  InkWell(
                      focusColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        joinDialog();
                      },
                      child: Container(
                        height: 30,
                        margin: EdgeInsets.only(right: 15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          border: Border.all(
                            color: Color(0xff0080ff),
                            width: 1,
                          ),
                        ),
                        padding: const EdgeInsets.only(
                          left: 8,
                          right: 4,
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              "Inbox",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0xff5cabff),
                                fontSize: 14,
                                fontFamily: "Proxima Nova",
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(width: 4),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(2),
                                color: Color(0xffe53535),
                              ),
                              padding: const EdgeInsets.all(2),
                              child: Text(
                                unreadInboxMessageCount.toString(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 10,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            ],
          ),
          body: InkWell(
            focusColor: Colors.transparent,
            highlightColor: Colors.transparent,
            hoverColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              AppConstants.hideKeyboard(context);
            },
            child: _getBody(),
          ),
          bottomSheet: widget.newThread && showBottomSheet
              ? Container(
            width: double.infinity,
            decoration: BoxDecoration(
              //borderRadius: BorderRadius.only(topLeft: Radius.circular(12)),
              color: Color(0xff3a3a3c),
            ),
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
              vertical: 24,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  child: Text(
                    "How Feed Works?",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                SizedBox(height: 16),
                SizedBox(
                  child: Row(
                    children: [
                      Icon(
                        Icons.circle,
                        color: Colors.white,
                        size: 6,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        child: Container(
                          child: Text(
                            "The messages you sent will be shown will show in feed.",
                            style: TextStyle(
                              color: Color(0xfff2f2f7),
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 8),
                SizedBox(
                  child: Row(
                    children: [
                      Icon(
                        Icons.circle,
                        color: Colors.white,
                        size: 6,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        child: Container(
                          child: Text(
                            "Only admin can send messages to feed.",
                            style: TextStyle(
                              color: Color(0xfff2f2f7),
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 8),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      color: Colors.white,
                      size: 6,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        child: Text(
                          'Also, you can reply to the message sent by the audience which will be shown in feed',
                          style: TextStyle(
                            color: Color(0xfff2f2f7),
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16),
                AppWidgets.getWtaButton(
                    buttonName: 'Ok, Got it!',
                    fontSize: 16,
                    width: double.infinity,
                    height: 40,
                    isLoader: false,
                    onPressed: () {
                      setState(() {
                        showBottomSheet = false;
                        showDialogBox = true;
                      });
                    })
              ],
            ),
          )
              : Container(
            height: 0,
            width: 0,
          ),
        ));
  }

  Widget _getBody() {
    return Stack(
      children: [
        messagesList == null
            ? Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: SizedBox(
                      height: 50,
                      width: 50,
                      child: CircularProgressIndicator.adaptive(
                          strokeWidth: 2,
                          backgroundColor: AppWidgets.buttonTextColor,
                          valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.white))),
                )
              ],
            ))
            : Stack(
          children: [
            widget.newThread
                ? showDialogBox
                ? _getInfoCard(
              "Here you can see all the messages sent by the audience and you can reply and share them to the live feed",
            )
                : Container()
                : messagesList!.isEmpty
                ? Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin:
                      EdgeInsets.only(left: 20, right: 20),
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Color(0xff464649),
                        ),
                        borderRadius: BorderRadius.circular(25),
                        color: Color(0xff3a3a3c),
                      ),
                      child: Column(
                          mainAxisAlignment:
                          MainAxisAlignment.center,
                          crossAxisAlignment:
                          CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 160,
                              width: 160,
                              margin: EdgeInsets.all(20),
                              child: Align(
                                  alignment: Alignment.center,
                                  child: Image.asset(
                                      'assets/images/thread_empty_image.png')),
                            ),
                            Container(
                              width: MediaQuery.of(context)
                                  .size
                                  .width *
                                  .8,
                              child: SizedBox(
                                child: Text(
                                  "Send you first message and start the engagement with your audience",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Messages in Feed can be seen by everyone.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0xffa4a4ab),
                                fontSize: 12,
                              ),
                            ),
                          ]),
                    )
                  ],
                ))
                : Container(
              padding: EdgeInsets.only(bottom: 68),
              child: Container(
                margin: EdgeInsets.only(
                    left: 20, right: 20, top: 54),
                child: ListView.builder(
                    controller: scrollController,
                    itemCount: messagesList!.length,
                    physics: AlwaysScrollableScrollPhysics(),
                    shrinkWrap: true,
                    reverse: true,
                    itemBuilder: (context, index1) {
                      return Row(
                        mainAxisAlignment: (messagesList![messagesList!.length - 1 - index1]!
                            .properties![
                        'original_sender_id'] !=
                            null ||
                            messagesList![messagesList!.length - 1 - index1]!
                                .properties![
                            'original_sender_id'] ==
                                AppInjector.resolve<AppPreferences>()
                                    .getQbUserId()
                                    .toString()
                            ? int.parse(messagesList![
                        messagesList!
                            .length -
                            1 -
                            index1]!
                            .properties![
                        'original_sender_id']!)
                            : 0) ==
                            AppInjector.resolve<AppPreferences>()
                                .getQbUserId()
                            ? MainAxisAlignment.end
                            : MainAxisAlignment.start,
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          AppWidgets.adminInboxConversation(
                              messageObj: messagesList![
                              messagesList!.length -
                                  1 -
                                  index1]!,
                              context: context,
                              isFromReplyMessage: false)
                        ],
                      );
                    }),
              ),
            ),
          ],
        ),
        Container(
          height: 48,
          color: Color(0xff2c2c2e),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    padding: const EdgeInsets.all(4),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 12,
                          height: 12,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color(0xff07cf87),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 8),
                  Text(
                    "Feed",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Container(
                color: AppWidgets.titleColor4,
                width: double.infinity,
                height: 1,
              ),
            ],
          ),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: Color(0xff171717),
              padding: const EdgeInsets.all(8),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        border: Border.all(
                          color: Color(0xff464649),
                          width: 1,
                        ),
                        color: Color(0xff1c1c1e),
                      ),
                      padding: const EdgeInsets.symmetric(
                        horizontal: 8,
                      ),
                      child: TextField(
                        controller: _sendMessageController,
                        onChanged: (value) {},
                        style: TextStyle(
                          color: AppWidgets.titleColor3,
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontFamily: "Proxima Nova",
                        ),
                        decoration: InputDecoration(
                          hintText: "Send your first message",
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                            color: AppWidgets.titleColor3,
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            fontFamily: "Proxima Nova",
                          ),
                          suffixIcon: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              IconButton(
                                  onPressed: () async {
                                    _sendMessageController.text.isEmpty ||
                                        _sendMessageController.text == ""
                                        ? BotToast.showText(
                                        text: "Can't send empty messages")
                                        : sendMessage(
                                      _sendMessageController.text,
                                      <QBAttachment>[],
                                      null,
                                      "",
                                      AppInjector.resolve<
                                          AppPreferences>()
                                          .getQbUserId(),
                                    );
                                    _sendMessageController.text = "";
                                    AppConstants.hideKeyboard(context);
                                  },
                                  icon: Container(
                                    width: 24,
                                    height: 24,
                                    child: Image.asset(
                                      "assets/images/logout_icon.png",
                                      fit: BoxFit.fill,
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  IconButton(
                    onPressed: () async {
                      AppConstants.hideKeyboard(context);
                      showModalBottomSheet(
                          isDismissible: false,
                          enableDrag: false,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          backgroundColor: AppWidgets.backgroundColor,
                          context: context,
                          builder: (context) {
                            return StatefulBuilder(builder: (BuildContext
                            context,
                                StateSetter
                                setModalState /*You can rename this!*/) {
                              return Container(
                                height: 50,
                                margin: EdgeInsets.all(10),
                                child: Column(
                                  children: [
                                    AppWidgets.getWtaButton(
                                        buttonName: "Select Images",
                                        fontSize: 14,
                                        width: double.infinity,
                                        height: 48,
                                        isLoader: false,
                                        onPressed: () async {
                                          AppConstants.hideKeyboard(context);

                                          Navigator.pop(context);
                                          _getProfileFromGallery();
                                        }),
                                    // SizedBox(
                                    //   height: 10,
                                    // ),
                                    // AppWidgets.getWtaButton(
                                    //     buttonName: "Camera",
                                    //     fontSize: 14,
                                    //     width: double.infinity,
                                    //     height: 48,
                                    //     isLoader: false,
                                    //     onPressed: () {
                                    //
                                    //       Navigator.pop(
                                    //           context);
                                    //       _getProfileFromCamera(
                                    //           width: 80,
                                    //           height: 80);
                                    //     }),
                                  ],
                                ),
                              );
                            });
                          });
                    },
                    icon: Container(
                      width: 24,
                      height: 24,
                      child: Image.asset(
                        "assets/images/camera_blue_icon.png",
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  // SizedBox(width: 8),
                  // IconButton(
                  //   onPressed: () {},
                  //   icon: Container(
                  //     width: 24,
                  //     height: 24,
                  //     child: Image.asset(
                  //       "assets/images/mic_blue_icon.png",
                  //       fit: BoxFit.fill,
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ))
      ],
    );
  }

  Widget _getInfoCard(subTitle) {
    return Column(
      //mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0, 0, 30, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ClipPath(
                clipper: TriangleClipper(),
                child: Container(
                  // margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
                  color: Color(0xff3a3a3c),
                  height: 15,
                  width: 20,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(30, 0, 10,
              0), //extra 10 for top padding because triangle's height = 10
          decoration: BoxDecoration(
            color: Color(0xff3a3a3c),
            borderRadius: BorderRadius.circular(10),
          ),

          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Container(
                      margin: EdgeInsets.all(12),
                      child: Column(
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width - 20,
                            child: Text(
                              subTitle,
                              style: TextStyle(
                                color: Color(0xffa4a4ab),
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ))),
              InkWell(
                focusColor: Colors.transparent,
                highlightColor: Colors.transparent,
                hoverColor: Colors.transparent,
                splashColor: Colors.transparent,
                onTap: () {
                  setState(() {
                    showDialogBox = false;
                  });
                },
                child: Container(
                  margin: EdgeInsets.all(12),
                  width: 16,
                  height: 16,
                  child: Image.asset(
                    "assets/images/cancel_icon.png",
                    fit: BoxFit.fill,
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Future<void> sendMessage(messageBody, attachment, message_id, image_path,
      original_sender_id) async {
    checkJoinDialog();

    if (message_id == null) {
      message_id = Uuid().v1();
      QBMessage messageObj = QBMessage();
      messageObj.id = null;
      messageObj.body = messageBody;
      messageObj.properties = {
        "message_id": message_id,
        "image_path": image_path,
        "original_sender_id": original_sender_id.toString(),
        "original_sender_name": "",
      };
      messageObj.dateSent = DateTime.now().millisecondsSinceEpoch;
      messageObj.attachments = attachment;
      setState(() {
        messagesList!.add(messageObj);
      });
    }
    var original_sender_name =
    await getUsersName(AppInjector.resolve<AppPreferences>().getQbUserId());
    _newMessageSubscription!.resume();
    AppInjector.resolve<QuickbloxApiService>()
        .sendMessage(widget.threadDetails['feed_id'],
        body: messageBody,
        attachments: attachment,
        properties: {
          "message_id": message_id,
          "image_path": image_path,
          "original_sender_id": original_sender_id.toString(),
          "original_sender_name": original_sender_name,
        },
        saveToHistory: true,
        markable: true);
    //     .then((value) {
    //   if (value.status ==
    //       QuickbloxApiResponseStatus
    //           .completed) {
    //     setState(() {
    //       scrollController.jumpTo(
    //           scrollController.position
    //               .minScrollExtent);
    //     });
    //   } else {
    //     BotToast.showText(
    //         text:
    //             "Couldn't send message. Try again later.");
    //   }
    // });
  }

  /// Get from gallery
  Future<void> _getProfileFromGallery() async {
    var pickedFile = await FilePicker.platform.pickFiles();

    if (pickedFile != null) {
      _profileImageFile = File(pickedFile.files.single.path!);

      var message_id = Uuid().v1();

      QBMessage messageObj = QBMessage();
      messageObj.id = null;
      messageObj.body = " ";
      messageObj.attachments = <QBAttachment>[];
      messageObj.dateSent = DateTime.now().millisecondsSinceEpoch;
      messageObj.properties = {
        "message_id": message_id,
        "image_path": _profileImageFile!.path,
        "original_sender_id":
        AppInjector.resolve<AppPreferences>().getQbUserId().toString(),
        "original_sender_name":
        AppInjector.resolve<AppPreferences>().getQbUserId().toString(),
      };
      setState(() {
        messagesList!.add(messageObj);
      });
      List<QBAttachment> attachmentsResponse =
      await uploadImageToContentQB(_profileImageFile!.path);
      if (attachmentsResponse != []) {
        sendMessage(
          " ",
          attachmentsResponse,
          message_id,
          _profileImageFile!.path,
          AppInjector.resolve<AppPreferences>().getQbUserId(),
        );
      } else {}
    }
  }

  // /// Get from Camera
  // Future<void> _getProfileFromCamera(
  //     {required double width, required double height}) async {
  //   XFile? pickedFile = await ImagePicker().pickImage(
  //     source: ImageSource.camera,
  //     maxWidth: 640,
  //     maxHeight: 480,
  //   );
  //   if (pickedFile != null) {
  //     setState(() {
  //       _profileImageFile = File(pickedFile.path);
  //     });
  //   }
  // }
  Future<void> checkJoinDialog() async {
    QBFilter filter = QBFilter();
    filter.field = "_id";
    filter.operator = QBChatDialogFilterOperators.ALL;
    filter.value = widget.threadDetails['feed_id'];

    QBSort sort = QBSort();
    sort.field = QBChatDialogSorts.LAST_MESSAGE_DATE_SENT;
    sort.ascending = false;

    int limit = 0;
    int skip = 0;

    await AppInjector.resolve<QuickbloxApiService>()
        .getDialogs(skip: skip, sort: sort, filter: filter, limit: limit)
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(
            "Before send msg dialog status ${(value.data[0] as QBDialog).isJoined}");
      } else {
        BotToast.showText(
            text: "Before send msg dialog status,Try again later");
      }
    });
  }

  Future<String> getUsersName(senderId) async {
    var allUsers =
    await AppInjector.resolve<QuickbloxApiService>().getQBUsers();
    String fullName = "";
    if (allUsers.status == QuickbloxApiResponseStatus.completed) {
      print(
          "Before send msg dialog status ${(allUsers.data[0] as QBUser).fullName}");
      for (var index = 0; index < allUsers.data.length; index++) {
        if ((allUsers.data[index] as QBUser).id == senderId) {
          fullName = (allUsers.data[index] as QBUser).fullName!;
        }
      }

      return fullName;
    } else {
      BotToast.showText(text: "Before send msg dialog status,Try again later");
      return fullName;
    }
  }

  Future<void> joinDialog() async {
    AppInjector.resolve<QuickbloxApiService>()
        .joinDialog(widget.threadDetails['feed_id'])
        .then((value) {
      if (value.status == QuickbloxApiResponseStatus.completed) {
        print(value.data);
        checkJoinDialog();
        AppInjector.resolve<AppRoutes>()
            .navigatorKey
            .currentState!
            .pushNamed(InboxThreadScreen.routeName, arguments: [
          widget.threadDetails,
          widget.generalDialogId,
          widget.newThread
        ]).then((value) {
          getSubscribe();
          getMessageHistory();
        });
      } else {
        BotToast.showText(text: "Couldn't join the thread,Try again later");
      }
    });
  }

  Future<List<QBAttachment>> uploadImageToContentQB(_profileImageFile) async {
    try {
      QBFile? file = await QB.content.upload(_profileImageFile, public: true);
      if (file != null) {
        int? id = file.id;
        String? contentType = file.contentType;

        QBAttachment attachment = QBAttachment();

        attachment.id = id.toString();
        attachment.contentType = contentType;
        attachment.type = "PHOTO";

        List<QBAttachment> attachmentsList = [];
        attachmentsList.add(attachment);
        print("Attachment list added $attachmentsList");
        return attachmentsList;
      } else {
        return [];
      }
    } catch (error) {
      print("------------Error is $error");
      return [];
    }
  }
}

class TriangleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.moveTo(size.width / 2, 0.0);
    path.lineTo(size.width, size.height);
    path.lineTo(0.0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(TriangleClipper oldClipper) => false;
}
