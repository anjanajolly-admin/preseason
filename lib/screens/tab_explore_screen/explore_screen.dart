import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/screens/create_channel_screens/channel_details_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

import 'explore_search_screen.dart';

class ExploreChannels extends StatefulWidget {
  final bool isFromBottom;
  final List<dynamic> exploreChannelsList;
  final List<bool> joinExploreIndex;
  const ExploreChannels(
      {Key? key,
        required this.isFromBottom,
        required this.exploreChannelsList,
        required this.joinExploreIndex})
      : super(key: key);
  static const String routeName = "/ExploreChannels";

  @override
  _ExploreChannelsState createState() => _ExploreChannelsState();
}

class _ExploreChannelsState extends State<ExploreChannels> {
  List<dynamic>? exploreChannelsList;
  bool exploreChannelsLoader = false;
  int joinLoaderIndex = -1;
  List<bool> joinExploreIndex = [];
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  // bool isFromBottom = false;

  @override
  void initState() {
    super.initState();

    exploreChannelsList = widget.exploreChannelsList;
    joinExploreIndex = widget.joinExploreIndex;
    // _getAllApiCalls();
  }

  @override
  void didChangeDependencies() {
    print("Changes made in explore");
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<Null> refresh() async {
    await Future.delayed(Duration(seconds: 2));
    refreshKey.currentState?.show();
    _getAllApiCalls();
  }

  _getAllApiCalls() {
    AppInjector.resolve<ApiService>().suggestedChannelsForUser().then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          exploreChannelsLoader = false;
          // isFromBottom = false;
          exploreChannelsList = convert.jsonDecode(value.data);
          joinExploreIndex =
              List.generate(exploreChannelsList!.length, (index) => false);
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          // isFromBottom = false;
          exploreChannelsLoader = false;
          BotToast.showText(text: value.message[1]);
        });
      } else {
        setState(() {
          // isFromBottom = false;
          exploreChannelsLoader = false;
          BotToast.showText(text: "Something went wrong.Please try later");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff2c2c2e),
      appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                AppInjector.resolve<AppRoutes>()
                    .navigatorKey
                    .currentState!
                    .pushNamed(ExploreSearchScreen.routeName,
                    arguments: exploreChannelsList)
                    .then((value) {
                  setState(() {
                    exploreChannelsLoader = true;
                  });
                  _getAllApiCalls();
                });
              },
              icon: Image.asset(
                "assets/images/search_icon.png",
                fit: BoxFit.fill,
              ),
              iconSize: 12,
            ),
            SizedBox(
              width: 8,
            ),
          ],
          backgroundColor: AppWidgets.backgroundColor,
          brightness: Brightness.dark,
          elevation: 0,
          leadingWidth: 100,
          leading: Center(
            child: Text(
              "Explore",
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontFamily: "Proxima Nova",
                fontWeight: FontWeight.w600,
              ),
            ),
          )),
      body: _getBody(),
    );
  }

  Widget _getBody() {
    return Stack(children: [
      (exploreChannelsList!.isEmpty || exploreChannelsList!.length == 0)
          ? Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          child: Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 180,
                    height: 180,
                    child: Image.asset(
                      "assets/images/messages_empty_image.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: SizedBox(
                      width: double.infinity,
                      child: Text(
                        "No channel are there to be shown",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                ],
              )))
          : SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 16,
            ),
            ListView.builder(
              itemCount: exploreChannelsList!.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (BuildContext context, exploreIndex) {
                return Container(
                    margin: EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      children: [
                        InkWell(
                          focusColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          hoverColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onTap: () {
                            print(exploreChannelsList![exploreIndex]);

                            AppInjector.resolve<AppRoutes>()
                                .navigatorKey
                                .currentState!
                                .pushNamed(ChannelDetailsScreen.routeName,
                                arguments: [
                                  exploreChannelsList![exploreIndex]
                                  ['id'],
                                  false,
                                  exploreChannelsList![exploreIndex]
                                  ['profilePhoto'] ==
                                      null
                                      ? null
                                      : exploreChannelsList![exploreIndex]
                                  ['profilePhoto']['url']
                                  as String,
                                  exploreChannelsList![exploreIndex]
                                  ['coverPhoto'] ==
                                      null
                                      ? null
                                      : exploreChannelsList![exploreIndex]
                                  ['coverPhoto']['url'] as String,
                                  false,
                                  true
                                ]).then((_) {
                              setState(() {
                                _getAllApiCalls();
                              });
                            });
                          },
                          child: Container(
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Color(0xff3a3a3c),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                    BorderRadius.circular(8),
                                  ),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                          width: 40,
                                          height: 40,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.only(
                                              topLeft: Radius.circular(8),
                                              topRight:
                                              Radius.circular(8),
                                              bottomLeft:
                                              Radius.circular(0),
                                              bottomRight:
                                              Radius.circular(0),
                                            ),
                                          ),
                                          child: exploreChannelsList![
                                          exploreIndex]
                                          ['profilePhoto'] ==
                                              null
                                              ? Image.asset(
                                            "assets/images/suggested_channel_cover.png",
                                            fit: BoxFit.fill,
                                          )
                                              : CachedNetworkImage(
                                            imageUrl: exploreChannelsList![
                                            exploreIndex]
                                            ['profilePhoto']
                                            ['url'],
                                            fit: BoxFit.fill,
                                            placeholder:
                                                (context, url) =>
                                                Image.asset(
                                                  "assets/images/suggested_channel_cover.png",
                                                  fit: BoxFit.fill,
                                                ),
                                            //
                                            // placeholder: (context, url) =>
                                            //     CircularProgressIndicator(),
                                            errorWidget: (context,
                                                url, error) {
                                              print(
                                                  'errorWidget ${error.toString()} \n$url');
                                              return Image.asset(
                                                "assets/images/suggested_channel_cover.png",
                                                fit: BoxFit.fill,
                                              );
                                            },
                                          )),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    margin:
                                    EdgeInsets.only(left: 8, top: 3),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                      MainAxisAlignment.start,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: Text(
                                                  exploreChannelsList![
                                                  exploreIndex]
                                                  ['name'],
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16,
                                                    fontFamily:
                                                    "Proxima Nova",
                                                    fontWeight:
                                                    FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(height: 16),
                                        Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: Text(
                                                  "${exploreChannelsList![exploreIndex]['membersCount']} Members. Rate",
                                                  textAlign:
                                                  TextAlign.start,
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 14,
                                                  ),
                                                ),
                                              )
                                            ]),
                                        SizedBox(height: 8),
                                        ListView.builder(
                                          itemCount:
                                          (exploreChannelsList![
                                          exploreIndex]
                                          ['threads'] as List)
                                              .length,
                                          physics:
                                          NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          itemBuilder:
                                              (BuildContext context,
                                              threadIndex) {
                                            return Column(
                                              children: [
                                                Container(
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: 20,
                                                        height: 20,
                                                        child:
                                                        Image.asset(
                                                          "assets/images/message_icon.png",
                                                          fit:
                                                          BoxFit.fill,
                                                        ),
                                                      ),
                                                      SizedBox(width: 8),
                                                      Expanded(
                                                        child: Text(
                                                          exploreChannelsList![
                                                          exploreIndex]
                                                          [
                                                          'threads']
                                                          [
                                                          threadIndex]['name'],
                                                          style:
                                                          TextStyle(
                                                            color: Color(
                                                                0xffa4a4ab),
                                                            fontSize: 14,
                                                            fontFamily:
                                                            "Proxima Nova",
                                                            fontWeight:
                                                            FontWeight
                                                                .w600,
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 8,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                SizedBox(height: 12),
                                              ],
                                            );
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                InkWell(
                                    focusColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    hoverColor: Colors.transparent,
                                    splashColor: Colors.transparent,
                                    onTap: () {
                                      setState(() {
                                        joinLoaderIndex = exploreIndex;
                                      });
                                      if (joinExploreIndex[
                                      exploreIndex]) {
                                        joinChannelApi(
                                            AppInjector.resolve<
                                                AppPreferences>()
                                                .getUserId()!,
                                            exploreChannelsList![
                                            exploreIndex]['id'],
                                            "unfollow")
                                            .then((value) {
                                          if (value[0]) {
                                            setState(() {
                                              joinExploreIndex[
                                              exploreIndex] = false;
                                              joinLoaderIndex = -1;
                                            });
                                          } else {
                                            setState(() {
                                              joinLoaderIndex = -1;
                                              BotToast.showText(
                                                  text: value[1]);
                                            });
                                          }
                                        });
                                      } else {
                                        joinChannelApi(
                                            AppInjector.resolve<
                                                AppPreferences>()
                                                .getUserId()!,
                                            exploreChannelsList![
                                            exploreIndex]['id'],
                                            "follow")
                                            .then((value) {
                                          if (value[0]) {
                                            // _getAllApiCalls();
                                            setState(() {
                                              joinExploreIndex[
                                              exploreIndex] = true;
                                              joinLoaderIndex = -1;
                                            });
                                          } else {
                                            setState(() {
                                              joinLoaderIndex = -1;
                                              BotToast.showText(
                                                  text: value[1]);
                                            });
                                          }
                                        });
                                      }
                                    },
                                    child: joinExploreIndex[exploreIndex]
                                        ? Container(
                                      width: 90,
                                      height: 28,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(
                                            4),
                                        border: Border.all(
                                          color: Color(0xff07cf87),
                                          width: 1,
                                        ),
                                      ),
                                      child: Row(
                                        mainAxisSize:
                                        MainAxisSize.min,
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment
                                            .center,
                                        children: [
                                          Expanded(
                                            child: Text(
                                              "Joined",
                                              textAlign:
                                              TextAlign.center,
                                              style: TextStyle(
                                                color: Color(
                                                    0xff07cf87),
                                                fontSize: 14,
                                                fontFamily:
                                                "Proxima Nova",
                                                fontWeight:
                                                FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width: 12,
                                            height: 12,
                                            child: Image.asset(
                                              "assets/images/suggested_channel_tick.png",
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                          joinLoaderIndex ==
                                              exploreIndex
                                              ? SizedBox(width: 8)
                                              : SizedBox(width: 8),
                                          joinLoaderIndex ==
                                              exploreIndex
                                              ? SizedBox(
                                              height: 8,
                                              width: 8,
                                              child: CircularProgressIndicator.adaptive(
                                                  strokeWidth:
                                                  1,
                                                  backgroundColor:
                                                  AppWidgets
                                                      .buttonTextColor,
                                                  valueColor: AlwaysStoppedAnimation<
                                                      Color>(
                                                      Colors
                                                          .white)))
                                              : Container(),
                                          joinLoaderIndex ==
                                              exploreIndex
                                              ? SizedBox(
                                            width: 8,
                                          )
                                              : Container()
                                        ],
                                      ),
                                    )
                                        : Container(
                                      width: 90,
                                      height: 28,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(
                                            4),
                                        border: Border.all(
                                          color: Color(0xff5cabff),
                                          width: 1,
                                        ),
                                      ),
                                      child: Row(
                                        mainAxisSize:
                                        MainAxisSize.min,
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment
                                            .center,
                                        children: [
                                          Expanded(
                                            child: Text(
                                              "Join",
                                              textAlign:
                                              TextAlign.center,
                                              style: TextStyle(
                                                color: Color(
                                                    0xff5cabff),
                                                fontSize: 14,
                                                fontFamily:
                                                "Proxima Nova",
                                                fontWeight:
                                                FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          joinLoaderIndex ==
                                              exploreIndex
                                              ? SizedBox(
                                              height: 10,
                                              width: 10,
                                              child: CircularProgressIndicator.adaptive(
                                                  strokeWidth:
                                                  1,
                                                  backgroundColor:
                                                  AppWidgets
                                                      .buttonTextColor,
                                                  valueColor: AlwaysStoppedAnimation<
                                                      Color>(
                                                      Colors
                                                          .white)))
                                              : Container(),
                                          joinLoaderIndex ==
                                              exploreIndex
                                              ? SizedBox(
                                            width: 8,
                                          )
                                              : Container()
                                        ],
                                      ),
                                    ))
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 16)
                      ],
                    ));
              },
            ),
          ],
        ),
      ),
      exploreChannelsLoader
          ? Container(
        height: MediaQuery.of(context).size.height,
        child: Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  height: 30,
                  width: 30,
                  child: CircularProgressIndicator.adaptive(
                      strokeWidth: 2,
                      backgroundColor: AppWidgets.buttonTextColor,
                      valueColor:
                      AlwaysStoppedAnimation<Color>(Colors.white)),
                )
              ],
            )),
      )
          : Container(),
    ]);
  }

  Future<List> joinChannelApi(int userId, int channelId, type) async {
    var response = await AppInjector.resolve<ApiService>().joinChannel({
      "channel_id": channelId,
      "user_id": userId,
    }, type);

    if (response.status == ApiResponseStatus.completed) {
      return [true, ""];
    } else if (response.status == ApiResponseStatus.error) {
      print("response data init ${response.message[1]}");
      return [false, response.message[1]];
    } else {
      return [false, "Something went wrong.Please try later"];
    }
  }
}
