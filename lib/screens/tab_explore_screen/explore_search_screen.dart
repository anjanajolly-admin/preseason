import 'dart:convert' as convert;

import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_response.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/network/model/debounce.dart';
import 'package:pre_seasoned/screens/create_channel_screens/channel_details_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class ExploreSearchScreen extends StatefulWidget {
  final allExploreChannel;
  const ExploreSearchScreen({Key? key, required this.allExploreChannel})
      : super(key: key);
  static const String routeName = '/ExploreSearchScreen';

  @override
  _ExploreSearchScreenState createState() => _ExploreSearchScreenState();
}

class _ExploreSearchScreenState extends State<ExploreSearchScreen> {
  bool searchChannelsLoader = false;

  late String filter;
  final _debounce = Debounce();
  TextEditingController searchController = TextEditingController();
  List<dynamic> searchChannelList = [];
  int joinLoaderIndex = -1;
  List<bool> joinExploreIndex = [];

  @override
  void initState() {
    super.initState();
    print(widget.allExploreChannel);
    searchChannelList = widget.allExploreChannel;
    joinExploreIndex =
        List.generate(searchChannelList.length, (index) => false);
    if (mounted)
      searchController.addListener(() {
        setState(() {
          filter = searchController.text;
        });
      });
  }

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }

  _searchChannels(channelName) {
    AppInjector.resolve<ApiService>().searchChannels(channelName).then((value) {
      if (value.status == ApiResponseStatus.completed) {
        setState(() {
          searchChannelsLoader = false;
          Map<String, dynamic> map = convert.jsonDecode(value.data);
          searchChannelList = map["data"];
          joinExploreIndex =
              List.generate(searchChannelList.length, (index) => false);
        });
      } else if (value.status == ApiResponseStatus.error) {
        setState(() {
          searchChannelsLoader = false;
          searchChannelList = [];
          BotToast.showText(text: value.message[1]);
        });
      } else {
        setState(() {
          searchChannelsLoader = false;
          BotToast.showText(text: "Something went wrong.Please try later");
        });
      }
    });
  }

  onTextChanged(String query) {
    _debounce.call(() {
      setState(() {
        searchChannelsLoader = true;
      });
      _searchChannels(query);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      appBar: AppBar(
        toolbarHeight: 0,
        elevation: 0,
        backgroundColor: AppWidgets.backgroundColor,
        brightness: Brightness.dark,
      ),
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 16,
                ),
                Container(
                  width: double.infinity,
                  height: 56,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      InkWell(
                        focusColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        onTap: () {
                          AppInjector.resolve<AppRoutes>()
                              .navigatorKey
                              .currentState!
                              .pop();
                        },
                        child: Container(
                          width: 24,
                          height: 24,
                          child: Image.asset(
                            "assets/images/arrow_left_icon.png",
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        child: Container(
                            height: 40,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                color: Color(0xff464649),
                                width: 1,
                              ),
                              color: Color(0xff3a3a3c),
                            ),
                            child: TextFormField(
                                textAlign: TextAlign.left,
                                controller: searchController,
                                cursorColor: Colors.white,
                                style: TextStyle(
                                  color: Color(0xffa4a4ab),
                                  fontSize: 16,
                                ),
                                decoration: InputDecoration(
                                  hintText: "Search ",
                                  hintStyle: TextStyle(
                                    color: Color(0xffa4a4ab),
                                    fontSize: 16,
                                  ),
                                  border: InputBorder.none,
                                  prefixIcon: new IconButton(
                                    icon: Container(
                                      width: 24,
                                      height: 24,
                                      child: Image.asset(
                                        "assets/images/search_icon.png",
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    onPressed: () {},
                                  ),
                                ),
                                onChanged: (String query) =>
                                    onTextChanged(query))),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Expanded(
                    child: Container(
                        child: searchChannelList.isEmpty
                            ? Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Sorry, We couldn\'t find any results',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white60),
                                ),
                              ],
                            ))
                            : ListView.builder(
                            itemCount: searchChannelList.length,
                            itemBuilder: (BuildContext ctx, searchIndex) {
                              // Map data = channels[index];
                              return InkWell(
                                  focusColor: Colors.transparent,
                                  highlightColor: Colors.transparent,
                                  hoverColor: Colors.transparent,
                                  splashColor: Colors.transparent,
                                  onTap: () {
                                    AppInjector.resolve<AppRoutes>()
                                        .navigatorKey
                                        .currentState!
                                        .pushNamed(
                                        ChannelDetailsScreen.routeName,
                                        arguments: [
                                          searchChannelList[searchIndex]
                                          ['id'],
                                          false,
                                          searchChannelList[searchIndex]
                                          ['profilePhoto'] ==
                                              null
                                              ? null
                                              : searchChannelList[
                                          searchIndex]
                                          ['profilePhoto']
                                          ['url'] as String,
                                          searchChannelList[searchIndex]
                                          ['coverPhoto'] ==
                                              null
                                              ? null
                                              : searchChannelList[
                                          searchIndex]
                                          ['coverPhoto']['url']
                                          as String,
                                          false,
                                          true
                                        ]);
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(10),
                                    ),
                                    child: Card(
                                      color: Color(0xFF3A3A3C),
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12, top: 10, bottom: 10),
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  width: 50,
                                                  height: 45,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                      BorderRadius
                                                          .circular(
                                                          10)),
                                                  //padding: EdgeInsets.all(3),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                    BorderRadius
                                                        .circular(8.0),
                                                    child: (searchChannelList[
                                                    searchIndex]
                                                    [
                                                    "profilePhoto"] ==
                                                        null)
                                                        ? Image.asset(
                                                      "assets/images/suggested_channel_cover.png",
                                                      fit:
                                                      BoxFit.fill,
                                                    )
                                                        : CachedNetworkImage(
                                                      imageUrl: searchChannelList[
                                                      searchIndex]
                                                      [
                                                      "profilePhoto"]["url"],
                                                      fit:
                                                      BoxFit.fill,
                                                      placeholder: (context,
                                                          url) =>
                                                          Image.asset(
                                                            "assets/images/suggested_channel_cover.png",
                                                            fit: BoxFit
                                                                .fill,
                                                          ),

                                                      // placeholder: (context, url) =>
                                                      errorWidget:
                                                          (context,
                                                          url,
                                                          error) {
                                                        print(
                                                            'errorWidget ${error.toString()} \n$url');
                                                        return Image
                                                            .asset(
                                                          "assets/images/suggested_channel_cover.png",
                                                          fit: BoxFit
                                                              .fill,
                                                        );
                                                      },
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Expanded(
                                                  child: Text(
                                                    searchChannelList[
                                                    searchIndex]
                                                    ['name'],
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 18,
                                                      fontFamily:
                                                      "Proxima Nova",
                                                      fontWeight:
                                                      FontWeight.w600,
                                                    ),
                                                    // ignore: deprecated_member_use
                                                  ),
                                                ),
                                                InkWell(
                                                    focusColor:
                                                    Colors.transparent,
                                                    highlightColor:
                                                    Colors.transparent,
                                                    hoverColor:
                                                    Colors.transparent,
                                                    splashColor:
                                                    Colors.transparent,
                                                    onTap: () {
                                                      setState(() {
                                                        joinLoaderIndex =
                                                            searchIndex;
                                                      });
                                                      if (joinExploreIndex[
                                                      searchIndex]) {
                                                        joinChannelApi(
                                                            AppInjector.resolve<
                                                                AppPreferences>()
                                                                .getUserId()!,
                                                            searchChannelList[
                                                            searchIndex]
                                                            ['id'],
                                                            "unfollow")
                                                            .then((value) {
                                                          if (value[0]) {
                                                            setState(() {
                                                              joinExploreIndex[
                                                              searchIndex] =
                                                              false;
                                                              joinLoaderIndex =
                                                              -1;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              joinLoaderIndex =
                                                              -1;
                                                              BotToast.showText(
                                                                  text: value[
                                                                  1]);
                                                            });
                                                          }
                                                        });
                                                      } else {
                                                        joinChannelApi(
                                                            AppInjector.resolve<
                                                                AppPreferences>()
                                                                .getUserId()!,
                                                            searchChannelList[
                                                            searchIndex]
                                                            ['id'],
                                                            "follow")
                                                            .then((value) {
                                                          if (value[0]) {
                                                            setState(() {
                                                              joinExploreIndex[
                                                              searchIndex] =
                                                              true;
                                                              joinLoaderIndex =
                                                              -1;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              joinLoaderIndex =
                                                              -1;
                                                              BotToast.showText(
                                                                  text: value[
                                                                  1]);
                                                            });
                                                          }
                                                        });
                                                      }
                                                    },
                                                    child:
                                                    joinExploreIndex[
                                                    searchIndex]
                                                        ? Container(
                                                      width: 90,
                                                      height: 28,
                                                      decoration:
                                                      BoxDecoration(
                                                        borderRadius:
                                                        BorderRadius.circular(
                                                            4),
                                                        border:
                                                        Border
                                                            .all(
                                                          color: Color(
                                                              0xff07cf87),
                                                          width:
                                                          1,
                                                        ),
                                                      ),
                                                      child: Row(
                                                        mainAxisSize:
                                                        MainAxisSize
                                                            .min,
                                                        mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                        children: [
                                                          Expanded(
                                                            child:
                                                            Text(
                                                              "Joined",
                                                              textAlign:
                                                              TextAlign.center,
                                                              style:
                                                              TextStyle(
                                                                color: Color(0xff07cf87),
                                                                fontSize: 14,
                                                                fontFamily: "Proxima Nova",
                                                                fontWeight: FontWeight.w600,
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            width:
                                                            12,
                                                            height:
                                                            12,
                                                            child:
                                                            Image.asset(
                                                              "assets/images/suggested_channel_tick.png",
                                                              fit:
                                                              BoxFit.fill,
                                                            ),
                                                          ),
                                                          joinLoaderIndex ==
                                                              searchIndex
                                                              ? SizedBox(width: 8)
                                                              : SizedBox(width: 8),
                                                          joinLoaderIndex ==
                                                              searchIndex
                                                              ? SizedBox(
                                                              height: 8,
                                                              width: 8,
                                                              child: CircularProgressIndicator.adaptive(strokeWidth: 1, backgroundColor: AppWidgets.buttonTextColor, valueColor: AlwaysStoppedAnimation<Color>(Colors.white)))
                                                              : Container(),
                                                          joinLoaderIndex ==
                                                              searchIndex
                                                              ? SizedBox(
                                                            width: 8,
                                                          )
                                                              : Container()
                                                        ],
                                                      ),
                                                    )
                                                        : Container(
                                                      width: 90,
                                                      height: 28,
                                                      decoration:
                                                      BoxDecoration(
                                                        borderRadius:
                                                        BorderRadius.circular(
                                                            4),
                                                        border:
                                                        Border
                                                            .all(
                                                          color: Color(
                                                              0xff5cabff),
                                                          width:
                                                          1,
                                                        ),
                                                      ),
                                                      child: Row(
                                                        mainAxisSize:
                                                        MainAxisSize
                                                            .min,
                                                        mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                        children: [
                                                          Expanded(
                                                            child:
                                                            Text(
                                                              "Join",
                                                              textAlign:
                                                              TextAlign.center,
                                                              style:
                                                              TextStyle(
                                                                color: Color(0xff5cabff),
                                                                fontSize: 14,
                                                                fontFamily: "Proxima Nova",
                                                                fontWeight: FontWeight.w600,
                                                              ),
                                                            ),
                                                          ),
                                                          joinLoaderIndex ==
                                                              searchIndex
                                                              ? SizedBox(
                                                              height: 10,
                                                              width: 10,
                                                              child: CircularProgressIndicator.adaptive(strokeWidth: 1, backgroundColor: AppWidgets.buttonTextColor, valueColor: AlwaysStoppedAnimation<Color>(Colors.white)))
                                                              : Container(),
                                                          joinLoaderIndex ==
                                                              searchIndex
                                                              ? SizedBox(
                                                            width: 8,
                                                          )
                                                              : Container()
                                                        ],
                                                      ),
                                                    )),
                                                SizedBox(
                                                  width: 8,
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            ListView.builder(
                                              itemCount: (searchChannelList[
                                              searchIndex]
                                              ['threads'] as List)
                                                  .length,
                                              physics:
                                              NeverScrollableScrollPhysics(),
                                              shrinkWrap: true,
                                              itemBuilder:
                                                  (BuildContext context,
                                                  threadIndex) {
                                                return Column(
                                                  children: [
                                                    Container(
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            width: 20,
                                                            height: 20,
                                                            child:
                                                            Image.asset(
                                                              "assets/images/message_icon.png",
                                                              fit: BoxFit
                                                                  .fill,
                                                            ),
                                                          ),
                                                          SizedBox(
                                                              width: 8),
                                                          Expanded(
                                                            child: Text(
                                                              searchChannelList[
                                                              searchIndex]
                                                              [
                                                              'threads']
                                                              [
                                                              threadIndex]['name'],
                                                              style:
                                                              TextStyle(
                                                                color: Color(
                                                                    0xffa4a4ab),
                                                                fontSize:
                                                                14,
                                                                fontFamily:
                                                                "Proxima Nova",
                                                                fontWeight:
                                                                FontWeight
                                                                    .w600,
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: 8,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    SizedBox(height: 12),
                                                  ],
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ));
                            })))
              ],
            ),
          ),
          searchChannelsLoader
              ? Center(
            child: Container(
              child: SizedBox(
                  height: 50,
                  width: 50,
                  child: CircularProgressIndicator.adaptive(
                      strokeWidth: 2,
                      backgroundColor: AppWidgets.buttonTextColor,
                      valueColor:
                      AlwaysStoppedAnimation<Color>(Colors.white))),
            ),
          )
              : Container(),
        ],
      ),
    );
  }
}

Future<List> joinChannelApi(int userId, int channelId, type) async {
  var response = await AppInjector.resolve<ApiService>().joinChannel({
    "channel_id": channelId,
    "user_id": userId,
  }, type);

  if (response.status == ApiResponseStatus.completed) {
    return [true, ""];
  } else if (response.status == ApiResponseStatus.error) {
    print("response data init ${response.message[1]}");
    return [false, response.message[1]];
  } else {
    return [false, "Something went wrong.Please try later"];
  }
}

