import 'package:flutter/material.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/screens/main_container/main_container_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:pre_seasoned/widgets/app_widgets.dart';

class AnimationScreen extends StatefulWidget {
  const AnimationScreen({Key? key}) : super(key: key);
  static const String routeName = "/ChannelWelcomeScreen";

  @override
  _AnimationScreenState createState() => _AnimationScreenState();
}

class _AnimationScreenState extends State<AnimationScreen>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    String username =
        capitalize(AppInjector.resolve<AppPreferences>().getUserName()!);

    return Scaffold(
      backgroundColor: AppWidgets.backgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 200,
            width: MediaQuery.of(context).size.width * .5,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color(0xff3a3a3c),
            ),
            child: Image.asset('assets/images/image 2.png'),
          ),
          SizedBox(
            height: 50,
          ),
          Text(
            "Hey $username, \nYou are all set!",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 28,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(
            height: 25,
          ),
          SizedBox(
            child: Text(
              "You can now chat with your friends and the community",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(
            height: 35,
          ),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: AppWidgets.getWtaButton(
                buttonName: 'Done',
                fontSize: 16,
                width: double.infinity,
                height: 48,
                isLoader: false,
                onPressed: () {
                  AppInjector.resolve<AppRoutes>()
                      .navigatorKey
                      .currentState!
                      .popAndPushNamed(MainContainerScreen.routeName);
                }),
          )
        ],
      ),
    );
  }
}
