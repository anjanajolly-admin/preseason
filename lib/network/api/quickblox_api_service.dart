import 'dart:async';
import 'dart:convert';
import 'dart:convert' as convert;
import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/services.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/dialog_utils.dart';
import 'package:quickblox_sdk/auth/module.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/models/qb_attachment.dart';
import 'package:quickblox_sdk/models/qb_custom_object.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/models/qb_event.dart';
import 'package:quickblox_sdk/models/qb_filter.dart';
import 'package:quickblox_sdk/models/qb_message.dart';
import 'package:quickblox_sdk/models/qb_rtc_session.dart';
import 'package:quickblox_sdk/models/qb_session.dart';
import 'package:quickblox_sdk/models/qb_sort.dart';
import 'package:quickblox_sdk/models/qb_subscription.dart';
import 'package:quickblox_sdk/models/qb_user.dart';
import 'package:quickblox_sdk/notifications/constants.dart';
import 'package:quickblox_sdk/push/constants.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:quickblox_sdk/webrtc/constants.dart';

import '../client.dart';

class QuickbloxApiService {
  AppPreferences preferences = AppInjector.resolve<AppPreferences>();

  String appId = "93736";
  String authKey = "WMbeOywMXHjQfUm";
  String authSecret = "BJ5JRFHszyGVtzY";
  String accountKey = "RybhxJZmA-VNhgtRyzZV";
  // String apiEndpoint = "api.endpoint.com";
  // String chatEndpoint = "chat.enpoint.com";
  StreamSubscription? connectedSubscription;
  StreamSubscription? connectionClosedSubscription;
  StreamSubscription? reConnectionSuccessSubscription;
  StreamSubscription? reConnectionFailedSubscription;
  StreamSubscription? _callSubscription;

  String parseState(int state) {
    String parsedState = "";

    switch (state) {
      case QBRTCPeerConnectionStates.NEW:
        parsedState = "NEW";
        break;
      case QBRTCPeerConnectionStates.FAILED:
        parsedState = "FAILED";
        break;
      case QBRTCPeerConnectionStates.DISCONNECTED:
        parsedState = "DISCONNECTED";
        break;
      case QBRTCPeerConnectionStates.CLOSED:
        parsedState = "CLOSED";
        break;
      case QBRTCPeerConnectionStates.CONNECTED:
        parsedState = "CONNECTED";
        break;
    }

    return parsedState;
  }

  Future<void> initQuickBlox() async {
    try {
      await QB.settings.init(appId, authKey, authSecret, accountKey);
      print("Quickblox Initilize done");
    } catch (error) {
      print("error occured qickblox $error");
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<QuickbloxApiResponse<String>> getSession() async {
    try {
      QBSession? session = await QB.auth.getSession();
      if (session != null) {
        print("Session Exists");
      } else {
        var userSessionCreated =
            await AppInjector.resolve<QuickbloxApiService>().quickBloxLogin(
                AppInjector.resolve<AppPreferences>().getUserName(),
                AppInjector.resolve<AppPreferences>().getQbPassword());

        if (userSessionCreated.status == QuickbloxApiResponseStatus.completed) {
          print("set session success");
          return QuickbloxApiResponse.completed("");
        } else {
          print("set session error");
          return QuickbloxApiResponse.error("");
        }
      }
      return QuickbloxApiResponse.completed("");
    } catch (e) {
      print("get session error ${e.toString()}");
      var userSessionCreated = await AppInjector.resolve<QuickbloxApiService>()
          .quickBloxLogin(AppInjector.resolve<AppPreferences>().getUserName(),
              AppInjector.resolve<AppPreferences>().getQbPassword());

      if (userSessionCreated.status == QuickbloxApiResponseStatus.completed) {
        print("set session success");
        return QuickbloxApiResponse.completed("");
      } else {
        print("set session error");
        return QuickbloxApiResponse.error("");
      }
    }
  }

  // Future<QBUser?> quickBloxSignUp(String login, String password,
  //     {String? email,
  //     int? blobId,
  //     int? externalUserId,
  //     int? facebookId,
  //     int? twitterId,
  //     String? fullName,
  //     String? phone,
  //     String? website,
  //     String? customData,
  //     String? tagList}) async {
  //   try {
  //     QBUser? qbUser = await QB.users
  //         .createUser(login, password, email: email, fullName: "$fullName");
  //     preferences.setQbUserId(qbUser!.id!);
  //     print("Quickblox User ${qbUser.id}");
  //   } catch (e) {
  //     // Some error occurred, look at the exception message for more details
  //   }
  // }
  Future<QuickbloxApiResponse<String>> getQBUsers() async {
    try {
      List<QBUser?> userList = await QB.users.getUsers();
      return QuickbloxApiResponse.completed(userList);
    } catch (e) {
      print("Quickblox Login Error $e");
      return QuickbloxApiResponse.error(e.toString());
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<QuickbloxApiResponse<String>> quickBloxLogin(email, password) async {
    try {
      QBLoginResult result = await QB.auth.login(email, password);

      QBUser? qbUser = result.qbUser;
      QBSession? qbSession = result.qbSession;

      qbSession!.applicationId = int.parse(appId);
      qbSession.userId = qbUser!.id;
      // DataHolder.getInstance().setSession(qbSession);
      // DataHolder.getInstance().setUser(qbUser);

      preferences.setQbUserId(qbUser.id!);
      print("QB User ${qbUser.id} and session ${qbSession.token}");

      return QuickbloxApiResponse.completed("");
    } catch (e) {
      print("Quickblox Login Error");
      return QuickbloxApiResponse.error(e.toString());
    }
  }

  Future<QuickbloxApiResponse<String>> qbLogout() async {
    try {
      await QB.auth.logout();
      return QuickbloxApiResponse.completed("data");
    } catch (e) {
      return QuickbloxApiResponse.error("");
    }
  }

  Future<QuickbloxApiResponse<String>> connect() async {
    try {
      bool? connected = await QB.chat.isConnected();
      if (!connected!) {
        try {
          await QB.chat.connect(
              preferences.getQbUserId()!, preferences.getQbPassword()!);
          print(
              "Quickblox Chat server Connected ${preferences.getQbUserId()!} ${preferences.getQbPassword()!}");
          return QuickbloxApiResponse.completed("data");
        } catch (e) {
          print("Quickblox Chat server Connected Error $e");
          return QuickbloxApiResponse.error("");
          // Some error occurred, look at the exception message for more details
        }
      } else {
        print("Alreday connected to chat server");
        return QuickbloxApiResponse.completed("");
      }
    } catch (e) {
      return QuickbloxApiResponse.error("");
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<void> subscribeToChatEvents() async {
    try {
      // connectedSubscription
      QB.chat
          .subscribeChatEvent(QBChatEvents.CONNECTED, (data) {
            print("connectedSubscription $data");
          }, onErrorMethod: (error) {
            print("connectedSubscription Error $error");
          })
          .asStream()
          .listen((event) {
            print("connectedSubscription $event");
          });
      // connectionClosedSubscription =
      QB.chat
          .subscribeChatEvent(QBChatEvents.CONNECTION_CLOSED, (data) {
            print("connectionClosedSubscription $data");
          }, onErrorMethod: (error) {
            print("connectionClosedSubscription Error $error");
          })
          .asStream()
          .listen((event) {
            print("connectionClosedSubscription $event");
          });
      QB.chat
          .subscribeChatEvent(QBChatEvents.RECONNECTION_SUCCESSFUL, (data) {
            print("ReConnectionSuccessSubscription $data");
          }, onErrorMethod: (error) {
            print("reConnectionSuccessSubscription Error $error");
          })
          .asStream()
          .listen((event) {
            print("reConnectionSuccessSubscription $event");
          });
      QB.chat
          .subscribeChatEvent(QBChatEvents.RECONNECTION_FAILED, (data) {
            print("reConnectionFailedSubscription $data");
          }, onErrorMethod: (error) {
            print("reConnectionFailedSubscription Error $error");
          })
          .asStream()
          .listen((event) {
            print("reConnectionFailedSubscription $event");
          });
    } catch (e) {
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<QuickbloxApiResponse<String>> enableAutoReconnect() async {
    bool enable = true;
    try {
      await QB.settings.enableAutoReconnect(enable);
      print("Auto Reconnect");
      return QuickbloxApiResponse.completed("data");
    } catch (e) {
      return QuickbloxApiResponse.error(AppNetworkClient.handleError(e));
    }
  }

  Future<QuickbloxApiResponse<String>> initStreamManagement(
      messageTimeout, autoReconnect) async {
    try {
      await QB.settings
          .initStreamManagement(messageTimeout, autoReconnect: autoReconnect);
      return QuickbloxApiResponse.completed("data");
    } catch (e) {
      return QuickbloxApiResponse.error(AppNetworkClient.handleError(e));
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<QuickbloxApiResponse<String>> disconnect() async {
    try {
      await QB.chat.disconnect();
      return QuickbloxApiResponse.completed("data");
    } catch (e) {
      return QuickbloxApiResponse.error(AppNetworkClient.handleError(e));
      // DialogUtils.showError(context, e);
    }
  }

  Future<QuickbloxApiResponse<String>> createDialog(List<int> occupantsIds,
      {String? dialogName, int? dialogType, String? dialogPhoto}) async {
    try {
      QBDialog? createdDialog = await QB.chat.createDialog(
          occupantsIds, dialogName!,
          dialogType: dialogType, dialogPhoto: dialogPhoto);
      return QuickbloxApiResponse.completed(createdDialog);
    } catch (e) {
      return QuickbloxApiResponse.error("error creating $e");
    }
  }

  Future<QuickbloxApiResponse<String>> getDialogs(
      {QBSort? sort, QBFilter? filter, int? limit, int? skip}) async {
    try {
      List<QBDialog?> dialogs = await QB.chat
          .getDialogs(sort: sort, filter: filter, limit: limit, skip: skip);
      return QuickbloxApiResponse.completed(dialogs);
    } catch (e) {
      return QuickbloxApiResponse.error("error retrieving $e");
    }
  }

  Future<QuickbloxApiResponse<String>> updateDialog(String dialogId,
      {String? dialogName, String? dialogPhoto}) async {
    try {
      QBDialog? updatedDialog = await QB.chat.updateDialog(
        dialogId,
        dialogName: dialogName,
        dialogPhoto: dialogPhoto,
      );
      return QuickbloxApiResponse.completed(updatedDialog);
    } catch (e) {
      return QuickbloxApiResponse.error("error retrieving $e");
      // some error occurred, look at the exception message for more details
    }
  }

  Future<QuickbloxApiResponse<bool>> deleteDialog(String dialogId) async {
    try {
      await QB.chat.deleteDialog(dialogId);
      return QuickbloxApiResponse.completed(true);
    } catch (e) {
      return QuickbloxApiResponse.completed(false);
    }
  }

  Future<QuickbloxApiResponse<String>> leaveDialog(String dialogId) async {
    try {
      await QB.chat.leaveDialog(dialogId);
      return QuickbloxApiResponse.completed("");
    } catch (e) {
      return QuickbloxApiResponse.error(AppNetworkClient.handleError(e));
      // some error occurred, look at the exception message for more details
    }
  }

  Future<QuickbloxApiResponse<String>> joinDialog(String dialogId) async {
    var getSessionExist = await getSession();

    if (getSessionExist.status == QuickbloxApiResponseStatus.completed) {
      print("getSessionExist $getSessionExist");
      var connectToChatServer = await connect();
      if (connectToChatServer.status == QuickbloxApiResponseStatus.completed) {
        try {
          await QB.chat.joinDialog(dialogId);
          print("Joining dialog success $dialogId");
          return QuickbloxApiResponse.completed("Joining dialog success");
        } catch (e) {
          print("Error joining dialog $e");
          return QuickbloxApiResponse.error(AppNetworkClient.handleError(e));
          // some error occurred, look at the exception message for more details
        }
      } else {
        return QuickbloxApiResponse.error("error got");
      }
    } else {
      return QuickbloxApiResponse.error("error got");
    }
  }

  Future<QuickbloxApiResponse<String>> sendMessage(String dialogId,
      {String? body,
      List<QBAttachment>? attachments,
      Map<String, String>? properties,
      bool? markable,
      String? dateSent,
      bool? saveToHistory}) async {
    try {
      var getSessionExist = await getSession();
      if (getSessionExist.status == QuickbloxApiResponseStatus.completed) {
        try {
          QB.chat.sendMessage(dialogId,
              body: body,
              attachments: attachments,
              properties: properties,
              markable: true,
              // dateSent: dateSent,
              saveToHistory: true);
          print(
              "body $body saveToHistory $saveToHistory properties $properties attachments $attachments");
          return QuickbloxApiResponse.completed("Message has been Sent");
        } catch (error) {
          print("Error Sending Message $error ");
          return QuickbloxApiResponse.error("");
        }
      } else {
        print("Error Sending session ");
        return QuickbloxApiResponse.error("Did not join session");
      }
    } catch (e) {
      print("Catch error $e");
      return QuickbloxApiResponse.error("Did not join session");
    }
  }

  Future<QuickbloxApiResponse<String>> getDialogMessages(String dialogId,
      {QBSort? sort,
      QBFilter? filter,
      int? limit,
      int? skip,
      bool markAsRead = true}) async {
    try {
      List<QBMessage?> messages = await QB.chat.getDialogMessages(dialogId,
          markAsRead: markAsRead, sort: sort, limit: limit!, skip: skip!);

      return QuickbloxApiResponse.completed(messages);
    } catch (error) {
      return QuickbloxApiResponse.error("");
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<QuickbloxApiResponse<String>> markMessageRead(
      QBMessage message) async {
    try {
      await QB.chat.markMessageRead(message);
      print("Message Read Marked Manually");
      return QuickbloxApiResponse.completed("");
    } catch (e) {
      return QuickbloxApiResponse.error("");
    }
  }

  Future<QuickbloxApiResponse<String>> addGroupMember(
    String dialogId, {
    List<int>? addUsers,
  }) async {
    try {
      QBDialog? addMembers =
          await QB.chat.updateDialog(dialogId, addUsers: addUsers);

      return QuickbloxApiResponse.completed(addMembers);
    } catch (e) {
      return QuickbloxApiResponse.error("error retrieving $e");
      // some error occurred, look at the exception message for more details
    }
  }

  Future<QuickbloxApiResponse<String>> removeGroupMember(
    String dialogId, {
    List<int>? removeUsers,
  }) async {
    try {
      QBDialog? removeMembers =
          await QB.chat.updateDialog(dialogId, removeUsers: removeUsers);

      return QuickbloxApiResponse.completed(removeMembers);
    } catch (e) {
      return QuickbloxApiResponse.error("error retrieving $e");
      // some error occurred, look at the exception message for more details
    }
  }

  Future<QuickbloxApiResponse<String>> createPushSubscription() async {
    String? deviceToken = await FirebaseMessaging.instance.getToken();
    String pushChannel = QBPushChannelNames.GCM;
    print("Device Token is $deviceToken");
    var getSessionExist = await getSession();
    if (getSessionExist.status == QuickbloxApiResponseStatus.completed) {
      try {
        List<QBSubscription?> subscriptions =
            await QB.subscriptions.create(deviceToken!, pushChannel);
        print('Subscribed got is $subscriptions');
        return QuickbloxApiResponse.completed(subscriptions);
      } on PlatformException catch (e) {
        print('error retrieving $e');
        return QuickbloxApiResponse.error("error retrieving $e");
      }
    } else {
      print('error retrieving');
      return QuickbloxApiResponse.error("error retrieving ");
    }
  }

  Future<QuickbloxApiResponse<String>> getPushSubscriptions() async {
    try {
      List<QBSubscription?> subscriptions = await QB.subscriptions.get();
      int count = subscriptions.length;

      return QuickbloxApiResponse.completed(subscriptions);
    } on PlatformException catch (e) {
      print(e);
      return QuickbloxApiResponse.error("error retrieving $e");
    }
  }

  Future<QuickbloxApiResponse<String>> getNotifications() async {
    try {
      List<QBEvent?> qbEventsList = await QB.events.get();
      // int count = qbEventsList.length;
      return QuickbloxApiResponse.completed(qbEventsList);
    } on PlatformException catch (e) {
      return QuickbloxApiResponse.error("error retrieving $e");
    }
  }

  Future<QuickbloxApiResponse<String>> getByIdNotification() async {
    try {
      QBEvent? qbEvent = await QB.events.getById(37438602);
      int? notificationId = qbEvent!.id;
      var payload = (utf8.decode(base64.decode(((qbEvent.payload!)))));
      print('thread payload $payload, ${convert.jsonDecode(payload)}');
      return QuickbloxApiResponse.completed(qbEvent);
    } on PlatformException catch (e) {
      return QuickbloxApiResponse.error("error retrieving $e");
    }
  }

  Future<void> removePushSubscription() async {
    try {
      await QB.subscriptions
          .remove(AppInjector.resolve<AppPreferences>().getQbUserId()!);
    } on PlatformException catch (e) {
      print(e);
    }
  }

  Future<QuickbloxApiResponse<String>> sendPushNotification(
      {required String threadName,
      required String channelName,
      required String image,
      required String message}) async {
    String eventType = QBNotificationEventTypes.FIXED_DATE;
    String notificationEventType = QBNotificationTypes.PUSH;
    int pushType = QBNotificationPushTypes.GCM;
    int senderId = AppInjector.resolve<AppPreferences>().getQbUserId()!;
    Map<String, Object> payload = new Map();
    payload["channelName"] = channelName;
    payload["threadName"] = threadName;
    payload["message"] = message;
    payload["channelProfilePhoto"] = image;
    try {
      List<QBEvent?> events = await QB.events.create(
        eventType,
        notificationEventType,
        senderId,
        payload,
        pushType: pushType,
        // recipientsTagsAll: ["A2021G"],
        // recipientsIds: [
        //   "131350991",
        // ],
      );
      return QuickbloxApiResponse.completed(events);
    } catch (e) {
      print(e);
      return QuickbloxApiResponse.error("error retrieving $e");
      // Some error occurred, look at the exception message for more details
    }
  }

  Future<dynamic> getOnlineUsers(String dialogId) async {}

  Future<QuickbloxApiResponse<String>> createGroupDescriptionRecord(
      fieldsMap) async {
    try {
      List<QBCustomObject?> customObjectsList = await QB.data
          .create(className: "GroupDescription", fields: fieldsMap);
      return QuickbloxApiResponse.completed(customObjectsList);
    } catch (error) {
      return QuickbloxApiResponse.error("error retrieving $error");
    }
  }

  Future<QuickbloxApiResponse<String>> updateGroupDescriptionRecord(
      fieldsMap, id) async {
    try {
      QBCustomObject? customObject =
          await QB.data.update("GroupDescription", id: id, fields: fieldsMap);
      return QuickbloxApiResponse.completed(customObject);
    } catch (error) {
      return QuickbloxApiResponse.error("error retrieving $error");
    }
  }

  Future<QuickbloxApiResponse<String>> getGroupDescriptionRecord(
      {QBSort? sort,
      QBFilter? filter,
      int? limit,
      int? skip,
      bool markAsRead = true}) async {
    try {
      List<QBCustomObject?> customObjects =
          await QB.data.get("GroupDescription", filter: filter);
      return QuickbloxApiResponse.completed(customObjects);
    } catch (error) {
      return QuickbloxApiResponse.error("error retrieving $error");
    }
  }

  Future<QuickbloxApiResponse<String>> init() async {
    try {
      await QB.webrtc.init();

      return QuickbloxApiResponse.completed(
          'WebRTC Initialization Successfull');
    } catch (e) {
      print('Couldn' 't initialize call , $e');
      return QuickbloxApiResponse.error("error retrieving $e");
    }
  }

  Future<QuickbloxApiResponse<String>> callWebRTC(
      int sessionType, List<int> opponentIds) async {
    try {
      QBRTCSession? session = await QB.webrtc.call(opponentIds, sessionType);
      print("The call initiated ");
      return QuickbloxApiResponse.completed(session);
    } on PlatformException catch (e) {
      print(e);
      return QuickbloxApiResponse.error('error $e');
    }
  }

  Future<QuickbloxApiResponse<String>> acceptWebRTC(
    String sessionId,
    /*Map<String, Object> userInfo*/
  ) async {
    try {
      QBRTCSession? session = await QB.webrtc.accept(
        sessionId, /*userInfo: userInfo*/
      );
      String? receivedSessionId = session!.id;
      print("Session with id: $receivedSessionId was accepted");
      return QuickbloxApiResponse.completed(session);
    } catch (e) {
      return QuickbloxApiResponse.error("error retrieving $e");
    }
  }

  Future<QuickbloxApiResponse<String>> rejectWebRTC(
    String sessionId,
    /* Map<String, Object> userInfo*/
  ) async {
    try {
      QBRTCSession? session = await QB.webrtc.reject(
        sessionId, /*userInfo: userInfo*/
      );
      String? id = session!.id;
      print("Session with id: $id was rejected");
      return QuickbloxApiResponse.completed(session);
    } catch (e) {
      return QuickbloxApiResponse.error("error retrieving $e");
    }
  }

  Future<QuickbloxApiResponse<String>> hangUpWebRTC(
    String sessionId,
    /*Map<String, Object> userInfo*/
  ) async {
    try {
      QBRTCSession? session = await QB.webrtc.hangUp(
        sessionId, /*userInfo: userInfo*/
      );
      String? id = session!.id;
      print("Session with id: $id was hang up");
      return QuickbloxApiResponse.completed(session);
    } on PlatformException catch (e) {
      return QuickbloxApiResponse.error("error retrieving $e");
    }
  }

  Future<QuickbloxApiResponse<String>> releaseWebRTC() async {
    try {
      await QB.webrtc.release();
      print("WebRTC was released");
      return QuickbloxApiResponse.completed('WebRTC was released');
    } catch (e) {
      return QuickbloxApiResponse.error('error retrieving $e');
    }
  }

  Future<QuickbloxApiResponse<List>> muteAudio(
      sessionId, enable, userId) async {
    try {
      await QB.webrtc.enableAudio(sessionId, enable: true, userId: userId);

      return QuickbloxApiResponse.completed('Muted Audio');
    } catch (e) {
      BotToast.showText(text: 'Cannot mute audio, $e');
      return QuickbloxApiResponse.error('Cannot mute audio $e');
    }
  }

  Future<QuickbloxApiResponse<List>> disableCamera(
      sessionId, enable, userId) async {
    try {
      await QB.webrtc.enableVideo(sessionId, enable: true, userId: userId);
      return QuickbloxApiResponse.completed('Camera disabled');
    } catch (e) {
      return QuickbloxApiResponse.error(' Cannot disable camera $e ');
    }
  }

  Future<QuickbloxApiResponse<List>> switchCamera(sessionId) async {
    try {
      await QB.webrtc.switchCamera(sessionId!);
      print("Camera was switched");
      return QuickbloxApiResponse.completed('Camera switched');
    } on PlatformException catch (e) {
      return QuickbloxApiResponse.error(' Cannot switch camera $e ');
    }
  }

  Future<QuickbloxApiResponse<List>> mirrorCamera(userId) async {
    try {
      await QB.webrtc.mirrorCamera(userId, true);
      print("Camera was switched");
      return QuickbloxApiResponse.completed('Camera switched');
    } on PlatformException catch (e) {
      return QuickbloxApiResponse.error(' Cannot switch camera $e ');
    }
  }

  Future<bool> subscribeAccept() async {
    StreamSubscription? _acceptSubscription;
    if (_acceptSubscription != null) {
      print("You already have a subscription for: " + QBRTCEventTypes.ACCEPT);
      return true;
    }

    try {
      _acceptSubscription =
          await QB.webrtc.subscribeRTCEvent(QBRTCEventTypes.ACCEPT, (data) {
        int userId = data["payload"]["userId"];
        print("The user $userId was accepted your call");
      }, onErrorMethod: (error) {
        print(error);
        return false;
      });
      print("Subscribed: " + QBRTCEventTypes.ACCEPT);
      return true;
    } on PlatformException catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> subscribeHangUp() async {
    StreamSubscription? _hangUpSubscription;
    if (_hangUpSubscription != null) {
      print("You already have a subscription for: " + QBRTCEventTypes.HANG_UP);
      return true;
    }

    try {
      _hangUpSubscription =
          await QB.webrtc.subscribeRTCEvent(QBRTCEventTypes.HANG_UP, (data) {
        int userId = data["payload"]["userId"];
        print("the user $userId is hang up");
      }, onErrorMethod: (error) {
        print(error);
        return false;
      });
      print("Subscribed: " + QBRTCEventTypes.HANG_UP);
      return true;
    } on PlatformException catch (e) {
      print(e);
      return false;
    }
  }

  Future subscribePeerConnection() async {
    StreamSubscription? _peerConnectionSubscription;
    int state;
    String parsedState;
    if (_peerConnectionSubscription != null) {
      print("You already have a subscription for: " +
          QBRTCEventTypes.PEER_CONNECTION_STATE_CHANGED);
      return QBRTCEventTypes.PEER_CONNECTION_STATE_CHANGED;
    }

    try {
      _peerConnectionSubscription = await QB.webrtc.subscribeRTCEvent(
          QBRTCEventTypes.PEER_CONNECTION_STATE_CHANGED, (data) {
        state = data["payload"]["state"];
        parsedState = parseState(state);
        print("PeerConnection state: $parsedState");
      }, onErrorMethod: (error) {
        print(error);
      });

      print("Subscribed: " + QBRTCEventTypes.PEER_CONNECTION_STATE_CHANGED);
    } on PlatformException catch (e) {
      print(e);
    }
  }
}
it 