class ApiResponse<T> {
  ApiResponseStatus status;
  late T data;
  List<String> message = [];

  ApiResponse.completed(this.data) : status = ApiResponseStatus.completed;
  ApiResponse.error(this.message) : status = ApiResponseStatus.error;

  @override
  String toString() {
    return "Status : $status \n Message : $message \n Data : $data";
  }
}

enum ApiResponseStatus { completed, error }
