import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/mixpanel_helper.dart';
import 'package:pre_seasoned/utils/app_injector.dart';

import '../client.dart';
import 'api_response.dart';

class ApiService {
  AppNetworkClient client = AppInjector.resolve<AppNetworkClient>();
  AppPreferences preferences = AppInjector.resolve<AppPreferences>();
  String baseUrl = "http://3.13.226.133";
  Options getDioOptions(String method, [bool auth = false]) {
    Options options = Options();
    options.method = method;
    if (auth) {
      options.headers = {
        "Authorization":
        "Bearer ${AppInjector.resolve<AppPreferences>().getJwtToken()}",
      };
    }
    return options;
  }

  Future<ApiResponse<String>> registerUser(data) async {
    try {
      var result = await client.post(
        "$baseUrl/auth/local/register",
        data,
        getDioOptions("POST", false),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');
        return ApiResponse.error(result.message);
      }
      Map<String, dynamic> details = jsonDecode(result.data.data);
      mixpanelIdentifyUser(
        userName: details["user"]["username"],
        emailId: details["user"]["email"],
        firstName: details["user"]["firstName"],
        lastName: details["user"]["lastName"],
      );

      mixpanelTrackUserActivity(
          userName: details["user"]["username"],
          emailId: details["user"]["username"],
          eventName: "Sign In successfull",
          eventProperties: details);

      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> logInUser(Map<String, dynamic> data) async {
    try {
      var result = await client.post(
        "$baseUrl/auth/local",
        data,
        getDioOptions("POST", false),
        false,
      );

      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');
        return ApiResponse.error(result.message);
      }
      Map<String, dynamic> details = jsonDecode(result.data.data);

      mixpanelIdentifyUser(
        userName: details["user"]["username"],
        emailId: details["user"]["email"],
        firstName: details["user"]["firstName"],
        lastName: details["user"]["lastName"],
      );

      mixpanelTrackUserActivity(
          userName: details["user"]["username"],
          emailId: details["user"]["username"],
          eventName: "Log in successfull",
          eventProperties: details);

      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> availableEmail(Map<String, dynamic> data) async {
    try {
      var result = await client.post(
        "$baseUrl/users-permissions/email/availability",
        data,
        getDioOptions("POST", false),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');
        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> forgotPasswordApi(
      Map<String, dynamic> data) async {
    try {
      var result = await client.post('$baseUrl/auth/forgot-password', data,
          getDioOptions("POST", false), false);
      if (result.status == ApiResponseStatus.error) {
        print(result.message);
        return ApiResponse.error(result.message);
      }

      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error : $e");
      print("s: $s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> sendTokenApi(Map<String, dynamic> data) async {
    try {
      var result = await client.post('$baseUrl/notification-token', data,
          getDioOptions("POST", true), false);
      if (result.status == ApiResponseStatus.error) {
        print(result.message);
        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error : $e");
      print("s: $s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> interestUpdate(Map<String, dynamic> data) async {
    try {
      var result = await client.post(
        "$baseUrl/interests",
        data,
        getDioOptions("POST", true),
        false,
      );
      print("result status  ${result.status}");
      if (result.status == ApiResponseStatus.error) {
        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> createChannel(data) async {
    try {
      var result = await client.post(
        "$baseUrl/channels",
        data,
        getDioOptions("POST", true),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }

      var channelDetails = jsonDecode(result.data.data);

      mixpanelTrackUserActivity(
          userName: AppInjector.resolve<AppPreferences>().getUserName(),
          emailId: AppInjector.resolve<AppPreferences>().getEmail(),
          eventName: 'New Channel ${channelDetails["name"]} created',
          eventProperties: channelDetails);
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");

      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> joinChannel(
      Map<String, dynamic> data, String type) async {
    try {
      var result = await client.post(
        "$baseUrl/channels/$type",
        data,
        getDioOptions("POST", true),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");

      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> createTopic(data) async {
    try {
      var result = await client.post(
        "$baseUrl/topics",
        data,
        getDioOptions("POST", true),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');
        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> createThread(data) async {
    try {
      var result = await client.post(
        "$baseUrl/threads",
        data,
        getDioOptions("POST", true),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');
        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> sentFriendRequest(data) async {
    try {
      var result = await client.post(
        "$baseUrl/users-permissions/friend-request",
        data,
        getDioOptions("POST", true),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');
        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> acceptFriendRequest(data) async {
    try {
      var result = await client.post(
        "$baseUrl/users-permissions/friends",
        data,
        getDioOptions("POST", true),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');
        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> createGroup(data) async {
    try {
      var result = await client.post(
        "$baseUrl/groups",
        data,
        getDioOptions("POST", true),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");

      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> removeMemberfromGroup(data) async {
    try {
      var result = await client.post(
        "$baseUrl/groups/remove-member",
        data,
        getDioOptions("POST", true),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");

      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> leaveGroup(data) async {
    try {
      var result = await client.post(
        "$baseUrl/groups/leave",
        data,
        getDioOptions("POST", true),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");

      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> addMemberstoGroup(data) async {
    try {
      var result = await client.post(
        "$baseUrl/groups/add-members",
        data,
        getDioOptions("POST", true),
        false,
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");

      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  // All the dio.delete Apis
  Future<ApiResponse<String>> deleteFriendRequest(userId) async {
    try {
      var result = await client.delete(
        "$baseUrl/users-permissions/friend-request?id=$userId",
        getDioOptions("DELETE", true),
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');
        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> deleteGroup(userId) async {
    try {
      var result = await client.delete(
        "$baseUrl/groups/$userId",
        getDioOptions("DELETE", true),
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');
        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  // All the dio.put Apis
  Future<ApiResponse<String>> updateChannel(data, channelId) async {
    try {
      var result = await client.put(
        "$baseUrl/channels/$channelId",
        data,
        getDioOptions("PUT", true),
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");

      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> updateUserDetails(data, userId) async {
    try {
      var result = await client.put(
        "$baseUrl/users-permissions/$userId",
        data,
        getDioOptions("PUT", true),
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> updateGroupDetails(data, groupId) async {
    try {
      var result = await client.put(
        "$baseUrl/groups/$groupId",
        data,
        getDioOptions("PUT", true),
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("error:$e");
      print("s:$s");

      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  // All the dio.get Apis

  Future<ApiResponse<String>> getAllInterest() async {
    try {
      var result =
      await client.get("$baseUrl/interests", getDioOptions("GET", true));
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> searchChannels(String channelName) async {
    var cancelToken = CancelToken();
    try {
      var result = await client.get("$baseUrl/search/channels?q=$channelName",
          getDioOptions("GET", true), cancelToken);
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  var username = AppInjector.resolve<AppPreferences>().getUserName().toString();
  var emailId = AppInjector.resolve<AppPreferences>().getEmail().toString();

  Future<ApiResponse<String>> channelsUserFollows() async {
    try {
      var result = await client.get(
          "$baseUrl/channels/followed?userId=${AppInjector.resolve<AppPreferences>().getUserId()}",
          getDioOptions("GET", true));

      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      mixpanelTrackUserActivity(
          userName: AppInjector.resolve<AppPreferences>().getUserName(),
          emailId: AppInjector.resolve<AppPreferences>().getEmail(),
          eventName: "Channels managed by $username",
          eventProperties: {'channels managed': jsonDecode(result.data.data)});

      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> channelsUserManges() async {
    try {
      var result = await client.get(
          "$baseUrl/channels/managed?userId=${AppInjector.resolve<AppPreferences>().getUserId()}",
          getDioOptions("GET", true));

      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');
        return ApiResponse.error(result.message);
      }
      mixpanelTrackUserActivity(
          userName: AppInjector.resolve<AppPreferences>().getUserName(),
          emailId: AppInjector.resolve<AppPreferences>().getEmail(),
          eventName: "Channels managed by $username",
          eventProperties: {'channels managed': jsonDecode(result.data.data)});

      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> getUserDetails() async {
    try {
      var result = await client.get(
          "$baseUrl/users-permissions/me", getDioOptions("GET", true));
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> suggestedChannelsForUser() async {
    try {
      var result =
      await client.get("$baseUrl/channels", getDioOptions("GET", true));
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> channelDetails(createChannelId) async {
    try {
      var result = await client.get(
          "$baseUrl/channels/$createChannelId", getDioOptions("GET", true));
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> topicDetails(createChannelId) async {
    try {
      var result = await client.get(
          "$baseUrl//topics?channelId=$createChannelId",
          getDioOptions("GET", true));
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> getAllFriends() async {
    try {
      var result = await client.get(
          "$baseUrl/users-permissions/friends",
          getDioOptions(
            "GET",
            true,
          ));
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> pendingFriendRequests() async {
    try {
      var result = await client.get(
          "$baseUrl/users-permissions/friend-request",
          getDioOptions(
            "GET",
            true,
          ));
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> searchUsers(String userName) async {
    var cancelToken = CancelToken();
    try {
      var result = await client.get(
          "$baseUrl/users-permissions/search?q=$userName&_limit=20&_start=0",
          getDioOptions("GET", true),
          cancelToken);
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> getGroupDetails(String groupId) async {
    try {
      var result = await client.get(
        "$baseUrl/groups/$groupId",
        getDioOptions("GET", true),
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }

  Future<ApiResponse<String>> getAllGroups() async {
    try {
      var result = await client.get(
        "$baseUrl/groups",
        getDioOptions("GET", true),
      );
      if (result.status == ApiResponseStatus.error) {
        print('RESULT NULL=${result.message}');

        return ApiResponse.error(result.message);
      }
      return ApiResponse.completed(result.data.data);
    } catch (e, s) {
      print("e:$e");
      print("s:$s");
      return ApiResponse.error([AppNetworkClient.handleError(e)]);
    }
  }
}
