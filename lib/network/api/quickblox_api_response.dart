class QuickbloxApiResponse<T> {
  QuickbloxApiResponseStatus status;
  dynamic data;
  String message = "";

  QuickbloxApiResponse.completed(this.data)
      : status = QuickbloxApiResponseStatus.completed;
  QuickbloxApiResponse.error(this.message)
      : status = QuickbloxApiResponseStatus.error;

  @override
  String toString() {
    return "Status : $status \n Message : $message \n Data : $data";
  }
}

enum QuickbloxApiResponseStatus { completed, error }
