import 'dart:convert' as convert;

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import 'api/api_response.dart';

/// Network client helper used to perform the network operations.
///
/// Can call get, post and put apis.
/// Cancell all the active apis.
class AppNetworkClient {
  /// Holds the reference to the instance of [Dio] provided by the [Injector].
  ///
  /// This is library that performs the network requests.
  final Dio _dio = AppInjector.resolve<Dio>();

  /// Holds the reference to the instance of [AppRefreshInterceptor] provided by the [Injector].
  ///
  /// This is used as an interceptor for adding the access tokens for the requests and refreshing the
  /// access token on 401 network response.
  // final AppRefreshInterceptor _refreshInterceptor =
  //     AppInjector.resolve<AppRefreshInterceptor>();

  /// Holds the reference to the instance of [PrettyDioLogger] provided by the [Injector].
  ///
  /// This is used for logging the network requests and responses.
  final PrettyDioLogger _loggerInterceptor =
      AppInjector.resolve<PrettyDioLogger>();

  /// List of cancel token of the all active apis.
  List<CancelToken> _cancelTokenList = [];

  AppNetworkClient({bool isTest = false}) {
    if (!(isTest)) {
      _dio.options.connectTimeout = 52000;
      _dio.options.receiveTimeout = 99000;
      _dio.options.sendTimeout = 52000;
      _dio.options.baseUrl = 'http://3.16.23.116/';

      // Logging
      loggingInterceptor(shouldEnable: true);
    }
  }

  /// Cancels all the tokens present in the [_cancelTokenList].
  cancelAllRequests() {
    if (_cancelTokenList.isNotEmpty) {
      for (var cancelToken in _cancelTokenList) {
        if (!cancelToken.isCancelled) {
          cancelToken.cancel();
        }
      }
    }
    _cancelTokenList = [];
  }

  /// Adds the [PrettyDioLogger] logging interceptor to the interceptors list if [shouldEnable] flag is set to true.
  loggingInterceptor({bool shouldEnable = false}) {
    if (shouldEnable) {
      if (foundation.kDebugMode) {
        if (!_dio.interceptors.contains(_loggerInterceptor)) {
          _dio.interceptors.add(_loggerInterceptor);
        }
      }
    } else {
      if (_dio.interceptors.contains(_loggerInterceptor)) {
        _dio.interceptors.remove(_loggerInterceptor);
      }
    }
  }

  /// Performs post network request
  ///
  /// [api] specifies the api end point of the request.
  /// [body] specifies the body of the request.
  /// [params] specifies the query params of the request.
  /// [options] specifies the headers of the request.
  /// enables refresh token interceptor if [shouldRefreshToken] is set to true.
  /// [cancelToken] specifies the cancel token of the request.
  Future<ApiResponse<Response<dynamic>>> post(
    String api,
    body,
    Options options,
    bool shouldRefreshToken,
  ) async {
    try {
      String url = api;
      Response<dynamic> result = await _dio.post<String>(
        url,
        // data: convert.json.encode(body),
        data: body,
        options: options,
      );

      print("Dio Success Result=>${result.data}");
      return ApiResponse.completed(result);
    } catch (error) {
      if (error is DioError) {
        if (error.response != null) {
          if (error.response!.statusCode == 400) {
            var errorResponse = convert.jsonDecode(error.response!.data)
                as Map<String, dynamic>;

            print("Dio Error Result =>$errorResponse");
            return ApiResponse.error([
              errorResponse['message'][0]['messages'][0]['id'],
              errorResponse['message'][0]['messages'][0]['message']
            ]);
          }
          return ApiResponse.error([
            "Something went wrong.Please try later",
            "Something went wrong.Please try later"
          ]);
        }
      }
      return ApiResponse.error([
        "Something went wrong.Please try later",
        "Something went wrong.Please try later"
      ]);
    } finally {}
  }

  /// Performs get network request
  ///
  /// [api] specifies the api end point of the request.
  /// [body] specifies the body of the request.
  /// [params] specifies the query params of the request.
  /// [options] specifies the headers of the request.
  /// enables refresh token interceptor if [shouldRefreshToken] is set to true.
  /// [cancelToken] specifies the cancel token of the request.
  Future<ApiResponse<Response<dynamic>>> get(String api, Options options,
      [var cancelToken]) async {
    try {
      String url = api;
      Response<dynamic> result = await _dio.get<String>(
        url,
        cancelToken: cancelToken,
        options: options,
      );

      print("Dio Success Result=>${result.data}");
      return ApiResponse.completed(result);
    } catch (error) {
      if (error is DioError) {
        if (error.response != null) {
          if (error.response!.statusCode == 400) {
            var errorResponse = convert.jsonDecode(error.response!.data)
                as Map<String, dynamic>;

            print("Dio Error Result =>$errorResponse");
            return ApiResponse.error([
              errorResponse['message'][0]['messages'][0]['id'],
              errorResponse['message'][0]['messages'][0]['message']
            ]);
          }
          return ApiResponse.error([
            "Something went wrong.Please try later",
            "Something went wrong.Please try later"
          ]);
        }
      }
      return ApiResponse.error([
        "Something went wrong.Please try later",
        "Something went wrong.Please try later"
      ]);
    } finally {}
  }

  /// Performs put network request
  ///
  /// [api] specifies the api end point of the request.
  /// [body] specifies the body of the request.
  /// [params] specifies the query params of the request.
  /// [options] specifies the headers of the request.
  /// enables refresh token interceptor if [shouldRefreshToken] is set to true.
  /// [cancelToken] specifies the cancel token of the request.
  Future<ApiResponse<Response<dynamic>>> put(
    String api,
    body,
    Options options,
  ) async {
    try {
      String url = api;
      Response<dynamic> result = await _dio.put<String>(
        url,
        data: body,
        options: options,
      );

      print("Dio Success Result=>${result.data}");
      return ApiResponse.completed(result);
    } catch (error) {
      if (error is DioError) {
        if (error.response != null) {
          if (error.response!.statusCode == 400) {
            var errorResponse = convert.jsonDecode(error.response!.data)
                as Map<String, dynamic>;

            print("Dio Error Result =>$errorResponse");
            return ApiResponse.error([
              errorResponse['message'][0]['messages'][0]['id'],
              errorResponse['message'][0]['messages'][0]['message']
            ]);
          }
          return ApiResponse.error([
            "Something went wrong.Please try later",
            "Something went wrong.Please try later"
          ]);
        }
      }
      return ApiResponse.error([
        "Something went wrong.Please try later",
        "Something went wrong.Please try later"
      ]);
    } finally {}
  }

  /// Performs delete network request
  ///
  /// [api] specifies the api end point of the request.
  /// [body] specifies the body of the request.
  /// [params] specifies the query params of the request.
  /// [options] specifies the headers of the request.
  /// enables refresh token interceptor if [shouldRefreshToken] is set to true.
  /// [cancelToken] specifies the cancel token of the request.
  ///
  Future<ApiResponse<Response<dynamic>>> delete(
    String api,
    Options options,
  ) async {
    try {
      String url = api;
      Response<dynamic> result = await _dio.delete<String>(
        url,
        options: options,
      );

      print("Dio Success Result=>${result.data}");
      return ApiResponse.completed(result);
    } catch (error) {
      if (error is DioError) {
        if (error.response != null) {
          if (error.response!.statusCode == 400) {
            var errorResponse = convert.jsonDecode(error.response!.data)
                as Map<String, dynamic>;

            print("Dio Error Result =>$errorResponse");
            return ApiResponse.error([
              errorResponse['message'][0]['messages'][0]['id'],
              errorResponse['message'][0]['messages'][0]['message']
            ]);
          }
          return ApiResponse.error([
            "Something went wrong.Please try later",
            "Something went wrong.Please try later"
          ]);
        }
      }
      return ApiResponse.error([
        "Something went wrong.Please try later",
        "Something went wrong.Please try later"
      ]);
    } finally {}
  }

  static String handleError(Object exception) {
    String apiError = "";
    if (exception is Error) {
      apiError = "Something went wrong, please try again! ";
    } else if (exception is Exception) {
      apiError = "Something went wrong, please try again! ";
      if (exception is DioError) {
        switch (exception.type) {
          case DioErrorType.cancel:
            apiError = "Request cancelled";
            break;
          case DioErrorType.other:
            apiError =
                "Connection failed, Please check your internet connection and try again!";
            break;
          case DioErrorType.connectTimeout:
          case DioErrorType.sendTimeout:
          case DioErrorType.receiveTimeout:
            apiError = "Connection timeout";
            break;
          case DioErrorType.response:
          default:
            apiError = "Something went wrong, please try again! ";
            break;
        }
      }
    }
    return apiError;
  }
}
