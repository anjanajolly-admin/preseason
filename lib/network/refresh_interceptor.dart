// import 'package:dio/dio.dart';
// import 'package:pre_seasoned/database/app_prefereneces.dart';
// import 'package:pre_seasoned/utils/app_injector.dart';
//
//
// /// Request and Response interceptor
// ///
// /// Intercepts the requests to add the "Authorization" header.
// /// Intercepts the response for 401 and refreshes the access token.
// class AppRefreshInterceptor extends InterceptorsWrapper {
//   /// Holds the reference to the instance of [AppPreferences] provided by [Injector].
//   ///
//   /// This is used to fetch the access and refresh tokens from the preference.
//   final AppPreferences _preferences = AppInjector.resolve<AppPreferences>();
//
//   /// Holds the reference to the instance of [Dio] provided by [Injector].
//   final Dio _dio = AppInjector.resolve<Dio>();
//
//   @override
//   Future<dynamic> onRequest(RequestOptions options) async {
//     try {
//       final String authorizationHeader = await _getAuthorizationHeader();
//
//       if (authorizationHeader != null) {
//         // options.headers["Authorization"] = authorizationHeader;
//         // return options;
//       } else {
//         // await _logoutUser();
//       }
//     } catch (error) {
//       return options;
//     }
//   }
//
//   @override
//   Future<dynamic> onResponse(Response<dynamic> response) async {
//     return response;
//   }
//
//   @override
//   Future<dynamic> onError(DioError err) async {
//     try {
//       if (err.response?.statusCode == 401) {
//         final String currentAuthHeader = await _getAuthorizationHeader();
//
//         if (err.request.headers["authorization"] == (currentAuthHeader ?? "")) {
//           // Access token needs to be refreshed
//           // final AuthTokenApiService _authTokenApiService =
//           //     Injector.resolve<AuthTokenApiService>();
//           // _dio.interceptors.requestLock.lock();
//           // _dio.interceptors.responseLock.lock();
//           // final refreshResponse = await _authTokenApiService.refreshToken();
//           // _dio.interceptors.requestLock.unlock();
//           // _dio.interceptors.responseLock.unlock();
//           //
//           // return refreshResponse.fold((response) async {
//           //   return await _retryRequest(error.request);
//           // }, (error) async {
//           //   // return error;
//           //   await _logoutUser();
//           // });
//         } else {
//           // New access token is available
//           return await _retryRequest(err.request);
//         }
//       } else {
//         return err;
//       }
//     } catch (caughtError) {
//       return err;
//     }
//   }
//
//   /// Logs out the user by adding [LogoutEvent] to [SessionBloc].
//   // _logoutUser() async {}
//
//   /// Retries a request based on the [requestOptions] provided.
//   Future<Response<dynamic>> _retryRequest(RequestOptions requestOptions) async {
//     final options = await onRequest(requestOptions);
//     return await _dio.request(requestOptions.path,
//         data: requestOptions.data,
//         queryParameters: requestOptions.queryParameters,
//         options: options as Options);
//   }
//
//   /// Returns the authorization header string that can be appended to the request header.
//   ///
//   /// Returns [null] if access token is not available.
//   Future<String> _getAuthorizationHeader() async {
//     // final String currentAccessToken = _preferences.getAccessToken();
//     // if (currentAccessToken == null || currentAccessToken.trim().isEmpty) {
//     //   return null;
//     // }
//     // return currentAccessToken;
//   }
// }
