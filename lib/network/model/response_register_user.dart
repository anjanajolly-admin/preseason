// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

class LoginResponse {
  String jwt;
  User user;

  LoginResponse(
    this.jwt,
    this.user,
  );

  factory LoginResponse.fromRawJson(String str) =>
      LoginResponse.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        json["jwt"] == null ? null : json["jwt"],
        (json["user"] == null ? null : User.fromJson(json["user"]))!,
      );

  Map<String, dynamic> toJson() => {
        "jwt": jwt == null ? null : jwt,
        "user": user == null ? null : user.toJson(),
      };
}

class User {
  User({
    required this.id,
    required this.username,
    required this.email,
    required this.provider,
    required this.confirmed,
    required this.blocked,
    required this.role,
    required this.createdAt,
    required this.updatedAt,
    required this.firstName,
    required this.lastName,
    required this.userId,
    required this.profilePhoto,
    required this.channelsFollowed,
    required this.channelsManaged,
    required this.threadsManaged,
    required this.topicsManaged,
    required this.notificationTokens,
    required this.interests,
  });

  int id;
  String username;
  String email;
  String provider;
  bool confirmed;
  bool blocked;
  Role role;
  DateTime createdAt;
  DateTime updatedAt;
  String firstName;
  String lastName;
  String userId;
  dynamic profilePhoto;
  List<dynamic> channelsFollowed;
  List<dynamic> channelsManaged;
  List<dynamic> threadsManaged;
  List<dynamic> topicsManaged;
  List<dynamic> notificationTokens;
  List<Interest> interests;

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        username: json["username"],
        email: json["email"],
        provider: json["provider"],
        confirmed: json["confirmed"],
        blocked: json["blocked"],
        role: Role.fromJson(json["role"]),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        firstName: json["firstName"],
        lastName: json["lastName"],
        userId: json["user_id"],
        profilePhoto: json["profilePhoto"],
        channelsFollowed:
            List<dynamic>.from(json["channels_followed"].map((x) => x)),
        channelsManaged:
            List<dynamic>.from(json["channels_managed"].map((x) => x)),
        threadsManaged:
            List<dynamic>.from(json["threads_managed"].map((x) => x)),
        topicsManaged: List<dynamic>.from(json["topics_managed"].map((x) => x)),
        notificationTokens:
            List<dynamic>.from(json["notification_tokens"].map((x) => x)),
        interests: List<Interest>.from(
            json["interests"].map((x) => Interest.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "username": username,
        "email": email,
        "provider": provider,
        "confirmed": confirmed,
        "blocked": blocked,
        "role": role.toJson(),
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "firstName": firstName,
        "lastName": lastName,
        "user_id": userId,
        "profilePhoto": profilePhoto,
        "channels_followed":
            List<dynamic>.from(channelsFollowed.map((x) => x.toJson())),
        "channels_managed":
            List<dynamic>.from(channelsManaged.map((x) => x.toJson())),
        "threads_managed": List<dynamic>.from(threadsManaged.map((x) => x)),
        "topics_managed": List<dynamic>.from(topicsManaged.map((x) => x)),
        "notification_tokens":
            List<dynamic>.from(notificationTokens.map((x) => x)),
        "interests": List<dynamic>.from(interests.map((x) => x.toJson())),
      };
}

// class Channels {
//   Channels({
//     required this.id,
//     required this.name,
//     required this.bio,
//     required this.code,
//     required this.publishedAt,
//     required this.createdAt,
//     required this.updatedAt,
//     required this.channelId,
//     required this.profilePhoto,
//     required this.coverPhoto,
//   });
//
//   int id;
//   String name;
//   String bio;
//   String code;
//   DateTime publishedAt;
//   DateTime createdAt;
//   DateTime updatedAt;
//   String channelId;
//   Photo profilePhoto;
//   Photo coverPhoto;
//
//   factory Channels.fromRawJson(String str) =>
//       Channels.fromJson(json.decode(str));
//
//   String toRawJson() => json.encode(toJson());
//
//   factory Channels.fromJson(Map<String, dynamic> json) => Channels(
//         id: json["id"] == null ? null : json["id"],
//         name: json["name"] == null ? null : json["name"],
//         bio: json["bio"] == null ? null : json["bio"],
//         code: json["code"] == null ? null : json["code"],
//         publishedAt: (json["published_at"] == null
//             ? null
//             : DateTime.parse(json["published_at"]))!,
//         createdAt: (json["created_at"] == null
//             ? null
//             : DateTime.parse(json["created_at"]))!,
//         updatedAt: (json["updated_at"] == null
//             ? null
//             : DateTime.parse(json["updated_at"]))!,
//         channelId: json["channel_id"] == null ? null : json["channel_id"],
//         coverPhoto: (json["coverPhoto"] == null
//             ? null
//             : Photo.fromJson(json["coverPhoto"]))!,
//         profilePhoto: (json["profilePhoto"] == null
//             ? null
//             : Photo.fromJson(json["profilePhoto"]))!,
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id == null ? null : id,
//         "name": name == null ? null : name,
//         "bio": bio == null ? null : bio,
//         "code": code == null ? null : code,
//         "published_at":
//             publishedAt == null ? null : publishedAt.toIso8601String(),
//         "created_at": createdAt == null ? null : createdAt.toIso8601String(),
//         "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
//         "channel_id": channelId == null ? null : channelId,
//         "profilePhoto": profilePhoto == null ? null : profilePhoto.toJson(),
//         "coverPhoto": coverPhoto == null ? null : coverPhoto.toJson(),
//       };
// }

class Photo {
  Photo({
    required this.id,
    required this.name,
    required this.alternativeText,
    required this.caption,
    required this.width,
    required this.height,
    required this.formats,
    required this.hash,
    required this.ext,
    required this.mime,
    required this.size,
    required this.url,
    required this.previewUrl,
    required this.provider,
    required this.providerMetadata,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String name;
  dynamic alternativeText;
  dynamic caption;
  int width;
  int height;
  Formats formats;
  String hash;
  Ext ext;
  Mime mime;
  double size;
  String url;
  dynamic previewUrl;
  Provider provider;
  dynamic providerMetadata;
  DateTime createdAt;
  DateTime updatedAt;

  factory Photo.fromRawJson(String str) => Photo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        alternativeText: json["alternativeText"],
        caption: json["caption"],
        width: json["width"] == null ? null : json["width"],
        height: json["height"] == null ? null : json["height"],
        formats: (json["formats"] == null
            ? null
            : Formats.fromJson(json["formats"]))!,
        hash: json["hash"] == null ? null : json["hash"],
        ext: (json["ext"] == null ? null : extValues.map[json["ext"]])!,
        mime: (json["mime"] == null ? null : mimeValues.map[json["mime"]])!,
        size: json["size"] == null ? null : json["size"].toDouble(),
        url: json["url"] == null ? null : json["url"],
        previewUrl: json["previewUrl"],
        provider: (json["provider"] == null
            ? null
            : providerValues.map[json["provider"]])!,
        providerMetadata: json["provider_metadata"],
        createdAt: (json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]))!,
        updatedAt: (json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]))!,
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "alternativeText": alternativeText,
        "caption": caption,
        "width": width == null ? null : width,
        "height": height == null ? null : height,
        "formats": formats == null ? null : formats.toJson(),
        "hash": hash == null ? null : hash,
        "ext": ext == null ? null : extValues.reverse[ext],
        "mime": mime == null ? null : mimeValues.reverse[mime],
        "size": size == null ? null : size,
        "url": url == null ? null : url,
        "previewUrl": previewUrl,
        "provider": provider == null ? null : providerValues.reverse[provider],
        "provider_metadata": providerMetadata,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}

enum Ext { JPG, PNG, WEBP }

final extValues =
    EnumValues({".jpg": Ext.JPG, ".png": Ext.PNG, ".webp": Ext.WEBP});

class Formats {
  Formats({
    required this.small,
    required this.medium,
    required this.large,
  });

  Large small;
  Large medium;
  Large large;

  factory Formats.fromRawJson(String str) => Formats.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Formats.fromJson(Map<String, dynamic> json) => Formats(
        small: (json["small"] == null ? null : Large.fromJson(json["small"]))!,
        medium:
            (json["medium"] == null ? null : Large.fromJson(json["medium"]))!,
        large: (json["large"] == null ? null : Large.fromJson(json["large"]))!,
      );

  Map<String, dynamic> toJson() => {
        "small": (small == null ? null : small.toJson())!,
        "medium": (medium == null ? null : medium.toJson())!,
        "large": (large == null ? null : large.toJson())!,
      };
}

class Large {
  Large({
    required this.ext,
    required this.url,
    required this.hash,
    required this.mime,
    required this.name,
    required this.path,
    required this.size,
    required this.width,
    required this.height,
  });

  Ext ext;
  String url;
  String hash;
  Mime mime;
  String name;
  dynamic path;
  double size;
  int width;
  int height;

  factory Large.fromRawJson(String str) => Large.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Large.fromJson(Map<String, dynamic> json) => Large(
        ext: (json["ext"] == null ? null : extValues.map[json["ext"]])!,
        url: json["url"] == null ? null : json["url"],
        hash: json["hash"] == null ? null : json["hash"],
        mime: (json["mime"] == null ? null : mimeValues.map[json["mime"]])!,
        name: json["name"] == null ? null : json["name"],
        path: json["path"],
        size: json["size"] == null ? null : json["size"].toDouble(),
        width: json["width"] == null ? null : json["width"],
        height: json["height"] == null ? null : json["height"],
      );

  Map<String, dynamic> toJson() => {
        "ext": ext == null ? null : extValues.reverse[ext],
        "url": url == null ? null : url,
        "hash": hash == null ? null : hash,
        "mime": mime == null ? null : mimeValues.reverse[mime],
        "name": name == null ? null : name,
        "path": path,
        "size": size == null ? null : size,
        "width": width == null ? null : width,
        "height": height == null ? null : height,
      };
}

enum Mime { APPLICATION_OCTET_STREAM }

final mimeValues =
    EnumValues({"application/octet-stream": Mime.APPLICATION_OCTET_STREAM});

enum Provider { AWS_S3 }

final providerValues = EnumValues({"aws-s3": Provider.AWS_S3});

class Interest {
  Interest({
    required this.id,
    required this.name,
    required this.publishedAt,
    required this.createdAt,
    required this.updatedAt,
    required this.interestId,
  });

  int id;
  String name;
  DateTime publishedAt;
  DateTime createdAt;
  DateTime updatedAt;
  String interestId;

  factory Interest.fromRawJson(String str) =>
      Interest.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Interest.fromJson(Map<String, dynamic> json) => Interest(
        id: json["id"],
        name: json["name"],
        publishedAt: DateTime.parse(json["published_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        interestId: json["interest_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "published_at": publishedAt.toIso8601String(),
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "interest_id": interestId,
      };
}

class Role {
  Role({
    required this.id,
    required this.name,
    required this.description,
    required this.type,
  });

  int id;
  String name;
  String description;
  String type;

  factory Role.fromRawJson(String str) => Role.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Role.fromJson(Map<String, dynamic> json) => Role(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "type": type,
      };
}

class EnumValues<T> {
  late Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    // ignore: unnecessary_null_comparison
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
