import 'dart:async';

import 'package:flutter/foundation.dart';

class Debounce {
  final Duration delay;
  Timer? _timer;
  VoidCallback? action;

  Debounce({this.delay = const Duration(milliseconds: 1000)});

  call(action) {
    _timer?.cancel();
    _timer = Timer(delay, action);
  }
}
