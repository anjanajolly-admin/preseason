import 'dart:async';
import 'dart:convert' as convert;
import 'dart:ui';

import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/foundation.dart' show kDebugMode, kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/group_messages_screens/group_message_details_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/messages_details_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/friends_screen/pending_invitation_screen.dart';
import 'package:pre_seasoned/screens/thread_screens/general_thread_screen.dart';
import 'package:pre_seasoned/screens/welcome_screen/welcome_screen.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:pre_seasoned/utils/app_routes.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:rxdart/subjects.dart';

import 'database/app_prefereneces.dart';
import 'network/api/api_response.dart';
import 'network/api/api_service.dart';
import 'network/api/quickblox_api_service.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  channel = const AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    description:
        'This channel is used for important notifications.', // description
    importance: Importance.high,
  );
  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);
  if (message.data.isEmpty) {
    print(
        "Message got in Background is ${message.notification!.body} and ${message.data} and ${message.notification!.hashCode}");
    await flutterLocalNotificationsPlugin.show(
      message.notification.hashCode,
      message.notification!.title == null
          ? "Notification"
          : message.notification!.title,
      message.notification!.body,
      NotificationDetails(
          android: AndroidNotificationDetails(
            channel.id,
            channel.name,
            channelDescription: channel.description,
            icon: null,
            playSound: true,
            priority: Priority.high,
            importance: Importance.max,
          ),
          iOS: IOSNotificationDetails(
              presentAlert: true,
              presentBadge: true,
              presentSound: true,
              badgeNumber: 1,
              // attachments: List<IOSNotificationAttachment>
              subtitle: "Got it",
              threadIdentifier: "threadIdentifier")),
      payload: null,
    );
  } else {
    print(
        "Message got in Background is ${message.notification} and ${message.data}");

    final notificationDetails =
        (message.data['message'] as String).contains("{")
            ? convert.jsonDecode(message.data['message'])
            : "";
    final tempPayload = (message.data['message'] as String).contains("{")
        ? convert.jsonEncode({
            "screen": notificationDetails['screen'],
            "navigationObject": notificationDetails['navigationObject'],
          })
        : null;
    await flutterLocalNotificationsPlugin.show(
      0,
      notificationDetails != ""
          ? notificationDetails['notificationName']
          : "Notification",
      notificationDetails != ""
          ? notificationDetails['senderName'] == ""
              ? notificationDetails['message']
              : "${notificationDetails['senderName']}: ${notificationDetails['message']}"
          : "Server: ${message.data['message']}",
      NotificationDetails(
          android: AndroidNotificationDetails(
            channel.id,
            channel.name,
            channelDescription: channel.description,
            icon: null,
            playSound: true,
            priority: Priority.high,
            importance: Importance.max,
          ),
          iOS: IOSNotificationDetails(
              presentAlert: true,
              presentBadge: true,
              presentSound: true,
              badgeNumber: 1,
              // attachments: List<IOSNotificationAttachment>
              subtitle: "Got it",
              threadIdentifier: "threadIdentifier")),
      payload: tempPayload,
    );
  }

  print('Handling a background message ${message.data}');
}

late AndroidNotificationChannel channel;
late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject =
    BehaviorSubject<ReceivedNotification>();

final BehaviorSubject<String?> selectNotificationSubject =
    BehaviorSubject<String?>();

class ReceivedNotification {
  ReceivedNotification({
    required this.id,
    required this.title,
    required this.body,
    required this.payload,
  });

  final int id;
  final String? title;
  final String? body;
  final String? payload;
}

String? selectedNotificationPayload;

void main() async {
  //Comment
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await Firebase.initializeApp();
  await AppInjector.setUp();
  if (kDebugMode) {
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(false);
  } else {
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  }
  if (!kIsWeb) {
    channel = const AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      description:
          'This channel is used for important notifications.', // description
      importance: Importance.high,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  if (FirebaseCrashlytics.instance.isCrashlyticsCollectionEnabled)
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  runApp(
    MyApp(),
  );
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  void initState() {
    print("Main Init Check");
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            requestAlertPermission: false,
            requestBadgePermission: false,
            requestSoundPermission: false,
            onDidReceiveLocalNotification: (
              int id,
              String? title,
              String? body,
              String? payload,
            ) async {
              didReceiveLocalNotificationSubject.add(
                ReceivedNotification(
                  id: id,
                  title: title,
                  body: body,
                  payload: payload,
                ),
              );
            });
    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    if (WidgetsBinding.instance != null) {
      WidgetsBinding.instance!.addObserver(this);
    }
    super.initState();
    initQuickblox();
    initTokenRefresh();
    initFirebaseMessaging();
  }

  @override
  void dispose() {
    if (WidgetsBinding.instance != null) {
      WidgetsBinding.instance!.removeObserver(this);
    }
    super.dispose();
  }

  Future<void> initFirebaseMessaging() async {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) async {
      print(
          "A new onMessageOpenedApp event was published ---> getInitialMessage");
      if (message != null) {
        await Firebase.initializeApp();
        channel = const AndroidNotificationChannel(
          'high_importance_channel', // id
          'High Importance Notifications', // title
          description:
              'This channel is used for important notifications.', // description
          importance: Importance.high,
        );
        flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
        await flutterLocalNotificationsPlugin
            .resolvePlatformSpecificImplementation<
                AndroidFlutterLocalNotificationsPlugin>()
            ?.createNotificationChannel(channel);
        if (message.data.isEmpty) {
          print(
              "Message got in Background is ${message.notification!.body} and ${message.data} and ${message.notification!.hashCode}");
          await flutterLocalNotificationsPlugin.show(
            message.notification.hashCode,
            message.notification!.title == null
                ? "Notification"
                : message.notification!.title,
            message.notification!.body,
            NotificationDetails(
                android: AndroidNotificationDetails(
                  channel.id,
                  channel.name,
                  channelDescription: channel.description,
                  icon: null,
                  playSound: true,
                  priority: Priority.high,
                  importance: Importance.max,
                ),
                iOS: IOSNotificationDetails(
                    presentAlert: true,
                    presentBadge: true,
                    presentSound: true,
                    badgeNumber: 1,
                    // attachments: List<IOSNotificationAttachment>
                    subtitle: "Got it",
                    threadIdentifier: "threadIdentifier")),
            payload: null,
          );
        } else {
          print(
              "Message got in Background is ${message.notification} and ${message.data}");

          final notificationDetails =
              (message.data['message'] as String).contains("{")
                  ? convert.jsonDecode(message.data['message'])
                  : "";
          final tempPayload = (message.data['message'] as String).contains("{")
              ? convert.jsonEncode({
                  "screen": notificationDetails['screen'],
                  "navigationObject": notificationDetails['navigationObject'],
                })
              : null;
          await flutterLocalNotificationsPlugin.show(
            0,
            notificationDetails != ""
                ? notificationDetails['notificationName']
                : "Notification",
            notificationDetails != ""
                ? notificationDetails['senderName'] == ""
                    ? notificationDetails['message']
                    : "${notificationDetails['senderName']}: ${notificationDetails['message']}"
                : "Server: ${message.data['message']}",
            NotificationDetails(
                android: AndroidNotificationDetails(
                  channel.id,
                  channel.name,
                  channelDescription: channel.description,
                  icon: null,
                  playSound: true,
                  priority: Priority.high,
                  importance: Importance.max,
                ),
                iOS: IOSNotificationDetails(
                    presentAlert: true,
                    presentBadge: true,
                    presentSound: true,
                    badgeNumber: 1,
                    // attachments: List<IOSNotificationAttachment>
                    subtitle: "Got it",
                    threadIdentifier: "threadIdentifier")),
            payload: tempPayload,
          );
        }
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      // final notificationDetails =
      //     (message.data['message'] as String).contains("{")
      //         ? jsonDecode(message.data['message'])
      //         : "";
      print("Message got in foreground is notificationDetails");
      // await flutterLocalNotificationsPlugin.show(
      //   0,
      //   notificationDetails != ""
      //       ? notificationDetails['channelName']
      //       : "Notification",
      //   notificationDetails != ""
      //       ? notificationDetails['message']
      //       : message.data['message'],
      //   NotificationDetails(
      //       android: AndroidNotificationDetails(
      //         channel.id,
      //         channel.name,
      //         channelDescription: channel.description,
      //         icon: null,
      //         playSound: true,
      //         priority: Priority.high,
      //         importance: Importance.max,
      //       ),
      //       iOS: IOSNotificationDetails(
      //           presentAlert: true,
      //           presentBadge: true,
      //           presentSound: true,
      //           badgeNumber: 1,
      //           // attachments: List<IOSNotificationAttachment>
      //           subtitle: "Got it",
      //           threadIdentifier: "threadIdentifier")),
      //   payload: null,
      // );
      // }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');

      // Navigator.pushNamed(context, '/message',
      //     arguments: MessageArguments(message, true));
    });
  }

  Future<void> initQuickblox() async {
    AppInjector.resolve<QuickbloxApiService>().initQuickBlox();
    AppInjector.resolve<QuickbloxApiService>().initStreamManagement(3, true);
    AppInjector.resolve<QuickbloxApiService>().enableAutoReconnect();
  }

  Future initTokenRefresh() async {
    print("Token is initial refreshed");
    FirebaseMessaging.instance.onTokenRefresh.listen((newToken) {
      print('New Token is generated  :$newToken');
      if (AppInjector.resolve<AppPreferences>().getJwtToken() != newToken) {
        AppInjector.resolve<AppPreferences>().setJwtToken(newToken);
      }

      String? userId =
          AppInjector.resolve<AppPreferences>().getUserId().toString();
      print('userId :$userId');
      // ignore: unnecessary_null_comparison
      if (userId != null) {
        sendTokenApi(newToken, userId);
      } else
        return;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(375, 812),
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Pre Seasoned",
        builder: BotToastInit(),
        navigatorObservers: [BotToastNavigatorObserver()],
        navigatorKey: AppInjector.resolve<AppRoutes>().navigatorKey,
        onGenerateRoute: AppInjector.resolve<AppRoutes>().obtainRoute,
        initialRoute: WelcomeScreen.routeName,
        theme: ThemeData(primarySwatch: Colors.blue),
      ),
    );
  }

  Future<void> sendTokenApi(token, userId) async {
    var response = await AppInjector.resolve<ApiService>().sendTokenApi({
      "token": token,
      "user": userId,
    });

    if (response.status == ApiResponseStatus.completed) {
      return;
    } else if (response.status == ApiResponseStatus.error) {
      print("response data init ${response.message[1]}");
      return;
    } else {
      return;
    }
  }

  Future<dynamic> onSelectNotification(payload) async {
    if (payload != null) {
      debugPrint('Notification payload: $payload');
      var jsonPayload = convert.jsonDecode(payload) as Map<String, dynamic>;
      debugPrint('Notification payload decoded: $jsonPayload');

      if (jsonPayload['screen'] == "1") {
        debugPrint("Inside Screen 1 ");

        AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pushNamed(
            GeneralThreadScreen.routeName,
            arguments: [jsonPayload['navigationObject'], false]);
      } else if (jsonPayload['screen'] == "2") {
        debugPrint("Inside Screen 2 ");
        QBDialog messageDetails = QBDialog();
        messageDetails.isJoined = jsonPayload['navigationObject']['isJoined'];
        messageDetails.createdAt = jsonPayload['navigationObject']['createdAt'];
        messageDetails.lastMessage =
            jsonPayload['navigationObject']['lastMessage'];
        messageDetails.lastMessageDateSent =
            jsonPayload['navigationObject']['lastMessageDateSent'];
        messageDetails.lastMessageUserId =
            jsonPayload['navigationObject']['lastMessageUserId'];
        messageDetails.name = jsonPayload['navigationObject']['name'];
        messageDetails.photo = jsonPayload['navigationObject']['photo'];
        messageDetails.type = jsonPayload['navigationObject']['type'];
        messageDetails.unreadMessagesCount =
            jsonPayload['navigationObject']['unreadMessagesCount'];
        messageDetails.updatedAt = jsonPayload['navigationObject']['updatedAt'];
        messageDetails.userId = jsonPayload['navigationObject']['userId'];
        messageDetails.roomJid = jsonPayload['navigationObject']['roomJid'];
        messageDetails.id = jsonPayload['navigationObject']['id'];
        List<int>? tempOccupantId = [];
        for (int index = 0;
            index <
                (jsonPayload['navigationObject']['occupantsIds'] as List)
                    .length;
            index++) {
          tempOccupantId
              .add(jsonPayload['navigationObject']['occupantsIds'][index]);
        }
        messageDetails.occupantsIds = tempOccupantId;
        messageDetails.customData =
            jsonPayload['navigationObject']['customData'];

        AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pushNamed(
            MessageDetailsScreen.routeName,
            arguments: [messageDetails]);
      } else if (jsonPayload['screen'] == "3") {
        debugPrint("Inside Screen 3 ");
        QBDialog messageDetails = QBDialog();
        messageDetails.isJoined = jsonPayload['navigationObject']['isJoined'];
        messageDetails.createdAt = jsonPayload['navigationObject']['createdAt'];
        messageDetails.lastMessage =
            jsonPayload['navigationObject']['lastMessage'];
        messageDetails.lastMessageDateSent =
            jsonPayload['navigationObject']['lastMessageDateSent'];
        messageDetails.lastMessageUserId =
            jsonPayload['navigationObject']['lastMessageUserId'];
        messageDetails.name = jsonPayload['navigationObject']['name'];
        messageDetails.photo = jsonPayload['navigationObject']['photo'];
        messageDetails.type = jsonPayload['navigationObject']['type'];
        messageDetails.unreadMessagesCount =
            jsonPayload['navigationObject']['unreadMessagesCount'];
        messageDetails.updatedAt = jsonPayload['navigationObject']['updatedAt'];
        messageDetails.userId = jsonPayload['navigationObject']['userId'];
        messageDetails.roomJid = jsonPayload['navigationObject']['roomJid'];
        messageDetails.id = jsonPayload['navigationObject']['id'];
        List<int>? tempOccupantId = [];
        for (int index = 0;
            index <
                (jsonPayload['navigationObject']['occupantsIds'] as List)
                    .length;
            index++) {
          tempOccupantId
              .add(jsonPayload['navigationObject']['occupantsIds'][index]);
        }
        messageDetails.occupantsIds = tempOccupantId;
        messageDetails.customData =
            jsonPayload['navigationObject']['customData'];

        AppInjector.resolve<AppRoutes>().navigatorKey.currentState!.pushNamed(
            GroupMessageDetailsScreen.routeName,
            arguments: [messageDetails]);
      } else if (jsonPayload['screen'] == "4") {
        AppInjector.resolve<AppRoutes>()
            .navigatorKey
            .currentState!
            .pushNamed(PendingInvitationScreen.routeName);
      }
    }
    selectedNotificationPayload = payload;
    selectNotificationSubject.add(payload);
  }
}
