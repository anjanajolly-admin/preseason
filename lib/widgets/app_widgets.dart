import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:quickblox_sdk/models/qb_attachment.dart';
import 'package:quickblox_sdk/models/qb_file.dart';
import 'package:quickblox_sdk/models/qb_message.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

class AppWidgets {
  static var backgroundColor = Color(0xff2c2c2e);

  static Color titleColor1 = Color(0xFF1C2C40);
  static Color titleColor2 = Color(0xFF2F2F7);
  static Color titleColor3 = Color(0xFFA4A4AB);
  static Color titleColor4 = Color(0xFF75757A);
  static Color titleColor5 = Color(0xFF464649);

  static var primaryColor = Color(0xff0080ff);
  static var outlineColor = Color(0xFF75757A);
  static var overlayColor = Color(0xFF18191F).withOpacity(0.6);
  static var buttonTextColor = Color(0xff5cabff);
  static var widgetBackgroundColor = Color(0xff565656);

  static Widget getTextButton(String buttonName, Color textColor,
      double fontSize, void Function() onPressed) {
    return TextButton(
        onPressed: onPressed,
        style: ButtonStyle(
          overlayColor:
          MaterialStateColor.resolveWith((states) => Colors.transparent),
        ),
        child: Text(
          buttonName,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: buttonTextColor,
            fontSize: fontSize,
            fontFamily: "Proxima Nova",
            fontWeight: FontWeight.w600,
          ),
        ));
  }

  static Widget getButton({
    required String buttonName,
    required bool isLoader,
    required double fontSize,
    required double width,
    required double height,
    required Color color,
    required Color textColor,
    required void Function() onPressed,
  }) {
    final ButtonStyle flatButtonStyle = ElevatedButton.styleFrom(
        minimumSize: Size(width, height),
        primary: color,
        elevation: 0,
        shadowColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        alignment: Alignment.center);
    return ElevatedButton(
        style: flatButtonStyle,
        onPressed: onPressed,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            buttonName,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: textColor,
              fontSize: 16,
              fontFamily: "Proxima Nova",
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          isLoader
              ? SizedBox(
              height: 16,
              width: 16,
              child: CircularProgressIndicator.adaptive(
                  strokeWidth: 2,
                  backgroundColor: AppWidgets.buttonTextColor,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white)))
              : Container(),
        ]));
  }

  static Widget getWtaButton(
      {required String buttonName,
        required double fontSize,
        required double width,
        required double height,
        required bool isLoader,
        required void Function() onPressed}) {
    return getButton(
        buttonName: buttonName,
        isLoader: isLoader,
        fontSize: fontSize,
        width: width,
        height: height,
        onPressed: onPressed,
        color: primaryColor,
        textColor: Colors.white);
  }

  static Widget getEditText({
    required TextEditingController controller,
    required Function(String) onChanged,
    required TextInputType keyboardType,
    required int maxLength,
    required String hint,
    required List<TextInputFormatter> inputFormatter,
    required bool isObscureText,
  }) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
            child: Container(
              height: 48,
              padding: EdgeInsets.only(left: 10, right: 10),
              decoration: BoxDecoration(
                color: backgroundColor,
                border: Border.all(
                  color: AppWidgets.titleColor4,
                  width: 1,
                ), // set border width
                borderRadius: BorderRadius.all(
                    Radius.circular(8.0)), // set rounded corner radius
              ),
              child: TextField(
                controller: controller,
                inputFormatters: inputFormatter,
                obscureText: isObscureText,
                onChanged: onChanged,
                // maxLines: isObscureText ? 1 : null,
                style: TextStyle(
                  color: AppWidgets.titleColor3,
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontFamily: "Proxima Nova",
                ),
                decoration: InputDecoration(
                  hintText: hint,
                  border: InputBorder.none,
                  hintStyle: TextStyle(
                    color: AppWidgets.titleColor3,
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    fontFamily: "Proxima Nova",
                  ),
                ),
              ),
            )),
      ],
    );
  }

  static Widget getEditPasswordText(
      {required TextEditingController controller,
        required Function(String) onChanged,
        required String hint,
        required List<TextInputFormatter> inputFormatter,
        required bool isObscureText,
        required Widget suffixIcon}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
            child: Container(
              height: 48,
              padding: EdgeInsets.only(left: 10, right: 10),
              decoration: BoxDecoration(
                color: backgroundColor,
                border: Border.all(
                  color: AppWidgets.titleColor4,
                  width: 1,
                ), // set border width
                borderRadius: BorderRadius.all(
                    Radius.circular(8.0)), // set rounded corner radius
              ),
              child: TextField(
                controller: controller,
                inputFormatters: inputFormatter,
                obscureText: isObscureText,
                onChanged: onChanged,
                maxLines: isObscureText ? 1 : null,
                style: TextStyle(
                  color: AppWidgets.titleColor3,
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontFamily: "Proxima Nova",
                ),
                decoration: InputDecoration(
                  hintText: hint,
                  border: InputBorder.none,
                  hintStyle: TextStyle(
                    color: AppWidgets.titleColor3,
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    fontFamily: "Proxima Nova",
                  ),
                  suffixIcon: suffixIcon,
                ),
              ),
            )),
      ],
    );
  }

  static Widget getHomeScreenChannelCard(
      {required List channelsList,
        required int index,
        required Map<String, int?>? channelsUnreadMessageCount,
        required void onTap()}) {
    return InkWell(
      focusColor: Colors.transparent,
      highlightColor: Colors.transparent,
      hoverColor: Colors.transparent,
      splashColor: Colors.transparent,
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Color(0xff1c1c1e),
        ),
        child: Row(
          children: [
            Container(
              width: 122,
              height: 122,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
              ),
              child: Container(
                width: 80,
                height: 80,
                child: channelsList[index]['profilePhoto'] == null
                    ? Image.asset(
                  "assets/images/suggested_channel_cover.png",
                  fit: BoxFit.fill,
                )
                    : CachedNetworkImage(
                  imageUrl: channelsList[index]['profilePhoto']['url'],
                  fit: BoxFit.fill,
                  placeholder: (context, url) => Image.asset(
                    "assets/images/suggested_channel_cover.png",
                    fit: BoxFit.fill,
                  ),

                  // placeholder: (context, url) =>
                  //     CircularProgressIndicator(),
                  errorWidget: (context, url, error) {
                    print('errorWidget ${error.toString()} \n$url');
                    return Image.asset(
                      "assets/images/suggested_channel_cover.png",
                      fit: BoxFit.fill,
                    );
                  },
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              capitalize(channelsList[index]['name']),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontFamily: "Proxima Nova",
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          channelsUnreadMessageCount == null
                              ? Container()
                              : channelsUnreadMessageCount.isEmpty ||
                              channelsUnreadMessageCount[
                              channelsList[index]
                              ['generalThread']
                              ['feed_id']] ==
                                  null
                              ? Container()
                              : Container(
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.circular(100),
                              color: Color(0xffe53535),
                            ),
                            padding: const EdgeInsets.all(4),
                            child: Column(
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              children: [
                                Text(
                                  channelsUnreadMessageCount[
                                  channelsList[index]
                                  ['generalThread']
                                  ['feed_id']]
                                      .toString(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontFamily: "Proxima Nova",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 12),
                    ListView.builder(
                      itemCount: channelsList[index]['threads'].length,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, threadIndex) {
                        return Column(
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    width: 20,
                                    height: 20,
                                    child: Image.asset(
                                      "assets/images/message_icon.png",
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      capitalize(channelsList[index]['threads']
                                      [threadIndex]['name']),
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                  channelsUnreadMessageCount == null
                                      ? Container()
                                      : channelsUnreadMessageCount == null
                                      ? Container()
                                      : channelsUnreadMessageCount
                                      .isEmpty ||
                                      channelsUnreadMessageCount[
                                      channelsList[index][
                                      'threads']
                                      [threadIndex]
                                      ['feed_id']] ==
                                          null
                                      ? Container()
                                      : SizedBox(
                                    child: Text(
                                      channelsUnreadMessageCount[
                                      channelsList[index][
                                      'threads']
                                      [
                                      threadIndex]
                                      ['feed_id']]
                                          .toString(),
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                        fontFamily:
                                        "Proxima Nova",
                                        fontWeight:
                                        FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  )
                                ],
                              ),
                            ),
                            SizedBox(height: 12),
                          ],
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  static Widget getAlertDialog(
      {required String title,
        required String content,
        required String confirmButton,
        required String cancelButton,
        required void onConfirmTap(),
        required void onCancelTap()}) {
    return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
        child: AlertDialog(
          backgroundColor: Color(0xff3a3a3c),
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(20)),
          title: new Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontWeight: FontWeight.w700,
              fontFamily: "Proxima Nova",
            ),
          ),
          content: new Text(
            content,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontWeight: FontWeight.w700,
              fontFamily: "Proxima Nova",
            ),
          ),
          actions: <Widget>[
            new TextButton(
              child: Text(
                confirmButton,
                style: TextStyle(
                  color: titleColor3,
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontFamily: "Proxima Nova",
                ),
              ),
              onPressed: onConfirmTap,
            ),
            new TextButton(
              child: Text(
                cancelButton,
                style: TextStyle(
                  color: titleColor3,
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  fontFamily: "Proxima Nova",
                ),
              ),
              onPressed: onCancelTap,
            ),
          ],
        ));
  }

  static Widget adminMessageSent(
      {required QBMessage messageObj,
        context,
        required bool isGeneralThread,
        required bool isFromInboxShared,
        required bool isFromReplyMessage}) {
    return messageObj.id == null
        ? Container(
        width: (MediaQuery.of(context).size.width / 2) + 20,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            messageObj.properties!['image_path'] != null &&
                messageObj.properties!['image_path'] != ""
                ? Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    width: 130,
                    height: 130,
                    child: Stack(
                      alignment: Alignment.centerLeft,
                      children: [
                        Positioned.fill(
                            child: Container(
                              width: 130,
                              height: 130,
                              child: Image.file(
                                File(messageObj
                                    .properties!['image_path']!),
                                fit: BoxFit.fill,
                              ),
                            )),
                        Positioned.fill(
                          bottom: -16,
                          right: -16,
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(100),
                                color: Colors.transparent,
                              ),
                              padding: const EdgeInsets.only(
                                  left: 8,
                                  right: 20,
                                  top: 8,
                                  bottom: 20),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  DateFormat('yyyy-MM-dd').format(DateTime
                                      .fromMillisecondsSinceEpoch(
                                      messageObj
                                          .dateSent!)) !=
                                      DateFormat('yyyy-MM-dd')
                                          .format(DateTime.now())
                                      ? Text(
                                      DateFormat('dd-MM-yyyy')
                                          .format(DateTime
                                          .fromMillisecondsSinceEpoch(
                                          messageObj
                                              .dateSent!)),
                                      style: TextStyle(
                                        color: titleColor3,
                                        fontSize: 10,
                                        fontFamily:
                                        "Proxima Nova",
                                        fontWeight:
                                        FontWeight.w700,
                                      ))
                                      : SizedBox(
                                    width: 4,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    DateFormat('hh:mm a').format(
                                        DateTime
                                            .fromMillisecondsSinceEpoch(
                                            messageObj
                                                .dateSent!)),
                                    // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                                    style: TextStyle(
                                      color: titleColor3,
                                      fontSize: 10,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Icon(
                                    Icons.watch_later_outlined,
                                    size: 10,
                                    color: titleColor3,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
                SizedBox(
                  height: 8,
                ),
              ],
            )
                : Container(),
            messageObj.body != " "
                ? Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.black),
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                messageObj.body!,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                            )
                          ]),
                      SizedBox(height: 4),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          // Container(
                          //   width: 16,
                          //   height: 16,
                          //   child: Image.asset(
                          //     "assets/images/message_read_view.png",
                          //     fit: BoxFit.fill,
                          //   ),
                          // ),
                          // SizedBox(width: 4),
                          // Text(
                          //   messageObj.properties!['read'].toString(),
                          //   style: TextStyle(
                          //     color: titleColor3,
                          //     fontSize: 10,
                          //     fontFamily: "Proxima Nova",
                          //     fontWeight: FontWeight.w700,
                          //   ),
                          // ),
                          DateFormat('yyyy-MM-dd').format(DateTime
                              .fromMillisecondsSinceEpoch(
                              messageObj.dateSent!)) !=
                              DateFormat('yyyy-MM-dd')
                                  .format(DateTime.now())
                              ? Text(
                              DateFormat('dd-MM-yyyy').format(
                                  DateTime
                                      .fromMillisecondsSinceEpoch(
                                      messageObj.dateSent!)),
                              style: TextStyle(
                                color: titleColor3,
                                fontSize: 10,
                                fontFamily: "Proxima Nova",
                                fontWeight: FontWeight.w700,
                              ))
                              : Container(),
                          SizedBox(width: 4),
                          Text(
                            DateFormat('hh:mm a').format(
                                DateTime.fromMillisecondsSinceEpoch(
                                    messageObj.dateSent!)),
                            // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                            style: TextStyle(
                              color: titleColor3,
                              fontSize: 10,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Icon(
                            Icons.watch_later_outlined,
                            size: 10,
                            color: titleColor3,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
              ],
            )
                : Container()
          ],
        ))
        : Container(
        width: (MediaQuery.of(context).size.width / 2) + 20,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            messageObj.body != " "
                ? Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // isFromInboxShared ||
                //         messageObj.properties![
                //                 'original_sender_name'] !=
                //             null ||
                //         messageObj.properties![
                //                 'original_sender_name'] !=
                //             ""
                //     ? Container(
                //         height: 24,
                //         width: double.infinity,
                //         decoration: BoxDecoration(
                //           borderRadius: BorderRadius.only(
                //             topLeft: Radius.circular(6),
                //             topRight: Radius.circular(6),
                //             bottomLeft: Radius.circular(0),
                //             bottomRight: Radius.circular(0),
                //           ),
                //           color: Color(0xff0080ff),
                //         ),
                //         padding:
                //             const EdgeInsets.only(left: 8, right: 6),
                //         child: Column(
                //           mainAxisSize: MainAxisSize.min,
                //           mainAxisAlignment: MainAxisAlignment.center,
                //           crossAxisAlignment:
                //               CrossAxisAlignment.start,
                //           children: [
                //             Text(
                //               "Thread : CSK vs RCB",
                //               style: TextStyle(
                //                 color: Colors.white,
                //                 fontSize: 12,
                //                 fontFamily: "Proxima Nova",
                //                 fontWeight: FontWeight.w700,
                //               ),
                //             ),
                //           ],
                //         ),
                //       )
                //     : Container(),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: isFromInboxShared
                        ? BorderRadius.only(
                      topLeft: Radius.circular(0),
                      topRight: Radius.circular(0),
                      bottomLeft: Radius.circular(6),
                      bottomRight: Radius.circular(6),
                    )
                        : BorderRadius.circular(6),
                    color: Color(0xff3a3a3c),
                  ),
                  padding: const EdgeInsets.all(4),
                  child: Column(
                    children: [
                      isFromReplyMessage
                          ? Container(
                        margin: EdgeInsets.only(bottom: 4),
                        child: Row(
                          mainAxisAlignment:
                          MainAxisAlignment.start,
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 4,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(6),
                                  topRight: Radius.circular(0),
                                  bottomLeft:
                                  Radius.circular(6),
                                  bottomRight:
                                  Radius.circular(0),
                                ),
                                color: Color(0xff07cf87),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.only(
                                    topLeft: Radius.circular(0),
                                    topRight:
                                    Radius.circular(6),
                                    bottomLeft:
                                    Radius.circular(0),
                                    bottomRight:
                                    Radius.circular(6),
                                  ),
                                  color: Color(0xff1c1c1e),
                                ),
                                padding:
                                const EdgeInsets.all(8),
                                child: Column(
                                  mainAxisSize:
                                  MainAxisSize.min,
                                  mainAxisAlignment:
                                  MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Arun",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                        fontFamily:
                                        "Proxima Nova",
                                        fontWeight:
                                        FontWeight.w600,
                                      ),
                                    ),
                                    SizedBox(height: 4),
                                    Text(
                                      "If RCB wins the match what would happen for DC",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                          : Container(),
                      Container(
                        padding: EdgeInsets.only(left: 8, right: 8),
                        child: messageObj.senderId ==
                            AppInjector.resolve<
                                AppPreferences>()
                                .getQbUserId() ||
                            messageObj.senderId == null
                            ? Container()
                            : Row(
                            mainAxisAlignment:
                            MainAxisAlignment.start,
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Text(
                                  "Admin",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                  ),
                                ),
                              )
                            ]),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            left: 8, right: 8, top: 8),
                        child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.start,
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Text(
                                  messageObj.body!,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                  ),
                                ),
                              )
                            ]),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            bottom: 8, right: 8, top: 4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment:
                          CrossAxisAlignment.center,
                          children: [
                            // Container(
                            //   width: 16,
                            //   height: 16,
                            //   child: Image.asset(
                            //     "assets/images/message_read_view.png",
                            //     fit: BoxFit.fill,
                            //   ),
                            // ),
                            // SizedBox(width: 4),
                            // Text(
                            //   messageObj.properties!['read'].toString(),
                            //   style: TextStyle(
                            //     color: titleColor3,
                            //     fontSize: 10,
                            //     fontFamily: "Proxima Nova",
                            //     fontWeight: FontWeight.w700,
                            //   ),
                            // ),
                            DateFormat('yyyy-MM-dd').format(DateTime
                                .fromMillisecondsSinceEpoch(
                                messageObj.dateSent!)) !=
                                DateFormat('yyyy-MM-dd')
                                    .format(DateTime.now())
                                ? Text(
                                DateFormat('dd-MM-yyyy').format(
                                    DateTime
                                        .fromMillisecondsSinceEpoch(
                                        messageObj
                                            .dateSent!)),
                                style: TextStyle(
                                  color: titleColor3,
                                  fontSize: 10,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w700,
                                ))
                                : Container(),
                            SizedBox(width: 4),
                            Text(
                              DateFormat('hh:mm a').format(
                                  DateTime.fromMillisecondsSinceEpoch(
                                      messageObj.dateSent!)),
                              // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                              style: TextStyle(
                                color: titleColor3,
                                fontSize: 10,
                                fontFamily: "Proxima Nova",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            SizedBox(width: 4),
                            Icon(
                              Icons.library_add_check_outlined,
                              size: 10,
                              color: titleColor3,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
              ],
            )
                : messageObj.attachments == null
                ? Container()
                : Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                isGeneralThread
                    ? Container()
                    : Container(
                  width: 130,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(6),
                        topRight: Radius.circular(6)),
                    color: Color(0xff3a3a3c),
                  ),
                  padding: const EdgeInsets.only(
                      left: 8, top: 4, bottom: 4),
                  child: Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      crossAxisAlignment:
                      CrossAxisAlignment.center,
                      children: [
                        Expanded(
                            child: Text(
                              "Admin",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ))
                      ]),
                ),
                Container(
                    width: 130,
                    height: 130,
                    child: Stack(
                      alignment: Alignment.centerLeft,
                      children: [
                        Positioned.fill(
                            child: Container(
                              child: ListView.builder(
                                  itemCount:
                                  messageObj.attachments!.length,
                                  physics:
                                  NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemBuilder: (BuildContext context,
                                      imageIndex) {
                                    return FutureBuilder<String>(
                                        future: getQbImageMessage(
                                          id: (messageObj.attachments![
                                          imageIndex] !=
                                              null
                                              ? messageObj
                                              .attachments![
                                          imageIndex]!
                                              .id
                                              : "")!,
                                        ),
                                        builder: (context, snapshot) {
                                          if (snapshot.hasData) {
                                            return Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment
                                                    .start,
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Container(
                                                      width: 130,
                                                      height: 130,
                                                      child:
                                                      CachedNetworkImage(
                                                        imageUrl: snapshot
                                                            .data !=
                                                            ""
                                                            ? snapshot
                                                            .data!
                                                            : "",
                                                        fit: BoxFit
                                                            .fill,
                                                        placeholder: (context,
                                                            url) =>
                                                            Image
                                                                .asset(
                                                              "assets/images/suggested_channel_cover.png",
                                                              fit: BoxFit
                                                                  .fill,
                                                            ),
                                                        errorWidget:
                                                            (context,
                                                            url,
                                                            error) {
                                                          print(
                                                              'errorWidget ${error.toString()} \n$url');
                                                          return Image
                                                              .asset(
                                                            "assets/images/suggested_channel_cover.png",
                                                            fit: BoxFit
                                                                .fill,
                                                          );
                                                        },
                                                      ))
                                                ]);
                                          }
                                          return Container(
                                            width: 130,
                                            height: 130,
                                            child: Image.asset(
                                              "assets/images/suggested_channel_cover.png",
                                              fit: BoxFit.fill,
                                            ),
                                          );
                                        });
                                  }),
                            )),
                        Positioned.fill(
                          bottom: -16,
                          right: -16,
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(100),
                                color: Colors.transparent,
                              ),
                              padding: const EdgeInsets.only(
                                  left: 8,
                                  right: 20,
                                  top: 8,
                                  bottom: 20),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  DateFormat('yyyy-MM-dd').format(
                                      DateTime.fromMillisecondsSinceEpoch(
                                          messageObj
                                              .dateSent!)) !=
                                      DateFormat('yyyy-MM-dd')
                                          .format(
                                          DateTime.now())
                                      ? Text(
                                      DateFormat('dd-MM-yyyy')
                                          .format(DateTime
                                          .fromMillisecondsSinceEpoch(
                                          messageObj
                                              .dateSent!)),
                                      style: TextStyle(
                                        color: titleColor3,
                                        fontSize: 10,
                                        fontFamily:
                                        "Proxima Nova",
                                        fontWeight:
                                        FontWeight.w700,
                                      ))
                                      : SizedBox(
                                    width: 4,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    DateFormat('hh:mm a').format(
                                        DateTime
                                            .fromMillisecondsSinceEpoch(
                                            messageObj
                                                .dateSent!)),
                                    // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                                    style: TextStyle(
                                      color: titleColor3,
                                      fontSize: 10,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  SizedBox(width: 4),
                                  Icon(
                                    Icons
                                        .library_add_check_outlined,
                                    size: 10,
                                    color: titleColor3,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
                SizedBox(
                  height: 8,
                ),
              ],
            ),
          ],
        ));
  }

  static Widget adminInboxConversation(
      {required QBMessage messageObj,
        context,
        required bool isFromReplyMessage}) {
    return messageObj.id == null
        ? Container(
        width: (MediaQuery.of(context).size.width / 2) + 20,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: messageObj.senderId ==
              AppInjector.resolve<AppPreferences>().getQbUserId() ||
              messageObj.senderId == null
              ? CrossAxisAlignment.end
              : CrossAxisAlignment.start,
          children: [
            messageObj.properties!['image_path'] != null &&
                messageObj.properties!['image_path'] != ""
                ? Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    width: 130,
                    height: 130,
                    child: Stack(
                      alignment: Alignment.centerLeft,
                      children: [
                        Positioned.fill(
                            child: Container(
                              width: 130,
                              height: 130,
                              child: Image.file(
                                File(messageObj
                                    .properties!['image_path']!),
                                fit: BoxFit.fill,
                              ),
                            )),
                        Positioned.fill(
                          bottom: -16,
                          right: -16,
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(100),
                                color: Colors.transparent,
                              ),
                              padding: const EdgeInsets.only(
                                  left: 8,
                                  right: 20,
                                  top: 8,
                                  bottom: 20),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  DateFormat('yyyy-MM-dd').format(DateTime
                                      .fromMillisecondsSinceEpoch(
                                      messageObj
                                          .dateSent!)) !=
                                      DateFormat('yyyy-MM-dd')
                                          .format(DateTime.now())
                                      ? Text(
                                      DateFormat('dd-MM-yyyy')
                                          .format(DateTime
                                          .fromMillisecondsSinceEpoch(
                                          messageObj
                                              .dateSent!)),
                                      style: TextStyle(
                                        color: titleColor3,
                                        fontSize: 10,
                                        fontFamily:
                                        "Proxima Nova",
                                        fontWeight:
                                        FontWeight.w700,
                                      ))
                                      : SizedBox(
                                    width: 4,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    DateFormat('hh:mm a').format(
                                        DateTime
                                            .fromMillisecondsSinceEpoch(
                                            messageObj
                                                .dateSent!)),
                                    // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                                    style: TextStyle(
                                      color: titleColor3,
                                      fontSize: 10,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Icon(
                                    Icons.watch_later_outlined,
                                    size: 10,
                                    color: titleColor3,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
                SizedBox(
                  height: 8,
                ),
              ],
            )
                : Container(),
            messageObj.body != " "
                ? Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.black),
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                messageObj.body!,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                            )
                          ]),
                      SizedBox(height: 4),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          // Container(
                          //   width: 16,
                          //   height: 16,
                          //   child: Image.asset(
                          //     "assets/images/message_read_view.png",
                          //     fit: BoxFit.fill,
                          //   ),
                          // ),
                          // SizedBox(width: 4),
                          // Text(
                          //   messageObj.properties!['read'].toString(),
                          //   style: TextStyle(
                          //     color: titleColor3,
                          //     fontSize: 10,
                          //     fontFamily: "Proxima Nova",
                          //     fontWeight: FontWeight.w700,
                          //   ),
                          // ),
                          DateFormat('yyyy-MM-dd').format(DateTime
                              .fromMillisecondsSinceEpoch(
                              messageObj.dateSent!)) !=
                              DateFormat('yyyy-MM-dd')
                                  .format(DateTime.now())
                              ? Text(
                              DateFormat('dd-MM-yyyy').format(
                                  DateTime
                                      .fromMillisecondsSinceEpoch(
                                      messageObj.dateSent!)),
                              style: TextStyle(
                                color: titleColor3,
                                fontSize: 10,
                                fontFamily: "Proxima Nova",
                                fontWeight: FontWeight.w700,
                              ))
                              : Container(),
                          SizedBox(width: 4),
                          Text(
                            DateFormat('hh:mm a').format(
                                DateTime.fromMillisecondsSinceEpoch(
                                    messageObj.dateSent!)),
                            // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                            style: TextStyle(
                              color: titleColor3,
                              fontSize: 10,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Icon(
                            Icons.watch_later_outlined,
                            size: 10,
                            color: titleColor3,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
              ],
            )
                : Container()
          ],
        ))
        : Container(
        width: (MediaQuery.of(context).size.width / 2) + 20,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: (messageObj
              .properties!['original_sender_id'] !=
              null
              ? int.parse(
              messageObj.properties!['original_sender_id']!)
              : 0) ==
              AppInjector.resolve<AppPreferences>().getQbUserId() ||
              messageObj.senderId == null
              ? CrossAxisAlignment.end
              : CrossAxisAlignment.start,
          children: [
            messageObj.body != " "
                ? Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: Color(0xff3a3a3c),
                  ),
                  padding: const EdgeInsets.all(4),
                  child: Column(
                    children: [
                      isFromReplyMessage
                          ? Container(
                        margin: EdgeInsets.only(bottom: 4),
                        child: Row(
                          mainAxisAlignment:
                          MainAxisAlignment.start,
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 4,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(6),
                                  topRight: Radius.circular(0),
                                  bottomLeft:
                                  Radius.circular(6),
                                  bottomRight:
                                  Radius.circular(0),
                                ),
                                color: Color(0xff07cf87),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.only(
                                    topLeft: Radius.circular(0),
                                    topRight:
                                    Radius.circular(6),
                                    bottomLeft:
                                    Radius.circular(0),
                                    bottomRight:
                                    Radius.circular(6),
                                  ),
                                  color: Color(0xff1c1c1e),
                                ),
                                padding:
                                const EdgeInsets.all(8),
                                child: Column(
                                  mainAxisSize:
                                  MainAxisSize.min,
                                  mainAxisAlignment:
                                  MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Arun",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                        fontFamily:
                                        "Proxima Nova",
                                        fontWeight:
                                        FontWeight.w600,
                                      ),
                                    ),
                                    SizedBox(height: 4),
                                    Text(
                                      "If RCB wins the match what would happen for DC",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                          : Container(),
                      Container(
                        padding: EdgeInsets.only(left: 8, right: 8),
                        child: (messageObj.properties![
                        'original_sender_id'] !=
                            null
                            ? int.parse(
                            messageObj.properties![
                            'original_sender_id']!)
                            : 0) ==
                            AppInjector.resolve<AppPreferences>()
                                .getQbUserId()
                            ? Container()
                            : Container(
                            padding: EdgeInsets.only(top: 8),
                            child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Text(
                                      (messageObj.properties![
                                      'original_sender_name'] !=
                                          null
                                          ? messageObj
                                          .properties![
                                      'original_sender_name']!
                                          : messageObj.senderId
                                          .toString()),
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                      ),
                                    ),
                                  )
                                ])),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            left: 8, right: 8, top: 8),
                        child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.start,
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Text(
                                  messageObj.body!,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                  ),
                                ),
                              )
                            ]),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            bottom: 8, right: 8, top: 4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment:
                          CrossAxisAlignment.center,
                          children: [
                            // Container(
                            //   width: 16,
                            //   height: 16,
                            //   child: Image.asset(
                            //     "assets/images/message_read_view.png",
                            //     fit: BoxFit.fill,
                            //   ),
                            // ),
                            // SizedBox(width: 4),
                            // Text(
                            //   messageObj.properties!['read'].toString(),
                            //   style: TextStyle(
                            //     color: titleColor3,
                            //     fontSize: 10,
                            //     fontFamily: "Proxima Nova",
                            //     fontWeight: FontWeight.w700,
                            //   ),
                            // ),
                            DateFormat('yyyy-MM-dd').format(DateTime
                                .fromMillisecondsSinceEpoch(
                                messageObj.dateSent!)) !=
                                DateFormat('yyyy-MM-dd')
                                    .format(DateTime.now())
                                ? Text(
                                DateFormat('dd-MM-yyyy').format(
                                    DateTime
                                        .fromMillisecondsSinceEpoch(
                                        messageObj
                                            .dateSent!)),
                                style: TextStyle(
                                  color: titleColor3,
                                  fontSize: 10,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w700,
                                ))
                                : Container(),
                            SizedBox(width: 4),
                            Text(
                              DateFormat('hh:mm a').format(
                                  DateTime.fromMillisecondsSinceEpoch(
                                      messageObj.dateSent!)),
                              // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                              style: TextStyle(
                                color: titleColor3,
                                fontSize: 10,
                                fontFamily: "Proxima Nova",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            SizedBox(width: 4),
                            Icon(
                              Icons.library_add_check_outlined,
                              size: 10,
                              color: titleColor3,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
              ],
            )
                : messageObj.attachments == null
                ? Container()
                : Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                (messageObj.properties!['original_sender_id'] !=
                    null
                    ? int.parse(
                    messageObj.properties![
                    'original_sender_id']!)
                    : 0) ==
                    AppInjector.resolve<AppPreferences>()
                        .getQbUserId() ||
                    messageObj.senderId == null
                    ? Container()
                    : Container(
                  width: 130,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(6),
                        topRight: Radius.circular(6)),
                    color: Color(0xff3a3a3c),
                  ),
                  padding: const EdgeInsets.only(
                      left: 8, top: 4, bottom: 4),
                  child: Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      crossAxisAlignment:
                      CrossAxisAlignment.center,
                      children: [
                        Expanded(
                            child: Text(
                              messageObj.properties![
                              'original_sender_name'] !=
                                  null
                                  ? messageObj.properties![
                              'original_sender_name']!
                                  : "----|-----",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ))
                      ]),
                ),
                Container(
                    width: 130,
                    height: 130,
                    child: Stack(
                      alignment: Alignment.centerLeft,
                      children: [
                        Positioned.fill(
                            child: Container(
                              child: ListView.builder(
                                  itemCount:
                                  messageObj.attachments!.length,
                                  physics:
                                  NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemBuilder: (BuildContext context,
                                      imageIndex) {
                                    return FutureBuilder<String>(
                                        future: getQbImageMessage(
                                          id: (messageObj.attachments![
                                          imageIndex] !=
                                              null
                                              ? messageObj
                                              .attachments![
                                          imageIndex]!
                                              .id
                                              : "")!,
                                        ),
                                        builder: (context, snapshot) {
                                          if (snapshot.hasData) {
                                            return Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment
                                                    .start,
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Container(
                                                      width: 130,
                                                      height: 130,
                                                      child:
                                                      CachedNetworkImage(
                                                        imageUrl: snapshot
                                                            .data !=
                                                            ""
                                                            ? snapshot
                                                            .data!
                                                            : "",
                                                        fit: BoxFit
                                                            .fill,
                                                        placeholder: (context,
                                                            url) =>
                                                            Image
                                                                .asset(
                                                              "assets/images/suggested_channel_cover.png",
                                                              fit: BoxFit
                                                                  .fill,
                                                            ),
                                                        errorWidget:
                                                            (context,
                                                            url,
                                                            error) {
                                                          print(
                                                              'errorWidget ${error.toString()} \n$url');
                                                          return Image
                                                              .asset(
                                                            "assets/images/suggested_channel_cover.png",
                                                            fit: BoxFit
                                                                .fill,
                                                          );
                                                        },
                                                      ))
                                                ]);
                                          }
                                          return Container(
                                            width: 130,
                                            height: 130,
                                            child: Image.asset(
                                              "assets/images/suggested_channel_cover.png",
                                              fit: BoxFit.fill,
                                            ),
                                          );
                                        });
                                  }),
                            )),
                        Positioned.fill(
                          bottom: -16,
                          right: -16,
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(100),
                                color: Colors.transparent,
                              ),
                              padding: const EdgeInsets.only(
                                  left: 8,
                                  right: 20,
                                  top: 8,
                                  bottom: 20),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  DateFormat('yyyy-MM-dd').format(
                                      DateTime.fromMillisecondsSinceEpoch(
                                          messageObj
                                              .dateSent!)) !=
                                      DateFormat('yyyy-MM-dd')
                                          .format(
                                          DateTime.now())
                                      ? Text(
                                      DateFormat('dd-MM-yyyy')
                                          .format(DateTime
                                          .fromMillisecondsSinceEpoch(
                                          messageObj
                                              .dateSent!)),
                                      style: TextStyle(
                                        color: titleColor3,
                                        fontSize: 10,
                                        fontFamily:
                                        "Proxima Nova",
                                        fontWeight:
                                        FontWeight.w700,
                                      ))
                                      : SizedBox(
                                    width: 4,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    DateFormat('hh:mm a').format(
                                        DateTime
                                            .fromMillisecondsSinceEpoch(
                                            messageObj
                                                .dateSent!)),
                                    // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                                    style: TextStyle(
                                      color: titleColor3,
                                      fontSize: 10,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  SizedBox(width: 4),
                                  Icon(
                                    Icons
                                        .library_add_check_outlined,
                                    size: 10,
                                    color: titleColor3,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
                SizedBox(
                  height: 8,
                ),
              ],
            ),
          ],
        ));
  }

  static Widget messageDetailsConversation(
      {required QBMessage messageObj,
        context,
        required bool isFromGroup,
        required bool isFromReplyMessage}) {
    return messageObj.id == null
        ? Container(
        width: (MediaQuery.of(context).size.width / 2) + 20,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: messageObj.senderId ==
              AppInjector.resolve<AppPreferences>().getQbUserId() ||
              messageObj.senderId == null
              ? CrossAxisAlignment.end
              : CrossAxisAlignment.start,
          children: [
            messageObj.properties!['image_path'] != null &&
                messageObj.properties!['image_path'] != ""
                ? Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    width: 130,
                    height: 130,
                    child: Stack(
                      alignment: Alignment.centerLeft,
                      children: [
                        Positioned.fill(
                            child: Container(
                              width: 130,
                              height: 130,
                              child: Image.file(
                                File(messageObj
                                    .properties!['image_path']!),
                                fit: BoxFit.fill,
                              ),
                            )),
                        Positioned.fill(
                          bottom: -16,
                          right: -16,
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(100),
                                color: Colors.transparent,
                              ),
                              padding: const EdgeInsets.only(
                                  left: 8,
                                  right: 20,
                                  top: 8,
                                  bottom: 20),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  DateFormat('yyyy-MM-dd').format(DateTime
                                      .fromMillisecondsSinceEpoch(
                                      messageObj
                                          .dateSent!)) !=
                                      DateFormat('yyyy-MM-dd')
                                          .format(DateTime.now())
                                      ? Text(
                                      DateFormat('dd-MM-yyyy')
                                          .format(DateTime
                                          .fromMillisecondsSinceEpoch(
                                          messageObj
                                              .dateSent!)),
                                      style: TextStyle(
                                        color: titleColor3,
                                        fontSize: 10,
                                        fontFamily:
                                        "Proxima Nova",
                                        fontWeight:
                                        FontWeight.w700,
                                      ))
                                      : SizedBox(
                                    width: 4,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    DateFormat('hh:mm a').format(
                                        DateTime
                                            .fromMillisecondsSinceEpoch(
                                            messageObj
                                                .dateSent!)),
                                    // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                                    style: TextStyle(
                                      color: titleColor3,
                                      fontSize: 10,
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Icon(
                                    Icons.watch_later_outlined,
                                    size: 10,
                                    color: titleColor3,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
                SizedBox(
                  height: 8,
                ),
              ],
            )
                : Container(),
            messageObj.body != " "
                ? Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.black),
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                messageObj.body!,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                            )
                          ]),
                      SizedBox(height: 4),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          // Container(
                          //   width: 16,
                          //   height: 16,
                          //   child: Image.asset(
                          //     "assets/images/message_read_view.png",
                          //     fit: BoxFit.fill,
                          //   ),
                          // ),
                          // SizedBox(width: 4),
                          // Text(
                          //   messageObj.properties!['read'].toString(),
                          //   style: TextStyle(
                          //     color: titleColor3,
                          //     fontSize: 10,
                          //     fontFamily: "Proxima Nova",
                          //     fontWeight: FontWeight.w700,
                          //   ),
                          // ),
                          DateFormat('yyyy-MM-dd').format(DateTime
                              .fromMillisecondsSinceEpoch(
                              messageObj.dateSent!)) !=
                              DateFormat('yyyy-MM-dd')
                                  .format(DateTime.now())
                              ? Text(
                              DateFormat('dd-MM-yyyy').format(
                                  DateTime
                                      .fromMillisecondsSinceEpoch(
                                      messageObj.dateSent!)),
                              style: TextStyle(
                                color: titleColor3,
                                fontSize: 10,
                                fontFamily: "Proxima Nova",
                                fontWeight: FontWeight.w700,
                              ))
                              : Container(),
                          SizedBox(width: 4),
                          Text(
                            DateFormat('hh:mm a').format(
                                DateTime.fromMillisecondsSinceEpoch(
                                    messageObj.dateSent!)),
                            // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                            style: TextStyle(
                              color: titleColor3,
                              fontSize: 10,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Icon(
                            Icons.watch_later_outlined,
                            size: 10,
                            color: titleColor3,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
              ],
            )
                : Container()
          ],
        ))
        : Container(
        width: (MediaQuery.of(context).size.width / 2) + 20,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: messageObj.senderId ==
              AppInjector.resolve<AppPreferences>().getQbUserId() ||
              messageObj.senderId == null
              ? CrossAxisAlignment.end
              : CrossAxisAlignment.start,
          children: [
            messageObj.body != " "
                ? Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Color(0xff3a3a3c),
              ),
              padding: const EdgeInsets.all(4),
              margin: const EdgeInsets.only(bottom: 8),
              child: Column(
                children: [
                  isFromReplyMessage
                      ? Container(
                    margin: EdgeInsets.only(bottom: 4),
                    child: Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      crossAxisAlignment:
                      CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 4,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(6),
                              topRight: Radius.circular(0),
                              bottomLeft: Radius.circular(6),
                              bottomRight: Radius.circular(0),
                            ),
                            color: Color(0xff07cf87),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(0),
                                topRight: Radius.circular(6),
                                bottomLeft: Radius.circular(0),
                                bottomRight: Radius.circular(6),
                              ),
                              color: Color(0xff1c1c1e),
                            ),
                            padding: const EdgeInsets.all(8),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Arun",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontFamily: "Proxima Nova",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                SizedBox(height: 4),
                                Text(
                                  "If RCB wins the match what would happen for DC",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                      : Container(),
                  isFromGroup
                      ? Container(
                    padding: EdgeInsets.only(left: 8, right: 8),
                    child: messageObj.senderId ==
                        AppInjector.resolve<
                            AppPreferences>()
                            .getQbUserId() ||
                        messageObj.senderId == null
                        ? Container()
                        : Row(
                        mainAxisAlignment:
                        MainAxisAlignment.start,
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              messageObj.properties![
                              'original_sender_name'] ==
                                  null ||
                                  messageObj.properties![
                                  'original_sender_name'] ==
                                      ""
                                  ? "-----|-----"
                                  : messageObj
                                  .properties![
                              'original_sender_name']!,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          )
                        ]),
                  )
                      : Container(),
                  Container(
                    padding:
                    EdgeInsets.only(left: 8, right: 8, top: 8),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              messageObj.body!,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          )
                        ]),
                  ),
                  Container(
                    padding:
                    EdgeInsets.only(bottom: 8, right: 8, top: 4),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        DateFormat('yyyy-MM-dd').format(DateTime
                            .fromMillisecondsSinceEpoch(
                            messageObj.dateSent!)) !=
                            DateFormat('yyyy-MM-dd')
                                .format(DateTime.now())
                            ? Text(
                            DateFormat('dd-MM-yyyy').format(
                                DateTime
                                    .fromMillisecondsSinceEpoch(
                                    messageObj.dateSent!)),
                            style: TextStyle(
                              color: titleColor3,
                              fontSize: 10,
                              fontFamily: "Proxima Nova",
                              fontWeight: FontWeight.w700,
                            ))
                            : Container(),
                        SizedBox(width: 4),
                        Text(
                          DateFormat('hh:mm a').format(
                              DateTime.fromMillisecondsSinceEpoch(
                                  messageObj.dateSent!)),
                          // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                          style: TextStyle(
                            color: titleColor3,
                            fontSize: 10,
                            fontFamily: "Proxima Nova",
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(width: 4),
                        Icon(
                          Icons.library_add_check_outlined,
                          size: 10,
                          color: titleColor3,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )
                : messageObj.attachments == null
                ? Container()
                : Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: messageObj.senderId ==
                    AppInjector.resolve<AppPreferences>()
                        .getQbUserId() ||
                    messageObj.senderId == null
                    ? CrossAxisAlignment.end
                    : CrossAxisAlignment.start,
                children: [
                  !isFromGroup
                      ? Container()
                      : messageObj.senderId ==
                      AppInjector.resolve<
                          AppPreferences>()
                          .getQbUserId() ||
                      messageObj.senderId == null
                      ? Container()
                      : Container(
                    width: 130,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(6),
                          topRight: Radius.circular(6)),
                      color: Color(0xff3a3a3c),
                    ),
                    padding: const EdgeInsets.only(
                        left: 8, top: 4, bottom: 4),
                    child: Row(
                        mainAxisAlignment:
                        MainAxisAlignment.start,
                        crossAxisAlignment:
                        CrossAxisAlignment.center,
                        children: [
                          Expanded(
                              child: Text(
                                messageObj.properties![
                                'original_sender_name'] !=
                                    null
                                    ? messageObj
                                    .properties![
                                'original_sender_name']!
                                    : "----|-----",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ))
                        ]),
                  ),
                  Container(
                      width: 130,
                      height: 130,
                      child: Stack(
                        alignment: Alignment.centerLeft,
                        children: [
                          Positioned.fill(
                              child: Container(
                                child: ListView.builder(
                                    itemCount: messageObj
                                        .attachments!.length,
                                    physics:
                                    NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemBuilder:
                                        (BuildContext context,
                                        imageIndex) {
                                      return FutureBuilder<String>(
                                          future: getQbImageMessage(
                                            id: (messageObj.attachments![
                                            imageIndex] !=
                                                null
                                                ? messageObj
                                                .attachments![
                                            imageIndex]!
                                                .id
                                                : "")!,
                                          ),
                                          builder:
                                              (context, snapshot) {
                                            if (snapshot.hasData) {
                                              return Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .start,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment
                                                      .start,
                                                  children: [
                                                    Container(
                                                        width: 130,
                                                        height: 130,
                                                        child:
                                                        CachedNetworkImage(
                                                          imageUrl: snapshot.data !=
                                                              ""
                                                              ? snapshot
                                                              .data!
                                                              : "",
                                                          fit: BoxFit
                                                              .fill,
                                                          placeholder:
                                                              (context,
                                                              url) =>
                                                              Image.asset(
                                                                "assets/images/suggested_channel_cover.png",
                                                                fit: BoxFit
                                                                    .fill,
                                                              ),
                                                          errorWidget:
                                                              (context,
                                                              url,
                                                              error) {
                                                            print(
                                                                'errorWidget ${error.toString()} \n$url');
                                                            return Image
                                                                .asset(
                                                              "assets/images/suggested_channel_cover.png",
                                                              fit: BoxFit
                                                                  .fill,
                                                            );
                                                          },
                                                        ))
                                                  ]);
                                            }
                                            return Container(
                                              width: 130,
                                              height: 130,
                                              child: Image.asset(
                                                "assets/images/suggested_channel_cover.png",
                                                fit: BoxFit.fill,
                                              ),
                                            );
                                          });
                                    }),
                              )),
                          Positioned.fill(
                            bottom: -16,
                            right: -16,
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.circular(100),
                                  color: Colors.transparent,
                                ),
                                padding: const EdgeInsets.only(
                                    left: 8,
                                    right: 20,
                                    top: 8,
                                    bottom: 20),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment:
                                  MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    DateFormat('yyyy-MM-dd')
                                        .format(DateTime
                                        .fromMillisecondsSinceEpoch(
                                        messageObj
                                            .dateSent!)) !=
                                        DateFormat(
                                            'yyyy-MM-dd')
                                            .format(DateTime
                                            .now())
                                        ? Text(
                                        DateFormat(
                                            'dd-MM-yyyy')
                                            .format(DateTime
                                            .fromMillisecondsSinceEpoch(
                                            messageObj
                                                .dateSent!)),
                                        style: TextStyle(
                                          color: titleColor3,
                                          fontSize: 10,
                                          fontFamily:
                                          "Proxima Nova",
                                          fontWeight:
                                          FontWeight.w700,
                                        ))
                                        : SizedBox(
                                      width: 4,
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Text(
                                      DateFormat('hh:mm a')
                                          .format(DateTime
                                          .fromMillisecondsSinceEpoch(
                                          messageObj
                                              .dateSent!)),
                                      // ("${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).hour}: ${DateTime.parse(DateTime.fromMicrosecondsSinceEpoch(messageObj.dateSent!).toLocal().toString()).minute} PM"),
                                      style: TextStyle(
                                        color: titleColor3,
                                        fontSize: 10,
                                        fontFamily:
                                        "Proxima Nova",
                                        fontWeight:
                                        FontWeight.w700,
                                      ),
                                    ),
                                    SizedBox(width: 4),
                                    Icon(
                                      Icons
                                          .library_add_check_outlined,
                                      size: 10,
                                      color: titleColor3,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      )),
                  SizedBox(
                    height: 8,
                  )
                ]),
          ],
        ));
  }

  static Widget getMyFriendsCard(
      {required leadingIcon, required title, required onTap}) {
    return InkWell(
        focusColor: Colors.transparent,
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: onTap,
        child: Container(
          margin: EdgeInsets.only(top: 8, bottom: 8),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 24,
                height: 24,
                child: Image.asset(
                  leadingIcon,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(width: 16),
              Expanded(
                child: Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontFamily: "Proxima Nova",
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(width: 16),
              Container(
                width: 24,
                height: 24,
                child: Image.asset(
                  "assets/images/chevron_right_icon.png",
                  fit: BoxFit.fill,
                ),
              ),
            ],
          ),
        ));
  }

  static Widget getInfoCard(mainTitle, subTitle, onTap, context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ClipPath(
          clipper: TriangleClipper(),
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 10, 0,
                0), //extra 10 for top padding because triangle's height = 10
            decoration: BoxDecoration(
              color: Color(0xff3a3a3c),
              border: Border.all(
                color: AppWidgets.titleColor5,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(10),
            ),

            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    child: Container(
                        margin: EdgeInsets.all(12),
                        child: Column(
                          children: [
                            SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Text(
                                mainTitle,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                            SizedBox(height: 4),
                            SizedBox(
                              width: MediaQuery.of(context).size.width - 20,
                              child: Text(
                                subTitle,
                                style: TextStyle(
                                  color: Color(0xffa4a4ab),
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ],
                        ))),
                InkWell(
                  focusColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  onTap: onTap,
                  child: Container(
                    margin: EdgeInsets.all(12),
                    width: 16,
                    height: 16,
                    child: Image.asset(
                      "assets/images/cancel_icon.png",
                      fit: BoxFit.fill,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

String capitalize(String text) {
  return "${text[0].toUpperCase()}${text.substring(1)}";
}

Future<String>? getQbImageMessage({required String id}) async {
  String? url = "";
  try {
    QBFile? file = await QB.content.getInfo(int.parse(id));
    if (file != null) {
      String? uid = file.uid;
      url = await getUrl(uid);
    }
  } catch (e) {
    print("Error info $e");
  }
  return url!;
}

Future<String> getUrl(uid) async {
  try {
    String? url = await QB.content.getPublicURL(uid!);
    print("url got is $url");
    return url!;
  } catch (e) {
    print("Error got $e");
    return "";
  }
}

Future<QBAttachment> uploadImageToContentQB(_profileImageFile) async {
  try {
    QBFile? file = await QB.content.upload(_profileImageFile, public: true);
    if (file != null) {
      int? id = file.id;
      String? contentType = file.contentType;

      QBAttachment attachment = QBAttachment();

      attachment.id = id.toString();
      attachment.contentType = contentType;
      attachment.type = "PHOTO";

      return attachment;
    } else {
      return QBAttachment();
    }
  } catch (error) {
    print("------------Error is $error");
    return QBAttachment();
  }
}

class TriangleClipper extends CustomClipper<Path> {
  double radius = 10, tw = 20, th = 10; //tw & th = triangle width & height

  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, th + radius);
    path.arcToPoint(Offset(size.width - radius, th),
        radius: Radius.circular(radius), clockwise: false);
    path.lineTo(radius + 10 + tw, th);
    path.lineTo(
      0,
      radius + 10 + tw / 2,
    ); //in these lines, the 10 is to have a space of 10 between the top-left corner curve and the triangle
    path.lineTo(radius + 10, th);
    path.lineTo(radius, th);
    path.arcToPoint(Offset(0, th + radius),
        radius: Radius.circular(radius), clockwise: false);
    return path;
  }

  @override
  bool shouldReclip(TriangleClipper oldClipper) => false;
}
