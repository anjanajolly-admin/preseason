import 'package:flutter/material.dart';

class AppConstants {
  static const ddMMyyyy = "dd/MM/yyyy";
  static const ddMMyyyyHHmmaa = "dd/MM/yyyy, hh:mm aa";
  static const ddMMyyyyHHmmss = "dd-MM-yyyy HH:mm:ss";
  static const ddMMyyyy2 = "dd-MM-yyyy";
  static const hhmmaa = "hh:mm aa";

  static List<String> interestTypes = [
    "Cricket",
    "Football",
    "Tennis",
    "Golf",
    "Badminton",
    "Table Tennis",
    "Field Hockey",
    "Baseball",
    "Swimming",
    "Wrestling",
    "Cycling",
    "Shooting",
    "Fencing",
    "Weightlifting"
  ];

  static hideKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }
}
