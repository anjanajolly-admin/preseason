import 'package:equatable/equatable.dart';

class AppError extends Equatable {
  final Exception error;
  AppError({required this.error});
  @override
  List<Object> get props => [error];
}
