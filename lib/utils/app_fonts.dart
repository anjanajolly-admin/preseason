class AppFonts {
  static const siemensSansBold = 'Sans_Bold';
  static const siemensSansRoman = 'Sans_Roman';
  static const siemensSHBreeHeadline = 'SHBree_Headline';
}
