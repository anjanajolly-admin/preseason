import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_cubit.dart';
import 'package:pre_seasoned/screens/channel_settings_screens/channel_info_screens/channel_info_screen.dart';
import 'package:pre_seasoned/screens/channel_settings_screens/channel_info_screens/channel_members_screen.dart';
import 'package:pre_seasoned/screens/channel_settings_screens/channel_setting_screen.dart';
import 'package:pre_seasoned/screens/channel_settings_screens/role_management_screen/add_role_screen.dart';
import 'package:pre_seasoned/screens/channel_settings_screens/role_management_screen/edit_role_screen.dart';
import 'package:pre_seasoned/screens/channel_settings_screens/role_management_screen/role_management_screen.dart';
import 'package:pre_seasoned/screens/create_channel_screens/channel_details_screen.dart';
import 'package:pre_seasoned/screens/create_channel_screens/create_channel_screen.dart';
import 'package:pre_seasoned/screens/create_channel_screens/invite_member_screen.dart';
import 'package:pre_seasoned/screens/create_thread_screens/create_thread_screen.dart';
import 'package:pre_seasoned/screens/create_thread_screens/create_thread_video_screen.dart';
import 'package:pre_seasoned/screens/email_verification_screen/email_verification_screen.dart';
import 'package:pre_seasoned/screens/forgot_password_screen/forgot_password_screen.dart';
import 'package:pre_seasoned/screens/illustration_screen/animation_screen.dart';
import 'package:pre_seasoned/screens/interest_screen/interest_screen.dart';
import 'package:pre_seasoned/screens/login_screen/login_screen.dart';
import 'package:pre_seasoned/screens/main_container/main_container_screen.dart';
import 'package:pre_seasoned/screens/notifications_screen/notifications_screen.dart';
import 'package:pre_seasoned/screens/signup_screen/signup_screen.dart';
import 'package:pre_seasoned/screens/suggest_channels_screen/suggested_channel_illustration_screen.dart';
import 'package:pre_seasoned/screens/suggest_channels_screen/suggested_channels_screen.dart';
import 'package:pre_seasoned/screens/tab_explore_screen/explore_screen.dart';
import 'package:pre_seasoned/screens/tab_explore_screen/explore_search_screen.dart';
import 'package:pre_seasoned/screens/tab_home_screen/home_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/create_messages_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/group_messages_screens/create_group_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/group_messages_screens/edit_group_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/group_messages_screens/group_info_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/group_messages_screens/group_message_details_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/group_messages_screens/select_members_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/messages_details_screen.dart';
import 'package:pre_seasoned/screens/tab_messages_screens/messages_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/edit_profile_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/friends_screen/add_friends_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/friends_screen/find_friends_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/friends_screen/friend_profile_view_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/friends_screen/my_friends_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/friends_screen/pending_invitation_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/manage_preferences_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/profile_screen.dart';
import 'package:pre_seasoned/screens/tab_profile_screens/reset_password_screen.dart';
import 'package:pre_seasoned/screens/thread_screens/general_thread_screen.dart';
import 'package:pre_seasoned/screens/thread_screens/non_general_thread_screen.dart';
import 'package:pre_seasoned/screens/thread_screens/non_general_thread_user_screen.dart';
import 'package:pre_seasoned/screens/thread_screens/thread_inbox_screen.dart';
import 'package:pre_seasoned/screens/upload_profilepicture_screen.dart/upload_profile_picture_screen.dart';
import 'package:pre_seasoned/screens/username_screen/username_details_screen.dart';
import 'package:pre_seasoned/screens/username_screen/username_screen.dart';
import 'package:pre_seasoned/screens/welcome_screen/welcome_screen.dart';

import 'app_injector.dart';

class AppRoutes {
  var navigatorKey = GlobalKey<NavigatorState>();

  CupertinoPageRoute invalidRoute(String? name) {
    return CupertinoPageRoute(
        settings: RouteSettings(name: name),
        builder: (_) => Scaffold(
              body: Center(
                key: ValueKey('invalid_screen'),
                child: Text('No route defined for $name'),
              ),
            ));
  }

  Route<dynamic> obtainRoute(RouteSettings setting) {
    switch (setting.name) {
      case WelcomeScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: WelcomeScreen()));

      case MainContainerScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: MainContainerScreen()));

      case SignUpScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: SignUpScreen()));

      case LoginScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: LoginScreen()));

      case HomeScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: HomeScreen(
                      isFromBottom: list[0],
                      managedChannels: list[1],
                      followedChannels: list[2],
                      unreadMessageCount: list[3],
                    )));
      case UsernameDetailsScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: UsernameDetailsScreen(
                      emailId: list[0],
                      password: list[1],
                    )));
      case UploadProfilePictureScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: UploadProfilePictureScreen(
                      emailId: list[0],
                      password: list[1],
                      firstName: list[2],
                      lastName: list[3],
                    )));
      case UsernameScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: UsernameScreen(
                      emailId: list[0],
                      password: list[1],
                      firstName: list[2],
                      lastName: list[3],
                      profilePhoto: list[4],
                    )));
      case ForgotPasswordScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: ForgotPasswordScreen()));
      case EmailVerificationScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: EmailVerificationScreen()));
      case InterestScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: InterestScreen()));
      case SuggestedChannelIllustrationScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: SuggestedChannelIllustrationScreen()));
      case SuggestedChannelsScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: SuggestedChannelsScreen()));

      case CreateChannelScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
          settings: setting,
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<BaseCubit>.value(
                  value: AppInjector.resolve<BaseCubit>()),
            ],
            child: CreateChannelScreen(
              editChannelResponse: list[0],
            ),
          ),
        );
      case ChannelDetailsScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
          settings: setting,
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<BaseCubit>.value(
                  value: AppInjector.resolve<BaseCubit>()),
            ],
            child: ChannelDetailsScreen(
              createChannelId: list[0],
              isFromCreateChannel: list[1],
              profilePhoto: list[2] == null ? null : list[2] as String,
              coverPhoto: list[3] == null ? null : list[3] as String,
              isChannelManaged: list[4],
              isFromExplore: list[5],
            ),
          ),
        );
      case InviteMemberScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
          settings: setting,
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<BaseCubit>.value(
                  value: AppInjector.resolve<BaseCubit>()),
            ],
            child: InviteMemberScreen(
              inviteCode: list[0],
            ),
          ),
        );
      case ChannelSettingsScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: ChannelSettingsScreen(
                      channelId: list[0],
                    )));
      case ChannelInfoScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: ChannelInfoScreen(
                      channelId: list[0],
                    )));

      case ChannelMembersScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: ChannelMembersScreen()));
      case RoleManagementScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: RoleManagementScreen()));
      case AddRoleManagement.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: AddRoleManagement()));
      case EditRoleManagement.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: EditRoleManagement()));
      case GeneralThreadScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: GeneralThreadScreen(
                      threadDetails: list[0],
                      isChannelManaged: list[1],
                    )));
      case NonGeneralThreadScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: NonGeneralThreadScreen(
                      threadDetails: list[0],
                      generalDialogId: list[1],
                      newThread: list[2],
                    )));
      case NonGeneralThreadUserScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: NonGeneralThreadUserScreen(
                      threadDetails: list[0],
                    )));
      case InboxThreadScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: InboxThreadScreen(
                      threadDetails: list[0],
                      generalDialogId: list[1],
                      isNewThread: list[2],
                    )));
      case CreateThreadVideoScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: CreateThreadVideoScreen(channelId: list[0])));

      case CreateThreadScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: CreateThreadScreen(
                      channelId: list[0],
                      topicId: list[1],
                      topicName: list[2],
                    )));

      case ExploreChannels.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: ExploreChannels(
                      isFromBottom: list[0],
                      joinExploreIndex: list[1],
                      exploreChannelsList: list[3],
                    )));

      case ExploreSearchScreen.routeName:
        var list = setting.arguments;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: ExploreSearchScreen(allExploreChannel: list)));

      case ProfileScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: ProfileScreen()));

      case EditProfileScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: EditProfileScreen(userDetails: list[0])));

      case ManagedPreferencesScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: ManagedPreferencesScreen(interestsList: list[0])));

      case ResetPasswordScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: ResetPasswordScreen()));

      case MyFriendsScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: MyFriendsScreen()));

      case AddFriendsScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: AddFriendsScreen()));

      case PendingInvitationScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: PendingInvitationScreen()));
      case FriendProfileViewScreen.routeName:
        List list = setting.arguments as List;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: FriendProfileViewScreen(
                      friendDetails: list[0],
                    )));

      case MessagesScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: MessagesScreen()));

      case MessageDetailsScreen.routeName:
        List list = setting.arguments as List;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: MessageDetailsScreen(
                      messageDetails: list[0],
                    )));
      case CreateMessageScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: CreateMessageScreen()));

      case SelectMembersScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: SelectMembersScreen(
                      allFriends: list[0],
                      dialogId: list[1],
                    )));

      case CreateGroupScreen.routeName:
        var list = setting.arguments as List<dynamic>;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: CreateGroupScreen(selectedMembers: list[0])));

      case GroupMessageDetailsScreen.routeName:
        var list = setting.arguments as List;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: GroupMessageDetailsScreen(
                      groupDetails: list[0],
                    )));
      case GroupInfoScreen.routeName:
        var list = setting.arguments as List;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: GroupInfoScreen(
                      dialogId: list[0],
                    )));
      case EditGroupScreen.routeName:
        var list = setting.arguments as List;
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(
                    providers: [
                      BlocProvider<BaseCubit>.value(
                          value: AppInjector.resolve<BaseCubit>()),
                    ],
                    child: EditGroupScreen(
                      groupDetails: list[0],
                      groupDescriptionDetails: list[1],
                    )));
      case NotificationScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: NotificationScreen()));
      case AnimationScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: AnimationScreen()));
      case FindFriendsScreen.routeName:
        return MaterialPageRoute(
            settings: setting,
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider<BaseCubit>.value(
                      value: AppInjector.resolve<BaseCubit>()),
                ], child: FindFriendsScreen()));

      default:
        return invalidRoute(setting.name);
    }
  }
}
