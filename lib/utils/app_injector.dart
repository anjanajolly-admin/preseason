import 'package:dio/dio.dart';
import 'package:kiwi/kiwi.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_cubit.dart';
import 'package:pre_seasoned/cubits/base_cubit/base_state.dart';
import 'package:pre_seasoned/database/app_prefereneces.dart';
import 'package:pre_seasoned/network/api/api_service.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';
import 'package:pre_seasoned/network/client.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_routes.dart';

class AppInjector {
  /// Singleton class instance provided by the [kiwi] library.
  static KiwiContainer container = KiwiContainer();

  /// Configures the Injector.
  static setUp() async {
    await _configure();
  }

  /// Provides the instances of registered classes from the singleton class [Container].
  static final resolve = container.resolve;

  /// Clears all the instances of registered classes from the singleton class [Container].
  static clear() => container.clear();

  /// Stores the instances of the registered classes in to the singleton class [Container]
  /// provided by the [kiwi] library.
  static _configure() async {
    container.registerSingleton((container) => AppRoutes());
    container.registerSingleton((c) => PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: true,
          compact: false,
        ));

    container.registerSingleton((c) => Dio());
    container.registerSingleton((c) => AppNetworkClient());
    final prefsInstance = await SharedPreferences.getInstance();
    container.registerSingleton((c) => prefsInstance);

    container.registerFactory((container) => BaseCubit(LoginInitialState()));
    container.registerSingleton((c) => AppPreferences());
    container.registerFactory((container) => ApiService());
    container.registerFactory((container) => QuickbloxApiService());
  }
}
