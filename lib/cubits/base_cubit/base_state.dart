import 'package:equatable/equatable.dart';

abstract class BaseState extends Equatable {}

class LoginSuccessState extends BaseState {
  final bool isLogin;

  LoginSuccessState(this.isLogin);
  @override
  List<Object> get props => [isLogin];
}

class LoginFailureState extends BaseState {
  final String failureMsg;

  LoginFailureState(this.failureMsg);
  @override
  List<Object> get props => [failureMsg];
}

class LoginVerifyState extends BaseState {
  @override
  List<Object> get props => [];
}

class LogOutSuccessState extends BaseState {
  @override
  List<Object> get props => [];
}

class LoadingState extends BaseState {
  @override
  List<Object> get props => [];
}

class LoginInterestState extends BaseState {
  LoginInterestState();
  @override
  List<Object> get props => [];
}

class LoginSuggestedState extends BaseState {
  LoginSuggestedState();
  @override
  List<Object> get props => [];
}

class IsLoggedInSuccessState extends BaseState {
  @override
  List<Object> get props => [];
}

class ShowMessageState extends BaseState {
  final String errorMsg;

  ShowMessageState(this.errorMsg);

  @override
  List<Object> get props => [errorMsg];
}

class LoginInitialState extends BaseState {
  @override
  List<Object> get props => [];
}

class PasswordChangeConfirmState extends BaseState {
  @override
  List<Object> get props => [];
}

class PasswordChangeFailureState extends BaseState {
  final String failureMsg;

  PasswordChangeFailureState(this.failureMsg);
  @override
  List<Object> get props => [failureMsg];
}

class EmailAvailable extends BaseState {
  @override
  List<Object> get props => [];
}

class EmailUnAvailable extends BaseState {
  @override
  List<Object> get props => [];
}
