import 'dart:convert' as convert;
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pre_seasoned/network/api/quickblox_api_response.dart';
import 'package:pre_seasoned/network/api/quickblox_api_service.dart';

import '/database/app_prefereneces.dart';
import '/network/api/api_response.dart';
import '/network/api/api_service.dart';
import '/utils/app_injector.dart';
import 'base_state.dart';

class BaseCubit extends Cubit<BaseState> {
  BaseCubit(BaseState initialState) : super(initialState);
  AppPreferences preferences = AppInjector.resolve<AppPreferences>();
  ApiService service = AppInjector.resolve<ApiService>();
  // late Mixpanel mixpanel;

  Future<List> emailSignIn(
      emailId,
      password,
      firstName,
      lastName,
      userName,
      profilePhoto,
      ) async {
    emit(LoadingState());

    FormData formData = new FormData.fromMap({
      "data": json.encode({
        "username": userName,
        "email": emailId,
        "password": password,
        "firstName": firstName,
        "lastName": lastName,
      }),
      'files.profilePhoto': (profilePhoto == null)
          ? null
          : await MultipartFile.fromFile(
        profilePhoto.path,
        filename: profilePhoto.path.split('/').last,
      )
    });
    var apiResponse = await service.registerUser(formData);

    if (apiResponse.status == ApiResponseStatus.completed) {
      var jsonSigninResponse =
      convert.jsonDecode(apiResponse.data) as Map<String, dynamic>;
      print("SignIn Response ===> ${apiResponse.data}");
      preferences.setUserId(jsonSigninResponse['user']["id"]);
      preferences.setUserName(jsonSigninResponse['user']["username"]);
      preferences.setEmail(jsonSigninResponse['user']["email"]);
      print(jsonSigninResponse);

      return [true, ""];
    } else {
      return [false, apiResponse.message[1]];
    }
  }

  void emailLogin(emailId, password) async {
    emit(LoadingState());

    var apiResponse = await service.logInUser({
      "identifier": emailId,
      "password": password,
    });

    if (apiResponse.status == ApiResponseStatus.completed) {
      var jsonLoginResponse =
      convert.jsonDecode(apiResponse.data) as Map<String, dynamic>;
      var userObj = jsonLoginResponse["user"];

      print("Login Response ===> $jsonLoginResponse");

      preferences.setJwtToken(jsonLoginResponse["jwt"]);
      preferences.setUserId(userObj["id"]);
      preferences.setUserName(userObj["username"]);
      preferences.setEmail(userObj["email"]);
      preferences.setQbPassword(userObj["user_id"]);
      var apiResponseManaged = await service.channelsUserManges();
      var apiResponseFollowed = await service.channelsUserFollows();
      bool isListEmpty = false;
      if (apiResponseManaged.status == ApiResponseStatus.completed &&
          apiResponseFollowed.status == ApiResponseStatus.completed) {
        var managedChannels =
        convert.jsonDecode(apiResponseManaged.data) as List;
        var followedChannels =
        convert.jsonDecode(apiResponseFollowed.data) as List;
        if ((managedChannels).isNotEmpty || (followedChannels).isNotEmpty) {
          isListEmpty = false;
        } else {
          isListEmpty = true;
        }
      } else {
        isListEmpty = true;
      }
      if ((userObj["interests"] as List).isEmpty) {
        AppInjector.resolve<AppPreferences>().isLogged("InterestState");
        emit(LoginInterestState());
      } else if (isListEmpty) {
        AppInjector.resolve<AppPreferences>().isLogged("SuggestedState");
        emit(LoginSuggestedState());
      } else {
        var qbLogin = await AppInjector.resolve<QuickbloxApiService>()
            .quickBloxLogin(userObj["username"], userObj["user_id"]);
        if (qbLogin.status == QuickbloxApiResponseStatus.completed) {
          var createSubscription =
          await AppInjector.resolve<QuickbloxApiService>()
              .createPushSubscription();
          if (createSubscription.status ==
              QuickbloxApiResponseStatus.completed) {
            AppInjector.resolve<AppPreferences>().isLogged("LoggedInState");
            emit(LoginSuccessState(true));
          } else {
            emit(LoginFailureState("Something went wrong!Please try later"));
          }
        } else {
          emit(LoginFailureState("Something went wrong!Please try later"));
        }
      }
    } else {
      if (apiResponse.message[0] == "Auth.form.error.confirmed") {
        emit(LoginVerifyState());
      } else if (apiResponse.message[0] == "Auth.form.error.invalid") {
        emit(LoginFailureState(apiResponse.message[1]));
      } else {
        emit(LoginFailureState("Something Went Wrong!Please try later"));
      }
    }
  }

  void availableEmail(emailId) async {
    emit(LoadingState());

    var apiResponse = await service.availableEmail({
      "email": emailId,
    });
    print("Valida email$apiResponse");
    if (apiResponse.status == ApiResponseStatus.completed) {
      var jsonResponse = convert.jsonDecode(apiResponse.data);
      if (jsonResponse['id'] == "email.available")
        emit(EmailAvailable());
      else
        emit(EmailUnAvailable());
    } else {
      emit(EmailUnAvailable());
    }
  }

  void forgotPassword(emailId) async {
    emit(LoadingState());

    var apiResponse = await service.forgotPasswordApi({
      "email": emailId,
    });
    if (apiResponse.status == ApiResponseStatus.completed) {
      emit(PasswordChangeConfirmState());
    } else if (apiResponse.message[0] == "Auth.form.error.user.not-exist") {
      emit(PasswordChangeFailureState(apiResponse.message[1]));
    } else if (apiResponse.message[0] == "Auth.form.error.email.format") {
      emit(PasswordChangeFailureState(apiResponse.message[1]));
    } else {
      emit(PasswordChangeFailureState("Something Went Wrong!Please try later"));
    }
  }

  void isLoggedIn() async {
    if (preferences.getIsLogged() == "LoggedInState") {
      var qbLogin = await AppInjector.resolve<QuickbloxApiService>()
          .quickBloxLogin(
          preferences.getUserName(), preferences.getQbPassword());
      if (qbLogin.status == QuickbloxApiResponseStatus.completed) {
        var connectToChatServer =
        await AppInjector.resolve<QuickbloxApiService>().connect();
        if (connectToChatServer.status ==
            QuickbloxApiResponseStatus.completed) {
          var createSubscription =
          await AppInjector.resolve<QuickbloxApiService>()
              .createPushSubscription();
          if (createSubscription.status ==
              QuickbloxApiResponseStatus.completed) {
            AppInjector.resolve<AppPreferences>().isLogged("LoggedInState");
            print('Subscribed to notifications ${createSubscription.data}');
            // AppInjector.resolve<QuickbloxApiService>().subscribeToChatEvents();

            emit(LoginSuccessState(true));
          } else {
            emit(LoginSuccessState(false));
          }
        } else {
          emit(LoginSuccessState(false));
        }
      } else {
        emit(LoginSuccessState(false));
      }
    } else if (preferences.getIsLogged() == "InterestState") {
      emit(LoginInterestState());
    } else if (preferences.getIsLogged() == "SuggestedState") {
      emit(LoginSuggestedState());
    } else {
      emit(LoginSuccessState(false));
    }
  }

  void logout() async {
    await FirebaseMessaging.instance.deleteToken();
    var logoutSuccess =
    await AppInjector.resolve<QuickbloxApiService>().qbLogout();
    if (logoutSuccess.status == QuickbloxApiResponseStatus.completed) {
      // mixpanelReset();
      print("LOGOUT is Successful");
      emit(LogOutSuccessState());
    } else {
      print("LOGOUT Not Successful");
    }
  }
}
