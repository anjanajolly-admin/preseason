class DatabaseConstants {
  static const String database_name = "somaHealth.db";
  static const int database_version = 2;

  static const String LOG_ROW_LIMIT = "log_row_limit";
  static const String COMM_LOG_ROW_LIMIT = "commlog_row_limit";
  static const int LOG_ROW_LIMIT_MAX = 10000;

  static String getTrigger(
      String triggerName, String tableName, int max, String id) {
    return "CREATE TRIGGER $triggerName BEFORE INSERT ON $tableName" +
        " WHEN ( SELECT count(*) FROM  $tableName ) > $max" +
        " BEGIN" +
        " DELETE FROM $tableName WHERE $id NOT IN" +
        " (SELECT $id FROM $tableName ORDER BY $id DESC LIMIT $max );" +
        " END";
  }

  static String deleteTrigger(String triggerName) {
    return "DROP TRIGGER IF EXISTS $triggerName";
  }
}
