import 'dart:convert';

import 'package:floor/floor.dart';

// @TypeConverters([DateTimeConverter])
@Entity(tableName: ChatHistory.TABLE_NAME, primaryKeys: ['chatId', 'toId'])
class ChatHistory {
  static const TABLE_NAME = "ChatHistory";

  ChatHistory({
    required this.chatId,
    required this.parentChatId,
    required this.fromId,
    required this.toId,
    required this.chatGroupId,
    required this.displayname,
    required this.message,
    required this.groupName,
    required this.messageDate,
    required this.messageDeleted,
    required this.modifiedDate,
    required this.isActive,
    required this.isRead,
    required this.isGroupChat,
    required this.localdbid,
    required this.chatGroupName,
    required this.isAttachment,
    required this.attachmentFileName,
    required this.attachmentThumbnail,
    required this.parentMessage,
    required this.parentMsgMember,
    required this.parentMsgDate,
    required this.parentIsAttachment,
    required this.fromMemberProfilePhoto,
    required this.context,
  });

  int chatId;
  int parentChatId;
  String fromId;
  String toId;
  String chatGroupId;
  String displayname;
  String message;
  String groupName;
  String messageDate;
  bool messageDeleted;
  String modifiedDate;
  bool isActive;
  bool isRead;
  bool isGroupChat;
  String localdbid;
  String chatGroupName;
  bool isAttachment;
  String attachmentFileName;
  String attachmentThumbnail;
  String parentMessage;
  String parentMsgMember;
  String parentMsgDate;
  bool parentIsAttachment;
  String fromMemberProfilePhoto;
  String context;

  ChatHistory copyWith({
    required int chatId,
    required int parentChatId,
    required String fromId,
    required String toId,
    required String chatGroupId,
    required String displayname,
    required String message,
    required String groupName,
    required String messageDate,
    required bool messageDeleted,
    required String modifiedDate,
    required bool isActive,
    required bool isRead,
    required bool isGroupChat,
    required String localdbid,
    required String chatGroupName,
    required bool isAttachment,
    required String attachmentFileName,
    required String attachmentThumbnail,
    required String parentMessage,
    required String parentMsgMember,
    required String parentMsgDate,
    required bool parentIsAttachment,
    required String fromMemberProfilePhoto,
    required String context,
  }) =>
      ChatHistory(
        chatId: chatId,
        parentChatId: parentChatId,
        fromId: fromId,
        toId: toId,
        chatGroupId: chatGroupId,
        displayname: displayname,
        message: message,
        groupName: groupName,
        messageDate: messageDate,
        messageDeleted: messageDeleted,
        modifiedDate: modifiedDate,
        isActive: isActive,
        isRead: isRead,
        isGroupChat: isGroupChat,
        localdbid: localdbid,
        chatGroupName: chatGroupName,
        isAttachment: isAttachment,
        attachmentFileName: attachmentFileName,
        attachmentThumbnail: attachmentThumbnail,
        parentMessage: parentMessage,
        parentMsgMember: parentMsgMember,
        parentMsgDate: parentMsgDate,
        parentIsAttachment: parentIsAttachment,
        fromMemberProfilePhoto: fromMemberProfilePhoto,
        context: context,
      );

  factory ChatHistory.fromJson(String str) =>
      ChatHistory.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ChatHistory.fromMap(Map<String, dynamic> json) => ChatHistory(
        chatId: json["chatId"] == null ? null : json["chatId"],
        parentChatId:
            json["parentChatId"] == null ? null : json["parentChatId"],
        fromId: json["fromId"] == null ? null : json["fromId"],
        toId: json["toId"] == null ? null : json["toId"],
        chatGroupId: json["chatGroupId"] == null ? null : json["chatGroupId"],
        displayname: json["displayname"] == null ? null : json["displayname"],
        message: json["message"] == null ? null : json["message"],
        groupName: json["groupName"] == null ? null : json["groupName"],
        messageDate: json["messageDate"] == null ? null : json['messageDate'],
        messageDeleted:
            json["messageDeleted"] == null ? null : json["messageDeleted"],
        modifiedDate:
            json["modifiedDate"] == null ? null : json["modifiedDate"],
        isActive: json["isActive"] == null ? null : json["isActive"],
        isRead: json["isRead"] == null ? null : json["isRead"],
        isGroupChat: json["isGroupChat"] == null ? null : json["isGroupChat"],
        localdbid: json["localdbid"] == null ? null : json["localdbid"],
        chatGroupName:
            json["chatGroupName"] == null ? null : json["chatGroupName"],
        isAttachment:
            json["isAttachment"] == null ? null : json["isAttachment"],
        attachmentFileName: json["attachmentFileName"] == null
            ? null
            : json["attachmentFileName"],
        attachmentThumbnail: json["attachmentThumbnail"] == null
            ? null
            : json["attachmentThumbnail"],
        parentMessage:
            json["parentMessage"] == null ? null : json["parentMessage"],
        parentMsgMember:
            json["parentMsgMember"] == null ? null : json["parentMsgMember"],
        parentMsgDate:
            json["parentMsgDate"] == null ? null : json["parentMsgDate"],
        parentIsAttachment: json["parentIsAttachment"] == null
            ? null
            : json["parentIsAttachment"],
        fromMemberProfilePhoto: json["fromMemberProfilePhoto"] == null
            ? null
            : json["fromMemberProfilePhoto"],
        context: json["context"] == null ? null : json["context"],
      );

  Map<String, dynamic> toMap() => {
        "chatId": chatId == null ? null : chatId,
        "parentChatId": parentChatId == null ? null : parentChatId,
        "fromId": fromId == null ? null : fromId,
        "toId": toId == null ? null : toId,
        "chatGroupId": chatGroupId == null ? null : chatGroupId,
        "displayname": displayname == null ? null : displayname,
        "message": message == null ? null : message,
        "groupName": groupName == null ? null : groupName,
        "messageDate": messageDate == null ? null : messageDate.toString(),
        "messageDeleted": messageDeleted == null ? null : messageDeleted,
        "modifiedDate": modifiedDate == null ? null : modifiedDate,
        "isActive": isActive == null ? null : isActive,
        "isRead": isRead == null ? null : isRead,
        "isGroupChat": isGroupChat == null ? null : isGroupChat,
        "localdbid": localdbid == null ? null : localdbid,
        "chatGroupName": chatGroupName == null ? null : chatGroupName,
        "isAttachment": isAttachment == null ? null : isAttachment,
        "attachmentFileName":
            attachmentFileName == null ? null : attachmentFileName,
        "attachmentThumbnail":
            attachmentThumbnail == null ? null : attachmentThumbnail,
        "parentMessage": parentMessage == null ? null : parentMessage,
        "parentMsgMember": parentMsgMember == null ? null : parentMsgMember,
        "parentMsgDate": parentMsgDate == null ? null : parentMsgDate,
        "parentIsAttachment":
            parentIsAttachment == null ? null : parentIsAttachment,
        "fromMemberProfilePhoto":
            fromMemberProfilePhoto == null ? null : fromMemberProfilePhoto,
        "context": context == null ? null : context,
      };

  factory ChatHistory.fromJsonSR(String str) =>
      ChatHistory.fromMapSR(json.decode(str));

  factory ChatHistory.fromMapSR(Map<String, dynamic> json) {
    var isGroupChat = json["isGroupChat"];
    String toId;
    if (isGroupChat) {
      toId = json["chatGroupId"] == null ? null : json["chatGroupId"];
    } else {
      toId = json["toId"] == null ? null : json["toId"];
    }

    return ChatHistory(
      chatGroupId: json["chatGroupId"] == null ? null : json["chatGroupId"],
      chatId: json["chatId"] == null ? null : json["chatId"],
      parentChatId: json["parentChatId"] == null ? null : json["parentChatId"],
      fromId: json["fromId"] == null ? null : json["fromId"],
      toId: toId,
      displayname: json["displayname"] == null ? null : json["displayname"],
      message: json["message"] == null ? null : json["message"],
      groupName: json["groupName"] == null ? null : json["groupName"],
      localdbid: json["localdbid"] == null ? null : json["localdbid"],
      messageDate: json["messageDate"] == null ? null : (json["messageDate"]),
      modifiedDate: json["modifiedDate"] == null ? null : json["modifiedDate"],
      isRead: json["isRead"] == null ? null : json["isRead"],
      isGroupChat: isGroupChat,
      chatGroupName:
          json["chatGroupName"] == null ? null : json["chatGroupName"],
      isAttachment: json["isAttachment"] == null ? null : json["isAttachment"],
      attachmentFileName: json["attachmentFileName"] == null
          ? null
          : json["attachmentFileName"],
      attachmentThumbnail: json["attachmentThumbnail"] == null
          ? null
          : json["attachmentThumbnail"],
      fromMemberProfilePhoto: json["profilePhotoSignedUrl"] == null
          ? null
          : json["profilePhotoSignedUrl"],
      context: json["context"] == null ? null : json["context"],
      messageDeleted:
          json["messageDeleted"] == null ? null : json["messageDeleted"],
      isActive: json["isActive"] == null ? null : json["isActive"],
      parentMessage:
          json["parentMessage"] == null ? null : json["parentMessage"],
      parentMsgMember:
          json["parentMsgMember"] == null ? null : json["parentMsgMember"],
      parentMsgDate:
          json["parentMsgDate"] == null ? null : json["parentMsgDate"],
      parentIsAttachment: json["parentIsAttachment"] == null
          ? null
          : json["parentIsAttachment"],
    );
  }

  String toJsonSR() => json.encode(toMapSR());

  Map<String, dynamic> toMapSR() {
    String chatGroupId;
    String toId;
    if (isGroupChat) {
      chatGroupId = this.toId;
      toId = this.toId;
    } else {
      toId = this.toId;
    }
    return {
      // "chatGroupId": chatGroupId == null ? null : chatGroupId,
      "chatId": chatId == null ? null : chatId,
      "parentChatId": parentChatId == null ? null : parentChatId,
      "fromId": fromId == null ? null : fromId,
      "chatToId": toId == null ? null : toId,
      "displayname": displayname == null ? null : displayname,
      "message": message == null ? null : message,
      "groupName": groupName == null ? null : groupName,
      "localdbid": localdbid == null ? null : localdbid,
      "messageDate": messageDate == null ? null : "",
      "modifiedDate": modifiedDate == null ? null : modifiedDate,
      "isRead": isRead == null ? null : isRead,
      "isGroupChat": isGroupChat == null ? null : isGroupChat,
      "chatGroupName": chatGroupName == null ? null : chatGroupName,
      "isAttachment": isAttachment == null ? null : isAttachment,
      "attachmentFileName":
          attachmentFileName == null ? null : attachmentFileName,
      "attachmentThumbnail":
          attachmentThumbnail == null ? null : attachmentThumbnail,
      "profilePhotoSignedUrl":
          fromMemberProfilePhoto == null ? null : fromMemberProfilePhoto,
      "context": context == null ? null : context,
      "messageDeleted": messageDeleted == null ? null : messageDeleted,
      "isActive": isActive == null ? null : isActive,
      "parentMessage": parentMessage == null ? null : parentMessage,
      "parentMsgMember": parentMsgMember == null ? null : parentMsgMember,
      "parentMsgDate": parentMsgDate == null ? null : parentMsgDate,
      "parentIsAttachment":
          parentIsAttachment == null ? null : parentIsAttachment,
    };
  }

  @override
  String toString() {
    return 'ChatHistory{chatId: $chatId, message:$message, messageDate: $messageDate, fromId:$fromId, toId:$toId}';
  }
}
