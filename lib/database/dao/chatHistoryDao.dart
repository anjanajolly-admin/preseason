import 'package:floor/floor.dart';

import '../model/chat_history_table.dart';

@dao
abstract class ChatHistoryDao {
  @Insert(onConflict: OnConflictStrategy.replace)
  Future<List<int>> insertData(List<ChatHistory> list);

  @Update(onConflict: OnConflictStrategy.replace)
  Future<int> updateData(List<ChatHistory> list);

  @delete
  Future<int> deleteData(List<ChatHistory> list);

  @Query('DELETE FROM ${ChatHistory.TABLE_NAME}')
  Future<void> deleteAll();

  @Query('SELECT * FROM ${ChatHistory.TABLE_NAME}')
  Future<List<ChatHistory>> getAllData();

  @Query(
      'SELECT * FROM ${ChatHistory.TABLE_NAME} WHERE :condition ORDER BY messageDate DESC limit :limit')
  Future<List<ChatHistory>> getFirstBatch(String condition, int limit);

  @Query(
      'SELECT * FROM ${ChatHistory.TABLE_NAME} WHERE toId=:toId ORDER BY messageDate DESC limit :limit')
  Future<List<ChatHistory>> getFirstBatchG(String toId, int limit);

  @Query(
      'SELECT * FROM ${ChatHistory.TABLE_NAME} WHERE (:fromId) AND (:toId) AND messageDate<:time ORDER BY messageDate DESC limit :limit')
  Future<List<ChatHistory>> getNextBatch(
      String fromId, String toId, int time, int limit);

  @Query(
      'SELECT * FROM ${ChatHistory.TABLE_NAME} WHERE toId=:toId AND messageDate<:time ORDER BY messageDate DESC limit :limit')
  Future<List<ChatHistory>> getNextBatchG(String toId, int time, int limit);

  @Query("\$q --:q")
  Future<List<ChatHistory>> executeRawQuery(String q);

  @transaction
  Future<List<int>> replace(List<ChatHistory> list, bool clearAll) async {
    if (clearAll) {
      await deleteAll();
    }
    return await insertData(list);
  }
}
