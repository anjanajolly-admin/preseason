// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_db.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  ChatHistoryDao? _chatHistoryListDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 2,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `ChatHistory` (`chatId` INTEGER NOT NULL, `parentChatId` INTEGER NOT NULL, `fromId` TEXT NOT NULL, `toId` TEXT NOT NULL, `chatGroupId` TEXT NOT NULL, `displayname` TEXT NOT NULL, `message` TEXT NOT NULL, `groupName` TEXT NOT NULL, `messageDate` TEXT NOT NULL, `messageDeleted` INTEGER NOT NULL, `modifiedDate` TEXT NOT NULL, `isActive` INTEGER NOT NULL, `isRead` INTEGER NOT NULL, `isGroupChat` INTEGER NOT NULL, `localdbid` TEXT NOT NULL, `chatGroupName` TEXT NOT NULL, `isAttachment` INTEGER NOT NULL, `attachmentFileName` TEXT NOT NULL, `attachmentThumbnail` TEXT NOT NULL, `parentMessage` TEXT NOT NULL, `parentMsgMember` TEXT NOT NULL, `parentMsgDate` TEXT NOT NULL, `parentIsAttachment` INTEGER NOT NULL, `fromMemberProfilePhoto` TEXT NOT NULL, `context` TEXT NOT NULL, PRIMARY KEY (`chatId`, `toId`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  ChatHistoryDao get chatHistoryListDao {
    return _chatHistoryListDaoInstance ??=
        _$ChatHistoryDao(database, changeListener);
  }
}

class _$ChatHistoryDao extends ChatHistoryDao {
  _$ChatHistoryDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _chatHistoryInsertionAdapter = InsertionAdapter(
            database,
            'ChatHistory',
            (ChatHistory item) => <String, Object?>{
                  'chatId': item.chatId,
                  'parentChatId': item.parentChatId,
                  'fromId': item.fromId,
                  'toId': item.toId,
                  'chatGroupId': item.chatGroupId,
                  'displayname': item.displayname,
                  'message': item.message,
                  'groupName': item.groupName,
                  'messageDate': item.messageDate,
                  'messageDeleted': item.messageDeleted ? 1 : 0,
                  'modifiedDate': item.modifiedDate,
                  'isActive': item.isActive ? 1 : 0,
                  'isRead': item.isRead ? 1 : 0,
                  'isGroupChat': item.isGroupChat ? 1 : 0,
                  'localdbid': item.localdbid,
                  'chatGroupName': item.chatGroupName,
                  'isAttachment': item.isAttachment ? 1 : 0,
                  'attachmentFileName': item.attachmentFileName,
                  'attachmentThumbnail': item.attachmentThumbnail,
                  'parentMessage': item.parentMessage,
                  'parentMsgMember': item.parentMsgMember,
                  'parentMsgDate': item.parentMsgDate,
                  'parentIsAttachment': item.parentIsAttachment ? 1 : 0,
                  'fromMemberProfilePhoto': item.fromMemberProfilePhoto,
                  'context': item.context
                }),
        _chatHistoryUpdateAdapter = UpdateAdapter(
            database,
            'ChatHistory',
            ['chatId', 'toId'],
            (ChatHistory item) => <String, Object?>{
                  'chatId': item.chatId,
                  'parentChatId': item.parentChatId,
                  'fromId': item.fromId,
                  'toId': item.toId,
                  'chatGroupId': item.chatGroupId,
                  'displayname': item.displayname,
                  'message': item.message,
                  'groupName': item.groupName,
                  'messageDate': item.messageDate,
                  'messageDeleted': item.messageDeleted ? 1 : 0,
                  'modifiedDate': item.modifiedDate,
                  'isActive': item.isActive ? 1 : 0,
                  'isRead': item.isRead ? 1 : 0,
                  'isGroupChat': item.isGroupChat ? 1 : 0,
                  'localdbid': item.localdbid,
                  'chatGroupName': item.chatGroupName,
                  'isAttachment': item.isAttachment ? 1 : 0,
                  'attachmentFileName': item.attachmentFileName,
                  'attachmentThumbnail': item.attachmentThumbnail,
                  'parentMessage': item.parentMessage,
                  'parentMsgMember': item.parentMsgMember,
                  'parentMsgDate': item.parentMsgDate,
                  'parentIsAttachment': item.parentIsAttachment ? 1 : 0,
                  'fromMemberProfilePhoto': item.fromMemberProfilePhoto,
                  'context': item.context
                }),
        _chatHistoryDeletionAdapter = DeletionAdapter(
            database,
            'ChatHistory',
            ['chatId', 'toId'],
            (ChatHistory item) => <String, Object?>{
                  'chatId': item.chatId,
                  'parentChatId': item.parentChatId,
                  'fromId': item.fromId,
                  'toId': item.toId,
                  'chatGroupId': item.chatGroupId,
                  'displayname': item.displayname,
                  'message': item.message,
                  'groupName': item.groupName,
                  'messageDate': item.messageDate,
                  'messageDeleted': item.messageDeleted ? 1 : 0,
                  'modifiedDate': item.modifiedDate,
                  'isActive': item.isActive ? 1 : 0,
                  'isRead': item.isRead ? 1 : 0,
                  'isGroupChat': item.isGroupChat ? 1 : 0,
                  'localdbid': item.localdbid,
                  'chatGroupName': item.chatGroupName,
                  'isAttachment': item.isAttachment ? 1 : 0,
                  'attachmentFileName': item.attachmentFileName,
                  'attachmentThumbnail': item.attachmentThumbnail,
                  'parentMessage': item.parentMessage,
                  'parentMsgMember': item.parentMsgMember,
                  'parentMsgDate': item.parentMsgDate,
                  'parentIsAttachment': item.parentIsAttachment ? 1 : 0,
                  'fromMemberProfilePhoto': item.fromMemberProfilePhoto,
                  'context': item.context
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<ChatHistory> _chatHistoryInsertionAdapter;

  final UpdateAdapter<ChatHistory> _chatHistoryUpdateAdapter;

  final DeletionAdapter<ChatHistory> _chatHistoryDeletionAdapter;

  @override
  Future<void> deleteAll() async {
    await _queryAdapter.queryNoReturn('DELETE FROM ChatHistory');
  }

  @override
  Future<List<ChatHistory>> getAllData() async {
    return _queryAdapter.queryList('SELECT * FROM ChatHistory',
        mapper: (Map<String, Object?> row) => ChatHistory(
            chatId: row['chatId'] as int,
            parentChatId: row['parentChatId'] as int,
            fromId: row['fromId'] as String,
            toId: row['toId'] as String,
            chatGroupId: row['chatGroupId'] as String,
            displayname: row['displayname'] as String,
            message: row['message'] as String,
            groupName: row['groupName'] as String,
            messageDate: row['messageDate'] as String,
            messageDeleted: (row['messageDeleted'] as int) != 0,
            modifiedDate: row['modifiedDate'] as String,
            isActive: (row['isActive'] as int) != 0,
            isRead: (row['isRead'] as int) != 0,
            isGroupChat: (row['isGroupChat'] as int) != 0,
            localdbid: row['localdbid'] as String,
            chatGroupName: row['chatGroupName'] as String,
            isAttachment: (row['isAttachment'] as int) != 0,
            attachmentFileName: row['attachmentFileName'] as String,
            attachmentThumbnail: row['attachmentThumbnail'] as String,
            parentMessage: row['parentMessage'] as String,
            parentMsgMember: row['parentMsgMember'] as String,
            parentMsgDate: row['parentMsgDate'] as String,
            parentIsAttachment: (row['parentIsAttachment'] as int) != 0,
            fromMemberProfilePhoto: row['fromMemberProfilePhoto'] as String,
            context: row['context'] as String));
  }

  @override
  Future<List<ChatHistory>> getFirstBatch(String condition, int limit) async {
    return _queryAdapter.queryList(
        'SELECT * FROM ChatHistory WHERE ?1 ORDER BY messageDate DESC limit ?2',
        mapper: (Map<String, Object?> row) => ChatHistory(
            chatId: row['chatId'] as int,
            parentChatId: row['parentChatId'] as int,
            fromId: row['fromId'] as String,
            toId: row['toId'] as String,
            chatGroupId: row['chatGroupId'] as String,
            displayname: row['displayname'] as String,
            message: row['message'] as String,
            groupName: row['groupName'] as String,
            messageDate: row['messageDate'] as String,
            messageDeleted: (row['messageDeleted'] as int) != 0,
            modifiedDate: row['modifiedDate'] as String,
            isActive: (row['isActive'] as int) != 0,
            isRead: (row['isRead'] as int) != 0,
            isGroupChat: (row['isGroupChat'] as int) != 0,
            localdbid: row['localdbid'] as String,
            chatGroupName: row['chatGroupName'] as String,
            isAttachment: (row['isAttachment'] as int) != 0,
            attachmentFileName: row['attachmentFileName'] as String,
            attachmentThumbnail: row['attachmentThumbnail'] as String,
            parentMessage: row['parentMessage'] as String,
            parentMsgMember: row['parentMsgMember'] as String,
            parentMsgDate: row['parentMsgDate'] as String,
            parentIsAttachment: (row['parentIsAttachment'] as int) != 0,
            fromMemberProfilePhoto: row['fromMemberProfilePhoto'] as String,
            context: row['context'] as String),
        arguments: [condition, limit]);
  }

  @override
  Future<List<ChatHistory>> getFirstBatchG(String toId, int limit) async {
    return _queryAdapter.queryList(
        'SELECT * FROM ChatHistory WHERE toId=?1 ORDER BY messageDate DESC limit ?2',
        mapper: (Map<String, Object?> row) => ChatHistory(chatId: row['chatId'] as int, parentChatId: row['parentChatId'] as int, fromId: row['fromId'] as String, toId: row['toId'] as String, chatGroupId: row['chatGroupId'] as String, displayname: row['displayname'] as String, message: row['message'] as String, groupName: row['groupName'] as String, messageDate: row['messageDate'] as String, messageDeleted: (row['messageDeleted'] as int) != 0, modifiedDate: row['modifiedDate'] as String, isActive: (row['isActive'] as int) != 0, isRead: (row['isRead'] as int) != 0, isGroupChat: (row['isGroupChat'] as int) != 0, localdbid: row['localdbid'] as String, chatGroupName: row['chatGroupName'] as String, isAttachment: (row['isAttachment'] as int) != 0, attachmentFileName: row['attachmentFileName'] as String, attachmentThumbnail: row['attachmentThumbnail'] as String, parentMessage: row['parentMessage'] as String, parentMsgMember: row['parentMsgMember'] as String, parentMsgDate: row['parentMsgDate'] as String, parentIsAttachment: (row['parentIsAttachment'] as int) != 0, fromMemberProfilePhoto: row['fromMemberProfilePhoto'] as String, context: row['context'] as String),
        arguments: [toId, limit]);
  }

  @override
  Future<List<ChatHistory>> getNextBatch(
      String fromId, String toId, int time, int limit) async {
    return _queryAdapter.queryList(
        'SELECT * FROM ChatHistory WHERE (?1) AND (?2) AND messageDate<?3 ORDER BY messageDate DESC limit ?4',
        mapper: (Map<String, Object?> row) => ChatHistory(chatId: row['chatId'] as int, parentChatId: row['parentChatId'] as int, fromId: row['fromId'] as String, toId: row['toId'] as String, chatGroupId: row['chatGroupId'] as String, displayname: row['displayname'] as String, message: row['message'] as String, groupName: row['groupName'] as String, messageDate: row['messageDate'] as String, messageDeleted: (row['messageDeleted'] as int) != 0, modifiedDate: row['modifiedDate'] as String, isActive: (row['isActive'] as int) != 0, isRead: (row['isRead'] as int) != 0, isGroupChat: (row['isGroupChat'] as int) != 0, localdbid: row['localdbid'] as String, chatGroupName: row['chatGroupName'] as String, isAttachment: (row['isAttachment'] as int) != 0, attachmentFileName: row['attachmentFileName'] as String, attachmentThumbnail: row['attachmentThumbnail'] as String, parentMessage: row['parentMessage'] as String, parentMsgMember: row['parentMsgMember'] as String, parentMsgDate: row['parentMsgDate'] as String, parentIsAttachment: (row['parentIsAttachment'] as int) != 0, fromMemberProfilePhoto: row['fromMemberProfilePhoto'] as String, context: row['context'] as String),
        arguments: [fromId, toId, time, limit]);
  }

  @override
  Future<List<ChatHistory>> getNextBatchG(
      String toId, int time, int limit) async {
    return _queryAdapter.queryList(
        'SELECT * FROM ChatHistory WHERE toId=?1 AND messageDate<?2 ORDER BY messageDate DESC limit ?3',
        mapper: (Map<String, Object?> row) => ChatHistory(chatId: row['chatId'] as int, parentChatId: row['parentChatId'] as int, fromId: row['fromId'] as String, toId: row['toId'] as String, chatGroupId: row['chatGroupId'] as String, displayname: row['displayname'] as String, message: row['message'] as String, groupName: row['groupName'] as String, messageDate: row['messageDate'] as String, messageDeleted: (row['messageDeleted'] as int) != 0, modifiedDate: row['modifiedDate'] as String, isActive: (row['isActive'] as int) != 0, isRead: (row['isRead'] as int) != 0, isGroupChat: (row['isGroupChat'] as int) != 0, localdbid: row['localdbid'] as String, chatGroupName: row['chatGroupName'] as String, isAttachment: (row['isAttachment'] as int) != 0, attachmentFileName: row['attachmentFileName'] as String, attachmentThumbnail: row['attachmentThumbnail'] as String, parentMessage: row['parentMessage'] as String, parentMsgMember: row['parentMsgMember'] as String, parentMsgDate: row['parentMsgDate'] as String, parentIsAttachment: (row['parentIsAttachment'] as int) != 0, fromMemberProfilePhoto: row['fromMemberProfilePhoto'] as String, context: row['context'] as String),
        arguments: [toId, time, limit]);
  }

  @override
  Future<List<ChatHistory>> executeRawQuery(String q) async {
    return _queryAdapter.queryList('$q --?1',
        mapper: (Map<String, Object?> row) => ChatHistory(
            chatId: row['chatId'] as int,
            parentChatId: row['parentChatId'] as int,
            fromId: row['fromId'] as String,
            toId: row['toId'] as String,
            chatGroupId: row['chatGroupId'] as String,
            displayname: row['displayname'] as String,
            message: row['message'] as String,
            groupName: row['groupName'] as String,
            messageDate: row['messageDate'] as String,
            messageDeleted: (row['messageDeleted'] as int) != 0,
            modifiedDate: row['modifiedDate'] as String,
            isActive: (row['isActive'] as int) != 0,
            isRead: (row['isRead'] as int) != 0,
            isGroupChat: (row['isGroupChat'] as int) != 0,
            localdbid: row['localdbid'] as String,
            chatGroupName: row['chatGroupName'] as String,
            isAttachment: (row['isAttachment'] as int) != 0,
            attachmentFileName: row['attachmentFileName'] as String,
            attachmentThumbnail: row['attachmentThumbnail'] as String,
            parentMessage: row['parentMessage'] as String,
            parentMsgMember: row['parentMsgMember'] as String,
            parentMsgDate: row['parentMsgDate'] as String,
            parentIsAttachment: (row['parentIsAttachment'] as int) != 0,
            fromMemberProfilePhoto: row['fromMemberProfilePhoto'] as String,
            context: row['context'] as String),
        arguments: [q]);
  }

  @override
  Future<List<int>> insertData(List<ChatHistory> list) {
    return _chatHistoryInsertionAdapter.insertListAndReturnIds(
        list, OnConflictStrategy.replace);
  }

  @override
  Future<int> updateData(List<ChatHistory> list) {
    return _chatHistoryUpdateAdapter.updateListAndReturnChangedRows(
        list, OnConflictStrategy.replace);
  }

  @override
  Future<int> deleteData(List<ChatHistory> list) {
    return _chatHistoryDeletionAdapter.deleteListAndReturnChangedRows(list);
  }

  @override
  Future<List<int>> replace(List<ChatHistory> list, bool clearAll) async {
    if (database is sqflite.Transaction) {
      return super.replace(list, clearAll);
    } else {
      return (database as sqflite.Database)
          .transaction<List<int>>((transaction) async {
        final transactionDatabase = _$AppDatabase(changeListener)
          ..database = transaction;
        return transactionDatabase.chatHistoryListDao.replace(list, clearAll);
      });
    }
  }
}
