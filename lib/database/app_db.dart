import 'dart:async';

import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'app_db_utils.dart';
import 'dao/chatHistoryDao.dart';
import 'model/chat_history_table.dart';

part 'app_db.g.dart';

@Database(version: DatabaseConstants.database_version, entities: [
  ChatHistory,
])
abstract class AppDatabase extends FloorDatabase {
  // FriendsListDao get friendsListDao;

  ChatHistoryDao get chatHistoryListDao;
}
