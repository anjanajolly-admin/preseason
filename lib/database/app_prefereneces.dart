import 'package:pre_seasoned/utils/app_injector.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  final SharedPreferences _preferences =
      AppInjector.resolve<SharedPreferences>();

  String? getIsLogged() {
    return _preferences.getString("isLogged");
  }

  String? getLoginID() {
    return _preferences.getString(AppPreferencesKeys.getLoginID);
  }

  String? getEmail() {
    return _preferences.getString("email");
  }

  String? getUserName() {
    return _preferences.getString("userName");
  }

  String? getJwtToken() {
    return _preferences.getString("jwtToken");
  }

  int? getUserId() {
    return _preferences.getInt("userId");
  }

  String? getQbPassword() {
    return _preferences.getString("qbPassword");
  }

  String? getfirstName() {
    return _preferences.getString("firstName");
  }

  String? getlastName() {
    return _preferences.getString("lastName");
  }

  int? getQbUserId() {
    return _preferences.getInt("qbUserId");
  }

  int? getDialogId() {
    return _preferences.getInt("qbDialogId");
  }

  bool? getAgreementStatus(name) {
    return _preferences.getBool("isAgreed_$name");
  }

  Future<bool> isLogged(String isLogged) async {
    return await _preferences.setString("isLogged", isLogged);
  }

  Future<bool> setLoginID(String loginID) async {
    return await _preferences.setString(AppPreferencesKeys.getLoginID, loginID);
  }

  Future<bool> setfirstName(String firstName) async {
    return await _preferences.setString("firstName", firstName);
  }

  Future<bool> setlastName(String lastName) async {
    return await _preferences.setString("lastName", lastName);
  }

  Future<bool> setEmail(String email) async {
    return await _preferences.setString("email", email);
  }

  Future<bool> setJwtToken(String jwtToken) async {
    return await _preferences.setString("jwtToken", jwtToken);
  }

  Future<bool> setUserId(int userId) async {
    return await _preferences.setInt("userId", userId);
  }

  Future<bool> setUserName(String name) async {
    return await _preferences.setString("userName", name);
  }

  Future<bool> setQbPassword(String password) async {
    return await _preferences.setString("qbPassword", password);
  }

  Future<bool> setQbUserId(int qbUserId) async {
    return await _preferences.setInt("qbUserId", qbUserId);
  }

  Future<bool> setDialogId(int dialogId) async {
    return await _preferences.setInt("qbDialogId", dialogId);
  }

  Future<bool> setAgreementStatus(bool isAgreed, String name) async {
    return await _preferences.setBool("isAgreed_$name", isAgreed);
  }

  Future<dynamic> clear() async {}
}

class AppPreferencesKeys {
  static const memberId = "member_id";
  static const frequentLabDataSearches = "frequent_lab_data_search";
  static const frequentVitalsSearches = "frequent_vitals_search";
  static const getLoginID = "get_login_id";
  static const getTimerValue = "get_timer_value";
}
