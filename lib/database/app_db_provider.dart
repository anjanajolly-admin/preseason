import 'package:floor/floor.dart';

import 'app_db.dart';
import 'app_db_utils.dart';

///
/// CLI => $flutter packages pub run build_runner build
///
class DatabaseProvider {
  static final _instance = DatabaseProvider._internal();
  static DatabaseProvider get = _instance;
  bool isInitialized = false;
  late AppDatabase _db;

  // Making constructor private, so that it can't create multiple objects
  DatabaseProvider._internal();

  Future<AppDatabase> db() async {
    if (!isInitialized) await _init();
    return _db;
  }

  // create migration
  final migration1to2 = Migration(1, 2, (database) async {
    print("migration1to2");
    try {
      await database.execute(
          "ALTER TABLE 'dummy' RENAME TO '${AppLogsEntry.TABLE_NAME}'");
      await database.execute(
          'CREATE TABLE IF NOT EXISTS `CommunicationLogs` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT)');
    } catch (e, s) {
      print("-------------------");
      print("migration1to2 error");
      print(e);
      print(s);
      print("-------------------");
    }
  });

  Future _init() async {
    isInitialized = true;
    final callback = Callback(
      onCreate: (database, version) {
        print("database has been created.($version)");
      },
      onOpen: (database) {
        print("database has been opened.");
        database.execute(
            DatabaseConstants.deleteTrigger(DatabaseConstants.LOG_ROW_LIMIT));
        database.execute(DatabaseConstants.deleteTrigger(
            DatabaseConstants.COMM_LOG_ROW_LIMIT));
        database.execute(DatabaseConstants.getTrigger(
          DatabaseConstants.COMM_LOG_ROW_LIMIT,
          CommunicationLogs.TABLE_NAME,
          DatabaseConstants.LOG_ROW_LIMIT_MAX,
          "id",
        ));
        database.execute(DatabaseConstants.getTrigger(
          DatabaseConstants.LOG_ROW_LIMIT,
          AppLogsEntry.TABLE_NAME,
          DatabaseConstants.LOG_ROW_LIMIT_MAX,
          "id",
        ));
      },
      onUpgrade: (database, startVersion, endVersion) {
        print("database has been upgraded.");
      },
    );
    _db = await $FloorAppDatabase
        .databaseBuilder(DatabaseConstants.database_name)
        .addMigrations([migration1to2])
        .addCallback(callback)
        .build();
  }
}

@Entity(tableName: CommunicationLogs.TABLE_NAME)
class CommunicationLogs {
  static const TABLE_NAME = "CommunicationLogs";

  @PrimaryKey(autoGenerate: true)
  final int id;

  final String name;

  CommunicationLogs(this.id, this.name);
}

@Entity(tableName: AppLogsEntry.TABLE_NAME)
class AppLogsEntry {
  static const TABLE_NAME = "AppLogs";

  @PrimaryKey(autoGenerate: true)
  final int id;

  final String name;

  AppLogsEntry(this.id, this.name);
}
