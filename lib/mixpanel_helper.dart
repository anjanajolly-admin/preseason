import 'package:mixpanel_flutter/mixpanel_flutter.dart';

Future<Mixpanel> initMixpanel() async {
  late Mixpanel mixpanel;

  mixpanel = await Mixpanel.init("c4b6cba1cc4506afee6ae57cacc328cf",
      optOutTrackingDefault: false);

  return mixpanel;
}

Future<void> mixpanelIdentifyUser({
  userName,
  emailId,
  var firstName,
  var lastName,
}) async {
  late Mixpanel mixpanel;
  late People _people;

  mixpanel = await initMixpanel();
  _people = mixpanel.getPeople();
  var distinctId = await mixpanel.getDistinctId();
  print('distinctId $distinctId');

  /*if (distinctId != emailId) {
    mixpanel.alias(emailId, distinctId!);
  }*/
  mixpanel.identify(emailId);

  String name = firstName! + ' ' + lastName!;
  _people.set("\$email", emailId);
  _people.set("\$name", name);
}

Future<void> mixpanelTrackUserActivity(
    {userName,
    emailId,
    eventName,
    Map<String, dynamic>? eventProperties}) async {
  late Mixpanel mixpanel;
  mixpanel = await initMixpanel();
  mixpanel.flush();
  mixpanel.identify(emailId);
  mixpanel.track(eventName, properties: eventProperties);

  mixpanel.setLoggingEnabled(true);
}

mixpanelReset() async {
  Mixpanel mixpanel = await initMixpanel();
  mixpanel.reset();
}
